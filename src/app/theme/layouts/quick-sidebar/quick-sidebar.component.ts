import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { IdentityService } from "../../../_services/identity.service";
import { NoticeService, EventService } from "../../../_services";
import { ConversationService } from "../../../_services/conversation.service";
import { ConversationIdService } from "../../../_services/conversationId.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-quick-sidebar",
  templateUrl: "./quick-sidebar.component.html",
  encapsulation: ViewEncapsulation.None
})
export class QuickSidebarComponent implements OnInit {
  public myClass: any;

  public myGroup: any;
  public events: any;
  public notices: any;

  constructor(
    private _identityService: IdentityService,
    private _conversationService: ConversationService,
    private _conversationIdService: ConversationIdService,
    private noticeService: NoticeService,
    private eventService: EventService,
    private _router: Router
  ) {
    // this.getRecentEvents();
    // this.getRecentNotice();
    // this.myClass = _identityService.class;
    // console.log("my class: "+this.myClass);
  }

  ngOnInit() {
    // this.getClass();
  }

  getClass() {
    this._conversationService.getConversation().subscribe(
      data => {
        this.myGroup = data.payload;
      },
      error => {
        console.error("Error while getting my group list. stack: ", error);
      }
    );
  }

  newId(id) {
    // this._groupIdService.changeId(id);
    // this._router.navigate(['/letsDiscuss']);
  }

  getRecentNotice() {
    this.noticeService.getRecentNotice().subscribe(
      noticeData => {
        this.notices = noticeData.payload;
        console.log("notice: ", this.notices);
      },
      error => {
        console.error("Error while getting Recent Notice data. Stack: ", error);
      }
    );
  }

  // getRecentEvents() {
  //   this.eventService.getRecentEvents().subscribe(eventData => {
  //     this.events = eventData.payload;
  //     console.log('events: ', this.events);
  //   },
  //     error => {
  //       console.error('Error while getting Recent Notice data. Stack: ', error);
  //     });
  // }
}
