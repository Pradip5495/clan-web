import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { IdentityService } from '../../../_services/identity.service';

declare let mLayout: any;
@Component({
  selector: 'app-aside-nav',
  templateUrl: './aside-nav.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./aside-nav.component.css']
})
export class AsideNavComponent implements OnInit, AfterViewInit {


  public role: any;
  public optionsBox: any=false;
  public visibleStudentPage: boolean;

  constructor(private _identityService: IdentityService) {

  }
  ngOnInit() {
    this.getRole();
    //this.options_menu(event: Event);
  }

  getRole() {
    this.role = this._identityService.role;
    console.log('role in side nav: ' + this.role);

  }

  ngAfterViewInit() {

    mLayout.initAside();

  }

  options_menu(event: Event){
    
    console.log("event:", event)
    if(this.optionsBox){
      this.optionsBox = false;

    }
    else{
      this.optionsBox = true;
    }

    
  }

}
