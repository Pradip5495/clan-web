import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import {MatSidenav} from '@angular/material/sidenav';

import { ScriptLoaderService, DashboardService, IdentityService, NoticeService, EventService } from '../../../_services';
import { User } from '../../../_models';


@Component({
  selector: 'app-right-sidebar',
  templateUrl: './right-sidebar.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['right-sidebar.component.css']
})
export class RightSidebarComponent implements OnInit {

  public maside: any;
  public masideStatus: any;
  public stats: any;
  public profile: User;
  public notices: any;
  public events: any;
  public role: any;
  public uid: any;
  public arrow: any;
  public selectedChild: any;
  public uuid: any;
  public school: any;

  constructor(private identityService: IdentityService,
    private noticeService: NoticeService,
    private eventService: EventService) {
    this.masideStatus = false;
    this.maside = 'm-aside-right-panel--minimize';

    this.profile = this.identityService.identity;
    this.role = this.identityService.role;
    this.uuid = this.identityService.uid;
    this.school = this.identityService.school;
   }

   

  ngOnInit() {
    if (this.role === "parent") {
      if (localStorage.getItem("selectedChild")) {
        this.selectedChild = JSON.parse(localStorage.getItem("selectedChild"));
        this.uid = this.selectedChild.uid;
        console.log('id for student',this.uid);
        this.getRecentEventForParent();
        this.getRecentNoticeForParent();
      } 
     
    }

      this.arrow = '<i class="fa fa-caret-right" aria-hidden="true"></i>';
      if (this.role === 'school' || this.role === 'teacher' || this.role === 'coadmin' || this.role === 'librarian' || this.role == 'clerk' || this.role === 'director') {
        this.getRecentEvents();
        this.getRecentNotice();
        
      }

      
  }

  

  getRecentEventForParent() {
    this.eventService.getEventForParent(this.uid).subscribe(eventData => {
      this.events = eventData.payload;
      console.log('events new data: ', this.events);
    },
      error => {
        console.error('Error while getting Recent Notice data. Stack: ', error);
      });
  } 

  getRecentNoticeForParent() {
    this.noticeService.getNoticeForParent(this.uid).subscribe(noticeData => {
      this.notices = noticeData.payload;
      console.log('notice1: ', this.notices);
    },
      error => {
        console.error('Error while getting Recent Notice data. Stack: ', error);
      });
  }

  getRecentNotice() {
    this.noticeService.getRecentNotice().subscribe(noticeData => {
      this.notices = noticeData.payload;
      // console.log('notice: ', this.notices);
    },
      error => {
        console.error('Error while getting Recent Notice data. Stack: ', error);
      });
  }

  getRecentEvents() {
    this.eventService.getRecentEvents().subscribe(eventData => {
      this.events = eventData.payload;
      // console.log('events: ', this.events);
    },
      error => {
        console.error('Error while getting Recent Notice data. Stack: ', error);
      });
  } 
  
  eventToggle() {    

    if (!this.masideStatus) {
      this.arrow = '<i class="fa fa-caret-right" aria-hidden="true"></i>';
      // console.log("true:",  this.arrow);
      this.miximizeRightAside();
    } else {
      this.arrow = '<i class="fa fa-caret-left" aria-hidden="true"></i>';
      // console.log("false:",  this.arrow);
      this.minimizeRightAside();
    }
  }

  minimizeRightAside() {    
    this.masideStatus = false;
    this.maside = 'm-aside-right-panel--minimize';
  }

  miximizeRightAside() {    
    this.masideStatus = true;
    this.maside = 'm-aside-right-panel--fixed';
  }


  /*@ViewChild('sidenav', {static: false}) sidenav: MatSidenav;

  reason = '';

  close(reason: string) {
    this.reason = reason;
    this.sidenav.close();
  }*/

}
