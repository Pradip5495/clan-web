import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { User } from '../../../_models';
import { IdentityService } from '../../../_services/identity.service';
import { ChildService } from '../../../_services/child.service';
import { Location } from '@angular/common';

declare let mLayout: any;
declare let $: any;
@Component({
  selector: 'app-header-nav',
  templateUrl: './header-nav.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./header-nav.component.css']
})
export class HeaderNavComponent implements OnInit, AfterViewInit {

  public role: any;
  public selectedChild: any;
  public _child_list: any;
  public uid: any;
  public studentDetails: any;
  mySubscription: any

  constructor(public profile: User, 
    private _identityService: IdentityService,
    private _child: ChildService) {
    this.profile = JSON.parse(localStorage.getItem('currentUser'));    

  }

  ngOnInit() {
    this.getRole();

    if (this.role === 'parent') {
      if (localStorage.getItem('selectedChild')) {
        this.selectedChild = JSON.parse(localStorage.getItem('selectedChild'));
        this.uid = this.selectedChild.uid;
        this.studentDetails = this.selectedChild;
      } else{
        this.getChild();
      }
    }

  }

  ngAfterViewInit() {

    mLayout.initHeader();

  }

  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }

  getRole() {
    this.role = this._identityService.role;

  }

  get child_list(): any {
    return this._child_list;
  }

  set child_list(value: any) {
    this._child_list = value;
  }

  getChild() {
    let value: any; 
    this._child.getChild().subscribe(data => {
      this._child_list = data.payload;
        $('#m_modal_student_5').modal('show');
    });
  }

  setChild(data) {
     localStorage.removeItem('selectedChild');
     localStorage.setItem('selectedChild', JSON.stringify(data));
     this.studentDetails = data;
     this.uid = data.uid;  
     location.reload();   
     $('#m_modal_student_5').modal('hide');
  }
}
