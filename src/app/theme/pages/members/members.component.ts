import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import {
  ActivatedRoute,
  Router,
  ParamMap,
  NavigationExtras
} from "@angular/router";
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { ApprovalService } from '../../../_services/approval.service';
import { IdentityService } from '../../../_services/identity.service';
import { LectureService } from '../../../_services/lecture.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ListInboxItemsService } from '../../../_services/listInboxItems.service';
import { SecurityService } from '../../../_services/security.service';
import { LibraryService } from '../../../_services/library.service';
import { MembersService } from '../../../_services/members.service';
import { ToasterService } from "../../../_services/toaster.service";

declare var $: any;
@Component({
  selector: 'app-members',
  templateUrl: 'members.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['members.component.css']
})
export class MembersComponent implements OnInit {

  public memberFlag: any = true;
  public heading: any;
  public active_librarian_list: any;
  public librarian_approval_list: any;
  public active_security_list: any;
  public security_approval_list: any;
  public active_assistant_list: any;
  public assistant_approval_list: any;
  public isAdmin: Boolean = false;
  public isTeacher: Boolean = false;
  public isDirector: Boolean = false;
  public isCoadmin: Boolean = false;
  public class: any;
  public role: any;
  public leture_data: any;
  public selected_teacher_info: any;
  public category_list: any = ['complain', 'Suggestion', 'Praise', 'Feedback'];
  public category: any;
  public form_values: any;
  public report: any;
  public to: any;
  public userDetails: any;
  public securityStats: any;

  userForm: FormGroup;
  constructor(private _library: LibraryService,
    private _script: ScriptLoaderService,
    private toasterService: ToasterService,
    private _approvalService: ApprovalService,
    private _identityService: IdentityService,
    private _lectureService: LectureService,
    private formBuilder: FormBuilder,
    private _listInboxItemsService: ListInboxItemsService,
    private _security: SecurityService,
    private router: Router,
    private route: ActivatedRoute,
    private _members: MembersService
    ) { 
      this.class = this._identityService.class;
      this.role = this._identityService.role;
      this.isAdmin = this._identityService.isAdmin;
      this.isTeacher = this._identityService.isClassTeacher;
      this.isDirector = this._identityService.isDirector;
      this.isCoadmin = this._identityService.isCoadmin;
    }

  ngOnInit() {
    if (this.isAdmin === true) {
      this.getSecurityStats();
    }
    this.userForm = this.formBuilder.group({
      category: ["", Validators.required],
      subject: ["", Validators.required],
      message: ["", Validators.required]
    });

    if (this.route.params["value"].type === 'Security') {
      this.memberFlag = false;
      this.heading = 'Security';
      this.getActiveSecurity();
      this.getApprovalSecurity();
    }
    
    if (this.route.params["value"].type === 'Librarian') {
      this.memberFlag = false;
      this.heading = 'Librarian';
      this.getActiveLibrarian();
      this.getApprovalLibrarian();      
    }
    
    if (this.route.params["value"].type === 'Assistant') {    
      this.memberFlag = false;
      this.heading = 'Assistant';
      this.getActiveAssistant();
      this.getApprovalAssistant();   
    }


  }

  getMemberView() {
    this.memberFlag = true;
  }

  getHeading(heading) {
    this.memberFlag = false;
    this.heading = heading;
    if (heading == 'Librarian') {
      this.getActiveLibrarian();
      this.getApprovalLibrarian();
    } else if(heading == 'Assistant') {
      this.getActiveAssistant();
      this.getApprovalAssistant();
    }else if(heading == 'Security') {
      this.getActiveSecurity();
      this.getApprovalSecurity();
    }
  }

  getActiveLibrarian() {    
    if (this.isAdmin || this.isTeacher || this.isDirector || this.isCoadmin) {
      this._library.getLibrarian().subscribe(Librarianlist => {
        if(Librarianlist.status == 200){
          this.active_librarian_list = Librarianlist.payload;
        }       
      },
        error => {
          console.error('Error while getting teachers list. stack: ', error);
        });
    }
  }

  getApprovalLibrarian() {
    if (this.isAdmin === true) {
      this._approvalService.getLibrarianAproval().subscribe(librarianApprovallist => {
        if (librarianApprovallist) {
          this.librarian_approval_list = librarianApprovallist.payload;
        } else {
          this.librarian_approval_list = null;
        }
      },
        error => {
          console.error('Error while getting teachers approval list. stack: ', error);
        });
    }
  }

  getActiveAssistant() {    
    if (this.isAdmin || this.isTeacher || this.isDirector || this.isCoadmin) {
      this._members.getAssistant().subscribe(Assistantlist => {
        if(Assistantlist.status == 200){
          this.active_assistant_list = Assistantlist.payload;
        }       
      },
        error => {
          console.error('Error while getting teachers list. stack: ', error);
        });
    }
  }

  getApprovalAssistant() {
    if (this.isAdmin === true) {
      this._approvalService.getAssistantAproval().subscribe(AssistantApprovallist => {
        if (AssistantApprovallist) {
          this.assistant_approval_list = AssistantApprovallist.payload;
        } else {
          this.librarian_approval_list = null;
        }
      },
        error => {
          console.error('Error while getting teachers approval list. stack: ', error);
        });
    }
  }


  getActiveSecurity() {    
    if (this.isAdmin || this.isTeacher || this.isDirector || this.isCoadmin) {
      this._members.getSecurity().subscribe(securitylist => {
        if(securitylist.status == 200){
          this.active_security_list = securitylist.payload;
        }       
      },
        error => {
          console.error('Error while getting teachers list. stack: ', error);
        });
    }
  }

  sendMessageBox(userData) {
    this.userDetails = userData;
    $('#m_modal_5').modal('show');
  }

  onSubmit() {
    // this.category, this.userForm.value.subject, this.to, this.userForm.value.message
    this._listInboxItemsService
      .postMemberMessage(this.userForm.value, this.userDetails.uid)
      .subscribe(data => {
        if (data.status === 201) {
          this.report = data.payload;
          this.toasterService.Success("Report Generated Successfully");
          $("#m_modal_5").modal("hide");
          this.userForm.reset();
        } else {
          this.toasterService.Error("Report Not Generated!");
        }
      });

    /*this.report = data.payload);
    console.log('report generate' + this.report);
    if (this.userForm.value.subject === null && this.userForm.value.message === null) {
      alert('Please Enter Subject & message');
    } else {
      // alert('Report Generated Successfully');
      //$('#m_modal_5').modal('hide');
    }
    this.userForm.value.reset();*/
  }

  getApprovalSecurity() {
    if (this.isAdmin === true) {
      this._approvalService.getAssistantSecurity().subscribe(securtyApprovallist => {
        if (securtyApprovallist) {
          this.security_approval_list = securtyApprovallist.payload;
        } else {
          this.security_approval_list = null;
        }
      },
        error => {
          console.error('Error while getting teachers approval list. stack: ', error);
        });
    }
  }


  getUserDetails(userDetails)
  { 
    this.userDetails = userDetails;
    $('#m_modal_4').modal('show'); 
  }

  /** Get Security Stats Data */
  getSecurityStats() {
    this._security.getSecurityStats().subscribe(securityStats => {
      if(securityStats.status === 200){
        this.securityStats = securityStats.payload;
      }
    });
  }

}
