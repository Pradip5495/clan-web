import { Component, OnInit, ViewEncapsulation, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { StudentService } from '../../../_services/student.service';
import { ApprovalService } from '../../../_services/approval.service';
import { IdentityService } from '../../../_services/identity.service';
import { ClassService } from '../../../_services/class.service';
import { ExaminationService } from '../../../_services/examination.service';
import { ResultService } from '../../../_services/result.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';

declare var $: any;

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class ResultComponent implements OnInit {
  // @Input exam_code: string;
  exam_code: any;
  private result_data;
  private selected_uid;
  public exam_data;
  public selected_exam_info;
  private myName;
  public id;
  public current_date = new Date();
  private data;
  // private date1;
  userForm: FormGroup;
  today: Date = new Date();


  // private route;
  @Input()
  totalMarks: number = this.totalMarks;
  date: Date = this.date;


  code: string = this.exam_code;

  constructor(private _examinationService: ExaminationService,
    private _classService: ClassService,
    private _studentService: StudentService,
    private _resultService: ResultService,
    private formBuilder: FormBuilder,
    private router: Router,
    private datePipe: DatePipe) {

    let dates = new Date();
    let date1 = this.datePipe.transform(dates, 'yyyy-MM-dd')
    console.log(' todays date', this.datePipe.transform(dates, 'yyyy-MM-dd'));
    console.log(date1);

    this.router = router;
    this._examinationService = _examinationService;
    //  console.log('cone called' + this.code);
    this.exam_code = _examinationService.getData(this.exam_code);
    this.totalMarks = _examinationService.getMarks(this.totalMarks);
    this.date = _examinationService.getDate(this.date);

    console.log(typeof this.exam_code);
    console.log('total marks', this.totalMarks);
  }


  ngOnInit() {

    console.log(this.exam_code);
    this.passData(this.exam_code);
    this.getResult(this.id);
    this.userForm = this.formBuilder.group({
      marks: [],
    });
  }
  passData(exam_code) {
    this._examinationService.saveData(this.exam_code, this.totalMarks, this.date);
    console.log('date in result', this.date);
  }
  getResult(exam_code) {
    this._resultService.getResult(this.exam_code).subscribe(result_data => {
      this.result_data = result_data.payload;
      console.log('result data-:', this.result_data);


    });
  }

  marks(selected_uid) {
    console.log('today', this.today);
    let today1 = this.datePipe.transform(this.today, 'yyyy-MM-dd');
    console.log('today1', this.datePipe.transform(this.today, 'yyyy-MM-dd'));
    console.log('servic date', this.date);
    console.log('exam code', this.exam_code);
    console.log('student', this.selected_uid);
    console.log(this.userForm.value);
    console.log('marks data', this.result_data);
    if (this.userForm.value.marks > this.totalMarks) {
      alert('Marks should be less than of total marks');
    } else {
      this._resultService.getMarks(this.selected_uid, this.exam_code, this.userForm.value.marks).subscribe(data => {
        this.data = data.payload;
        console.log('marks data:', this.data);
        if (this.userForm.valid) {
          alert('Report Generated Successfully!');
        }
        this.userForm.reset();
        $('#m_modal_5').modal('hide');

        this.getResult(this.exam_code);
      });
    }
  }
  /* getExamination(code) {
     this._resultService.getExamination(code).subscribe(exam_data => {
         this.exam_data = exam_data.payload;
       for (const exam of this.exam_data) {
         // console.log('exam data:', this.exam_data);
         if (exam.code === code) {
             this.selected_exam_info = exam;
             console.log('selected exam info: ', this.selected_exam_info);
             this.exam_code = this.selected_exam_info.code;
             console.log(this.exam_code);

           }
         }
         // this.DatatableDataLocalDemo(this.exam_data).init();
       },
       error => {
         console.error('Error while getting exam data. stack: ', error);
       });
   }*/
  assignUid(result_uid) {
    console.log('today', this.today);
    const today1 = this.datePipe.transform(this.today, 'yyyy-MM-dd');
    const todate = this.datePipe.transform(this.date, 'yyyy-MM-dd');
    console.log('today1', this.datePipe.transform(this.today, 'yyyy-MM-dd'));
    console.log('servic date', this.date);
    if (this.datePipe.transform(this.today, 'yyyy-MM-dd') < todate) {
      alert('You can not enter marks before exam');
    } else {
      console.log('hiii', result_uid);
      this.selected_uid = result_uid;
    }
  }
}


