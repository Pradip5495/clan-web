import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ScriptLoaderService } from "../../../_services/script-loader.service";
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-privacy',
  templateUrl: "./privacy.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./privacy.component.css"]
})
export class PrivacyComponent implements OnInit {

  url: string = "https://www.clan.school/policy/privacy.html";
  urlSafe: SafeResourceUrl;

  constructor(public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.urlSafe= this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
  }

}
