import { AfterViewInit, Component, OnInit, ViewEncapsulation } from '@angular/core';

// Models
import { User } from '../../../_models/user';

// Services
import { IdentityService } from '../../../_services';
import { ListInboxItemsService } from '../../../_services/listInboxItems.service';
import { MessageService } from '../../../_services/message.service';
@Component({
  selector: 'app-header-inbox',
  templateUrl: './inbox.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./inbox.component.css']
})

export class InboxComponent implements OnInit {

  public profile: User;
  public role: string;
  public uid: any;

  private complain_list: any;
  private suggestion_list: any;
  private praise_list: any;
  private feedback_list: any;
  private conversation: any;
  public sendmsg: any;
  


  public id: any;
  public msgid: any;

  private fullview: String = 'col-xl-12';
  private halfview: String = 'col-xl-6 col-lg-12';
  private applyfullview: Boolean = true;
  private applyhalfview: Boolean = false;

  constructor(private identityService: IdentityService,
    private _listInboxItemsService: ListInboxItemsService,
    private _messageService: MessageService) {

    this.profile = this.identityService.identity;
    this.role = this.identityService.role;
    this.uid = this.identityService.uid;
  }

  ngOnInit() {
    this.complainClick();
    this.feedbackClick();
  }

  addCssClass(isapplyhalfview: boolean, isapplyfullview: boolean) {

    let classes = {
      'col-xl-12 col-md-12': isapplyhalfview,
      'col-xl-12': isapplyfullview
    }
    return classes;
  }

  item_inbox_complain_click(id) {

    this.id = id;
    

    this.applyfullview = false;
    this.applyhalfview = true;

    this._messageService.getMessage(id).subscribe(data => {
      this.conversation = data.payload;

    },
      error => {
        console.error('Error while getting message. stack: ', error);
      });
  }

  complainClick() {
    this._listInboxItemsService.getListInboxItemsService('Complain').subscribe(complain_list => {
      if (complain_list.status === 200) {
        this.complain_list = complain_list.payload;
      } else {
        this.complain_list = [];
      }
      // for(let i of this.complain_list) {
      //   this.complain_message = this.getMessage(i.id);
      //   console.log('complain message: ');
      //
      //   if (this.complain_message){
      //     console.log(this.complain_message);
      //   }
      // }

    },
      error => {
        this.complain_list = '';
        console.error('Error while getting complain data. stack this.complain_list: ', this.complain_list);
        console.error('Error while getting complain data. stack: ', error);
      });


    // this.getMessage(this.complain_list.id);
  }

  suggestionClick() {
    this._listInboxItemsService.getListInboxItemsService('Suggestion').subscribe(suggestion_list => {
      this.suggestion_list = suggestion_list.payload;

      // for(let i of this.suggestion_list){
      //   console.log('suggestion message: ');
      //   this.getMessage(i.id);
      // }
    },
      error => {
        console.error('Error while getting suggestion data. stack: ', error);
      });
  }

  praiseClick() {
    this._listInboxItemsService.getListInboxItemsService('Praise').subscribe(praise_list => {
      this.praise_list = praise_list.payload;

      // for(let i of this.praise_list){
      //   console.log('praise message: ');
      //   this.getMessage(i.id);
      // }
    },
      error => {
        console.error('Error while getting praise data. stack: ', error);
      });
  }

  feedbackClick() {
    this._listInboxItemsService.getListInboxItemsService('Feedback').subscribe(feedback_list => {
      this.feedback_list = feedback_list.payload;

      // for(let i of this.feedback_list){
      //   console.log('feedback message: ');
      //   this.getMessage(i.id);
      // }
    },
      error => {
        console.error('Error while getting feadback data. stack: ', error);
      });
  }

  getMessage(id: any): any {

    this._messageService.getMessage(id).subscribe(data => {
      this.conversation = data.payload;
      // console.log(this.message);

    },
      error => {
        console.error('Error while getting message. stack: ', error);
      });
  }

  postMessage(msg) {

    if (msg) {

      this._messageService.postMessage(this.id, msg).subscribe(data => {
        this.sendmsg = '';
        this.getMessage(this.id);

      },
        error => {
          console.error('Error while getting conversation list. stack: ', error);
        });

      this.sendmsg = '';
      this.getMessage(this.id);
    }
  }
}
