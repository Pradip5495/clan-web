
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MessageService } from '../../../_services/message.service';
import { ConversationIdService } from '../../../_services/conversationId.service';
import { IdentityService } from '../../../_services/identity.service';
import { Router } from '@angular/router';
import { MemberinfoService } from '../../../_services/memberinfo.service';

@Component({
  selector: 'app-members-info',
  templateUrl: './members-info.component.html',
  encapsulation: ViewEncapsulation.None,
})

export class MembersInfoComponent implements OnInit {

  public id: any;
  public conversation: any;
  public uid: any;
  public sendmsg: any;
  public myGroup: any;


  // TODO: losing id of conversation on refreshing this page

  constructor(private _messageService: MessageService,
    private _membersinfoService: MemberinfoService,
    private _conversationIdService: ConversationIdService,
    private _identityService: IdentityService,
    private _router: Router) {

    console.log('memberlist is hit');
    this.uid = _identityService.uid;
    // this.id = _identityService.uid;

    console.log('uid: ' + this.uid);
  }

  ngOnInit(): void {
    this._conversationIdService.currentId.subscribe(data => this.id = data);

    if (this.id) {
      this.getGroupList(this.id);
      console.log('current id in members info page: ' + this.id);
    }
  }

  getGroupList(id) {
    this._membersinfoService.getGroupList(id).subscribe(data => {
      this.myGroup = data.payload;
      console.log('my group list: ', this.myGroup);
      this.conversation = data.payload;
      console.log(' Group conversation list: ', this.conversation);
      console.log("list" + id);
    },
      error => {
        console.error('Error while getting conversation list. stack: ', error);
      });
  }

  /* postMessage(msg) {
 
     if (msg) {
       this._messageService.postMessage(this.id, msg).subscribe(data => {
 
           console.log('message is send');
           this.getMessage(this.id);
 
         },
         error => {
           console.error('Error while getting conversation list. stack: ', error);
         });
 
     }
 
     this.sendmsg = '';
     this.getMessage(this.id);
 
   }*/
}
