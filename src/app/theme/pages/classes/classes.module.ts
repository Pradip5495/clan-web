import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../../layouts/layout.module';
import { Routes, RouterModule } from '@angular/router';
import { ClassesComponent } from './classes.component';
import { DefaultComponent } from '../default/default.component';

const routes: Routes = [
  {
    'path': '',
    'component': DefaultComponent,
    'children': [
      {
        'path': '',
        'component': ClassesComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule
  ], exports: [
    RouterModule
  ], declarations: [
    ClassesComponent
  ]
})
export class ClassesModule {


}
