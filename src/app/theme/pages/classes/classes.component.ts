import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { ActivatedRoute, Router, ParamMap, NavigationExtras } from '@angular/router';
import { ClassService } from '../../../_services/class.service';
import { StudentService } from '../../../_services/student.service';
import { TeacherService } from '../../../_services/teacher.service';
import { IdentityService } from '../../../_services/identity.service';
import { AssignService } from '../../../_services/assign.service';
import { ConversationService } from '../../../_services/conversation.service';
import { PassengerService } from '../../../_services/passenger.service';
import { LectureService } from '../../../_services/lecture.service';


@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./classes.component.css']
})
export class ClassesComponent implements OnInit {

  private teacher_data: any;
  private student_data: any;
  public conversation: any;
  private _class_data: any;
  private route_code: any;
  private _class_value: any = null;
  private class1: any;
  public myGroup: any;
  private passenger_data: any;
  public colors;
  public leture_data: any;
  public active_teachers_list: any;
  public selected_teacher_info: any;
  public studentListView: any = false; 
  checked: any;

  @Input()
  code: string = this.route_code;

  get class_data(): any {
    return this._class_data;
  }

  set class_data(value: any) {
    this._class_data = value;
  }

  get class_value(): any {
    return this._class_value;
  }

  set class_value(value: any) {
    this._class_value = value;
  }

  constructor(private _classService: ClassService,
    private _teacherService: TeacherService,
    private _studentService: StudentService,
    private _identityService: IdentityService,
    private _assignService: AssignService,
    private _lectureService: LectureService,
    private _conversationService: ConversationService,
    private route: ActivatedRoute,
    private _passengerService: PassengerService, private router: Router) {
    this.colors = ['#53D476', '#078EFC', '#31A3AB', '#FF6565', '#FFB300', '#6B5AED', '#DCA758', '#F82ABA', '#BABF24', '#A067D1'];
    this.studentListView = false;

  }

  ngOnInit() {
    this.passData(this.route_code);
    // this.getBus(this.route_code);
    this.getClass();
    this.getTeacher(this.class1);
    this.getConversation();
   

    if(this.route.params['value'].class){
      this.class1 = this.route.params['value'].class;
      this.studentListView = true;
      this.getTeacher(this.class1);
      this.getStudent(this.class1);
    } else {
      this.studentListView = false;
    }

  }

  getConversation() {
    this._conversationService.getConversation().subscribe(data => {

      this.myGroup = data.payload;
      let count = this.myGroup.length;
      this.colors = ['#53D476', '#078EFC', '#31A3AB', '#FF6565', '#FFB300', '#6B5AED', '#DCA758', '#F82ABA', '#BABF24', '#A067D1'];


    },
      error => {
        console.error('Error while getting my group list. stack: ', error);
      });
  }

  getColor() {
    this.colors = ['#53D476', '#078EFC', '#31A3AB','#FF6565', '#FFB300', '#6B5AED', '#DCA758', '#F82ABA', '#BABF24', '#A067D1'];
    let count = this.myGroup.length;
    for (let j = 0; j <= count; j++) {
      for (let i = 0; i <= 10; i++) {
      }
      // --count;


    }
  }


  passData(ruid) {
    this._assignService.saveData(this.route_code);

  }

  getClass() {
    this._classService.getClass().subscribe(class_data => {
      this.class_data = class_data.payload;
    },
      error => {
        console.error('Error while getting class data. stack: ', error);
      });
  }

  getTeacher(class1: any) {
    this._teacherService.getTeacherByClass(class1).subscribe(teacher_data => {
      this.teacher_data = teacher_data.payload;
    },
      error => {
        console.error('Error while getting teacher data. stack: ', error);
      });
  }

  getStudent(event: any) {
    this._studentService.getStudentByClass(event).subscribe(student_data => {
      this.student_data = student_data.payload;
    },
      error => {
        console.error('Error while getting student data. stack: ', error);
      });
  }

  addPassenger() {
    // const checked = (document.getElementById('check') as HTMLTextAreaElement).value;
    this._assignService.saveData(this.route_code);
    if (document.getElementById('check')) {
      this._passengerService.getPassenger(this.route_code).subscribe(passenger_data => {
        this.passenger_data = passenger_data.payload;
      });
      // document.getElementById('chekedBoxHide').disabled = true;
    }
  }

  /** Get  All Student List */
  getAllStudent() {
    this._studentService.getStudent().subscribe(student_data => {
      this.student_data = student_data.payload;
    },
      error => {
        console.error('Error while getting student data. stack: ', error);
      });
  }

  /** redirect for view student and attendance details */
  gotoAttendace(student: any, student_uid: any) {
    localStorage.setItem('StudentProfile', JSON.stringify(student));
    this.router.navigate(['attendance/student/' + student_uid]);
  }

  /* Get Teacher Info */
  getSelectedTeacherInfo(id) {

    for (const teacher of this.active_teachers_list) {
      if (teacher.uid === id) {
        this.selected_teacher_info = teacher;
        // console.log('selected teacher info: ' , this.selected_teacher_info);
      }
    }
  }
  /* Get Active Teachers */
  getActiveTeacher() {
    this._teacherService.getTeacherByClass(this.class1).subscribe(teacherlist => {
      this.active_teachers_list = teacherlist.payload;
    },
      error => {
        console.error('Error while getting teachers list. stack: ', error);
      });

    this._teacherService.getTeacher().subscribe(teacherlist => {
      this.active_teachers_list = teacherlist.payload;
    },
      error => {
        console.error('Error while getting teachers list. stack: ', error);
      });
  }

  /* Get Teachers profile and lecture details */
  getProfileAndLecture(uid) {
    this._lectureService.getLectureById(uid).subscribe(data => {
      this.leture_data = data.payload;
      this.getSelectedTeacherInfo(uid);
    },
      error => {
        console.error('Error while getting leture data. stack: ', error);
      });
  }
}
