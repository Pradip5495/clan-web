
import { AfterViewInit, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// Models
import { User } from '../../../_models/user';

// Services
import { IdentityService } from '../../../_services';
import { ListInboxItemsService } from '../../../_services/listInboxItems.service';
import { AssignService } from '../../../_services/assign.service';
declare var $: any;

@Component({
  selector: 'app-assign',
  templateUrl: './assign.component.html',
  encapsulation: ViewEncapsulation.None,
})

export class AssignComponent implements OnInit {

  public show: Boolean = false;
  public buttonName: any = 'Assign Driver';


  public profile: User;
  public role: string;
  public uid: any;
  public coadmin: any;

  private bus_data: any;
  private data: any;
  private driver_data: any;
  private feedback_list: any;
  private conversation: any;
  public driver: any;
  public all_driver: any;
  public selected_driver_info: any;
  public selected_driver_route: any;
  public ruid: any;
  public delete_diver: any;
  public assign_data: any;
  private selected_driver: any;
  private delete_route: any;
  private all_route: any;
  private router_data: any;
  private bus_list: any;
  private selected_route: any;

  busesForm: FormGroup;

  public id: any;

  private fullview: String = 'col-xl-12';
  private halfview: String = 'col-xl-6 col-lg-12';
  private applyfullview: Boolean = true;
  private applyhalfview: Boolean = false;

  constructor(private identityService: IdentityService,
    private _assignService: AssignService) {

    this.profile = this.identityService.identity;
    this.role = this.identityService.role;
    this.uid = this.identityService.uid;
    this.coadmin = this.identityService.isCoadmin;

    console.log('roleee ', this.role);
    console.log(this.uid);
  }

  ngOnInit() {
    this.busClick();
    //  this.busList();
    this.allRoute();
  }


  addCssClass(isapplyhalfview: boolean, isapplyfullview: boolean) {

    let classes = {
      'col-xl-6 col-lg-12': isapplyhalfview,
      'col-xl-12': isapplyfullview
    }
    return classes;
  }

  busClick() {
    this._assignService.busList().subscribe(bus_data => {
      this.bus_data = bus_data.payload;
      console.log('bus data:', this.bus_data);


    },
      error => {
        console.error('Error while getting bus data. stack: ', error);
      });
  }
  allRoute() {
    this._assignService.getBusRoute().subscribe(all_route => {
      this.all_route = all_route.payload;
      console.log('all route :', this.all_route);
    },
      error => {
        console.error('Error while getting driver data. stack: ', error);
      });

  }

  driverClick() {
    this._assignService.getRouteDriver().subscribe(driver_data => {
      this.driver_data = driver_data.payload;
      console.log('driver data:', this.driver_data);
      this.addDriverForRoute();
    },
      error => {
        console.error('Error while getting driver data. stack: ', error);
      });
  }

  addDriverForRoute() {
    this._assignService.addDriverForRoute().subscribe(all_driver => {
      this.all_driver = all_driver.payload;
      //    console.log('all driver :', this.all_driver);
    },
      error => {
        console.error('Error while getting driver data. stack: ', error);
      });

  }

  deleteRoute(route_info) {
    // console.log('hiii', route_info.number);
    this._assignService.deleteRoute(route_info.ruid, route_info.buid).subscribe(delete_route => {
      this.delete_route = delete_route.payload;
      this.busClick();
    },
      error => {
        console.error('Error while getting router data. stack: ', error);
      });
  }
  selectedRoute(route_data) {
    //    console.log('route infooo ruid', route_data);
    //  console.log('route buid', this.selected_route);
    $('#m_modal_5').modal('hide');
    this._assignService.assignRoute(route_data.ruid, this.selected_route.buid).subscribe(router_data => {
      this.router_data = router_data.payload;
      this.busClick();
    });
  }
  selectedDriver(driver_info) {
    //   console.log('driver infooo', driver_info);
    //   console.log('driver ruid', this.selected_driver);
    $('#m_modal_4').modal('hide');

    this._assignService.assignDriver(this.selected_driver.ruid, driver_info.uid).subscribe(driver_data => {
      this.driver_data = driver_data.payload;
      this.driverClick();
    });
  }

  selectedRouteId(id) {

    for (const drivers of this.driver_data) {
      if (drivers.uid === id) {
        this.selected_driver_route = drivers;
        console.log('selected raute driver: ', this.selected_driver_route);
      }
    }
  }

  deleteDriver(driver_info) {
    console.log('hiii', driver_info);
    this._assignService.deleteDriver(driver_info.ruid, driver_info.driver.uid).subscribe(delete_diver => {
      this.delete_diver = delete_diver.payload;
      this.driverClick();
    },
      error => {
        console.error('Error while getting driver data. stack: ', error);
      });
  }
  assignDriver(assign_driver) {
    // console.log('hiii', assign_driver);
    this.selected_driver = assign_driver;
  }
  assignRoute(assign_route) {
    //   console.log('assign route code', assign_route);
    this.selected_route = assign_route;

  }
}
