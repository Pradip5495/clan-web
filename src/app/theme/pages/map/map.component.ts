import { Component, ViewChild, ViewEncapsulation, OnInit } from "@angular/core";
//import { MouseEvent } from '@agm/core';

@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["map.component.css"]
})
export class MapComponent implements OnInit {
  // @ViewChild('googleMap') gmapElement: any;
  // map: google.maps.Map;
  // public currentLat: any;
  // isTracking: boolean;
  // public currentLong: any;
  // public marker: any;
  // constructor() { }
  // lat = 51.678418;
  // lng = 7.809007;

  // //google maps zoom
  // zoom: Number = 14;
  // //Get Directions
  // dir = undefined;
  ngOnInit() {
    /* var mapProp = {
       center: new google.maps.LatLng(28.4595, 77.0266),
       zoom: 14,
       // mapTypeId: google.maps.MapTypeId.ROADMAP
       mapTypeId: google.maps.MapTypeId.HYBRID
       // mapTypeId: google.maps.MapTypeId.SATELLITE
       // mapTypeId: google.maps.MapTypeId.TERRAIN
     };
 
     this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
     var marker = new google.maps.Marker({ position: mapProp.center });
     marker.setMap(this.map);
 
     var infowindow = new google.maps.InfoWindow({
       content: 'Hey, We are here'
     });
     infowindow.open(this.map, marker);*/
    //  this.dir = {
    //   origin: { lat: 24.799448, lng: 120.979021 },
    //   destination: { lat: 24.799524, lng: 120.975017 }
    // }
  }
  //

  lat: Number = 18.6008;
  lng: Number = 73.7398;
  zoom: Number = 15;

  public markerOptions = {
    origin: {
        // icon: 'https://www.shareicon.net/data/32x32/2016/04/28/756617_face_512x512.png',
        // draggable: true,
    },
    destination: {
        icon: '././assets/images/bus_icon.png',
        opacity: 0.8,
    },
}

public renderOptions = {
  suppressMarkers: true,
}
 
  dir = {
  origin: { lat: 18.6414, lng: 73.7398 },
  destination: { lat: 18.5236, lng: 73.8478 },
  waypoints: [
    { location: { lat: 18.6414, lng: 72.7398 } },
    { location: { lat: 18.6012, lng: 73.7407 } },
    { location: { lat: 18.601, lng: 73.7405 } },
    { location: { lat: 18.5236, lng: 73.8478 } }
  ],
  //optimizeWaypoints: true,
  renderOptions: {
    suppressMarkers: true
  },
  visible: true
};

constructor() { }

//   public redColor() {
//   this.dir.renderOptions = {
//     polylineOptions: {
//       strokeColor: "#f00",
//       strokeOpacity: 0.6,
//       strokeWeight: 5
//     }
//   };
// }

//   public greenColor() {
//   this.dir.renderOptions = {
//     polylineOptions: {
//       strokeColor: "#0f0",
//       strokeOpacity: 0.8,
//       strokeWeight: 10
//     }
//   };
//}
}

// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
