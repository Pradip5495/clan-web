import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../default/default.component';
import { MapComponent } from './map.component';
import { FormsModule } from '@angular/forms';
// Google Map Implement module
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction'; // agm-direction


const routes: Routes = [
  {
    'path': '',
    'component': DefaultComponent,
    'children': [
      {
        'path': '',
        'component': MapComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule,
    AgmCoreModule.forRoot({ //@agm/core
      apiKey: 'AIzaSyBFYhE4lgP8bEq6-xuPmH9bWE_MPct1ROo' //AIzaSyBbUr9k3JchtdRRTnyrl2opuLJkBd7XT_w
    }),
    AgmDirectionModule, //agm-direction
  ], exports: [
    RouterModule
  ], declarations: [
    MapComponent
  ],
  providers: [GoogleMapsAPIWrapper]
})
export class MapModule {


}
