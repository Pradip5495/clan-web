import {
  Component,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  AfterViewInit
} from "@angular/core";
import { ScriptLoaderService } from "../../../_services/script-loader.service";
import { HomeworkService } from "../../../_services/homework.service";
import { ClassService } from "../../../_services/class.service";
import { ChildService } from "../../../_services/child.service";
import { ToasterService } from "../../../_services/toaster.service";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { StudentService } from "../../../_services/student.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TransportService } from "../../../_services/transport.service";
import { TeacherService } from "../../../_services/teacher.service";
import { LectureService } from "../../../_services/lecture.service";
import { IdentityService } from "../../../_services/identity.service";
import {
  FileUploader,
  FileSelectDirective
} from "ng2-file-upload/ng2-file-upload";
import { DomSanitizer } from "@angular/platform-browser";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { environment } from "../../../../environments/environment";

declare let $: any;

@Component({
  selector: "app-homework",
  templateUrl: "./homework.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./homework.component.css"]
})
export class HomeworkComponent implements OnInit {
  userForm: FormGroup;
  // @ViewChild('fileInput') fileInput;

  today: number = Date.now();
  imagePath: any;
  public homework_data;
  private selectedClass;
  private toReturnImage: any;
  // private class_data: any;
  public class_value: any;
  private students_list: any;
  private id;
  public role;
  public homeworkDetails: any;
  private class;
  private selected_class_homework: any;
  private class1;
  public leture_data: any;
  private data;
  private subject;
  private active_teachers_list: any;
  homeworkForm: FormGroup;
  public class_data: any = [];
  public subject_data: any = [];
  public homeworkListView: any = false;
  public selected_class_value: any = null;
  public selected_subject: any = null;
  public image_file: File = null;
  public homeword_in_progress = 0;
  public homeword_completed: any = [];
  public homeword_Incomplete: any = [];
  public router;
  public homework1: any;
  public selectedChild: any;
  public uid: any;
  public studentDetails: any;
  public schoolCode: any;
  public _child_list: any;

  // attachments: string;
  attachments: any = "";

  /*
    public fileChangeEvent(fileInput: any) {
      console.log('img path want')
      if (fileInput.target.files && fileInput.target.files[0]) {
        var reader = new FileReader();
        console.log('img path' , reader);
        console.log('img path123' , reader);
        reader.onload = function (e: any) {
          $('#preview').attr('src', e.target.result );
          console.log('full target img',  e.target.result );

          //  console.log('full', e.target.result);
        }

        this.imgSrc = reader.readAsDataURL(fileInput.target.files[0]);
        console.log('full path img', this.imgSrc );
      }
    }
  */
  handleInputChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    // console.log('pattern', data)

    if (!file.type.match(pattern)) {
      alert("invalid format");
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleReaderLoaded(e) {
    let reader = e.target;
    this.attachments = reader.result;
  }
  constructor(
    private _homeworkService: HomeworkService,
    private _router: Router,
    private _sanitizer: DomSanitizer,
    private _classService: ClassService,
    private _child: ChildService,
    private _studentService: StudentService,
    private formBuilder: FormBuilder,
    private _transportService: TransportService,
    private _teacherService: TeacherService,
    private toasterService: ToasterService,
    private _lectureService: LectureService,
    private route: ActivatedRoute,
    private identityService: IdentityService,
    private http: HttpClient
  ) {
    const id = this.identityService.uid;
    this.role = this.identityService.role;
  }

  ngOnInit() {
    const id = this.identityService.uid;
    if (this.role == "parent") {
      if (localStorage.getItem("selectedChild")) {
        this.selectedChild = JSON.parse(localStorage.getItem("selectedChild"));
        this.uid = this.selectedChild.uid;
        this.schoolCode = this.selectedChild.schoolCode;
        this.studentDetails = this.selectedChild;
        let std = this.selectedChild.std;
        let div = this.selectedChild.div;
        let studClass = std + "" + div;
        this.getHomeworkByUid();
      } else {
        this.getChild();
      }
    } else {
      this.getProfileAndLecture(id);

      this.getHomework();
      this.getClass();
    }

    //  this.readURL(event);
    //  this.getSelectedClass();
    this.userForm = this.formBuilder.group({
      imageFile: [null, Validators.required],

      title: ["", Validators.required],
      subject: ["", Validators.required],
      description: ["", Validators.required],
      std: ["", Validators.required],
      div: ["", Validators.required],
      submission: ["", Validators.required],
      attachments: ["", Validators.required]
    });
    if (this.route.params["value"].home) {
      this.homework1 = this.route.params["value"].home;
      this.homeworkListView = true;
    } else {
      this.homeworkListView = false;
    }
    /*  this.homeworkForm = this.formBuilder.group({
        title: [],
        subject: [],
        description: [],
        std: [],
        div: [],
        submission: [],
        attachments: [],
      });*/
  }

  get child_list(): any {
    return this._child_list;
  }

  set child_list(value: any) {
    this._child_list = value;
  }

  setClass(data) {
    this.selected_class_value = data;
  }

  setSubject(data) {
    this.selected_subject = data;
  }

  getHomework() {
    this._homeworkService.getHomeworkByClass(this.class1).subscribe(
      homework_data => {
        this.homework_data = homework_data.payload;
        for (let in_progress of this.homework_data) {
          if (in_progress.completed) {
          } else {
            this.homeword_in_progress = this.homeword_in_progress + 1;
          }
        }
      },
      error => {
        console.error("Error while getting homework data. stack: ", error);
        this.toasterService.Error("Error while getting leture data. stack");
      }
    );
  }

  getClass() {
    this._classService.getClass().subscribe(
      class_data => {
        this.class_data = class_data.payload;
        this.getProfileAndLecture(this.id);
      },
      error => {
        console.error("Error while getting class data. stack: ", error);
        this.toasterService.Error("Error while getting leture data. stack");
      }
    );
  }

  /** get Child list of Parent */
  getChild() {
    let value: any;
    this._child.getChild().subscribe(data => {
      this._child_list = data.payload;
      $("#m_modal_child_5").modal("show");
    });
  }

  /** Set Child list for View rendar */
  setChild(data) {
    localStorage.setItem("selectedChild", JSON.stringify(data));
    this.uid = data.uid;
    $("#m_modal_child_5").modal("hide");
  }

  /*
    getSelectedClass() {
      for ( const selected_class of this.class_data) {
        if (selected_class) {
          console.log('get Homework data by class');
          this._lectureService.getLectureById(this.id).subscribe(data => {

            this.leture_data = data.payload;
            console.log('selected teachers lecture:  ', this.leture_data);
            console.log('idddd' + this.leture_data.subject);

            console.log();
            //  this.getSelectedTeacherInfo(uid);
          });

        }
      }
    }
  */
  getHomeworkByUid() {
    this._homeworkService.getHomeworkById(this.uid).subscribe(homework_data => {
      if (homework_data.status == 200) {
        this.homework_data = homework_data.payload;
        this.homeword_in_progress = 0;
        this.homeword_completed = [];
        this.homeword_Incomplete = [];
        for (let in_progress of this.homework_data) {
          if (in_progress.completed) {
            this.homeword_completed.push(in_progress);
          } else {
            this.homeword_Incomplete.push(in_progress);
            this.homeword_in_progress = this.homeword_in_progress + 1;
          }
        }
      }
    });
  }

  getHomeworkByClass(class1) {
    this.class_value = event;
    this._homeworkService
      .getHomeworkByClass(class1)
      .subscribe(homework_data => {
        this.homework_data = homework_data.payload;
        for (homework_data of this.class_data) {
          //  if (homework_data.class_data ===  class) {
          this.selected_class_homework = homework_data;
        }
      });
  }

  getStudent(event: any) {
    this.class_value = event;

    this._studentService.getStudentByClass(event).subscribe(
      students_list => {
        this.students_list = students_list.payload;
      },
      error => {
        console.error("Error while getting student list. stack: ", error);
        this.toasterService.Error("Error while getting leture data. stack");
      }
    );
  }
  getActiveTeacher() {
    this.subject = event;

    this._teacherService.getTeacher().subscribe(
      teacherlist => {
        this.active_teachers_list = teacherlist.payload;
      },
      error => {
        console.error("Error while getting teachers list. stack: ", error);
        this.toasterService.Error("Error while getting leture data. stack");
      }
    );
  }
  getProfileAndLecture(id) {
    this._lectureService.getLectureById(id).subscribe(
      data => {
        if (data !== null) {
          this.leture_data = data.payload;
         
          //  this.getSelectedTeacherInfo(uid);
        }
      },
      error => {
        this.toasterService.Error("Error while getting leture data. stack");
        console.error("Error while getting leture data. stack: ", error);
      }
    );
  }
  onFileChange(event) {
    this.image_file = <File>event.target.files[0];
  }

  /*onSubmit() {
    console.log(this.class_data, 'class data in onsubmit');
    this.postHomework(this.userForm.value.title, this.userForm.value.std,  this.userForm.value.div, this.subject, this.userForm.value.description, this.userForm.value.submission, this.image_file);
  }

  postHomework(title, std, div, subject, description, submission, attachments) {

    console.log('file before post: ', this.image_file);
    console.log('file before post: ', this.userForm.value);
    this._homeworkService.postHomework(title, std, div, subject, description, submission, attachments ).subscribe(data => {

        console.log('responce: ' + data);
      },
      error => {
        console.error('Error while getting uploading image. stack: ', error);
        alert('some error while uploading!!!');
      });
  }*/

  onSubmit() {
    let headers = new HttpHeaders();
    // headers.set('Content-Type', null);
    headers.set("Accept", "multipart/form-data");
    let params = new HttpParams();
    const formData: FormData = new FormData();
    const std_value = this.userForm.value.std;
    const std_div = std_value.split(" ", 2);
    const std = std_div[0];
    const div = std_div[1];
    for (let i = 0; i < 1; i++) {
      formData.append("attachment", this.image_file, this.image_file.name);
    }
    formData.append("title", this.userForm.value.title);
    formData.append("std", std);
    formData.append("div", div);
    formData.append("subject", this.userForm.value.subject);
    formData.append("description", this.userForm.value.description);

    formData.append("submission", this.userForm.value.submission);
    this.http
      .post(`${environment.API_ENDPOINT}/homework`, formData, {
        params,
        headers
      })
      .subscribe(
        res => {
          if (res) {
            this.toasterService.Success("Successfully Submitted!");
            $("#m_modal_5").modal("hide");
          } else {
            this.toasterService.Error("Not Submitted!");
          }
        },
        error => {
          this.toasterService.Error("Not Submitted!");
          console.error("Not Submitted!");
        }
      );
    this.getHomework();
  }
  getHomeworkDetails(homework) {
    this.homeworkDetails = homework;
    $("#m_modal_5").modal("show");
  }
}
