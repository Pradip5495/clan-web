/*
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-schools',
  template: `
    <p>
      schools works!
    </p>
  `,
  styles: []
})
export class SchoolsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  getSchools()
}
*/

import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { ConversationIdService } from '../../../_services/conversationId.service';
import { IdentityService } from '../../../_services/identity.service';
import { ConversationService } from '../../../_services/conversation.service';
import { Router } from '@angular/router';
import { TripService } from '../../../_services/trip.service';
import { CheckSchoolService } from "../../../_services/checkSchool.service";
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-schools',
  templateUrl: './schools.component.html',
  encapsulation: ViewEncapsulation.None,
})

export class SchoolsComponent implements OnInit {

  addSchoolForm: FormGroup;

  public id: any;
  public conversation: any;

  public myClass: any;
  public schools_list: any;
  public selectedIndex;
  public i;
  public colors;
  public new_school: any;
  public selected_trip_info: any;
  public str: any;
  public type: any = ['Select School Type', 'private', 'government'];
  public medium: any = ['select Medium', 'English', 'Hindi', 'Other'];
  object;
  object1;
  private fullview: String = 'col-xl-12';
  private halfview: String = 'col-xl-6 col-lg-12';
  private applyfullview: Boolean = true;
  private applyhalfview: Boolean = false;
  constructor(private _tripService: TripService,
    private _identityService: IdentityService,
    private formBuilder: FormBuilder,
    private _checkSchoolsService: CheckSchoolService,
    private _router: Router) {
    this.object = this.type[0];
    console.log('object', this.object);

    this.object1 = this.medium[0];
    console.log('object', this.object1);
  }

  ngOnInit(): void {
    this.addSchoolForm = this.formBuilder.group({
      name: [],
      type: [],
      board: [],
      medium: [],
      phoneNumber: [],
      email: [],
      state: [],
      district: [],
      city: [],
      area: [],
      zip: [],
      directorPhoneNumber: [],
      adminsPhoneNumber: [],
      addressLn1: [],
      startTime: [],
      endTime: [],
      country: [],
    });
    this.getSchools();
  }
  addCssClass(isapplyhalfview: boolean, isapplyfullview: boolean) {

    let classes = {
      'col-xl-6 col-lg-12': isapplyhalfview,
      'col-xl-12': isapplyfullview
    }
    return classes;
  }
  getSchools() {
    this._checkSchoolsService.getSchools().subscribe(schools_list => {
      this.schools_list = schools_list.payload;
      console.log(' schools list: ', this.schools_list);
    },
      error => {
        console.error('Error while getting bus track list. stack: ', error);
      });
  }
  onSubmit() {
    console.log(this.addSchoolForm.value);
    const startTime = this.addSchoolForm.value.startTime + 'AM';
    const endTime = this.addSchoolForm.value.endTime + 'PM';

    this._checkSchoolsService.addSchool(this.addSchoolForm.value.name, this.addSchoolForm.value.type, this.addSchoolForm.value.board,
      this.addSchoolForm.value.medium, this.addSchoolForm.value.phoneNumber, this.addSchoolForm.value.email, this.addSchoolForm.value.state,
      this.addSchoolForm.value.district, this.addSchoolForm.value.city, this.addSchoolForm.value.area, this.addSchoolForm.value.zip,
      this.addSchoolForm.value.directorPhoneNumber, this.addSchoolForm.value.adminsPhoneNumber, this.addSchoolForm.value.addressLn1,
      startTime, endTime, this.addSchoolForm.value.country, ).subscribe(new_school =>
        this.new_school = new_school.payload);
    if (this.addSchoolForm.valid) {
      // this.getSchools();
    }
    this.addSchoolForm.reset();

  }
}
