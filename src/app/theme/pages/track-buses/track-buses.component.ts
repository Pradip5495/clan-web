import { Component, OnInit, Input, ViewEncapsulation } from "@angular/core";
import { ConversationIdService } from "../../../_services/conversationId.service";
import { IdentityService } from "../../../_services/identity.service";
import { ConversationService } from "../../../_services/conversation.service";
import { Router } from "@angular/router";
import { TripService } from "../../../_services/trip.service";
import { TrackBusesService } from "../../../_services/track-buses.service";
import { ScriptLoaderService } from "../../../_services/script-loader.service";

import { StudentService } from "../../../_services/student.service";
import { ApprovalService } from "../../../_services/approval.service";

import { ClassService } from "../../../_services/class.service";
import { AssignService } from "../../../_services/assign.service";

import { PassengerService } from "../../../_services/passenger.service";
import { TeacherService } from "../../../_services/teacher.service";
import { FormBuilder } from "@angular/forms";

declare let $: any;
@Component({
  selector: "app-track-buses",
  templateUrl: "./track-buses.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./track-buses.component.css"]
})
export class TrackBusesComponent implements OnInit {
  get class_data(): any {
    return this._class_data;
  }

  set class_data(value: any) {
    this._class_data = value;
  }

  get class_value(): any {
    return this._class_value;
  }

  set class_value(value: any) {
    this._class_value = value;
  }

  get isClassTeacher(): boolean {
    return this._isClassTeacher;
  }

  set isClassTeacher(value: boolean) {
    this._isClassTeacher = value;
  }

  get students_list(): any {
    return this._students_list;
  }

  set students_list(value: any) {
    this._students_list = value;
  }

  public passenger_data: any;
  modal: boolean;

  public id: any;
  public conversation: any;
  public trackBus: any = true;
  public myClass: any;
  public track_bus: any;
  public selectedIndex;
  public i;
  public colors;
  public trip_id: any;
  public selected_trip_info: any;
  public str: any;
  public dir: any;
  public trackData: any;
  public waypoints: any;
  public passengerList: any = false;

  private _students_list: any;
  private _isClassTeacher = false;
  private route_code: any;
  private _class_value: any = null;

  public _class_data: any;
  public passenger: any;

  public passengerList_data: any;

  public passenger_code: any;
  public delete_data: any;
  public active_teachers_list: any;
  public uid;

  @Input()
  tuid: string = this.trip_id;
  code: string = this.route_code;
  studentFormArray: Array<any> = [];
  constructor(
    private _script: ScriptLoaderService,
    private _studentService: StudentService,
    private _approvalService: ApprovalService,
    private _identityService: IdentityService,
    private _tripService: TripService,
    private _teacherService: TeacherService,
    private _assignService: AssignService,
    private formBuilder: FormBuilder,
    private _classService: ClassService,
    private router: Router,
    private _passengerService: PassengerService,

    private _router: Router,

    private _trackBus: TrackBusesService
  ) {
    this.colors = ["#F68A22", "#F47F1F", "#009587", "#EF5350"];
    /* this.myClass = _identityService.class;
     console.log('my class: ' + this.myClass);*/
  }

  ngOnInit(): void {
    this.getTrackBus();
    this.getTripId(this.str);
  }

  getTrackBus() {
    this._tripService.getTrip().subscribe(
      track_bus => {
        if( track_bus != null){
          this.track_bus = track_bus.payload;
          this.colors = ["#F68A22", "#EF5350", "#009587", "#F47F1F"];
        }        
      },
      error => {
        console.error("Error while getting bus track list. stack: ", error);
      }
    );
  }
  getColor() {
    // this.colors = ['#F68A22', '#F47F1F', '#009587', '#EF5350'];

    for (let i = 0; i <= 4; i++) {
    }
  }
  /* trackByFn(index, track_bus) {
     return index; // or material.id
   }*/
  getTripId(str) {
    this._tripService.getTripId().subscribe(
      trip_id => {
        if(trip_id != null) {
          this.trip_id = trip_id.payload;
          for (const trip of this.trip_id) {
            this.selected_trip_info = trip;
            if (this.selected_trip_info.tuid === str) {
              this._tripService.saveData(this.selected_trip_info.tuid);
              this.passengerList = false;
              this.trackBus = true;
              this.duplicate();
              //this._router.navigate(["bus-passengers"]);
            }
          }
        }
      },
      error => {
        console.error("Error while getting passengers data. stack: ", error);
      }
    );
  }

  lat: Number = 18.6008;
  lng: Number = 73.7398;
  zoom: Number = 15;

  public markerOptions = {
    origin: {
      // icon: 'https://www.shareicon.net/data/32x32/2016/04/28/756617_face_512x512.png',
      // draggable: true,
    },
    destination: {
      icon: "././assets/images/bus_img.png",
      opacity: 0.8
    }
  };

  public renderOptions = {
    suppressMarkers: true
  };

  getBusLocation(tripUid) {
    this._trackBus.getBusLocation(tripUid).subscribe(trackData => {
      if (trackData.status == 200) {
        this.trackData = trackData.payload;
        if (this.trackData.length > 0) {
          let origin = {
            lat: this.trackData[0].lat,
            lng: this.trackData[0].lng
          };
          let lastKey = this.trackData.length - 1;
          let destination = {
            lat: this.trackData[lastKey].lat,
            lng: this.trackData[lastKey].lng
          };

          this.waypoints = [];

          this.trackData.forEach(waypoints => {
            let loc = { location: { lat: waypoints.lat, lng: waypoints.lat } };
            this.waypoints.push(loc);
          });

          this.dir = {
            origin: origin, //{ lat: 18.6414, lng: 73.7398 },
            destination: destination, //{ lat: 18.5236, lng: 73.8478 },
            waypoints: this.waypoints,
            //optimizeWaypoints: true,
            renderOptions: {
              suppressMarkers: true
            },
            visible: true
          };
          $("#m_modal_4").modal("show");
        }
      }
    });
  }

  /*------------------------------------------------ passenger data starts-----------------------------------*/

  ngAfterViewInit() {
    this._script.loadScripts("app-passenger", [
      "assets/demo/default/custom/components/calendar/basic.js"
    ]);
  }

  passData(ruid) {
    this._assignService.saveData(this.route_code);
  }

  getBus(ruid) {
    this._passengerService.getPassenger(ruid).subscribe(passenger_data => {
      this.passenger_data = passenger_data.payload;
    });
  }

  onDelete(uid) {
    this._passengerService
      .onDelete(this.route_code, uid)
      .subscribe(delete_data => {
        this.delete_data = delete_data.payload;
        this.getTripPassenger(this.route_code);
        //this.getBus(this.route_code);
      });
  }

  getClass() {
    this._classService.getClass().subscribe(
      class_data => {
        this.class_data = class_data.payload;
      },
      error => {
        console.error("Error while getting class data. stack: ", error);
      }
    );
  }

  getStudent(event: any) {
    this.class_value = event;

    this._studentService.getStudentByClass(event).subscribe(
      students_list => {
        this.students_list = students_list.payload;
      },
      error => {
        console.error("Error while getting student list. stack: ", error);
      }
    );
  }
  onChange(uid: string, isChecked: boolean) {
    if (isChecked) {
      this.studentFormArray.push(uid);
    } else {
      let index = this.studentFormArray.indexOf(uid);
      // this.studentFormArray.splice(index, 1);
    }
  }
  duplicate() {
    let ruid = this.route_code;
    this._passengerService
      .getPassengerList(this.route_code, this.studentFormArray)
      .subscribe(passenger_data => {
        this.passenger_data = passenger_data.payload;
      });
    // $("#m_modal_1").modal("hide");

    //$("#m_modal_5").modal("hide");
  }
  getActiveTeacher() {
    this._teacherService.getTeacher().subscribe(
      teacherlist => {
        this.active_teachers_list = teacherlist.payload;
      },
      error => {
        console.error("Error while getting teachers list. stack: ", error);
      }
    );
    //$("#m_modal_1").modal("hide");
  }

  getTripPassenger(tuid) {
    this._tripService.getTripPassenger(tuid).subscribe(passengerList_data => {
      this.passengerList_data = passengerList_data.payload;
      this.passengerList = true;
      this.trackBus = false;
    });
  }

  getPassengerList() {
    this.passengerList = false;
    this.trackBus = true;
  }

  /*------------------------------------------------ passenger data ends -----------------------------------*/
}

interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
