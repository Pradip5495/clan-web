
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../default/default.component';
import { TrackBusesComponent } from './track-buses.component';
// import {TripService} from '../../../_services/trip.service';
// Google Map Implement module
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction'; // agm-direction


const routes: Routes = [
  {
    'path': '',
    'component': DefaultComponent,
    'children': [
      {
        'path': '',
        'component': TrackBusesComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule,
    AgmCoreModule.forRoot({ //@agm/core
      apiKey: 'AIzaSyBFYhE4lgP8bEq6-xuPmH9bWE_MPct1ROo' //AIzaSyBbUr9k3JchtdRRTnyrl2opuLJkBd7XT_w
    }),
    AgmDirectionModule, //agm-direction
  ], exports: [
    RouterModule
  ], declarations: [
    TrackBusesComponent
  ],
  providers: [GoogleMapsAPIWrapper]
})
export class TrackBusesModule {


}
