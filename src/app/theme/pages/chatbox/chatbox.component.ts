import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MessageService } from '../../../_services/message.service';
import { ConversationIdService } from '../../../_services/conversationId.service';
import { IdentityService } from '../../../_services/identity.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chatbox',
  templateUrl: './chatbox.component.html',
  encapsulation: ViewEncapsulation.None,
})

export class ChatboxComponent implements OnInit {

  public id: any;
  public conversation: any;
  public uid: any;
  public sendmsg: any;

  // TODO: losing id of conversation on refreshing this page

  constructor(private _messageService: MessageService,
    private _conversationIdService: ConversationIdService,
    private _identityService: IdentityService,
    private _router: Router) {
    console.log('chatbox is hit');
    this.uid = _identityService.uid;
    console.log('uid: ' + this.uid);
  }

  ngOnInit(): void {
    this._conversationIdService.currentId.subscribe(data => this.id = data);

    if (this.id) {
      this.getMessage(this.id);
      console.log('current id in discuss page: ' + this.id);
    }
  }

  getMessage(uid) {
    this._messageService.getMessage(uid).subscribe(data => {
      this.conversation = data.payload;
      console.log('conversation list: ', this.conversation);
    },
      error => {
        console.error('Error while getting conversation list. stack: ', error);
      });
  }

  postMessage(msg) {

    if (msg) {
      this._messageService.postMessage(this.id, msg).subscribe(data => {

        console.log('message is send');
        this.getMessage(this.id);

      },
        error => {
          console.error('Error while getting conversation list. stack: ', error);
        });

    }

    this.sendmsg = '';
    this.getMessage(this.id);

  }
}
