
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PassengerComponent } from './passenger.component';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../default/default.component';
import { StudentsComponent } from '../students/students.component';

const routes: Routes = [
  {
    'path': '',
    'component': DefaultComponent,

    'children': [
      {
        'path': '',
        'component': PassengerComponent,
      },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule,
  ],
  exports: [
    RouterModule
  ], declarations: [
    PassengerComponent,
  ]
})
export class PassengerModule {


}
