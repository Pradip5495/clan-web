import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  AfterViewInit
} from "@angular/core";
import { ScriptLoaderService } from "../../../_services/script-loader.service";
import { StudentService } from "../../../_services/student.service";
import { ApprovalService } from "../../../_services/approval.service";
import { IdentityService } from "../../../_services/identity.service";
import { ClassService } from "../../../_services/class.service";
import { AssignService } from "../../../_services/assign.service";
import { Router } from "@angular/router";
import { PassengerService } from "../../../_services/passenger.service";
import { TripService } from "../../../_services/trip.service";
import {
  FormBuilder,
  FormArray,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import { TeacherService } from "../../../_services/teacher.service";
declare var $: any;

@Component({
  selector: "app-passenger",
  templateUrl: "./passenger.component.html",
  styleUrls: ["./passenger.component.css"],
  encapsulation: ViewEncapsulation.None
})
export class PassengerComponent implements OnInit, AfterViewInit {
  get class_data(): any {
    return this._class_data;
  }

  set class_data(value: any) {
    this._class_data = value;
  }

  get class_value(): any {
    return this._class_value;
  }

  set class_value(value: any) {
    this._class_value = value;
  }

  get isClassTeacher(): boolean {
    return this._isClassTeacher;
  }

  set isClassTeacher(value: boolean) {
    this._isClassTeacher = value;
  }

  get students_list(): any {
    return this._students_list;
  }

  set students_list(value: any) {
    this._students_list = value;
  }

  private passenger_data: any;
  modal: boolean;

  private _students_list: any;
  private _isClassTeacher = false;
  private route_code: any;
  private _class_value: any = null;
  public str: any;
  public _class_data: any;
  public passenger: any;
  public tuid: any;
  public passenger_code: any;
  public delete_data: any;
  public active_teachers_list: any;

  public uid;
  @Input()
  code: string = this.route_code;
  studentFormArray: Array<any> = [];
  constructor(
    private _script: ScriptLoaderService,
    private _studentService: StudentService,
    private _approvalService: ApprovalService,
    private _identityService: IdentityService,
    private _tripService: TripService,
    private _teacherService: TeacherService,
    private _assignService: AssignService,
    private formBuilder: FormBuilder,
    private _classService: ClassService,
    private router: Router,
    private _passengerService: PassengerService
  ) {
    this.userForm = this.formBuilder.group({
      uid: this.formBuilder.array([])
    });

    this.route_code = "";
    this.router = router;
    this._assignService = _assignService;
    console.log("cone called" + this.route_code);
    this.route_code = _assignService.getData(this.route_code);
    console.log(typeof this.route_code);
    console.log(this.route_code);
  }

  userForm: FormGroup;

  ngOnInit() {
    this.getClass();
    console.log(this.route_code);
    this.passData(this.route_code);
    this.getBus(this.route_code);
    this.getActiveTeacher();
    // this.getStudents(this.route_code);
    this._isClassTeacher = this._identityService.isClassTeacher;

    if (this.isClassTeacher) {
      this.class_value = this._identityService.class;
    }
  }

  ngAfterViewInit() {
    this._script.loadScripts("app-passenger", [
      "assets/demo/default/custom/components/calendar/basic.js"
    ]);
  }

  passData(ruid) {
    this._assignService.saveData(this.route_code);
    console.log("wlvdhbwj", this.route_code);
  }

  getBus(ruid) {
    this._passengerService
      .getPassenger(this.route_code)
      .subscribe(passenger_data => {
        this.passenger_data = passenger_data.payload;
        console.log("passenger data:", this.passenger_data);
      });
  }

  onDelete(uid) {
    console.log("dataaa", uid);
    console.log("roue data ", this.route_code);
    this._passengerService
      .onDelete(this.route_code, uid)
      .subscribe(delete_data => {
        this.delete_data = delete_data.payload;
        this.getBus(this.route_code);
      });
  }

  getClass() {
    this._classService.getClass().subscribe(
      class_data => {
        this.class_data = class_data.payload;
        console.log("class data:", this.class_data);
      },
      error => {
        console.error("Error while getting class data. stack: ", error);
      }
    );
  }

  getStudent(event: any) {
    this.class_value = event;

    this._studentService.getStudentByClass(event).subscribe(
      students_list => {
        this.students_list = students_list.payload;
        console.log("student-list:", this.students_list);
      },
      error => {
        console.error("Error while getting student list. stack: ", error);
      }
    );
  }
  onChange(uid: string, isChecked: boolean) {
    if (isChecked) {
      this.studentFormArray.push(uid);
    } else {
      let index = this.studentFormArray.indexOf(uid);
      // this.studentFormArray.splice(index, 1);
    }
  }
  duplicate() {
    let ruid = this.route_code;
    console.log(this.studentFormArray);
    this._passengerService
      .getPassengerList(this.route_code, this.studentFormArray)
      .subscribe(passenger_data => {
        this.passenger_data = passenger_data.payload;
        console.log("passenger data:", this.passenger_data);
      });
    $("#m_modal_1").modal("hide");

    $("#m_modal_5").modal("hide");
  }
  getActiveTeacher() {
    this._teacherService.getTeacher().subscribe(
      teacherlist => {
        this.active_teachers_list = teacherlist.payload;
        console.log("teacher-list:", this.active_teachers_list);
      },
      error => {
        console.error("Error while getting teachers list. stack: ", error);
      }
    );
    $("#m_modal_1").modal("hide");
  }
}
