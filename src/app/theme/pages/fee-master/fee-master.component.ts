import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewEncapsulation,
  Pipe,
  PipeTransform
} from "@angular/core";
import { ScriptLoaderService } from "../../../_services/script-loader.service";
import {
  ActivatedRoute,
  Router,
  ParamMap,
  NavigationExtras
} from "@angular/router";
import { ChildService } from "../../../_services/child.service";

import { StudentService } from "../../../_services/student.service";
import { FeeTemplateService } from "../../../_services/fee-template.service";
import { FeesTypeService } from "../../../_services/fees-type.service";
import { FeeMasterService } from "../../../_services/fee-master.service";
import { ClassService } from "../../../_services/class.service";
import { ToasterService } from "../../../_services/toaster.service";
import { IdentityService } from "../../../_services/identity.service";
import { User } from "../../../_models";
import { isEmpty } from "rxjs/operator/isEmpty";
import { PaymentService } from "../../../_services/payment.service";
import { environment } from "../../../../environments/environment";
import {
  FormGroup,
  FormBuilder,
  FormArray,
  FormControl,
  ValidatorFn,
  Validators
} from "@angular/forms";

declare var $: any;
declare const mApp, moment, Chartist, todayDate;
declare let Razorpay: any;
@Component({
  selector: "app-fee-master",
  templateUrl: "./fee-master.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./fee-master.component.css"]
})
export class FeeMasterComponent implements OnInit {
  public class_list: any;
  public colors: any;
  public contentView: any;
  public std: any;
  public uid: any;
  public studentDetails: any;
  public studentList: any;
  public studentListView: any;
  public assignFeeView: any;
  public assignFeeList: any;
  public feesDetails: any;
  public feeSubDetails: any;
  public assignFeeTemplateView: any;
  public assignFeeTemplate: any;
  public totalAmount: any;
  public type_list: any;
  public school_code: any;
  public profile: User;
  public role: any;
  public feeTemplateView: any = false;
  public templateList: any;
  public tempContentView: any;
  public feesTempDetails: any;
  public tempFeesData: any = false;
  public feeTemplateData: any;
  public feeContentView: any = false;
  public studentFeeView: any = false;
  public feesCodes: any;
  public payAmount: number = 0;
  public paymentDetails: any;
  public paymentSchoolData: any;
  public currentDate: any;
  public totalCollection: any;
  public dueAmount: any;
  public selectedChild: any;
  public schoolCode: any;
  public _child_list: any;
  public remainingAmt: any;
  public totalAmt: any;
  public payAmt: any;
  public paidAmt: any;
  public order: any;
  public paymentLogs: any;

  products: any[] = [];
  objectKeys = Object.keys;
  totalPrice = 0;
  quantity = 0;
  payableAmount = 0;
  WindowRef: any;
  processingPayment: boolean;
  paymentResponse: any = {};

  feeMaster: FormGroup;
  subDetails: FormArray;

  feeTemplate: FormGroup;
  subDetail: FormArray;
  templateForm: FormGroup;

  updateFeeAssign: FormGroup;
  updateSubDetails: FormArray;

  cashPayment: FormGroup;
  onlinePayment: FormGroup;
  paymentTransaction: FormGroup;
  //public class: any;

  constructor(
    private _classService: ClassService,
    private _studentService: StudentService,
    private _script: ScriptLoaderService,
    private _identityService: IdentityService,
    private route: ActivatedRoute,
    private _child: ChildService,
    private router: Router,
    private _formBuilder: FormBuilder,
    private _feeTemplateService: FeeTemplateService,
    private _feeMasterService: FeeMasterService,
    private _feesTypeService: FeesTypeService,
    private paymentService: PaymentService,
    private toasterService: ToasterService,
    private changeRef: ChangeDetectorRef
  ) {
    this.profile = this._identityService.identity;
    this.role = this._identityService.role;
    this.totalAmount = 0;
    const todayDate1 = moment().startOf("day");
    this.currentDate = moment(todayDate1).format("YYYY-MM-DD");
  }

  ngOnInit() {
    this.feeMaster = this._formBuilder.group({
      name: ["", Validators.required],
      class: [],
      dueDate: [],
      totalAmount: [],
      subDetails: this._formBuilder.array([this.addSubdetails()])
    });

    this.updateFeeAssign = this._formBuilder.group({
      uidFeesMaster: [],
      name: [],
      class: [],
      dueDate: [],
      totalAmount: [],
      subDetails: this._formBuilder.array([this.addMasterSubdetails()])
    });

    this.feeTemplate = this._formBuilder.group({
      name: [],
      class: [],
      dueDate: [],
      totalAmount: [],
      subDetails: this._formBuilder.array([this.addSubdetail()])
    });

    this.templateForm = this._formBuilder.group({
      uidFeeTemplateMaster: [],
      name: [],
      class: [],
      dueDate: [],
      totalAmount: [],
      subDetails: this._formBuilder.array([this.addTemplateSub()])
    });

    this.cashPayment = this._formBuilder.group({
      studentCode: [],
      amount: [],
      feesCodes: new FormArray([]),
      cashReceiptReference: []
    });

    this.onlinePayment = this._formBuilder.group({
      studentCode: [],
      amount: [],
      feesCodes: new FormArray([])
    });
    console.log('onlinepayment',this.onlinePayment);

    this.paymentTransaction = this._formBuilder.group({
      reciept: [],
      schoolId: [],
      studentCode: [],
      feesCode: [],
      orderId: [],
      paymentId: [],
      isOnline: [],
      isSuccess: [],
      errorCode: [],
      errorMessage: [],
      amount: [],
      razorPaySignature: []
    });
    const currentUser = JSON.parse(localStorage.getItem("currentUser"));
    this.school_code = currentUser.schoolCode;

    if (this.route.params["value"].uid) {
      this.uid = this.route.params["value"].uid;
      this.std = this.route.params["value"].std;
      this.assignFeeView = true;
      this.studentListView = false;
      this.contentView = false;
      this.assignFeeTemplateView = false;
      //this.getFeeAssignedDetails();
    } else if (
      this.route.params["value"].std ||
      this.route.params["value"].uid
    ) {
      // this.getClassStudent();
    } else if (this.route.params["value"].student) {
      this.uid = this.route.params["value"].student;
      this.std = this.route.params["value"].class;
      //this.getAssignData();
    } else {
      this.getClasses();
    }

    if (this.role == "parent") {
      if (localStorage.getItem("selectedChild")) {
        this.selectedChild = JSON.parse(localStorage.getItem("selectedChild"));
        this.uid = this.selectedChild.uid;
        this.schoolCode = this.selectedChild.schoolCode;
        this.studentDetails = this.selectedChild;
        let std = this.selectedChild.std;
        let div = this.selectedChild.div;
        let studClass = std + "" + div;
        this.getStudentFeeDetails(this.uid);
      } else {
        this.getChild();
      }
    } else {
      this.getFeesTypeList();
      this.schoolPayment();
    }

    this.WindowRef = this.paymentService.WindowRef;
  }

  ngAfterViewInit() {
    this._script.loadScripts("app-fee-master", [
      "https://checkout.razorpay.com/v1/checkout.js"
    ]);
  }

  /** Get All Class List By School*/
  getClasses() {
    const MethodName = "getClassBySchool";
    this._classService.getClasses().subscribe(
      class_list => {
        this.class_list = class_list.payload;
        this.colors = ["#F68A22", "#EF5350", "#009587", "#F47F1F"];
        this.contentView = true;
        this.tempContentView = true;
        this.feeContentView = true;
        this.studentListView = false;
      },
      error => {
        console.error(
          MethodName,
          "Error while getting class data. stack: ",
          error
        );
        this.toasterService.Error("Error while getting class data. stack");
      }
    );
  }

  get child_list(): any {
    return this._child_list;
  }

  set child_list(value: any) {
    this._child_list = value;
  }

  /** get Child list of Parent */
  getChild() {
    let value: any;
    this._child.getChild().subscribe(data => {
      this._child_list = data.payload;
      $("#m_modal_child_5").modal("show");
    });
  }

  /** Set Child list for View rendar */
  setChild(data) {
    localStorage.setItem("selectedChild", JSON.stringify(data));
    this.uid = data.uid;
    $("#m_modal_child_5").modal("hide");
  }

  /** Get All Student of Fee Assign List By School*/
  getClassStudent(std) {
    this.std = std;
    this.assignFeeView = false;
    this.studentListView = true;
    this.contentView = false;
    this.assignFeeTemplateView = false;
    const MethodName = "getClassBySchool";
    this._feeMasterService.getFeeMaster(this.std).subscribe(
      studentList => {
        console.log('student list',studentList);
        this.studentList = studentList.payload;
        this.colors = ["#F68A22", "#EF5350", "#009587", "#F47F1F"];
        this.contentView = false;
        if(this.studentList){
          let studentUid = this.studentList.studentList[0].uid;
          let studentFees = this.studentList.feeSubDetails;
          this.getFeeAssignedDetails(this.studentList.studentList[0], studentUid, studentFees);
        }
      },
      error => {
        console.error(
          MethodName,
          "Error while getting class data. stack: ",
          error
        );
      }
    );
  }

  /** Get All Student of Fee Assign List By School*/
  getClassFeeStudent(std) {
    this.std = std;
    this.feeContentView = false;
    this.studentFeeView = true;
    const MethodName = "getClassBySchool";
    this._feeMasterService.getFeeMaster(this.std).subscribe(
      studentList => {
        this.studentList = studentList.payload;
        this.colors = ["#F68A22", "#EF5350", "#009587", "#F47F1F"];
      },
      error => {
        console.error(
          MethodName,
          "Error while getting class data. stack: ",
          error
        );
        this.toasterService.Error("Error while getting class data. stack");
      }
    );
  }
  /** Add Sub details Dynamically */
  addSubdetails(): FormGroup {
    return this._formBuilder.group({
      type: "",
      uidFeeType: "",
      amount: "",
      uidTemplateSubDetail: ""
    });
  }

  /** Add Sub details Dynamically */
  addMasterSubdetails(): FormGroup {
    return this._formBuilder.group({
      type: "",
      feesMasterId: "",
      uidFeeType: "",
      amount: "",
      uidFeesSubDetail: ""
    });
  }

  /** Add Sub detail Dynamically */
  addSubdetail(): FormGroup {
    return this._formBuilder.group({
      type: "",
      uidFeeType: "",
      amount: ""
    });
  }

  /** update Sub details Dynamically */
  addTemplateSub(): FormGroup {
    return this._formBuilder.group({
      type: "",
      uidFeeType: "",
      uidTemplateSubDetail: "",
      amount: ""
    });
  }

  /** get Student list with Template details */
  getStudentFeeDetails(uid) {
    const MethodName = "getFeeAssignedDetails";
    this.uid = uid;
    // this.std = studentData.std + studentData.div;
    // this.studentDetails = studentData;
    // this.feesDetails = feesDetails;
    this.assignFeeView = true;
    // this.studentListView = true;
    // this.contentView = false;
    // this.assignFeeTemplateView = false;
    this._feeMasterService.getFeeAssignedDetails(this.uid).subscribe(
      assignedFeeList => {
        if (assignedFeeList.status == 200) {
          this.assignFeeList = assignedFeeList.payload;
          this.payAmt = 0;
          this.remainingAmt = 0;
          this.totalAmt = 0;
          this.paidAmt = 0;
          this.assignFeeList.forEach(fees => {
            console.log('ffeess',fees);
            this.payAmt = this.payAmt + fees.remainingAmount;
            this.remainingAmt = this.remainingAmt + fees.remainingAmount;
            this.totalAmt = this.totalAmt + fees.totalAmount;
            if (fees.isPaid) {
              const paidAmount = fees.totalAmount - fees.remainingAmount;
              this.paidAmt = this.paidAmt + paidAmount;
            }
          });
        }
      },
      error => {
        console.error(
          MethodName,
          "Error while getting class data. stack: ",
          error
        );
        this.toasterService.Error("Error while getting class data. stack");
      }
    );
  }

  /** get Student list with Template details */
  getFeeAssignedDetails(studentData, uid, feesDetails) {
    const MethodName = "getFeeAssignedDetails";
    this.uid = uid;
    this.std = studentData.std + studentData.div;
    this.studentDetails = studentData;
    this.feesDetails = feesDetails;
    this.assignFeeView = true;
    this.studentListView = true;
    this.contentView = false;
    this.assignFeeTemplateView = false;
    this._feeMasterService.getFeeAssignedDetails(this.uid).subscribe(
      assignedFeeList => {
        this.assignFeeList = assignedFeeList.payload;
      },
      error => {
        console.error(
          MethodName,
          "Error while getting class data. stack: ",
          error
        );
        this.toasterService.Error("Error while getting class data. stack:");
      }
    );
  }

  /** Get All Fee Type List */
  getFeesTypeList() {
    this._feesTypeService.getFeeTypes().subscribe(feestypelist => {
      this.type_list = feestypelist.payload;
    });
  }

  /** View Fee Subdetails Assign in Popup */
  viewSubDetails(data) {
    this.feeSubDetails = data;
    $("#subDetailModal").modal("show");
  }
  /** Get Assign Template Data for assigning to student */
  getAssignData(std) {
    const MethodName = "getClassBySchool";
    this.std = std;
    this._feeMasterService.getFeeMaster(this.std).subscribe(
      studentList => {
        this.assignFeeTemplate = studentList.payload;
        // this.assignFeeTemplateView = true;
        // this.assignFeeView = false;
        // this.studentListView = false;
        // this.contentView = false;
        $("#assignFeeModal").modal("show");
      },
      error => {
        console.error(
          MethodName,
          "Error while getting class data. stack: ",
          error
        );
      }
    );
  }

  /** Set Assign Master Form Data */
  setAssignFeeTemplate(assignFeeTemp) {
    this.getFeesTypeList();
    let amount = 0;
    this.totalAmount = 0;
    const formArray = this.feeMaster.controls["subDetails"] as FormArray;

    // For Remove Previouse FormArray data
    while (formArray.length) {
      formArray.removeAt(formArray.length - 1);
    }

    // For New Sub Details generate formArray
    for (let line = 0; line < assignFeeTemp.subDetails.length; line++) {
      amount = +amount + +assignFeeTemp.subDetails[line].amount;
      const subdata = this._formBuilder.group({
        type: [assignFeeTemp.subDetails[line].type],
        uidFeeType: [assignFeeTemp.subDetails[line].uidFeeType],
        uidTemplateSubDetail: [
          assignFeeTemp.subDetails[line].uidTemplateSubDetail
        ],
        amount: [assignFeeTemp.subDetails[line].amount]
      });
      formArray.push(subdata);
    }
    this.totalAmount = amount;
    //this.feeMaster.dueDate = '20-02-2020';
    this.feeMaster.patchValue(assignFeeTemp);
    $("#NewFeeMaster").modal("show");
  }

  /** Assign Fee Template to Student */
  onSubmit(studentData, feeMasterData) {
    let MastereData = this.feeMaster.value;
    MastereData.schoolCode = this.school_code;
    MastereData.studentCode = this.uid;
    MastereData.remainingAmount = this.totalAmount;
    MastereData.isPaid = false;
    MastereData.isPublished = false;
    this._feeMasterService.postFeeMaster(MastereData).subscribe(feeMasters => {
      if (feeMasters.status === 201) {
        // this.getClassStudent(feeMasterData, feeMasterData.uid, );
        this.toasterService.Success("Successfully Submitted!");
        this.getFeeAssignedDetails(studentData, studentData.uid, feeMasterData);
        this.assignFeeView = true;
        this.studentListView = true;
        this.contentView = false;
        this.assignFeeTemplateView = false;
        $("#NewFeeMaster").modal("hide");
        $("#assignFeeModal").modal("hide");
      } else {
        this.toasterService.Error("Not Submitted!");
      }
    });
  }

  /** Assign Fee Sub Amount changes retreive on view*/
  changeAmount($event) {
    const subdata = this.feeMaster.value.subDetails;
    let amount = 0;
    for (let i = 0; i < subdata.length; i++) {
      amount = +amount + +subdata[i].amount;
    }
    this.totalAmount = amount;
  }

  /** Delete Assign Fee Template Sub details  */
  deleteRow(idx, subdetail) {
    const formArray = this.feeMaster.controls["subDetails"] as FormArray;
    formArray.removeAt(idx);
    const subdata = this.feeMaster.value.subDetails;
    let amount = 0;
    for (let i = 0; i < subdata.length; i++) {
      amount = +amount + +subdata[i].amount;
    }
    this.totalAmount = amount;
  }

  /** Add Assign Fee Template Sub details Row  */
  addSubDetailsButtonClick(): void {
    this.subDetails = this.feeMaster.get("subDetails") as FormArray;
    this.subDetails.push(this.addSubdetails());
  }

  selectchange($event, idx) {
    const tagId = "#type" + idx;
    const controlArray = this.feeMaster.get("subDetails") as FormArray;
    controlArray.controls[idx]
      .get("type")
      .setValue($event.target.options[$event.target.selectedIndex].text);
  }

  /** Set Value for update fee assign popup page */
  setAssignFeeMaster(assignFeeMaster) {
    this.getFeesTypeList();
    let amount = 0;
    this.totalAmount = 0;
    const updateformArray = this.updateFeeAssign.controls[
      "subDetails"
    ] as FormArray;

    // For Remove Previouse FormArray data
    while (updateformArray.length) {
      updateformArray.removeAt(updateformArray.length - 1);
    }

    // For New Sub Details generate formArray
    for (let line = 0; line < assignFeeMaster.subDetails.length; line++) {
      amount = +amount + +assignFeeMaster.subDetails[line].amount;
      const subdata = this._formBuilder.group({
        type: [assignFeeMaster.subDetails[line].type],
        uidFeeType: [assignFeeMaster.subDetails[line].uidFeeType],
        uidFeesSubDetail: [
          assignFeeMaster.subDetails[line].uidTemplateSubDetail
        ],
        amount: [assignFeeMaster.subDetails[line].amount]
      });
      updateformArray.push(subdata);
    }
    this.totalAmount = amount;
    this.updateFeeAssign.patchValue(assignFeeMaster);
    $("#updateFeeMaster").modal("show");
  }

  /** Update Assign Fee Data */
  onUpdate(studentData, feeMasterData) {
    let updateMasterData = this.updateFeeAssign.value;
    updateMasterData.schoolCode = this.school_code;
    updateMasterData.studentCode = this.uid;
    updateMasterData.totalAmount = this.totalAmount;
    updateMasterData.remainingAmount = this.totalAmount;
    updateMasterData.isPaid = false;
    updateMasterData.isPublished = false;
    this._feeMasterService
      .putFeeMaster(updateMasterData)
      .subscribe(feeMasters => {
        if (feeMasters.status === 201) {
          this.toasterService.Success("Successfully Updated!");
          this.getFeeAssignedDetails(
            studentData,
            studentData.uid,
            feeMasterData
          );
          this.assignFeeView = true;
          this.studentListView = true;
          this.contentView = false;
          this.assignFeeTemplateView = false;
          $("#updateFeeMaster").modal("hide");
        } else {
          this.toasterService.Error("Not Updated!");
        }
      });
  }

  /** Delete Assign Template Fee Sub details row */
  deleteAssignFee(uidFeesMaster, studentData, feeMasterData) {
    this._feeMasterService
      .deleteFeeMaster(uidFeesMaster)
      .subscribe(feeMaster => {
        if (feeMaster.status === 200) {
          this.getFeeAssignedDetails(
            studentData,
            studentData.uid,
            feeMasterData
          );
          this.assignFeeView = true;
          this.studentListView = true;
          this.contentView = false;
          this.assignFeeTemplateView = false;
        }
      });
  }

  /** Assign Fee Sub Amount changes retreive on view*/
  changeTotalAmount($event) {
    const subdata = this.updateFeeAssign.value.subDetails;
    let amount = 0;
    for (let i = 0; i < subdata.length; i++) {
      amount = +amount + +subdata[i].amount;
    }
    this.totalAmount = amount;
  }

  /** Delete Assign Fee Sub details row */
  deleteSubDetailsRow(idx, subdetail, feesSubDetail) {
    const formArray = this.updateFeeAssign.controls["subDetails"] as FormArray;
    formArray.removeAt(idx);
    const subdata = this.updateFeeAssign.value.subDetails;
    let amount = 0;
    if (feesSubDetail.value.uidFeesSubDetail) {
      const uidFeesSubDetail = feesSubDetail.value.uidFeesSubDetail;
      this._feeMasterService
        .deleteFeeSubDetails(uidFeesSubDetail)
        .subscribe(feeSubDetails => {
          if (feeSubDetails.status === 200) {
          }
        });
    }
    for (let i = 0; i < subdata.length; i++) {
      amount = +amount + +subdata[i].amount;
    }
    this.totalAmount = amount;
  }

  /** Add new row Sub Details  */
  addSubDetailsOnClick(): void {
    this.subDetails = this.updateFeeAssign.get("subDetails") as FormArray;
    this.subDetails.push(this.addSubdetails());
  }

  /** Set value for selected Feild type  */
  selectFieldChange($event, idx) {
    const tagId = "#type" + idx;
    const controlArray = this.updateFeeAssign.get("subDetails") as FormArray;
    controlArray.controls[idx]
      .get("type")
      .setValue($event.target.options[$event.target.selectedIndex].text);
  }

  /** Set Value for update fee assign popup page */
  setPublishDetails(assignFeeMaster, studentData, feeMasterData) {
    this.getFeesTypeList();
    let amount = 0;
    this.totalAmount = 0;
    const updateformArray = this.updateFeeAssign.controls[
      "subDetails"
    ] as FormArray;
    //  For Remove Previouse FormArray data
    while (updateformArray.length) {
      updateformArray.removeAt(updateformArray.length - 1);
    }
    //  For New Sub Details generate formArray
    for (let line = 0; line < assignFeeMaster.subDetails.length; line++) {
      amount = +amount + +assignFeeMaster.subDetails[line].amount;
      const subdata = this._formBuilder.group({
        type: [assignFeeMaster.subDetails[line].type],
        uidFeeType: [assignFeeMaster.subDetails[line].uidFeeType],
        uidFeesSubDetail: [
          assignFeeMaster.subDetails[line].uidTemplateSubDetail
        ],
        amount: [assignFeeMaster.subDetails[line].amount]
      });
      updateformArray.push(subdata);
    }
    this.totalAmount = amount;
    this.updateFeeAssign.patchValue(assignFeeMaster);
    this.publishAssignFee(studentData, feeMasterData);
  }

  /** Update Assign Fee Data */
  publishAssignFee(studentData, feeMasterData) {
    let updateMasterData = this.updateFeeAssign.value;
    updateMasterData.schoolCode = this.school_code;
    updateMasterData.studentCode = this.uid;
    updateMasterData.totalAmount = this.totalAmount;
    updateMasterData.remainingAmount = this.totalAmount;
    updateMasterData.isPaid = false;
    updateMasterData.isPublished = true;
    this._feeMasterService
      .putFeeMaster(updateMasterData)
      .subscribe(feeMasters => {
        if (feeMasters.status === 201) {
          this.toasterService.Success("Successfully Assigned Fee!");
          this.getClassStudent(studentData.std);
          //this.getFeeAssignedDetails(studentData, studentData.uid, feeMasterData);

          this.assignFeeView = false;
          this.studentListView = true;
          this.contentView = false;
          this.assignFeeTemplateView = false;
        } else {
          this.toasterService.Error("Fees Not Assigned!");
        }
      });
  }

  /**
   * Fee Template
   */
  /** Get All Fee Type List */
  addTempSubDetails(): void {
    this.subDetail = this.feeTemplate.get("subDetails") as FormArray;
    this.subDetail.push(this.addSubdetail());
  }

  /** Submit New Fee Template details */
  onSubmitTemplate() {
    let feeTemplateData = this.feeTemplate.value;
    feeTemplateData.totalAmount = this.totalAmount;
    feeTemplateData.dueDate =
      moment(this.feeTemplate.value.dueDate).format("YYYY-MM-DD") + "T00:00:00";
    this._feeTemplateService.postFeeTemplate(this.feeTemplate.value).subscribe(
      feeTemplates => {
        if (feeTemplates.status === 201) {
          this.toasterService.Success("Successfully Submitted!");
          this.feeTemplate.reset();
          $("#NewFeeTemplate").modal("hide");
        } else {
          this.toasterService.Error("Template Not Created!");
        }
      },
      error => {
        this.toasterService.Error("Template Not Created!");
        console.error("Template Not created!");
      }
    );
  }
  /** Get Fee Template By Class */
  getFeeTemplate(std) {
    this.std = std;
    const MethodName = "getFeeTemplate";
    this._feeTemplateService
      .getFeeTemplate(this.std)
      .subscribe(tamplateList => {
        this.feeTemplateView = true;
        this.tempContentView = false;
        if (tamplateList.status === 200) {
          this.templateList = tamplateList.payload;
          if(this.templateList.length > 0) {
            this.getFeesTempDetails(this.templateList[0]);
          }
        }
      });
  }

  /** Total Amount calculate on change event */
  changeTempTotal($event, feeTemplate) {
    const subdata = this.feeTemplate.value.subDetails;
    let amount = 0;
    for (let i = 0; i < subdata.length; i++) {
      amount = +amount + +subdata[i].amount;
    }
    this.totalAmount = amount;
  }

  changeTempTotalAmt($event, feeTemplate) {
    const subdata = this.templateForm.value.subDetails;
    let amount = 0;
    for (let i = 0; i < subdata.length; i++) {
      amount = +amount + +subdata[i].amount;
    }
    this.totalAmount = amount;
  }

  /** Delete Fee Sub Details Row */
  deleteRowTemp(index: number, subDetailsAmt) {
    this.totalAmount = +this.totalAmount - +subDetailsAmt.amount;
    this.subDetail.removeAt(index);
  }

  /***
   *
   */
  getFeesTempDetails(temp) {
    this.feesTempDetails = temp;
    this.tempFeesData = true;
    this.feeTemplateView = true;
    this.tempContentView = false;
  }

  /** Delete Fee Template by uidFeeTemplateMaster */
  deleteFeeTemplate(uidFeeTemplateMaster, std) {
    this.std = std;
    this._feeTemplateService
      .deleteFeeTemplate(uidFeeTemplateMaster)
      .subscribe(feeTData => {
        this.getFeeTemplate(this.std);
      });
  }

  /** Get All Class List By School*/
  getClassTemp() {
    const MethodName = "getClassBySchool";
    this._classService.getClasses().subscribe(
      class_list => {
        this.class_list = class_list.payload;
        this.colors = ["#F68A22", "#EF5350", "#009587", "#F47F1F"];
        this.tempContentView = true;
        this.feeTemplateView = false;
      },
      error => {
        console.error(
          MethodName,
          "Error while getting class data. stack: ",
          error
        );
      }
    );
  }
  onUpdateTemplate(temp) {
    let feeTemplateData = this.templateForm.value;
    feeTemplateData.totalAmount = this.totalAmount;
    feeTemplateData.dueDate =
      moment(this.templateForm.value.dueDate).format("YYYY-MM-DD") +
      "T00:00:00";
    this._feeTemplateService
      .putFeeTemplate(feeTemplateData)
      .subscribe(feeTempate => {
        if (feeTempate.status === 201) {
          this.toasterService.Success("Successfully updated!");
          this.getFeeTemplate(temp.class);
          $("#updateFeeTemplate").modal("hide");
        } else {
          this.toasterService.Error("Not Updated!");
        }
      },
      error =>{
        this.toasterService.Error("Not Updated!");
      }
      
      );
  }

  /** Delete Fee Sub Details Row */
  deleteSubDetailRow(index: number, subDetailsAmt, feesTempSubDetail) {
    const formArray = this.templateForm.controls["subDetails"] as FormArray;
    formArray.removeAt(index);
    const subdata = this.templateForm.value.subDetails;
    let amount = 0;
    if (feesTempSubDetail.value.uidTemplateSubDetail) {
      const uidTemplateSubDetail = feesTempSubDetail.value.uidTemplateSubDetail;
      this._feeTemplateService
        .deleteTempSubDetails(uidTemplateSubDetail)
        .subscribe(feeSubDetails => {
          if (feeSubDetails.status === 200) {
          }
        });
    }
    for (let i = 0; i < subdata.length; i++) {
      amount = +amount + +subdata[i].amount;
    }
    this.totalAmount = amount;
  }

  /** Add Assign Fee Template Sub details Row  */
  addTemplateSubData(): void {
    this.subDetail = this.templateForm.get("subDetails") as FormArray;
    this.subDetail.push(this.addTemplateSub());
  }

  loadForm(templateData) {
    this.feeTemplateData = templateData;
    let amount = 0;
    this.totalAmount = 0;
    const updateformArray = this.templateForm.controls[
      "subDetails"
    ] as FormArray;
    // For Remove Previouse FormArray data
    while (updateformArray.length) {
      updateformArray.removeAt(updateformArray.length - 1);
    }
    // For New Sub Details generate formArray
    for (let line = 0; line < templateData.subDetails.length; line++) {
      amount = +amount + +templateData.subDetails[line].amount;
      const subdata = this._formBuilder.group({
        type: [templateData.subDetails[line].type],
        uidFeeType: [templateData.subDetails[line].uidFeeType],
        uidTemplateSubDetail: [
          templateData.subDetails[line].uidTemplateSubDetail
        ],
        amount: [templateData.subDetails[line].amount]
      });
      updateformArray.push(subdata);
    }
    this.totalAmount = amount;
    this.templateForm.patchValue(templateData);
    $("#updateFeeTemplate").modal("show");
  }

  /** Get All Class List By School*/
  getClassList() {
    const MethodName = "getClassBySchool";
    this._classService.getClasses().subscribe(
      class_list => {
        this.class_list = class_list.payload;
        this.colors = ["#F68A22", "#EF5350", "#009587", "#F47F1F"];
        this.contentView = true;
        this.tempContentView = true;
        this.feeTemplateView = false;
        this.studentListView = false;
      },
      error => {
        console.error(
          MethodName,
          "Error while getting class data. stack: ",
          error
        );
        this.toasterService.Error("Error while getting class data!");
      }
    );
  }

  /** Get All Class List By School*/
  getClassListFees() {
    const MethodName = "getClassBySchool";
    this._classService.getClasses().subscribe(
      class_list => {
        this.class_list = class_list.payload;
        this.colors = ["#F68A22", "#EF5350", "#009587", "#F47F1F"];
        this.feeContentView = true;
        this.studentFeeView = false;
      },
      error => {
        console.error(
          MethodName,
          "Error while getting class data. stack: ",
          error
        );
        this.toasterService.Error("Error while getting class data!");
      }
    );
  }

  // onChange(uid: string, isChecked: boolean) {
  //    const feesCode = this.cashPayment.controls.feesCodes;

  //   if (isChecked) {
  //     this.feesCodes.push(new FormControl(uid));
  //   } else {
  //     const index = feesCode.controls.findIndex(x => x.value === uid)
  //     this.feesCodes.removeAt(index);
  //   }
  // }

  onCashPayment(std) {
    this._feeMasterService
      .postCashPayment(this.cashPayment.value)
      .subscribe(paymentData => {
        if (paymentData.status === 200) {
          this.cashPayment.reset();
          this.getClassFeeStudent(std);
          this.assignFeeView = false;
          $("#PaymentModal").modal("hide");
          $("#assignFeeView").modal("hide");
        }
      });
  }

  onChange(feesCode: string, isChecked: boolean, payAmount: number, i: number) {
    const feesCodes = <FormArray>this.cashPayment.controls.feesCodes;
    let ii = 1;
    this.cashPayment.get("studentCode").setValue(this.uid);
    if (ii == 1 && isChecked == true) {
      ii = i + 1;
      this.payAmount = 0;
      feesCodes.removeAt(0);
    }
    if (isChecked) {
      feesCodes.push(new FormControl(feesCode));
      this.payAmount = +this.payAmount + +payAmount;
      this.cashPayment.get("amount").setValue(this.payAmount);
    } else {
      let index = feesCodes.controls.findIndex(x => x.value == feesCode);
      feesCodes.removeAt(index);
      this.payAmount = this.payAmount - payAmount;
      this.cashPayment.get("amount").setValue(this.payAmount);
    }
  }

  setPaymentReciept(paymentDetails) {
    this.paymentDetails = paymentDetails;
    $("#paymentView").modal("show");
  }

  substractAmt(totalAmt: number, remainingAmt: number) {
    return +totalAmt - +remainingAmt;
  }

  schoolPayment() {
    this._feeMasterService
      .getschoolPayment(this.school_code)
      .subscribe(schoolpayment => {
        if (schoolpayment.status == 200) {
          this.paymentSchoolData = schoolpayment.payload;
          this.dueAmount = (
            (100 * this.paymentSchoolData.amountDetails.duePayment) /
            this.paymentSchoolData.amountDetails.totalAmount
          ).toFixed();
          this.totalCollection = (
            (100 * this.paymentSchoolData.amountDetails.totalCollection) /
            this.paymentSchoolData.amountDetails.totalAmount
          ).toFixed();
          this.duePaymentChart(this.dueAmount);
          this.totalCollectionChart(this.totalCollection);
          this.paymentChart();
        }
      });
  }

  /** Student Subject perfomance Bar Chart */
  paymentChart = function() {
    if ($("#m_chart_performance").length === 0) {
      return;
    }
    const series = [];
    const label = [];
    for (const e of this.paymentSchoolData.feesAmountStats) {
      const perc = (e.paidAmount / e.totalDue) * 100;
      series.push(perc);
      label.push(e.Month);
    }
    const chart_data = Chartist.Bar(
      "#m_chart_performance",
      {
        labels: label,
        series: series
      },
      {
        barwidth: 100,
        distributeSeries: true,
        // low: 0,
        // high: 100,
        axisX: {
          showGrid: false
        },
        axisY: {
          showGrid: false,
          showLabel: false,
          offset: 0
        }
      }
    );
    chart_data.on("draw", function(data) {
      if (data.type === "bar") {
        data.element.attr({
          style: "stroke-width: 10%; stroke: #e18215"
        });
      }
    });
  };

  /** Due Payment Bar Chart */
  duePaymentChart = function(duePayment) {
    if ($("#m_chart_duepayment").length === 0) {
      return;
    }

    let dueAmount = 100 - duePayment;

    const duebarchart = Chartist.Pie(
      "#m_chart_duepayment",
      {
        series: [
          {
            value: dueAmount,
            className: "Paid",
            meta: {
              color: "#f2f3f8"
            }
          },
          {
            value: duePayment,
            className: "due",
            meta: {
              color: "#ff6565"
            }
          }
        ]
      },
      {
        donut: true,
        donutWidth: 10,
        showLabel: false
      }
    );
    duebarchart.on("draw", function(data) {
      if (data.type === "slice") {
        // Get the total path length in order to use for dash array animation
        const pathLength = data.element._node.getTotalLength();

        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
          "stroke-dasharray": pathLength + "px " + pathLength + "px"
        });

        // Create animation definition while also assigning an ID to the animation for later sync usage
        const animationDefinition = {
          "stroke-dashoffset": {
            id: "anim" + data.index,
            dur: 1000,
            from: -pathLength + "px",
            to: "0px",
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: "freeze",
            stroke: data.meta.color
          }
        };

        // // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if (data.index !== 0) {
          animationDefinition["stroke-dashoffset"]["begin"] =
            "anim" + (data.index - 1) + ".end";
        }

        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

        data.element.attr({
          "stroke-dashoffset": -pathLength + "px",
          stroke: data.meta.color
        });

        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
      }
    });
  };

  /** Due Payment Bar Chart */
  totalCollectionChart = function(totalCollection) {
    if ($("#m_chart_totalCollection").length === 0) {
      return;
    }

    let totalAmount = 100 - totalCollection;
    const collectionbarchart = Chartist.Pie(
      "#m_chart_totalCollection",
      {
        series: [
          {
            value: totalCollection,
            className: "Paid",
            meta: {
              color: "#53d476"
            }
          },
          {
            value: totalAmount,
            className: "due",
            meta: {
              color: "#f2f3f8"
            }
          }
        ]
      },
      {
        donut: true,
        donutWidth: 10,
        showLabel: false
      }
    );
    collectionbarchart.on("draw", function(data) {
      if (data.type === "slice") {
        // Get the total path length in order to use for dash array animation
        const pathLength = data.element._node.getTotalLength();
        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
          "stroke-dasharray": pathLength + "px " + pathLength + "px"
        });

        // Create animation definition while also assigning an ID to the animation for later sync usage
        const animationDefinition = {
          "stroke-dashoffset": {
            id: "anim" + data.index,
            dur: 1000,
            from: -pathLength + "px",
            to: "0px",
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: "freeze",
            stroke: data.meta.color
          }
        };

        // // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if (data.index !== 0) {
          animationDefinition["stroke-dashoffset"]["begin"] =
            "anim" + (data.index - 1) + ".end";
        }

        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

        data.element.attr({
          "stroke-dashoffset": -pathLength + "px",
          stroke: data.meta.color
        });

        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
      }
    });
  };

  onOnlinePayChange(
    feesCode: string,
    isChecked: boolean,
    payAmount: number,
    i: number
  ) {
    console.log('chekcked',isChecked);
    console.log('ammount',payAmount);
    const feesCodes = <FormArray>this.onlinePayment.controls.feesCodes;
    console.log('feescode',feesCodes);
    let ii = i;
    // if (!feesCodes) {
    //   this.payAmount = 0;
    //   feesCodes.removeAt(0);
    // }
    // this.onlinePayment.get("studentCode").setValue(this.uid);
    // if (isChecked) {
    //   feesCodes.push(new FormControl(feesCode));
    //   this.payAmount = +this.payAmount + +payAmount;
    //   this.onlinePayment.get("amount").setValue(this.payAmount);
    // } else {
    //   let index = feesCodes.controls.findIndex(x => x.value == feesCodes);
    //   feesCodes.removeAt(index);
    //   this.payAmount = this.payAmount - payAmount;
    //   this.onlinePayment.get("amount").setValue(this.payAmount);
    // }
    if (!feesCodes) {
      // this.payAmount = 0;
      feesCodes.removeAt(0);
    }
    this.onlinePayment.get("studentCode").setValue(this.uid);
    if (isChecked) {
      feesCodes.push(new FormControl(feesCode));
      this.payAmt = +this.payAmt + +payAmount;
      this.onlinePayment.get("amount").setValue(this.payAmt);
    } else {
      let index = feesCodes.controls.findIndex(x => x.value == feesCodes);
      feesCodes.removeAt(index);
      this.payAmt = this.payAmt - payAmount;
      this.onlinePayment.get("amount").setValue(this.payAmt);
    }
  }

  proceedToPay($event) {
    
    const paymentData = this.onlinePayment.value;
    paymentData.amount = this.payAmt;
    paymentData.studentCode = this.studentDetails.uid;
    console.log('data',paymentData);
    var ref = this;
    this._feeMasterService
      .generateOrderIdforPayment(paymentData)
      .subscribe(orderData => {
        if (orderData.status == 201) {
          this.order = orderData.payload;
          var rzp1 = new this.WindowRef.Razorpay(
            this.preparePaymentDetails(this.order)
          );
          this.processingPayment = false;
          rzp1.open();
          event.preventDefault();
          // rzp1.on('payment.success', function(resp)
          // {
          //   ref.handlePayment(resp, '');
          // });
          rzp1.on("payment.error", function(resp) {
            alert(resp.error.code);
            if (resp.error.code) {
              ref.handlePayment(resp, "faild");
            } else {
              ref.handlePayment(resp, "");
            }
          });
        }
      });
  }

  /** Prepare Payment Payload */
  preparePaymentDetails(order) {
    var ref = this;
    return {
      key: this.order.key_id, // Enter the Key ID generated from the Dashboard
      amount: this.order.amount, // Amount is in currency subunits. Default currency is INR. Hence, 29935 refers to 29935 paise or INR 299.35.
      name: "Clan.School",
      currency: this.order.currency,
      order_id: this.order.orderId, //This is a sample Order ID. Create an Order using Orders API. (https://razorpay.com/docs/payment-gateway/orders/integration/#step-1-create-an-order). Refer the Checkout form table given below
      image: "././assets/app/media/img/logos/clan_logo.png",
      handler: function(response) {
        ref.handlePayment(response, "");
      },
      prefill: {
        // 'name': this.profile.name,
        email: this.profile.email,
        contact: this.profile.phone
      },
      theme: {
        color: "#0d47a1"
      }
    };
  }

  handlePayment(response, status) {

    if (status === "faild") {
      let transaction = this.paymentTransaction.value;
      let feesCodes = this.onlinePayment.value;
      transaction.reciept = this.order.receipt;
      transaction.schoolId = this.studentDetails.schoolCode;
      transaction.studentCode = this.studentDetails.uid;
      transaction.feesCodes = feesCodes.feesCodes;
      transaction.orderId = this.order.orderId;
      transaction.paymentId = "";
      transaction.isOnline = true;
      transaction.isSuccess = false;
      transaction.errorCode = response.error.code;
      transaction.errorMessage = response.error.description;
      // transaction.amount = this.payAmount;
      transaction.amount = this.payAmt;
      transaction.razorPaySignature = "";
      this.transactionUpdate(transaction);
    } else {
      let transaction = this.paymentTransaction.value;
      let feesCodes = this.onlinePayment.value;
      transaction.reciept = this.order.receipt;
      transaction.schoolId = this.studentDetails.schoolCode;
      transaction.studentCode = this.studentDetails.uid;
      transaction.feesCodes = feesCodes.feesCodes;
      transaction.orderId = this.order.orderId;
      transaction.paymentId = response.razorpay_payment_id;
      transaction.isOnline = true;
      transaction.isSuccess = true;
      transaction.errorCode = "";
      transaction.errorMessage = "";
      // transaction.amount = this.payAmount;
      transaction.amount = this.payAmt;
      transaction.razorPaySignature = response.razorpay_signature;
      this.transactionUpdate(transaction);
    }
  }

  transactionUpdate(response) {
    this._feeMasterService.updatePaymentTransaction(response).subscribe(
      data => {
        if (data.status == 201) {
          this.paymentResponse = data;
        } else if (data.status == 400) {
          this.paymentResponse = data;
        }
        this.getStudentFeeDetails(this.uid);
        // this.changeRef.detectChanges();
      },
      error => {
        this.paymentResponse = error;
      }
    );
  }

  getPaymentLogs() {
    this._feeMasterService.getPaymentLog(this.studentDetails.uid).subscribe(
      logData => {
        if (logData.status == 200) {
          this.paymentLogs = logData.payload;
        }
      },
      error => {}
    );
  }

  getSplitDate(feeMasterData) {
    if(feeMasterData){
      let assignDate = feeMasterData.split('T');    
      return assignDate[0];
    }    
  }
}
