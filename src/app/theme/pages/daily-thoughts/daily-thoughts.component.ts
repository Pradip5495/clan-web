import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { DailyThoughtsService } from '../../../_services/daily-thoughts.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-daily-thoughts',
  templateUrl: './daily-thoughts.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DailyThoughtsComponent implements OnInit {
  userForm: FormGroup;
  public dailythoughts: any;

  constructor(public formBuilder: FormBuilder,
    public dailyThoughtsService: DailyThoughtsService,
    public http: HttpClient) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      title: [],
      message: [],
      attachment: []
    });
    this.onSubmit();
  }
  onSubmit() {
    console.log(this.userForm.value);
    this.dailyThoughtsService.dailyThoughts(this.userForm.value.title, this.userForm.value.message, this.userForm.value.attachment).subscribe(dailythoughts =>
      this.dailythoughts = dailythoughts.payload);
    console.log('daily thoughts list', this.dailythoughts);
  }
}
