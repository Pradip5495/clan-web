import { Component, OnInit, ViewEncapsulation } from '@angular/core';

// Models
import { User } from '../../../_models/user';

// Services
import { IdentityService } from '../../../_services';
import { ProfileService } from '../../../_services/profile.service';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { ListInboxItemsService } from '../../../_services/listInboxItems.service';
import { MustMatch } from './must-match.validator';
import { ToasterService } from '../../../_services/toaster.service';

declare var $: any;

@Component({
  selector: 'app-header-profile',
  templateUrl: './profile.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public profile_date: any;
  passwordForm: FormGroup;
  adminReportForm: FormGroup;
  //userForm: FormGroup;
  addParent: FormGroup;

  get editEnabled(): boolean {
    return this._editEnabled;
  }

  set editEnabled(value: boolean) {
    this._editEnabled = value;
  }

  public profile: User;
  public role: string;
  public _editEnabled: boolean;
  public data: any;
  public category_list: any = ['complain', 'Suggestion', 'Praise', 'Feedback'];
  public category: any;
  public report;
  public to;
  public code: any;
  public unamePattern: any;


  constructor(private identityService: IdentityService,
    private _profileService: ProfileService,
    private formBuilder: FormBuilder,
    private toasterService: ToasterService,
    private _listInboxItemsService: ListInboxItemsService) {
    this.profile = this.identityService.identity;
    this.role = this.identityService.role;
    this._editEnabled = false;
  }

  ngOnInit() {
    this.getProfile();
    this.unamePattern = "^[a-z0-9_-]{8,15}$";
    this.passwordForm = this.formBuilder.group({
      currentPassword: ['', Validators.required],
      newPassword: [
        '',
        [
          Validators.required,
          Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')
        ]
      ],
      confirmPassword: ['', Validators.required],
    }, {
      validator: MustMatch('newPassword', 'confirmPassword')
    });
    this.adminReportForm = this.formBuilder.group({
      subject: ['', Validators.required],
      message: ['', Validators.required],
      categoryList: ['', Validators.required]
    });
    this.addParent = this.formBuilder.group({
      parent: ['', Validators.required],
      confirmCode: ['', Validators.required],
    },{

      validator: MustMatch('parent', 'confirmCode')
    });
  }



  getProfile() {
    this._profileService.getProfile().subscribe(profile_date => {
      this.profile_date = profile_date.payload;
      console.log('profile data:', this.profile_date);

    },
      error => {
        console.error('Error while getting profile data. stack: ', error);
      });
  }
  changePassword() {
    //  console.log(this.category);
    console.log(this.passwordForm.value.currentPassword);
    console.log(this.passwordForm.value.newPassword);
    this._profileService.changePassword(this.passwordForm.value.currentPassword, this.passwordForm.value.newPassword).subscribe(data =>
      this.data = data.payload);
    if (this.passwordForm.valid) {
      //   alert('Report Generated Successfully!');
      // this.getChild();
    }
    this.passwordForm.reset();

  }

  Success() {
    this.toasterService.Success('Report Generated Successfully');
  }

  setCategory(cat) {
    this.category = cat;
  }
  onSubmit(category) {
    console.log(this.category);
    console.log(this.adminReportForm.value.subject);
    console.log(this.adminReportForm.value.message);

    console.log('this.adminReportForm: ', this.adminReportForm);
    // this.category, this.userForm.value.subject, this.to, this.userForm.value.message
    this._listInboxItemsService.onSubmit(this.adminReportForm, this.category).subscribe(data => {
      if (data.status === 201) {
        this.report = data.payload;
        this.toasterService.Success('Report Generated Successfully');
        console.log("Report Generated Successfully", data);
        $("#m_modal_4").modal("hide");
        this.adminReportForm.reset();
      }
      else {
        this.toasterService.Error("Report Not Generated!");
      }
    });
    /*this._listInboxItemsService.onSubmit(this.category, this.adminReportForm.value.subject, this.to, this.adminReportForm.value.message).subscribe(data =>
      this.report = data.payload);*/
    console.log('report generate' + this.report);
    /* alert('Report Generated Successfully');*/
    /*if (this.adminReportForm.valid) {
      this.Success();*/
    /* alert('Report Generated Successfully!');*/
    /*this.adminReportForm.reset();*/

  }



  addParents() {
    console.log(this.addParent.value);
    this._profileService.addParents(this.addParent.value.parent).subscribe(code =>

     this.code = code.payload);
    console.log('report generate' + this.code);
    if (this.addParent.valid) {
      //alert('Report Generated Successfully!');
      this.toasterService.Success('Parent Added Successfully!');
      $("#m_modal_6").modal("hide");
      this.addParent.reset();
      //  this.getProfile();

    }
    else {
      this.toasterService.Error("Parent Not Added!");
    }
  }

}
