/*
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-non-teaching-staff',
  templateUrl: './non-teaching-staff.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class NonTeachingStaffComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
*/
import {
  AfterViewInit,
  Component,
  OnInit,
  ViewEncapsulation
} from "@angular/core";

// Models
import { User } from "../../../_models/user";

// Services
//import { IdentityService } from '../../../_services';
import { IdentityService } from "../../../_services/identity.service";
import { ListInboxItemsService } from "../../../_services/listInboxItems.service";
import { MessageService } from "../../../_services/message.service";
import { NonTeachingStaffService } from "../../../_services/non-teaching-staff.service";
import { TeacherService } from "../../../_services/teacher.service";
import { SecurityService } from "../../../_services/security.service";
import { ApprovalService } from "../../../_services/approval.service";
import { ToasterService } from "../../../_services/toaster.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

declare let $: any;

@Component({
  selector: "app-non-teaching-staff",
  templateUrl: "./non-teaching-staff.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./non-teaching-staff.component.css"]
})
export class NonTeachingStaffComponent implements OnInit {
  public profile: User;
  public nonTeacher: User;
  public role: string;
  public uid: any;
  public assistant_details: any;
  public driver_details: any;
  public active_teachers_list: any;
  public teachers_approval_list: any;
  public isAdmin: Boolean = false;
  public isTeacher: Boolean = false;
  public isDirector: Boolean = false;
  public isCoadmin: Boolean = false;
  public class: any;
  public securityStats: any;

  private assistant_list: any;
  public category_list: any = ["complain", "Suggestion", "Praise", "Feedback"];
  public report: any;
  private driver_list: any;
  private praise_list: any;
  private feedback_list: any;
  private conversation: any;
  public sendmsg: any;
  public staffDetails: any;

  public id: any;
  public msgid: any;

  private fullview: String = "col-xl-12";
  private halfview: String = "col-xl-6 col-lg-12";
  private applyfullview: Boolean = true;
  private applyhalfview: Boolean = false;

  userForm: FormGroup;
  messageForm: FormGroup;

  constructor(
    private identityService: IdentityService,
    private _teacherService: TeacherService,
    private _approvalService: ApprovalService,
    private formBuilder: FormBuilder,
    private _identityService: IdentityService,
    private _listInboxItemsService: ListInboxItemsService,
    private toasterService: ToasterService,
    private _nonTeachingStaffService: NonTeachingStaffService,
    private _security: SecurityService,
    private _messageService: MessageService
  ) {
    this.profile = this.identityService.identity;
    this.role = this.identityService.role;
    this.uid = this.identityService.uid;
    this.isTeacher = this._identityService.isClassTeacher;
    this.isDirector = this._identityService.isDirector;
    this.isCoadmin = this._identityService.isCoadmin;
  }

  ngOnInit() {
    this.assistantClick();
    this.driverClick();
    //  this.waitList();

    this.messageForm = this.formBuilder.group({
      category: ["", Validators.required],
      subject: ["", Validators.required],
      message: ["", Validators.required]
    });
  }

  onMessageSubmit() {
    // this.category, this.messageForm.value.subject, this.to, this.messageForm.value.message
    this._listInboxItemsService
      .postMessage(this.messageForm.value)
      .subscribe(data => {
        if (data.status === 201) {
          this.report = data.payload;
          this.toasterService.Success("Report Generated Successfully");
          $("#m_modal_7").modal("hide");
          this.messageForm.reset();
        } else {
          this.toasterService.Error("Report Not Generated!");
        }
      });

    /*this.report = data.payload);
    console.log('report generate' + this.report);
    if (this.userForm.value.subject === null && this.userForm.value.message === null) {
      alert('Please Enter Subject & message');
    } else {
      // alert('Report Generated Successfully');
      //$('#m_modal_5').modal('hide');
    }
    this.userForm.value.reset();*/
  }
  addCssClass(isapplyhalfview: boolean, isapplyfullview: boolean) {
    let classes = {
      "col-xl-6 col-lg-12": isapplyhalfview,
      "col-xl-12": isapplyfullview
    };
    return classes;
  }

  item_inbox_complain_click(id) {
    this.id = id;

    this.applyfullview = false;
    this.applyhalfview = true;


    this._messageService.getMessage(id).subscribe(
      data => {
        this.conversation = data.payload;
      },
      error => {
        console.error("Error while getting message. stack: ", error);
      }
    );
  }

  assistantClick() {
    this._nonTeachingStaffService.getAssistant().subscribe(
      assistant_list => {
        this.assistant_list = assistant_list.payload;

        // for(let i of this.complain_list) {
        //   this.complain_message = this.getMessage(i.id);
        //   console.log('complain message: ');
        //
        //   if (this.complain_message){
        //     console.log(this.complain_message);
        //   }
        // }
      },
      error => {
        console.error("Error while getting complain data. stack: ", error);
      }
    );

    // this.getMessage(this.complain_list.id);
  }

  driverClick() {
    this._nonTeachingStaffService.getDriver().subscribe(
      driver_list => {
        this.driver_list = driver_list.payload;

        // for(let i of this.suggestion_list){
        //   console.log('suggestion message: ');
        //   this.getMessage(i.id);
        // }
      },
      error => {
        console.error("Error while getting suggestion data. stack: ", error);
      }
    );
  }
  getAssistantDetails() {
    this._nonTeachingStaffService.getAssistant().subscribe(
      assistant_list => {
        if (assistant_list.status == 200) {
          this.assistant_list = assistant_list.payload;
        }
      },
      error => {
        console.error("Error while getting assistants list. stack: ", error);
      }
    );
  }
  getDriverDetails() {
    this._nonTeachingStaffService.getDriver().subscribe(
      driver_list => {
        if (driver_list.status == 200) {
          this.driver_list = driver_list.payload;
        }
      },
      error => {
        console.error("Error while getting assistants list. stack: ", error);
      }
    );
  }

  getSecurityStats() {
    this._security.getSecurityStats().subscribe(securityStats => {
      if (securityStats.status === 200) {
        this.securityStats = securityStats.payload;
      }
    });
  }
  getNonTeacherDetails(nonTeacher) {
    this.staffDetails = nonTeacher;
    $("#m_modal_5").modal("show");
    // if (nonTeacher == 'assistant') {
    //   this.assistant_details = nonTeacher;
    //   //this.assistantClick();
    //   this.getAssistantDetails();
    //   $('#m_modal_5').modal('show');
    //   console.log("Assistant list", this.assistant_details);
    // }
    // else if (nonTeacher == 'driver') {
    //   this.driver_details = nonTeacher;
    //   //this.driverClick();
    //   this.getDriverDetails();
    //   $('#m_modal_6').modal('show');
    //   console.log("Assistant list", this.driver_details);
    // }
  }

  /* waitList() {
     this._nonTeachingStaffService.getListInboxItemsService('Praise').subscribe(praise_list => {
       this.praise_list = praise_list.payload;
       console.log('praise data:', this.praise_list);
 
       // for(let i of this.praise_list){
       //   console.log('praise message: ');
       //   this.getMessage(i.id);
       // }
     },
       error => {
         console.error('Error while getting praise data. stack: ', error);
       });
   }*/
}
