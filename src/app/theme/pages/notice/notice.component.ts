import {
  Component,
  OnInit,
  ViewEncapsulation,
  AfterViewInit
} from "@angular/core";
import { ScriptLoaderService } from "../../../_services/script-loader.service";
import { NoticeService } from "../../../_services/notice.service";
import { ToasterService } from "../../../_services/toaster.service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormArray,
  FormControl
} from "@angular/forms";
import { IdentityService } from "../../../_services/identity.service";

declare let $: any;
declare let mApp, moment;

@Component({
  selector: "app-notice",
  templateUrl: "./notice.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./notice.component.css"]
})
export class NoticeComponent implements OnInit, AfterViewInit {
  private school: any;
  private notice_date = null;
  private notice_list: any;
  public notice_list1: any;
  public noticeDetails: any;
  public selectedChild: any;
  public role: string;
  public uid: any;
  public notice_list2: any;
  userForm: FormGroup;
  myForm: FormGroup;
  public type: any = [
    "Choose Title",
    "Bus Time and Route",
    "School Timing",
    "Immidiate Meeting",
    "Emergency",
    "Custom"
  ];

  object;
  // type = [{types: 'Bus Time and Route'}, {types: 'School Timing'}, {types: 'Immidiate Meeting'}, {types: 'Emergency'}, {types: 'Custom'}];
  audience = [
    { audiences: "Teachers" },
    { audiences: "Parents" },
    { audiences: "Students" }
  ];

  constructor(
    private _script: ScriptLoaderService,
    private _noticeService: NoticeService,
    private formBuilder: FormBuilder,
    private toasterService: ToasterService,
    private identityService: IdentityService
  ) {
    this.role = this.identityService.role;
    this.school = this.identityService.school;

    this.object = this.type[0];
  }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      type: this.formBuilder,
      message: ["", Validators.required],
      dateTime: ["", Validators.required],
      date: ["", Validators.required],
      time: ["", Validators.required],

      audience: this.formBuilder.array([])
    });

    if (this.role === "parent") {
      if (localStorage.getItem("selectedChild")) {
        this.selectedChild = JSON.parse(localStorage.getItem("selectedChild"));
        this.uid = this.selectedChild.uid;
        console.log('id for student',this.uid);
        this.getNoticeForParent();
      } 
     
    }

    if (this.role === 'school' || this.role === 'teacher' || this.role === 'coadmin' || this.role === 'librarian' || this.role == 'clerk' || this.role === 'director') {
      this.getRecentNotice();
      this.getNotice();
      
    }
 
  }

  ngAfterViewInit() {
    this._script.loadScripts("app-notice", [
      "assets/demo/default/custom/components/calendar/basic.js"
    ]);
  }

  getRecentNotice() {
    this._noticeService.getRecentNotice().subscribe(
      notice_data => {
        this.notice_date = notice_data.payload;
        this.loadNotice(this.notice_date);
      },
      error => {
        console.error("Error while getting notice data. stack: ", error);
        this.toasterService.Error("Error while getting notice data");
      }
    );
  }

  getNotice() {
    this._noticeService.getNotice(this.school).subscribe(
      notice_list1 => {
        this.notice_list1 = notice_list1.payload;
      },
      error => {
        console.error("Error while getting notice data. stack: ", error);
        this.toasterService.Error("Error while getting notice data");
      }
    );
  }

  getNoticeForParent() {
    this._noticeService.getNoticeForParent(this.uid).subscribe(
      event_list2 => {
        this.notice_list2 = event_list2.payload;
        console.log('event for parent',this.notice_list2);
      },
      error => {
        console.error("Error while getting event data. stack: ", error);
      }
    );
  }

  // moment(n.createdAt).format('YYYY-MM-DD')
  loadNotice(notice) {
    let noticeArr = [];

    for (let n of notice) {
      noticeArr.push({
        title: n.type,
        start: n.createdAt,
        description: n.message,
        className: "m-fc-event--light m-fc-event--solid-warning"
      });
    }

    if ($("#m_calendar_notice").length === 0) {
      return;
    }

    let todayDate = moment().startOf("day");

    $("#m_calendar_notice").fullCalendar({
      header: {
        left: "prev,next today",
        center: "title",
        right: "month,agendaWeek,agendaDay,listWeek"
      },

      editable: true,
      eventLimit: true, // allow "more" link when too many events
      navLinks: true,
      defaultDate: moment(todayDate),

      events: noticeArr,

      eventRender: function(event, element) {
        if (element.hasClass("fc-day-grid-event")) {
          element.data("content", event.description);
          element.data("placement", "top");
          mApp.initPopover(element);
        } else if (element.hasClass("fc-time-grid-event")) {
          element
            .find(".fc-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        } else if (element.find(".fc-list-item-title").lenght !== 0) {
          element
            .find(".fc-list-item-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        }
      }
    });
  }

  onSubmit() {
    this.userForm.value.dateTime =
      moment(this.userForm.value.date).format("YYYY-MM-DD") +
      //moment(this.userForm.value.time).format("T00:00:00");
      "T00:00:00";
    this._noticeService
      .postResentNotice(
        this.userForm.value.type,
        this.userForm.value.message,
        this.userForm.value.audience,
        this.userForm.value.dateTime
      )
      .subscribe(
        (
          notice_data /*{
          this.notice_list = notice_data.payload;
          console.log(this.notice_list);
          this.loadNotice(this.notice_list);
          $("#m_modal_5").modal("hide");
        },
        error => {
          console.error("Error while getting notice data. stack: ", error);
        }*/
        ) => {
          if (notice_data.status === 201) {
            this.toasterService.Success("Successfully Submitted!");
            this.userForm.reset();
            $("#m_modal_5").modal("hide");
          } else {
            this.toasterService.Error("Template Not Created!");
          }
        },
        error => {
          this.toasterService.Error("Error while getting notice data");
          console.error("Error while getting notice data!");
        }
      );
  }

  onChange(audiences: string, isChecked: boolean) {
    const emailFormArray = <FormArray>this.userForm.controls.audience;

    if (isChecked) {
      emailFormArray.push(new FormControl(audiences));
    } else {
      let index = emailFormArray.controls.findIndex(x => x.value === audiences);
      emailFormArray.removeAt(index);
    }
  }

  cancel(userForm) {
    userForm.reset();
    $("input[type=checkbox]").removeAttr("checked");

    // userForm.value.audience.reset();
    $("#m_modal_4").modal("hide");
  }

  events() {
    $("#m_modal_4").modal("hide");
  }

  getNoticeDetails(notice) {
    this.noticeDetails = notice;
    console.log("noticedetails",this.noticeDetails);
    for(let i = 0; i<= notice.audience.length ; i++)
    {
      console.log("length:",notice.audience.length);
      
      if(notice.audience[i] == "PR"){
        this.noticeDetails.audience[i] = "Parent";
      }
      else if(notice.audience[i] == "ST"){
        this.noticeDetails.audience[i] = "Student";
      }
      else if(notice.audience[i] == "TE"){
        this.noticeDetails.audience[i] = "Teacher";
      }
      else if(notice.audience[i] == ("LB")){
        this.noticeDetails.audience[i] = "Librarian";
      }
      else if(notice.audience[i] == ("CL")){
        this.noticeDetails.audience[i] = "Clerk";
      }
      else if(notice.audience[i] == ("SQ")){
        this.noticeDetails.audience[i] = "Security";
      }
    }

    /*if(notice.audience == "PR"){
      this.noticeDetails.audience = "Parent";
    }
    else if(notice.audience == "ST"){
      this.noticeDetails.audience = "Student";
    }*/
    //this.noticeDetails = notice;
    $("#m_modal_10").modal("show");
    
  }
}
