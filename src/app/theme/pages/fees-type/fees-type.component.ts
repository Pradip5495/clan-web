import { Component, OnInit, AfterViewInit, ViewEncapsulation, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { FeesTypeService } from '../../../_services/fees-type.service';

declare var $: any;
@Component({
  selector: 'app-fees-type',
  templateUrl: './fees-type.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./fees-type.component.css']
})
export class FeesTypeComponent implements OnInit {

  feeTypeForm: FormGroup;
  public type: any;
  public type_list: any;
  public uidFeeType: any;

  constructor(
    private _feesTypeService: FeesTypeService,
    private formBuilder: FormBuilder,
  ) {
    this.feeTypeForm = this.formBuilder.group({
      type: []
    });
  }
  ngOnInit() {
    // this.feeTypeForm = this.formBuilder.group({
    //   type: []
    // });
    this.getFeesTypeList();
  }

  getFeesTypeList() {
    this._feesTypeService.getFeeTypes().subscribe(feestypelist => {
      this.type_list = feestypelist.payload;
    });
  }

  onSubmit() {
    console.warn(this.feeTypeForm.value.type);
    this._feesTypeService.postFeeType(this.feeTypeForm.value.type).subscribe(feetype => {
      if (feetype.status === 201) {
        this.getFeesTypeList();
        $('#m_modal_5').modal('hide');
      }
    });
  }

  getFeesType(data) {
    this.uidFeeType = data.uidFeeType;
    this.type = data.type;
    $('#m_modal_6').modal('show');
  }

  updateFeeType(uidFeeType) {
    this._feesTypeService.updateFeeType(uidFeeType, this.feeTypeForm.value.type).subscribe(feetype => {
      if (feetype.status === 200) {
        this.getFeesTypeList();
        $('#m_modal_6').modal('hide');
      }
    });
  }

  deleteFeeType(uidFeeType) {
    this._feesTypeService.deleteFeeType(uidFeeType).subscribe(feeType => {
      this.getFeesTypeList();
    });
  }

}
