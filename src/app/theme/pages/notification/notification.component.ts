
import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { StudentService } from '../../../_services/student.service';
import { ApprovalService } from '../../../_services/approval.service';
import { IdentityService } from '../../../_services/identity.service';
import { ClassService } from '../../../_services/class.service';
import { Router } from '@angular/router';
import { NotificationListService } from '../../../_services/notificationList.service';

declare let $: any;
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit, AfterViewInit {
  get class_data(): any {
    return this._class_data;
  }

  set class_data(value: any) {
    this._class_data = value;
  }

  get class_value(): any {
    return this._class_value;
  }

  set class_value(value: any) {
    this._class_value = value;
  }

  get isClassTeacher(): boolean {
    return this._isClassTeacher;
  }

  set isClassTeacher(value: boolean) {
    this._isClassTeacher = value;
  }

  get approval_list(): any {
    return this._approval_list;
  }

  set approval_list(value: any) {
    this._approval_list = value;
  }

  get students_list(): any {
    return this._students_list;
  }

  set students_list(value: any) {
    this._students_list = value;
  }

  private _students_list: any;
  private _approval_list: any;
  private _isClassTeacher = false;
  public notificationDetails = false;
  private _class_data: any;
  private _class_value: any = null;
  private location: any;
  private notification_list: any;

  constructor(private _script: ScriptLoaderService,
    private _studentService: StudentService,
    private _router: Router,
    private _identityService: IdentityService,
    private _classService: ClassService,
    private _notificationListService: NotificationListService) {
    this.location = _router.url;

  }

  ngOnInit() {
    this.getNotificationListService();
  }

  ngAfterViewInit() {
    this._script.loadScripts('app-teachers',
      ['assets/demo/default/custom/components/calendar/basic.js']);
  }


  getNotificationListService() {
    this._notificationListService.getNotificationListService().subscribe(notification_list => {
      this.notification_list = notification_list.payload;

    },
      error => {
        console.error('Error while getting notification list. stack: ', error);
      });
  }

  getNotificationDetails(notification)
  { 
    this.notificationDetails = notification;
    $('#m_modal_10').modal('show'); 
  
  }
}


