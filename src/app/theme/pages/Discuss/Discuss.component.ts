import {
  Component,
  OnInit,
  ViewEncapsulation,
  ViewChildren
} from "@angular/core";
import { ConversationIdService } from "../../../_services/conversationId.service";
import { IdentityService } from "../../../_services/identity.service";
import { ConversationService } from "../../../_services/conversation.service";
import { Router } from "@angular/router";
import { MessageService } from "../../../_services/message.service";

@Component({
  selector: "app-notice",
  templateUrl: "./Discuss.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./Discuss.component.css"]
})
export class DiscussComponent implements OnInit {
  get class_data(): any {
    return this._class_data;
  }

  set class_data(value: any) {
    this._class_data = value;
  }

  public id: any;
  public conversation: any;
  public _class_data: any;
  public myClass: any;
  public myGroup: any;
  public selectedIndex;
  public i;
  public colors;
  public randomItem;
  public chatBox: any = false;
  public uid: any;
  public sendmsg: any;

  constructor(
    private _conversationIdService: ConversationIdService,
    private _messageService: MessageService,
    private _identityService: IdentityService,
    private _conversationService: ConversationService,
    private _router: Router
  ) {
    this.colors = ["#F68A22", "#F47F1F", "#009587", "#EF5350"];
    this.myClass = _identityService.class;
    this.uid = _identityService.uid;
  }
  @ViewChildren("myVar") createdItems;

  ngOnInit(): void {
    this.getConversation();
    this._conversationIdService.currentId.subscribe(data => (this.id = data));

    if (this.id) {
      this.getMessage(this.id);

    }
  }
  getConversation() {
    this._conversationService.getConversation().subscribe(
      data => {
        this.myGroup = data.payload;
        let count = this.myGroup.length;
        this.colors = ["#F68A22", "#EF5350", "#009587", "#F47F1F"];
      },
      error => {
        console.error("Error while getting my group list. stack: ", error);
      }
    );
  }

  newId(id) {
    this._conversationIdService.changeId(id);
    this.getMessage(id);

    //this.getConversation()
    //this._router.navigate(['/chatbox']);
  }

  getColor() {
    this.colors = ["#F68A22", "#F47F1F", "#009587", "#EF5350"];
    let count = this.myGroup.length;
    for (let j = 0; j <= count; j++) {
      for (let i = 0; i <= 4; i++) {
      }
      // --count;
    }
  }
  trackByFn(index, myGroup) {
    return index; // or material.id
  }

  getMessage(uid) {
    this._messageService.getMessage(uid).subscribe(
      data => {
        this.conversation = data.payload;
      },
      error => {
        console.error("Error while getting conversation list. stack: ", error);
      }
    );
  }

  postMessage(msg) {
    if (msg) {
      this._messageService.postMessage(this.id, msg).subscribe(
        data => {
          this.getMessage(this.id);
        },
        error => {
          console.error(
            "Error while getting conversation list. stack: ",
            error
          );
        }
      );
    }

    this.sendmsg = "";
    this.getMessage(this.uid);
  }

  getChatBox(id) {
    this.chatBox = true;
    this._conversationIdService.changeId(id);
  }
}
