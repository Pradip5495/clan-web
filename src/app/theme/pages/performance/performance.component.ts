import { Component, OnInit, ViewEncapsulation, AfterViewInit, Input } from '@angular/core';
import { ActivatedRoute, Router, ParamMap, NavigationExtras } from '@angular/router';
import { Data } from '../../../_provider/data/data';

import { IdentityService } from '../../../_services/identity.service';
import { PerformanceByClassService } from '../../../_services/performanceByClass.service';
import { AttendanceService } from '../../../_services/attendance.service';
import { ExaminationService } from '../../../_services/examination.service';
import { StudentService } from '../../../_services/student.service';
import { TeacherService } from '../../../_services/teacher.service';
import { ClassService } from '../../../_services/class.service';


declare let $: any;
declare let mApp, moment, selectpicker, Chartist;


@Component({
  selector: 'app-notice',
  templateUrl: './performance.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./performance.component.css']
})

export class PerformanceComponent implements OnInit {


  public role: any;
  exam_code: any;
  public uid;
  private teacher_data: any;
  private student_data: any;
  private _class_data: any;
  public class_performance: any;
  public student_performance: any;
  public colors;
  public exam_list;
  public roll_title: any;
  private schoolCode: any;
  public class_list: any;
  public schoolPerformanaceStatus: any = false;
  public subjectList: any;
  public profile: any;
  public attendance_status = false;
  public attendance1: any;
  public attendance2: any;
  public studentAttendance: any;
  public present: any = 0;
  public absent: any = 0;
  public day_count: any = 0;
  public present_percent: any = 0;
  public absent_percent: any = 0;
  private exam_data;

  @Input()
  date: Date = this.date;
  code: string = this.exam_code;
  totalMarks: number = this.totalMarks;

  get class_data(): any {
    return this._class_data;
  }

  set class_data(value: any) {
    this._class_data = value;
  }
  constructor(private _identityService: IdentityService,
    private _performanceByClassService: PerformanceByClassService,
    private _attendanceService: AttendanceService,
    private _examinationService: ExaminationService, private _teacherService: TeacherService,
    private _studentService: StudentService, private router: Router, private data: Data,
    private route: ActivatedRoute, private _classService: ClassService) {

    this.exam_code = _examinationService.getData(this.exam_code);
    this.totalMarks = _examinationService.getMarks(this.totalMarks);
    this.date = _examinationService.getDate(this.date);
    this.role = _identityService.role;
    this.uid = this._identityService.uid;
    this.schoolCode = this._identityService.identity.schoolCode;

    console.log('role is perfromance class: ' + this.role + _identityService.uid);
  }

  ngOnInit(): void {
    console.log('service marks', this.totalMarks)
    this.getPerformanceByClassService();
    // this.getExaminationResult();
    // this.datatable();
    this.passData(this.exam_code);

    this.getExaminationOfChild();
    this.getAllStudent();

    if (this.route.params['value'].uid) {
      this.uid = this.route.params['value'].uid;
      console.log('UID set Here:', this.uid);
      if (this.uid !== undefined && this.uid != null) {
        this.profile = JSON.parse(localStorage.getItem('StudentProfile'));
        console.log('StudentProfile set Here:', this.profile);
        this.uid = this.profile.uid;
        this.getStudentPerformane();
        this.schoolPerformanaceStatus = true;
        this.attendance_status = false;
        console.log("status: ", this.schoolPerformanaceStatus);
      } else {
        this.attendance_status = true;
        console.log("status: ", this.attendance_status);
      }
    }


    /*    this.route.queryParams.subscribe(params => {
          this.profile = params;
          if (this.profile.uid !== undefined) {
            this.profile = params;
            this.uid = this.profile.uid;
            console.log('UID set Here:', params);
            this.getStudentPerformane();
            this.schoolPerformanaceStatus = true;
            console.log("performance status: ", this.schoolPerformanaceStatus);
          } else {
            this.attendance_status = true;
            this.schoolPerformanaceStatus = false;
            console.log("main page status: ", this.schoolPerformanaceStatus);
          }
        });*/
  }

  passData(exam_code) {
    this._examinationService.saveData(this.exam_code, this.totalMarks, this.date);
    console.log('date in result', this.date);
    console.log('servicess', this.totalMarks);
  }

  getPerformanceByClassService() {
    this._performanceByClassService.getPerformanceByClassService().subscribe(class_performance => {
      this.class_performance = class_performance.payload;

      console.log('class performance data:', this.class_performance);
      this.colors = ['#F68A22', '#EF5350', '#009587', '#F47F1F'];
      console.log(this.colors);

    },
      error => {
        console.error('Error while getting performance data. stack: ', error);
      });
  }

  getExaminationOfChild() {

    this._examinationService.getExaminationOfChild(this.uid).subscribe(exam_list => {
      this.exam_list = exam_list.payload;
      console.log('exam-list:', this.exam_list);
      console.log('exam-list UID:', this.uid);
      console.log('exam-list UID:', this.schoolCode);
      //  this.getActiveTeacher();
      //   this.getProfileAndLecture(this.id);
    },
      error => {
        console.error('Error while getting exam list. stack: ', error);
      });
  }

  /** Get Student List */
  getStudent(event: any) {
    console.log('class value in component for student: ' + event);
    this._studentService.getStudentByClass(event).subscribe(student_data => {
      this.student_data = student_data.payload;
      console.log('student data:', this.student_data);
    },
      error => {
        console.error('Error while getting student data. stack: ', error);
      });
  }

  /** Get  All Student List */
  getAllStudent() {
    console.log('class value in component for student: ' + event);
    this._studentService.getStudent().subscribe(student_data => {
      console.log('get All student:', student_data);
      if (student_data !== undefined && student_data !== null) {
        this.student_data = student_data.payload;
        console.log('student data:', this.student_data);
      }
    },
      error => {
        console.error('Error while getting student data. stack: ', error);
      });
  }

  /** Get Teacher List */
  getTeacher(class1: any) {
    console.log('class value in component for teacher: ' + event);
    this._teacherService.getTeacherByClass(class1).subscribe(teacher_data => {
      this.teacher_data = teacher_data.payload;
      console.log('teacher data:', this.teacher_data);
    },
      error => {
        console.error('Error while getting teacher data. stack: ', error);
      });
  }

  gotoAttendace(student: any, student_uid: any) {
    localStorage.setItem('StudentProfile', JSON.stringify(student));
    const navigationExtras: NavigationExtras = {
      queryParams: student
    };
    this.router.navigate(['attendance/student/' + student_uid]);
  }
  getStudentPerformanceRedirect(student: any, student_uid: any) {
    localStorage.setItem('StudentProfile', JSON.stringify(student));
    const navigationExtras: NavigationExtras = {
      queryParams: student
    };
    this.router.navigate(['performance/student/' + student_uid]);
  }
  getStudentPerformane() {
    //  const uid;
    const todayDate1 = moment().startOf('day');
    const YM = todayDate1.format('YYYY-MM-DD');
    this._attendanceService.getAttendanceForStudent(this.uid, YM).subscribe(attendance => {
      this.attendance1 = attendance.payload;
      for (const a of this.attendance1) {
        this.day_count++;
        if (a.isPresent) {
          this.present++;
        } else {
          this.absent++;
        }
      }
      this.present_percent = (this.present / this.day_count) * 100;
      this.absent_percent = (this.absent / this.day_count) * 100;
      this.getStudentPerformanceBySubject();
      this.getExamination();

    });
  }
  /** Get Student Examination Data */
  getStudentPerformanceBySubject() {
    this._performanceByClassService.getStudentPerfomanceBySubject(this.uid, this.schoolCode).subscribe(subjectList => {
      this.subjectList = subjectList.payload;
      console.log('Subject-list:', this.subjectList);
      this.performaceChart(this.subjectList);
    },
      error => {
        console.error('Error while getting exam list. stack: ', error);
      });


  }

  /** Student Subject perfomance Bar Chart */
  performaceChart = function(subjectList) {
    console.log('performance Bar: ' + subjectList);
    if ($('#m_chart_performance').length === 0) {
      return;
    }
    const series = [];
    const label = [];
    console.log('Subjectlist:', subjectList);
    for (const e of subjectList) {
      console.log(e);
      series.push(e.performance);
      label.push(e.subject);
    }
    console.log(series);
    const chart_data = Chartist.Bar('#m_chart_performance', {
      labels: label,
      series: series
    }, {
        barwidth: 100,
        distributeSeries: true,
        low: 0,
        high: 100
      });
    chart_data.on('draw', function(data) {
      if (data.type === 'bar') {
        data.element.attr({
          style: 'stroke-width: 10%'
        });
      }
    });
  };

  /** Get Examination Details */
  getExamination() {
    this._examinationService.getExamResultByStudent(this.uid).subscribe(exam_data => {
      this.exam_data = exam_data.payload;
      console.log('exam data:', this.exam_data);
    },
      error => {
        console.error('Error while getting exam data. stack: ', error);
      });
  }

  /** Get All Class List By School*/
  getClassBySchool() {
    this._classService.getClassBySchool().subscribe(class_list => {
      this.class_list = class_list.payload;
      this.colors = ['#F68A22', '#EF5350', '#009587', '#F47F1F'];
      console.log('class data54321:', this.class_list); // student count
    },
      error => {
        console.error('Error while getting class data. stack: ', error);
      });
  }
}
