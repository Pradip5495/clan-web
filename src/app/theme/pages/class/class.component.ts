/*
import {AfterViewInit, Component, OnInit, ViewEncapsulation} from '@angular/core';

// Models
import { User } from '../../../_models/user';

// Services
import { IdentityService } from '../../../_services';
import { ListInboxItemsService } from '../../../_services/listInboxItems.service';
import { MessageService } from '../../../_services/message.service';
@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  encapsulation: ViewEncapsulation.None,
})

export class ClassComponent implements OnInit {

  public profile: User;
  public role: string;
  public uid: any;

  private complain_list: any ;
  private suggestion_list: any ;
  private praise_list: any ;
  private feedback_list: any ;
  private conversation: any;
  public sendmsg: any;


  public id: any;
  public msgid: any;

  private fullview: String = 'col-xl-12';
  private halfview: String = 'col-xl-6 col-lg-12';
  private applyfullview: Boolean = true;
  private applyhalfview: Boolean = false;

  constructor(private identityService: IdentityService,
              private _listInboxItemsService: ListInboxItemsService,
              private _messageService: MessageService) {

    this.profile = this.identityService.identity;
    this.role = this.identityService.role;
    this.uid = this.identityService.uid;
  }

  ngOnInit() {
    this.complainClick();
    this.feedbackClick();
  }

  addCssClass(isapplyhalfview: boolean, isapplyfullview: boolean) {

    let classes = {
      'col-xl-6 col-lg-12' : isapplyhalfview,
      'col-xl-12': isapplyfullview
    }
    return classes;
  }

  item_inbox_complain_click(id) {

    this.id = id;

    this.applyfullview = false;
    this.applyhalfview = true;

    console.log(id);

    this._messageService.getMessage(id).subscribe(data => {
        this.conversation = data.payload;

      },
      error => {
        console.error('Error while getting message. stack: ', error);
      });
  }

  complainClick() {
    this._listInboxItemsService.getListInboxItemsService('Complain').subscribe(complain_list => {
        this.complain_list = complain_list.payload;
        console.log('complain data:', this.complain_list);

        // for(let i of this.complain_list) {
        //   this.complain_message = this.getMessage(i.id);
        //   console.log('complain message: ');
        //
        //   if (this.complain_message){
        //     console.log(this.complain_message);
        //   }
        // }

      },
      error => {
        console.error('Error while getting complain data. stack: ', error);
      });


    // this.getMessage(this.complain_list.id);
  }

  suggestionClick() {
    this._listInboxItemsService.getListInboxItemsService('Suggestion').subscribe(suggestion_list => {
        this.suggestion_list = suggestion_list.payload;
        console.log('suggestion data:', this.suggestion_list);

        // for(let i of this.suggestion_list){
        //   console.log('suggestion message: ');
        //   this.getMessage(i.id);
        // }
      },
      error => {
        console.error('Error while getting suggestion data. stack: ', error);
      });
  }

  praiseClick() {
    this._listInboxItemsService.getListInboxItemsService('Praise').subscribe(praise_list => {
        this.praise_list = praise_list.payload;
        console.log('praise data:', this.praise_list);

        // for(let i of this.praise_list){
        //   console.log('praise message: ');
        //   this.getMessage(i.id);
        // }
      },
      error => {
        console.error('Error while getting praise data. stack: ', error);
      });
  }

  feedbackClick() {
    this._listInboxItemsService.getListInboxItemsService('Feedback').subscribe(feedback_list => {
        this.feedback_list = feedback_list.payload;
        console.log('feadback data:', this.feedback_list);

        // for(let i of this.feedback_list){
        //   console.log('feedback message: ');
        //   this.getMessage(i.id);
        // }
      },
      error => {
        console.error('Error while getting feadback data. stack: ', error);
      });
  }

  getMessage(id: any): any {

    this._messageService.getMessage(id).subscribe(data => {
        this.conversation = data.payload;
        // console.log(this.message);

      },
      error => {
        console.error('Error while getting message. stack: ', error);
      });
  }

  postMessage( msg) {

    if (msg) {

      this._messageService.postMessage(this.id , msg).subscribe(data => {

          console.log('message is send');
          this.sendmsg = '';
          this.getMessage(this.id);

        },
        error => {
          console.error('Error while getting conversation list. stack: ', error);
        });

      this.sendmsg = '';
      this.getMessage(this.id);
    }
  }
}
*/
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ClassService } from '../../../_services/class.service';
import { StudentService } from '../../../_services/student.service';
import { TeacherService } from '../../../_services/teacher.service';

@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class ClassComponent implements OnInit {

  private teacher_data: any;
  private student_data: any;
  private _students_list: any;
  private _isClassTeacher = false;
  private _class_data: any;
  private _class_value: any = null;

  get class_data(): any {
    return this._class_data;
  }

  set class_data(value: any) {
    this._class_data = value;
  }

  get class_value(): any {
    return this._class_value;
  }

  set class_value(value: any) {
    this._class_value = value;
  }

  get isClassTeacher(): boolean {
    return this._isClassTeacher;
  }

  set isClassTeacher(value: boolean) {
    this._isClassTeacher = value;
  }

  get students_list(): any {
    return this._students_list;
  }

  set students_list(value: any) {
    this._students_list = value;
  }

  constructor(private _classService: ClassService,
    private _teacherService: TeacherService,
    private _studentService: StudentService) {

  }

  ngOnInit() {
    this.getClass();
    this.getTeacher(event);
    this.getStudent(event);
  }

  getClass() {
    this._classService.getClass().subscribe(class_data => {
      this.class_data = class_data.payload;
    },
      error => {
        console.error('Error while getting class data. stack: ', error);
      });
  }

  getTeacher(event: any) {
    console.log('class value in component for teacher: ' + event);
    this._teacherService.getTeacherByClass(event).subscribe(teacher_data => {
      this.teacher_data = teacher_data.payload;
      console.log('teacher data:', this.teacher_data);
    },
      error => {
        console.error('Error while getting teacher data. stack: ', error);
      });
  }
  getStudent(event: any) {
    console.log('class value in component for student: ' + event);
    this._studentService.getStudentByClass(event).subscribe(student_data => {
      this.student_data = student_data.payload;
      console.log('student data:', this.student_data);
    },
      error => {
        console.error('Error while getting student data. stack: ', error);
      });
  }

  getStudentAndTeacher(event: any) {


    this.class_value = event;

    console.log('class value in component for teacher: ' + event);

    this._teacherService.getTeacherByClass(event).subscribe(teacher_data => {
      this.teacher_data = teacher_data.payload;
      console.log('teacher data:', this.teacher_data);
    },
      error => {
        console.error('Error while getting teacher data. stack: ', error);
      });

    this._studentService.getStudentByClass(event).subscribe(student_data => {
      this.student_data = student_data.payload;
      console.log('student data:', this.student_data);
    },
      error => {
        console.error('Error while getting student data. stack: ', error);
      });


    this.class_value = null;

  }

}
