

import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { TeacherService } from '../../../_services/teacher.service';
import { ApprovalService } from '../../../_services/approval.service';
import { IdentityService } from '../../../_services/identity.service';
import { LectureService } from '../../../_services/lecture.service';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { ListInboxItemsService } from '../../../_services/listInboxItems.service';
import { DriverService } from '../../../_services/driver.service';
import { SecurityService } from '../../../_services/security.service';

/*import { FormsModule, ReactiveFormsModule } from '@angular/forms';*/
declare const $: any;
@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  encapsulation: ViewEncapsulation.None,  
  styleUrls: ['driver.component.css']
})
export class DriverComponent implements OnInit, AfterViewInit {

  public active_driver_list: any;
  public teachers_approval_list: any;
  public isAdmin: Boolean = false;
  public isTeacher: Boolean = false;
  public isDirector: Boolean = false;
  public class: any;
  public role: any;
  public leture_data: any;
  public selected_driver_info: any;
  public category_list: any = ['complain', 'Suggestion', 'Praise', 'Feedback'];
  public category: any;
  public form_values: any;
  public report: any;
  public to: any;
  public driver_detail: any;
  public driver_approval_list: any;
  public securityStats: any;

  userForm: FormGroup;

  constructor(private _script: ScriptLoaderService,
    private _approvalService: ApprovalService,
    private _identityService: IdentityService,
    private formBuilder: FormBuilder,
    private _listInboxItemsService: ListInboxItemsService,
    private _driverService: DriverService,
    private _security: SecurityService) {

    this.class = this._identityService.class;
    this.role = this._identityService.role;
    this.isAdmin = this._identityService.isAdmin;
    this.isTeacher = this._identityService.isClassTeacher;
    this.isDirector = this._identityService.isDirector;

  }

  ngOnInit() {
    this.getDriver();
    if (this.isAdmin === true) {
      this.getApprovalDriver();
      this.getSecurityStats();
    }
    this.userForm = this.formBuilder.group({
      subject: [],
      message: []
    });
  }

  ngAfterViewInit() {
    this._script.loadScripts('app-driver', ['assets/demo/default/custom/components/calendar/basic.js']);
  }

  getDriver() {
    this._driverService.getDriver().subscribe(driver_list => {
      this.active_driver_list = driver_list.payload;
      console.log('driver-list:', this.active_driver_list);
    },
      error => {
        console.error('Error while getting driver list. stack: ', error);
      });

  }

  getApprovalDriver() {
    console.log(this.isAdmin);
    if (this.isAdmin === true) {
      this._approvalService.getClerkAproval().subscribe(teacherapprovallist => {
        if (teacherapprovallist) {
          this.driver_approval_list = teacherapprovallist.payload;
          console.log('teacher approval list:', this.driver_approval_list);
        } else {
          this.driver_approval_list = null;
          console.log('teacher approval list Null:', this.driver_approval_list);
        }
      },
        error => {
          console.error('Error while getting teachers approval list. stack: ', error);
        });
    }
  }

  /*  getApprovalTeacher() {
  
      if (this.isAdmin) {
        this._approvalService.getTeacherAproval().subscribe(teacherapprovallist => {
            this.teachers_approval_list = teacherapprovallist.payload;
            console.log('teacher approval list:', this.teachers_approval_list);
          },
          error => {
            console.error('Error while getting teachers approval list. stack: ', error);
          });
  
      }
  
    }
  
    onTeacherApprovalClick(id: any) {
  
      console.log('APPROVE AT COMPONENT IS CLICKED.........' + id);
      this._approvalService.approveTeacher(id).subscribe(data => {
  
          console.log('send to approve: ', data.payload);
        },
        error => {
          console.error('Error while sending teachers approval. stack: ', error);
        });
    }
  
    onTeacherRejectClick(id: any) {
      console.log('REJECT AT COMPONENT IS CLICKED.........');
      this._approvalService.rejectTeacher(id).subscribe(data => {
          console.log('send to reject: ', data.payload);
        },
        error => {
          console.error('Error while sending teachers rejection. stack: ', error);
        });
    }
  
    getProfileAndLecture(uid) {
  
      this._lectureService.getLectureById(uid).subscribe(data => {
  
          this.leture_data = data.payload;
          console.log('selected teachers lecture:  ', this.leture_data);
          this.getSelectedTeacherInfo(uid);
        },
        error => {
          console.error('Error while getting leture data. stack: ', error);
        });
    }*/

  getSelectedDriverInfo(id) {

    for (const driver of this.active_driver_list) {
      if (driver.uid === id) {
        this.selected_driver_info = driver;
        console.log('selected driver info: ', this.selected_driver_info);
      }
    }
  }

  setCategory(cat) {
    this.category = cat;
  }

  onSubmit(category) {
    console.log(this.category);
    console.log(this.userForm.value.subject);
    console.log(this.userForm.value.message);
    // this.category, this.userForm.value.subject, this.to, this.userForm.value.message
    this._listInboxItemsService.onSubmit(this.userForm.value, this.category).subscribe(data =>
      this.report = data.payload);
    console.log('report generate' + this.report);
    /* alert('Report Generated Successfully');*/
    if (this.userForm.valid) {
      alert('Report Generated Successfully!');
      this.userForm.reset();

    }
  }

  getDriverDetails(clerk)
  { 
    this.driver_detail = clerk;
    $('#m_modal_4').modal('show'); 
    console.log("teacher list", this.driver_detail);


  }

  /** Get Security Stats Data */
  getSecurityStats() {
    this._security.getSecurityStats().subscribe(securityStats => {
      if(securityStats.status === 200){
        this.securityStats = securityStats.payload;
        console.log('Library borrow List', this.securityStats);
      }
    });
  }
}
