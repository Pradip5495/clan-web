
import { Component, OnInit, ViewEncapsulation, Input, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { StudentService } from '../../../_services/student.service';
import { ApprovalService } from '../../../_services/approval.service';
import { IdentityService } from '../../../_services/identity.service';
import { ClassService } from '../../../_services/class.service';
import { AssignService } from '../../../_services/assign.service';
import { Router } from '@angular/router';
import { PassengerService } from '../../../_services/passenger.service';
import { TripService } from '../../../_services/trip.service';

@Component({
  selector: 'app-bus-passengers',
  templateUrl: './bus-passengers.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class BusPassengersComponent implements OnInit, AfterViewInit {
  get class_data(): any {
    return this._class_data;
  }

  set class_data(value: any) {
    this._class_data = value;
  }

  get class_value(): any {
    return this._class_value;
  }

  set class_value(value: any) {
    this._class_value = value;
  }

  get isClassTeacher(): boolean {
    return this._isClassTeacher;
  }

  set isClassTeacher(value: boolean) {
    this._isClassTeacher = value;
  }

  get students_list(): any {
    return this._students_list;
  }

  set students_list(value: any) {
    this._students_list = value;
  }

  private passenger_data: any;
  private _students_list: any;
  private _isClassTeacher = false;
  private route_code: any;
  private _class_value: any = null;
  public str: any;
  public _class_data: any;
  public passenger: any;
  // public tuid: any;
  public trip_id: any;

  @Input()
  tuid: string = this.trip_id;

  constructor(private _script: ScriptLoaderService,
    private _studentService: StudentService,
    private _approvalService: ApprovalService,
    private _identityService: IdentityService,
    private _tripService: TripService,
    private _assignService: AssignService,
    private router: Router,
    private _passengerService: PassengerService) {
    this.router = router;
    this._tripService = _tripService;
    console.log('cone called' + this.trip_id);
    this.trip_id = _tripService.getData(this.trip_id);
    console.log(typeof this.trip_id);
    console.log(this.trip_id);
  }

  ngOnInit() {
    console.log(this.trip_id);
    this.passData(this.trip_id);
    this.getBus(this.trip_id);
    //  this.trip_id = '';
    //  this._isClassTeacher = this._identityService.isClassTeacher;

    this.getTripPassenger(this.trip_id);
  }

  ngAfterViewInit() {
    this._script.loadScripts('app-bus-passengers',
      ['assets/demo/default/custom/components/calendar/basic.js']);
  }
  passData(tuid) {
    this._tripService.saveData(this.trip_id);
    console.log('wlvdhbwj', this.trip_id);
    this._tripService.getTripPassenger(this.trip_id).subscribe(passenger => {
      this.passenger = passenger.payload;
      console.log('passengers:', this.passenger);
    });


  }
  getBus(tuid) {
    this._tripService.getTripPassenger(this.trip_id).subscribe(passenger => {
      this.passenger = passenger.payload;
      console.log('passenger data:', this.passenger);
    });
  }
  getTripPassenger(tuid) {
    this._tripService.getTripPassenger(this.tuid).subscribe(passenger => {
      this.passenger = passenger.payload;
      console.log('passengers:', this.passenger);
    });

  }
}


