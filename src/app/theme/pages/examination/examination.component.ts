import {
  Component,
  OnInit,
  AfterViewInit,
  ViewEncapsulation,
  Output,
  EventEmitter,
  Input
} from "@angular/core";
import { ExaminationService } from "../../../_services/examination.service";
import { NavigationExtras } from "@angular/router";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { ResultService } from "../../../_services/result.service";
import { ChildService } from "../../../_services/child.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TransportService } from "../../../_services/transport.service";
import { ClassService } from "../../../_services/class.service";
import { StudentService } from "../../../_services/student.service";
import { ToasterService } from "../../../_services/toaster.service";
import { LectureService } from "../../../_services/lecture.service";
import { TeacherService } from "../../../_services/teacher.service";
import { IdentityService } from "../../../_services/identity.service";
declare let $: any;
declare let selectpicker, mDatatable;

@Component({
  selector: "app-examination",
  templateUrl: "./examination.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./examination.component.css"]
})
export class ExaminationComponent implements OnInit {
  get class_data(): any {
    return this._class_data;
  }

  set class_data(value: any) {
    this._class_data = value;
  }

  get class_value(): any {
    return this._class_value;
  }
  set class_value(value: any) {
    this._class_value = value;
  }
  private exam_data;
  private class;
  private students_list;
  private _class_value: any;
  private exam_code;
  private result_data: any;
  private selected_exam_info;
  public router;
  private str;
  public uid;
  private active_teachers_list: any;
  private role: any;
  private leture_data: any;
  public isAdmin: Boolean = false;
  public isTeacher: Boolean = false;
  public isCoadmin: Boolean = false;
  public isDirector: Boolean = false;
  public exam1: any;
  private data: any;
  private _class_data: any;
  public examListView: any = false;
  public examDetails: any;
  private _subject: any;
  public today: any;
  public selectedChild: any;
  public schoolCode: any;
  public studentDetails: any;
  public _child_list: any;

  examForm: FormGroup;
  public id;

  @Input()
  code: string = this.exam_code;
  totalMarks: string;
  constructor(
    private _examinationService: ExaminationService,
    private _router: Router,
    private formBuilder: FormBuilder,
    private toasterService: ToasterService,
    private _child: ChildService,
    private _transportService: TransportService,
    private _teacherService: TeacherService,
    private _classService: ClassService,
    private _studentService: StudentService,
    private _lectureService: LectureService,
    private route: ActivatedRoute,

    private identityService: IdentityService
  ) {}

  ngOnInit() {
    this.role = this.identityService.role;
    const id = this.identityService.uid;
    if (this.role == "parent") {
      if (localStorage.getItem("selectedChild")) {
        this.selectedChild = JSON.parse(localStorage.getItem("selectedChild"));
        this.uid = this.selectedChild.uid;
        this.schoolCode = this.selectedChild.schoolCode;
        this.studentDetails = this.selectedChild;
        let std = this.selectedChild.std;
        let div = this.selectedChild.div;
        let studClass = std + "" + div;
        this.getStudentExamDetails(this.uid);
      } else {
        this.getChild();
      }
    } else if (this.role == "student") {
      this.uid = this.identityService.uid;
      this.getStudentExamDetails(this.uid);
    } else {
      this.getProfileAndLecture(id);
      this.getExamination(this.str);
      this.getClass();
    }
    this.examForm = this.formBuilder.group({
      title: ["", Validators.required],
      std: [],
      div: [],
      hr: [],
      min: [],
      totalMarks: [],
      subject: [],
      description: [],
      date: [],
      time: []
    });

    if (this.route.params["value"].exam) {
      this.exam1 = this.route.params["value"].exam;
      this.examListView = true;
    } else {
      this.examListView = false;
    }
  }

  get child_list(): any {
    return this._child_list;
  }

  set child_list(value: any) {
    this._child_list = value;
  }

  /** get Child list of Parent */
  getChild() {
    let value: any;
    this._child.getChild().subscribe(data => {
      this._child_list = data.payload;
      $("#m_modal_child_5").modal("show");
    });
  }

  /** Set Child list for View rendar */
  setChild(data) {
    localStorage.setItem("selectedChild", JSON.stringify(data));
    this.uid = data.uid;
    $("#m_modal_child_5").modal("hide");
  }

  /** Get Student wise exam details */
  getStudentExamDetails(uid) {
    this._examinationService.getStudentExam(this.uid).subscribe(
      exam_data => {
        if (exam_data.status === 200) {
          this.exam_data = exam_data.payload;
        }

        // for (const exam of this.exam_data) {
        //   this.selected_exam_info = exam;
        //   if (this.selected_exam_info.code === str && this.selected_exam_info.totalMarks) {
        //     console.log('selected exam info: ', this.selected_exam_info.code, this.selected_exam_info.totalMarks);
        //     this._examinationService.saveData(this.selected_exam_info.code, this.selected_exam_info.totalMarks, this.selected_exam_info.date);
        //     console.log('wlvdhbwj', this.selected_exam_info.code, this.selected_exam_info.totalMarks);
        //     this._router.navigate(['result']);
        //   }
        // }
      },
      error => {
        console.error("Error while getting exam data. stack: ", error);
      }
    );
  }

  getExamination(str) {
    this._examinationService.getExamination().subscribe(
      exam_data => {
        this.exam_data = exam_data.payload;
        for (const exam of this.exam_data) {
          this.selected_exam_info = exam;
          if (
            this.selected_exam_info.code === str &&
            this.selected_exam_info.totalMarks
          ) {
            this._examinationService.saveData(
              this.selected_exam_info.code,
              this.selected_exam_info.totalMarks,
              this.selected_exam_info.date
            );
            this._router.navigate(["result"]);
          }
        }
      },
      error => {
        console.error("Error while getting exam data. stack: ", error);
      }
    );
  }
  getClass() {
    this._classService.getClass().subscribe(
      class_data => {
        this.class_data = class_data.payload;
      },
      error => {
        console.error("Error while getting class data. stack: ", error);
      }
    );
  }

  // == Class definition
  DatatableDataLocalDemo(exam) {
    // == Private functions

    // demo initializer
    var demo = function() {
      // console.log('exam data:', exam);

      var datatable = $(".m_datatable").mDatatable({
        data: {
          type: "local",
          source: exam,
          pageSize: 10
        },

        // layout definition
        layout: {
          theme: "default", // datatable theme
          class: "", // custom wrapper class
          scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
          // height: 450, // datatable's body's fixed height
          footer: false // display/hide footer
        },

        // column sorting
        sortable: true,

        pagination: true,

        search: {
          input: $("#generalSearch")
        },

        columns: [
          {
            field: "subject",
            title: "subject",
            width: 60,
            textAlign: "center"
          },
          {
            field: "title",
            title: "title",
            textAlign: "center"
          },
          {
            field: "description",
            title: "description",
            width: 180,
            textAlign: "center"
          },
          {
            field: "totalMarks",
            title: "total Marks",
            width: 50,
            textAlign: "center"
          },

          // TODO: add class = std+div
          {
            field: "std",
            //  field: "div",
            title: "class",
            width: 80,
            textAlign: "center"
          },
          {
            field: "div",
            // title: "class",
            width: 70,
            textAlign: "center"
          },
          {
            field: "date",
            title: "date",
            type: "date",
            format: "MM/DD/YYYY",
            width: 70,
            textAlign: "center"
          },
          {
            field: "time",
            title: "time",
            width: 50,
            textAlign: "center"
          },
          {
            field: "duration",
            title: "duration",
            width: 70,
            textAlign: "center"
          },

          {
            field: "state",
            title: "state",
            width: 80,
            textAlign: "center"

            // TODO: css is not working(can't read property 'class' of undefined)
            // template: function (row) {
            //   var status = {
            //     1: {'title': 'In-Progress', 'class': 'm-badge--warning'},
            //     2: {'title': 'Completed', 'class': 'm-badge--success'},
            //     3: {'title': 'Scheduled', 'class': 'm-badge--danger'},
            //   };
            //   return '<span class="m-badge ' + status[row.state].class + ' m-badge--wide">' + status[row.state].title + '</span>';
            // }
          },
          {
            field: this.result_data,
            title: "result",
            width: 70,
            textAlign: "center"
          }
        ]
      });

      var query = datatable.getDataSourceQuery();

      $("#m_form_status")
        .on("change", function() {
          datatable.search($(this).val(), "Status");
        })
        .val(typeof query.Status !== "undefined" ? query.Status : "");

      $("#m_form_type")
        .on("change", function() {
          datatable.search($(this).val(), "Type");
        })
        .val(typeof query.Type !== "undefined" ? query.Type : "");

      $("#m_form_status, #m_form_type").selectpicker();
    };

    return {
      // == Public functions
      init: function() {
        // init dmeo
        demo();
      }
    };
  }
  onSubmit() {
    this._lectureService.getLectureById(this.id).subscribe(data => {
      this.leture_data = data.payload;
      for (const lectur of this.leture_data) {

      }
    });
    const duration =
      this.examForm.value.hr + "hr" + this.examForm.value.min + "min";
    const stdndard = this.examForm.value.std.split(" ");
    const division = this.examForm.value.std.split(" ");

    const div = division[1];
    const std = stdndard[0];

    console.log(this.examForm.value);
    this._transportService
      .examForm(
        this.examForm.value.title,
        std,
        div,
        duration,
        this.examForm.value.subject,
        this.examForm.value.description,
        this.examForm.value.totalMarks,
        this.examForm.value.date,
        this.examForm.value.time
      )
      .subscribe(
        (
          data // this.examForm.value.date, this.examForm.value.time
        ) => {
          if (data) {
            this.toasterService.Success("Successfully Submitted!");
            $("#m_modal_8").modal("hide");
            this.examForm.reset();
          } else {
            this.toasterService.Error("Not Submitted!");
          }
          console.log(data);
        }
      );
   
    //alert("Report Generated Successfully");
    /*if (this.examForm.valid) {
      alert("Report Generated Successfully!");
      this.getExamination(this.str);
    }*/
  }
  getStudent(event: any) {
    this._studentService.getStudentByClass(event).subscribe(
      students_list => {
        this.students_list = students_list.payload;
        //  this.getActiveTeacher();
        this.getProfileAndLecture(this.id);
      },
      error => {
        console.error("Error while getting student list. stack: ", error);
      }
    );
  }
  getActiveTeacher() {
    this._teacherService.getTeacher().subscribe(
      teacherlist => {
        this.active_teachers_list = teacherlist.payload;
      },
      error => {
        console.error("Error while getting teachers list. stack: ", error);
      }
    );
  }
  getProfileAndLecture(id) {
    this._lectureService.getLectureById(id).subscribe(
      data => {
        this.leture_data = data.payload;
      },
      error => {
        console.error("Error while getting leture data. stack: ", error);
      }
    );
  }

  getExamDetails(exam) {
    this.examDetails = exam;
    $("#m_modal_9").modal("show");
  }
  /** Student Exam Popup window data */
  getStudentExam(exam) {
    this.examDetails = exam;
    $("#m_modal_student_9").modal("show");
  }
}
