import {
  Component,
  OnInit,
  ViewEncapsulation,
  AfterViewInit
} from "@angular/core";
import { ScriptLoaderService } from "../../../_services/script-loader.service";
import { TeacherService } from "../../../_services/teacher.service";
import { ApprovalService } from "../../../_services/approval.service";
import { IdentityService } from "../../../_services/identity.service";
import { LectureService } from "../../../_services/lecture.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ListInboxItemsService } from "../../../_services/listInboxItems.service";
import { SecurityService } from "../../../_services/security.service";
import { ToasterService } from "../../../_services/toaster.service";

declare let $: any;
@Component({
  selector: "app-teachers",
  templateUrl: "./teachers.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./teachers.component.css"]
})
export class TeachersComponent implements OnInit, AfterViewInit {
  public active_teachers_list: any;
  public teachers_approval_list: any;
  public isAdmin: Boolean = false;
  public isTeacher: Boolean = false;
  public isDirector: Boolean = false;
  public isCoadmin: Boolean = false;
  public class: any;
  public role: any;
  public lecture_data: any;
  public selected_teacher_info: any;
  public category_list: any = ["complain", "Suggestion", "Praise", "Feedback"];
  public category: any;
  public form_values: any;
  public report: any;
  public to: any;
  public teacher_detail: any;
  public securityStats: any;
  public selectedChild: any;
  public schoolCode: any;

  userForm: FormGroup;

  constructor(
    private _script: ScriptLoaderService,
    private toasterService: ToasterService,
    private _teacherService: TeacherService,
    private _approvalService: ApprovalService,
    private _identityService: IdentityService,
    private _lectureService: LectureService,
    private formBuilder: FormBuilder,
    private _listInboxItemsService: ListInboxItemsService,
    private _security: SecurityService
  ) {
    this.class = this._identityService.class;
    this.role = this._identityService.role;
    this.isAdmin = this._identityService.isAdmin;
    this.isTeacher = this._identityService.isClassTeacher;
    this.isDirector = this._identityService.isDirector;
    this.isCoadmin = this._identityService.isCoadmin;
  }

  ngOnInit() {
    
    if (this.isAdmin === true) {
      this.getApprovalTeacher();
      this.getSecurityStats();
    } else {
      this.getSecurityStats();
    }
    this.userForm = this.formBuilder.group({
      category: ["", Validators.required],
      subject: ["", Validators.required],
      message: ["", Validators.required]
    });

    if (this.role === 'parent') {
      if (localStorage.getItem("selectedChild")) {
        this.selectedChild = JSON.parse(localStorage.getItem("selectedChild"));
        this.schoolCode = this.selectedChild.schoolCode;
        this.class = this.selectedChild.std + this.selectedChild.div;
        this.getActiveTeacher();
      }
    } else {
      this.getActiveTeacher();
    }

  }

  ngAfterViewInit() {
    this._script.loadScripts("app-teachers", [
      "assets/demo/default/custom/components/calendar/basic.js"
    ]);
  }

  addCssClass(isapplyhalfview: boolean, isapplyfullview: boolean) {
    let classes = {
      "col-xl-6 col-lg-12": isapplyhalfview,
      "col-xl-12": isapplyfullview
    };
    return classes;
  }

  getActiveTeacher() {
    if (this.role === "student") {
      this._teacherService.getTeacherByClass(this.class).subscribe(
        teacherlist => {
          this.active_teachers_list = teacherlist.payload;
          //  console.log('teacher-list:', this.active_teachers_list);
        },
        error => {
          console.error("Error while getting teachers list. stack: ", error);
        }
      );
    }

    if (this.role ==="parent") {
      this._teacherService.getTeacherByClassForParent(this.class, this.schoolCode).subscribe(
        teacherlist => {
          this.active_teachers_list = teacherlist.payload;
          //  console.log('teacher-list:', this.active_teachers_list);
        },
        error => {
          console.error("Error while getting teachers list. stack: ", error);
        }
      );
    }

    if (this.isAdmin || this.isTeacher || this.isDirector || this.isCoadmin) {
      this._teacherService.getTeacher().subscribe(
        teacherlist => {
          this.active_teachers_list = teacherlist.payload;
          
        },
        error => {
          console.error("Error while getting teachers list. stack: ", error);
        }
      );
    }
  }

  getApprovalTeacher() {
    console.log(this.isAdmin);
    if (this.isAdmin === true) {
      this._approvalService.getTeacherAproval().subscribe(
        teacherapprovallist => {
          if (teacherapprovallist) {
            this.teachers_approval_list = teacherapprovallist.payload;
           
          } else {
            this.teachers_approval_list = null;
            console.log(
              "teacher approval list Null:",
              this.teachers_approval_list
            );
          }
        },
        error => {
          console.error(
            "Error while getting teachers approval list. stack: ",
            error
          );
        }
      );
    }
  }

  onTeacherApprovalClick(id: any) {
    
    this._approvalService.approveTeacher(id).subscribe(
      data => {
        console.log("send to approve: ", data.payload);
      },
      error => {
        console.error("Error while sending teachers approval. stack: ", error);
      }
    );
  }

  onTeacherRejectClick(id: any) {
    console.log("REJECT AT COMPONENT IS CLICKED.........");
    this._approvalService.rejectTeacher(id).subscribe(
      data => {
        console.log("send to reject: ", data.payload);
      },
      error => {
        console.error("Error while sending teachers rejection. stack: ", error);
      }
    );
  }

  getProfileAndLecture(uid) {
    this._lectureService.getLectureById(uid).subscribe(
      data => {
        this.lecture_data = data.payload;
       
        this.getSelectedTeacherInfo(uid);
      },
      error => {
        console.error("Error while getting leture data. stack: ", error);
      }
    );
  }

  getSelectedTeacherInfo(id) {
    for (const teacher of this.active_teachers_list) {
      if (teacher.uid === id) {
        this.selected_teacher_info = teacher;
        
      }
    }
  }

  setCategory(cat) {
    this.category = cat;
  }

  onSubmit(category) {
    
    // this.category, this.userForm.value.subject, this.to, this.userForm.value.message
    this._listInboxItemsService
      .postMessage(this.userForm.value)
      .subscribe(data => {
        if (data.status === 201) {
          this.report = data.payload;
          this.toasterService.Success("Report Generated Successfully");
          $("#m_modal_5").modal("hide");
          this.userForm.reset();
        } else {
          this.toasterService.Error("Report Not Generated!");
        }
      });

    /*this.report = data.payload);
    console.log('report generate' + this.report);
    if (this.userForm.value.subject === null && this.userForm.value.message === null) {
      alert('Please Enter Subject & message');
    } else {
      // alert('Report Generated Successfully');
      //$('#m_modal_5').modal('hide');
    }
    this.userForm.value.reset();*/
  }

  getTeacherDetails(teacher) {
    this.teacher_detail = teacher;
    console.log('teacher',this.teacher_detail)
    this.getProfileAndLecture(teacher.uid);
    $("#m_modal_4").modal("show");
    
  }

  /** Get Security Stats Data */
  getSecurityStats() {
    this._security.getSecurityStats().subscribe(securityStats => {
      if (securityStats.status === 200) {
        this.securityStats = securityStats.payload;
        
      }
    });
  }
}
