import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { ApprovalService } from '../../../_services/approval.service';
import { IdentityService } from '../../../_services/identity.service';
import { LectureService } from '../../../_services/lecture.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ListInboxItemsService } from '../../../_services/listInboxItems.service';
import { SecurityService } from '../../../_services/security.service';
import { ClerkService } from '../../../_services/clerk.service';

declare var $: any;
@Component({
  selector: 'app-clerk',
  templateUrl: './clerk.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['clerk.component.css']
})
export class ClerkComponent implements OnInit {

  public active_clerk_list: any;
  public clerk_approval_list: any;
  public isAdmin: Boolean = false;
  public isTeacher: Boolean = false;
  public isDirector: Boolean = false;
  public isCoadmin: Boolean = false;
  public class: any;
  public role: any;
  public leture_data: any;
  public selected_teacher_info: any;
  public category_list: any = ['complain', 'Suggestion', 'Praise', 'Feedback'];
  public category: any;
  public form_values: any;
  public report: any;
  public to: any;
  public clerk_detail: any;
  public securityStats: any;

  userForm: FormGroup;
  constructor(
    private _script: ScriptLoaderService,
    private _approvalService: ApprovalService,
    private _identityService: IdentityService,
    private _lectureService: LectureService,
    private formBuilder: FormBuilder,
    private _listInboxItemsService: ListInboxItemsService,
    private _clerkService: ClerkService,
    private _security: SecurityService) {
      this.class = this._identityService.class;
      this.role = this._identityService.role;
      this.isAdmin = this._identityService.isAdmin;
      this.isTeacher = this._identityService.isClassTeacher;
      this.isDirector = this._identityService.isDirector;
      this.isCoadmin = this._identityService.isCoadmin;
    }

  ngOnInit() {

    this.getActiveClerk();
    if (this.isAdmin === true) {
      this.getApprovalClerk();
      this.getSecurityStats();
    }



    this.userForm = this.formBuilder.group({
      subject: [],
      message: []
    });

  }

  getActiveClerk() {    
    if (this.isAdmin || this.isTeacher || this.isDirector || this.isCoadmin) {
      this._clerkService.getClerk().subscribe(clerklist => {
        this.active_clerk_list = clerklist.payload;
      },
        error => {
          console.error('Error while getting teachers list. stack: ', error);
        });
    }
  }

  getApprovalClerk() {
    if (this.isAdmin === true) {
      this._approvalService.getClerkAproval().subscribe(teacherapprovallist => {
        if (teacherapprovallist) {
          this.clerk_approval_list = teacherapprovallist.payload;
        } else {
          this.clerk_approval_list = null;
        }
      },
        error => {
          console.error('Error while getting teachers approval list. stack: ', error);
        });
    }
  }

  onTeacherApprovalClick(id: any) {
    this._approvalService.approveTeacher(id).subscribe(data => {
    },
      error => {
        console.error('Error while sending teachers approval. stack: ', error);
      });
  }

  onTeacherRejectClick(id: any) {
    this._approvalService.rejectTeacher(id).subscribe(data => {
    },
      error => {
        console.error('Error while sending teachers rejection. stack: ', error);
      });
  }

  onSubmit(category) {
    // this.category, this.userForm.value.subject, this.to, this.userForm.value.message
    this._listInboxItemsService.onSubmit(this.userForm.value, this.category).subscribe(data =>
      this.report = data.payload);
    if (this.userForm.value.subject === null && this.userForm.value.message === null) {
      alert('Please Enter Subject & message');
    } else {
      // alert('Report Generated Successfully');
      //$('#m_modal_5').modal('hide');
    }
    this.userForm.value.reset();
  }

  getClerkDetails(clerk)
  { 
    this.clerk_detail = clerk;
    $('#m_modal_4').modal('show'); 
  }

  /** Get Security Stats Data */
  getSecurityStats() {
    this._security.getSecurityStats().subscribe(securityStats => {
      if(securityStats.status === 200){
        this.securityStats = securityStats.payload;
      }
    });
  }


}
