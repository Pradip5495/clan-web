
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

// Models
import { User } from '../../../_models/user';

// Services
import { IdentityService } from '../../../_services';
import { ProfileService } from '../../../_services/profile.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from './must-match.validator';
import { ListInboxItemsService } from '../../../_services/listInboxItems.service';
import { ToasterService } from '../../../_services/toaster.service';

declare var $: any;

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  public profile_date: any;
  passwordForm: FormGroup;
  adminReportForm: FormGroup;
  userForm: FormGroup;

  get editEnabled(): boolean {
    return this._editEnabled;
  }

  set editEnabled(value: boolean) {
    this._editEnabled = value;
  }

  public profile: User;
  public data: any;
  public role: string;
  public _editEnabled: boolean;
  public category: any;
  public report;
  public category_list: any = ['complain', 'Suggestion', 'Praise', 'Feedback'];
  public to;

  constructor(private identityService: IdentityService,
    private formBuilder: FormBuilder,
    private _listInboxItemsService: ListInboxItemsService,
    private toasterService: ToasterService,
    private _profileService: ProfileService) {
    this.profile = this.identityService.identity;
    this.role = this.identityService.role;
    this._editEnabled = false;
  }

  ngOnInit() {
    this.getProfile();
    this.passwordForm = this.formBuilder.group({
      currentPassword: ['', Validators.required],
      newPassword: [
        '',
        [
          Validators.required,
          Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')
         ]
      ],
      confirmPassword: ['', Validators.required],
    },{
      validator: MustMatch('newPassword', 'confirmPassword')
    });
    this.adminReportForm = this.formBuilder.group({
      subject: ['', Validators.required],
      message: ['', Validators.required],
      categoryList: ['', Validators.required]
    });
  }

  addCssClass(isapplyhalfview: boolean, isapplyfullview: boolean) {

    let classes = {
      'col-xl-6 col-lg-12': isapplyhalfview,
      'col-xl-12': isapplyfullview
    }
    return classes;
  }

  getProfile() {
    this._profileService.getProfile().subscribe(profile_date => {
      this.profile_date = profile_date.payload;
      console.log('profile data:', this.profile_date);
      for (let data of this.profile_date) {
        console.log(data.uid);
      }

    },
      error => {
        console.error('Error while getting profile data. stack: ', error);
      });
  }

  putProfile() { }

  changePassword() {
    //  console.log(this.category);
    console.log(this.passwordForm.value.currentPassword);
    console.log(this.passwordForm.value.newPassword);
    this._profileService.changePassword(this.passwordForm.value.currentPassword, this.passwordForm.value.newPassword).subscribe(data =>
      this.data = data.payload);
    if (this.passwordForm.valid) {
      //   alert('Report Generated Successfully!');
      // this.getChild();
    }
    this.passwordForm.reset();

  }

  setCategory(cat) {
    this.category = cat;
  }

  onSubmit(category) {
    console.log(this.category);
    console.log(this.adminReportForm.value.subject);
    console.log(this.adminReportForm.value.message);

    console.log('this.adminReportForm: ', this.adminReportForm);
    // this.category, this.userForm.value.subject, this.to, this.userForm.value.message
    this._listInboxItemsService.onSubmit(this.adminReportForm, this.category).subscribe(data => {
      if (data.status === 201) {
        this.report = data.payload;
        this.toasterService.Success('Report Generated Successfully');
        console.log("Report Generated Successfully", data);
        $("#m_modal_6").modal("hide");
        this.adminReportForm.reset();
      }
      else {
        this.toasterService.Error("Report Not Generated!");
      }
    });
  }

}
