import {
  Component,
  OnInit,
  ViewEncapsulation,
  AfterViewInit
} from "@angular/core";
import {
  ActivatedRoute,
  Router,
  ParamMap,
  NavigationExtras
} from "@angular/router";
import { ScriptLoaderService } from "../../../_services/script-loader.service";
import { StudentService } from "../../../_services/student.service";
import { AttendanceService } from "../../../_services/attendance.service";
import { PerformanceByClassService } from "../../../_services/performanceByClass.service";
import { ApprovalService } from "../../../_services/approval.service";
import { ToasterService } from "../../../_services/toaster.service";
import { IdentityService } from "../../../_services/identity.service";
import { ClassService } from "../../../_services/class.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ListInboxItemsService } from "../../../_services/listInboxItems.service";
import "rxjs/add/operator/map";

import { User } from "../../../_models/user";
import { isEmpty } from "rxjs/operator/isEmpty";
import { count } from "rxjs/operator/count";

declare const $: any;
declare const mApp, moment, Chartist, todayDate;
@Component({
  selector: "app-students",
  templateUrl: "./students.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./students.component.css"]
})
export class StudentsComponent implements OnInit, AfterViewInit {
  get class_data(): any {
    return this._class_data;
  }

  set class_data(value: any) {
    this._class_data = value;
  }

  get class_value(): any {
    return this._class_value;
  }

  set class_value(value: any) {
    this._class_value = value;
  }

  get isClassTeacher(): boolean {
    return this._isClassTeacher;
  }

  set isClassTeacher(value: boolean) {
    this._isClassTeacher = value;
  }

  get approval_list(): any {
    return this._approval_list;
  }

  set approval_list(value: any) {
    this._approval_list = value;
  }

  get students_list(): any {
    return this._students_list;
  }

  set students_list(value: any) {
    this._students_list = value;
  }

  private _students_list: any;
  private _approval_list: any;
  private _isClassTeacher = false;
  private _class_data: any;
  private _class_value: any = null;
  public category_list: any = ["complain", "Suggestion", "Praise", "Feedback"];
  public number = 47;
  private location: any;
  public to: any;
  public category: any;
  public studentData: any;
  public msgTo: any;
  public parentData: any;
  public parentDataUid: any;
  public report: any;
  public calendar_date: any;
  public calendar_btn_status = false; //Calendar Next & Previous button status
  public colors;
  public className: any;
  public currentDay: any;
  public totalStudent: any;
  public presentStudent: any;
  public absentStudent: any;
  public allStudentAttendanceView: any = false;
  public allClassView: any = false;
  public studentWeekAttendance: any;
  public schoolPerformanaceStatus: any = false;
  public attendance1: any;
  public attendance2: any;
  public studentAttendance: any;
  public classAttendanceView: any = false;
  public role: any;
  public present: any = 0;
  public absent: any = 0;
  public leave: any = 0;
  public day_count: any = 0;
  public present_percent: any = 0;
  public absent_percent: any = 0;
  public leave_percent: any = 0;
  public uid: any;
  public student_performance: any;
  public subjectList: any;
  private schoolCode: any;

  userForm: FormGroup;

  constructor(
    private _script: ScriptLoaderService,
    private _studentService: StudentService,
    private _router: Router,
    private _approvalService: ApprovalService,
    private _identityService: IdentityService,
    private _classService: ClassService,
    private router: Router,
    private toasterService: ToasterService,
    private formBuilder: FormBuilder,
    private _listInboxItemsService: ListInboxItemsService,
    private _performance: PerformanceByClassService,
    private _attendanceService: AttendanceService
  ) {
    this.location = _router.url;
    this.schoolCode = this._identityService.identity.schoolCode;
  }

  ngOnInit() {
    const todayDate1 = moment().startOf("day");
    const YM = todayDate1.format("YYYY-MM-DD");
    this.currentDay = todayDate1.format("YYYY-MM-DD");
    this.getClass();
    //  this.getStudent(event);
    this.userForm = this.formBuilder.group({
      to: ['', Validators.required],
      category: ['', Validators.required],
      title: ['', Validators.required],
      description: ['', Validators.required]
    });
    this._isClassTeacher = this._identityService.isClassTeacher;

    if (this.isClassTeacher) {
      this.class_value = this._identityService.class;

      this.getInitialStudent(this.class_value);
    }
  }

  ngAfterViewInit() {
    this._script.loadScripts("app-teachers", [
      "assets/demo/default/custom/components/calendar/basic.js"
    ]);
  }

  getInitialStudent(event) {
    console.log('event',event);
    if (this.class_value) {
      this._studentService.getStudentByClass(event).subscribe(
        students_list => {
          console.log('listofstud',students_list);
          if (students_list) {
            this.students_list = students_list.payload;
          } else {
            
          }
        },
        error => {
          console.error("Error while getting student list. stack: ", error);
        }
      );
      this._approvalService.getStudentAprovalByClass(event).subscribe(
        data => {
          if (data) {
            this.approval_list = data.payload;
          } else {
            this.approval_list = null;
            
          }
        },
        error => {
          console.error(
            "Error while getting student's approval. stack: ",
            error
          );
          this.toasterService.Error("Error while getting student's approval");
        }
      );
    }
  }

  getClass() {
    this._classService.getClass().subscribe(
      class_data => {
        console.log('classdata',class_data);
        this.class_data = class_data.payload;
        
      },
      error => {
        console.error("Error while getting class data. stack: ", error);
        this.toasterService.Error("Error while getting class data.");
      }
    );
  }

  getStudent(event: any) {
    this.class_value = event;
    console.log('data',this.class_value)
    this._studentService.getStudentByClass(event).subscribe(
      students_list => {
        console.log('studentdata34',students_list);
        this.students_list = students_list.payload;
        
      },
      error => {
        console.error("Error while getting student list. stack: ", error);
      }
    );
    this._approvalService.getStudentAprovalByClass(event).subscribe(
      data => {
        if (data != null) {
          this._approval_list = data.payload;
         
        } else {
          this._approval_list = null;
        }
      },
      error => {
        console.error("Error while getting student's approval. stack: ", error);
        this.toasterService.Error("Error while getting student's approval");
      }
    );
  }
  gotoAttendace(student: any, student_uid: any) {
    localStorage.setItem("StudentProfile", JSON.stringify(student));
    this.router.navigate(["attendance/student/" + student_uid]);
  }
  setCategory(cat) {
    this.category = cat;
  }

  sendMessageModalData(studentDetails) {
    this.studentData = studentDetails;
    this.parentData = studentDetails.father;
    this.parentDataUid = this.parentData.uid;
    
    $("#m_modal_5").modal("show");
  }

  setSendTo(sendToUid) {
    this.msgTo = sendToUid;
  }

  onSubmit(category) {
    let formData = this.userForm.value;
    formData.to = this.msgTo;
    this._listInboxItemsService.postMessage(formData).subscribe(
      data => {
        if (data.status == 201) {
          this.report = data.payload;
          this.toasterService.Success("Successfully Submitted!");
          $("#m_modal_5").modal("hide");
          this.userForm.reset();
        } else {
          
          this.toasterService.Error("Not Submitted!");
        }
      },
      error => {
        this.toasterService.Error("Error while Submitting the data");
        console.error("Error while Submitting the data");
      }

      /* (this.report = data.payload)*/
    );
    if (
      this.userForm.value.subject === null &&
      this.userForm.value.message === null
    ) {
      alert("Please Enter Subject & message");
    } else {
      // alert('Report Generated Successfully');
      $("#m_modal_5").modal("hide");
    }
    this.userForm.reset();
  }

  getStudentDetails(studentDetails) {
    this.studentData = studentDetails;
    this.uid = this.studentData.uid;
  
    if (this.studentData) {
      $("#m_modal_4").modal("show");
      this.getAttendanceforstudent();
      this.getStudentPerformanceBySubject();
    }
  }

  getAttendanceforstudent() {
    const YMcan = this.calendar_date;
    const todayDate = moment().startOf("day");
    this.attendance1 = [];
    this.calendar_btn_status = true;
    const YESTERDAY = todayDate
      .clone()
      .subtract(1, "month")
      .format("YYYY-MM-DD");
    const YM = todayDate.format("YYYY-MM-DD");
    
    this.calendar_date = YM;
    this._attendanceService
      .getAttendanceForStudent(this.uid, YM)
      .subscribe(attendance => {
        
        if (attendance !== null) {
          this.attendance1 = attendance.payload;
          
          this.present = 0;
          this.absent = 0;
          this.present_percent = 0;
          this.absent_percent = 0;
          this.leave_percent = 0;
          for (const a of this.attendance1) {
            this.day_count++;
            if (a.isPresent) {
              this.present++;
            } else if (!a.isPresent && !a.onLeave) {
              this.absent++;
            } else if (!a.isPresent && a.onLeave) {
              this.leave++;
            }
          }
          this.present_percent = (
            (this.present / this.day_count) *
            100
          ).toFixed(2);
          this.absent_percent = ((this.absent / this.day_count) * 100).toFixed(
            2
          );
          this.leave_percent = ((this.leave / this.day_count) * 100).toFixed(2);
          
          $("#m_calendar_event").fullCalendar("destroy");
          $("#m_calendar_event").fullCalendar("render");
          this.attendanceChart();
          this.loadCalender(this.attendance1);
        } else {
          this.attendance1 = [];
          this.present = 0;
          this.absent = 0;
          this.present_percent = 50;
          this.absent_percent = 25;
          this.leave_percent = 25;
          this.attendanceChart();
          this.loadCalender(this.attendance1);
        }
      });
  }

  /** Student Attendace show by Calendar */
  loadCalender(event) {
    const eventsArr = [];
    const recall_value = 1;
    let recall_status = false;
    for (const e of event) {
      /*console.log('student attendance', e);*/
      eventsArr.push({
        title: e.title,
        start: e.date,
        description: e.description,
        className: "m-fc-event--light m-fc-event--solid-warning"
      });
    }
    if ($("#m_calendar_event").length === 0) {
      return;
    }
    const YMcan = this.calendar_date;
    /*const todayDate = moment(YMcan).format('YYYY-MM-DD');*/
    const todayDate = moment(YMcan).startOf("day");
    const YESTERDAY = todayDate
      .clone()
      .subtract(1, "month")
      .format("YYYY-MM-DD");
    $("#m_calendar_event").fullCalendar({
      header: {
        left: "prev",
        center: "title",
        right: "next" // agendaWeek,agendaDay,listWeek
      },
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      navLinks: false,
      defaultDate: moment(todayDate),
      showNonCurrentDates: false,
      fixedWeekCount: false,
      dayRender: function(date, cell) {
        const today = $.fullCalendar.moment();
        for (const eventdata of event) {
          const eventdate = eventdata.date;
          recall_status = true;
          const calendarDates = date._d;
          const calendarDate = moment(calendarDates).format("YYYY-MM-DD");
          const event_date = moment(eventdate).format("MM");
          const days = moment(calendarDates).format("DD");
          const calendar_date = moment(calendarDate).format("MM");
          if (calendarDate === eventdate) {
            $("td")
              .find("[data-date='" + calendarDate + "']")
              .css("color", "white");
            if (eventdata.isPresent) {
              cell.css("background", "green");
              cell.css("color", "#fff");
              cell.css("border-radius", "40px");
              cell.css("text-align", "center");
              cell.addClass("class", "set-text-color");
            } else if (!eventdata.isPresent && !eventdata.onLeave) {
              cell.css("background", "red");
              cell.css("textColor", "white");
              cell.css("border-radius", "40px");
              cell.addClass("class", "set-text-color");
            } else if (!eventdata.isPresent && eventdata.onLeave) {
              cell.css("background", "#ffb822");
              cell.css("textColor", "white");
              cell.css("border-radius", "40px");
              cell.addClass("class", "set-text-color");
            }
          } else {
            /*  console.log('Else: ',calendarDate);
              console.log('calendarDates: ',calendarDate);
              console.log('calendarDates title: ',eventdata);*/
          }
        }
      },
      //events: eventsArr,
      eventRender: function(event, element) {
        if (element.hasClass("fc-day-grid-event")) {
          element.data("content", event.description);
          element.data("placement", "top");
          mApp.initPopover(element);
        } else if (element.hasClass("fc-time-grid-event")) {
          element
            .find(".fc-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        } else if (element.find(".fc-list-item-title").lenght !== 0) {
          element
            .find(".fc-list-item-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        }
      }
    });

    if (this.calendar_btn_status == true) {
      this.calendar_btn_status = false;
      $(".fc-prev-button").on("click", e => {
        this.getAttendanceforstudentByChange(null, true);
      });
      $(".fc-next-button").on("click", e => {
        this.getAttendanceforstudentByChange(null, false);
      });
    }

    if (recall_status) {
      $(".fc-prev-button").on("click", e => {
        this.getAttendanceforstudentByChange(null, true);
      });
      $(".fc-next-button").on("click", e => {
        this.getAttendanceforstudentByChange(null, false);
      });
    }
  }

  /** Student Subject Attendance Bar Chart */
  attendanceChart = function() {
    if ($("#m_chart_attendance").length === 0) {
      return;
    }

    const chart = Chartist.Pie(
      "#m_chart_attendance",
      {
        series: [
          {
            value: this.present_percent,
            className: "present",
            meta: {
              color: "#28a745"
            }
          },
          {
            value: this.absent_percent,
            className: "absent",
            meta: {
              color: "#dc3545"
            }
          },
          {
            value: this.leave_percent,
            className: "leave",
            meta: {
              color: "#ffb822"
            }
          }
        ]
      },
      {
        donut: true,
        donutWidth: 20,
        showLabel: true
      }
    );
   
    chart.on("draw", function(data) {
      
      if (data.type === "slice") {
        // Get the total path length in order to use for dash array animation
        const pathLength = data.element._node.getTotalLength();

        
        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
          "stroke-dasharray": pathLength + "px " + pathLength + "px"
        });

        // Create animation definition while also assigning an ID to the animation for later sync usage
        const animationDefinition = {
          "stroke-dashoffset": {
            id: "anim" + data.index,
            dur: 1000,
            from: -pathLength + "px",
            to: "0px",
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: "freeze",
            stroke: data.meta.color
          }
        };

        // // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if (data.index !== 0) {
          animationDefinition["stroke-dashoffset"]["begin"] =
            "anim" + (data.index - 1) + ".end";
        }

        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

        data.element.attr({
          "stroke-dashoffset": -pathLength + "px",
          stroke: data.meta.color
        });

        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
      }
    });
  };

  /** Student Subject perfomance Bar Chart */
  // performaceChart = function(subjectList) {
  //   console.log('performance Bar: ' + subjectList);
  //   if ($('#m_chart_performance').length === 0) {
  //     return;
  //   }
  //   const series = [];
  //   const label = [];
  //   console.log('Subjectlist:', subjectList);
  //   for (const e of subjectList) {
  //     console.log(e);
  //     series.push(e.performance);
  //     label.push(e.subject);
  //   }
  //   console.log(series);
  //   const chart_data = Chartist.Bar('#m_chart_performance', {
  //     labels: label,
  //     series: series
  //   }, {
  //       barwidth: 100,
  //       distributeSeries: true,
  //       low: 0,
  //       high: 100
  //     });
  //   chart_data.on('draw', function(data) {
  //     if (data.type === 'bar') {
  //       data.element.attr({
  //         style: 'stroke-width: 10%'
  //       });
  //     }
  //   });
  // };

  loadAttendence() {
    const SA = [];
    // this.getAttendance();
    const todayDate1 = moment().startOf("day");
    const YM = todayDate1.format("YYYY-MM-DD");
    
    for (const s of this.attendance1) {
      

      if (s.isPresent) {
        SA.push({
          title: "present",
          start: s.date,
          description: "present",
          className: "m-fc-event--danger m-fc-event--solid-warning"
        });
      } else if (!s.isPresent && !s.onLeave) {
        SA.push({
          title: "present",
          start: s.absent,
          description: "present",
          className: "m-fc-event--danger m-fc-event--solid-warning"
        });
      } else if (!s.isPresent && s.onLeave) {
        SA.push({
          title: "leave",
          start: s.date,
          description: "leave",
          className: "m-fc-event--light m-fc-event--solid-success"
        });
      }
    }

    if ($("#m_calendar_attendance").length === 0) {
      return;
    }

    const todayDate = moment().startOf("day");

    const month = todayDate.format("YYYY-MM");
    

    $("#m_calendar_attendance").fullCalendar({
      header: {
        left: "prev",
        center: "title",
        right: "next"
      },

      editable: true,
      eventLimit: true, // allow 'more' link when too many events
      navLinks: true,
      defaultDate: moment(todayDate),

      events: SA,

      eventRender: function(event, element) {
        if (element.hasClass("fc-day-grid-event")) {
          element.data("content", event.description);
          element.data("placement", "top");
          mApp.initPopover(element);
        } else if (element.hasClass("fc-time-grid-event")) {
          element
            .find(".fc-title")
            .append(
              '<div class= "fc-description">' + event.description + "</div>"
            );
        } else if (element.find(".fc-list-item-title").lenght !== 0) {
          element
            .find(".fc-list-item-title")
            .append(
              '<div class= "fc-description">' + event.description + "</div>"
            );
        }
      }
    });
  }

  getAttendanceforstudentByChange(
    recall_value: any,
    isPreviousMonthAttandance: boolean
  ) {
    const todayDate1 = moment().startOf("day");
    const YM = isPreviousMonthAttandance
      ? moment(this.calendar_date)
          .subtract(1, "month")
          .format("YYYY-MM-DD")
      : moment(this.calendar_date)
          .add(1, "month")
          .format("YYYY-MM-DD");
    this.calendar_date = YM;
    
    this._attendanceService
      .getAttendanceForStudent(this.uid, YM)
      .subscribe(attendance => {
       
        if (attendance !== null) {
          this.attendance2 = attendance.payload;
       
          this.present = 0;
          this.absent = 0;
          this.leave = 0;
          this.present_percent = 0;
          this.absent_percent = 0;
          this.leave_percent = 0;
          this.day_count = 0;
          for (const a of this.attendance2) {
            this.day_count++;
            if (a.isPresent) {
              this.present++;
            } else if (!a.isPresent && !a.onLeave) {
              this.absent++;
            } else if (!a.isPresent && a.onLeave) {
              this.leave++;
            }
          }
          
          this.present_percent = (
            (this.present / this.day_count) *
            100
          ).toFixed(2);
          this.absent_percent = ((this.absent / this.day_count) * 100).toFixed(
            2
          );
          this.leave_percent = ((this.leave / this.day_count) * 100).toFixed(2);
          $("#m_calendar_event").fullCalendar("destroy");
          $("#m_calendar_event").fullCalendar("render");
          this.attendanceChart();
          this.loadCalender(this.attendance2);
        } else {
          this.attendance2 = {};
          this.present = 0;
          this.absent = 0;
          this.leave = 0;
          this.day_count = 0;
          this.present_percent = 50;
          this.absent_percent = 25;
          this.leave_percent = 25;
          this.attendanceChart();
          this.loadCalender(this.attendance2);
        }
      });
  }

  /** Get Student Examination Data */
  getStudentPerformanceBySubject() {
    this._performance
      .getStudentPerfomanceBySubject(this.uid, this.schoolCode)
      .subscribe(
        subjectList => {
          this.subjectList = subjectList.payload;
         
          this.performaceChart(this.subjectList);
        },
        error => {
          console.error("Error while getting exam list. stack: ", error);
        }
      );
  }

  /** Student Subject perfomance Bar Chart */
  performaceChart = function(subjectList) {
    if ($("#m_chart_performance").length === 0) {
      return;
    }
    const series = [];
    const label = [];
    
    for (const e of subjectList) {
      
      series.push(e.performance);
      label.push(e.subject + "\n" + e.performance + "%");
    }
    ;
    const chart_data = Chartist.Bar(
      "#m_chart_performance",
      {
        labels: label,
        series: series
      },
      {
        barwidth: 100,
        distributeSeries: true,
        // low: 0,
        // high: 100,
        axisX: {
          showGrid: false
        },
        axisY: {
          showGrid: false,
          showLabel: false,
          offset: 0
        }
      }
    );
    chart_data.on("draw", function(data) {
      if (data.type === "bar") {
        data.element.attr({
          style: "stroke-width: 20px; "
        });
      }
    });
  };
}
