import { PipeTransform, Pipe } from "@angular/core";


@Pipe({
    name: 'filter'
})


export class LibraryFilterPipe implements PipeTransform{
    transform(bookTitle: any[], searchTerm: string){

        if(!bookTitle || !searchTerm)
        {
            return bookTitle;
        }

        return bookTitle.filter(books =>
             books.title.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1)

    }
}