import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LibraryComponent } from './library.component';

import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../default/default.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { LibraryFilterPipe } from './filter.pipe';
//import { JwPaginationComponent } from 'jw-angular-pagination';


const routes: Routes = [
  {
    'path': '',
    'component': DefaultComponent,
    'children': [
      {
        'path': '',
        'component': LibraryComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule, ReactiveFormsModule, FormsModule
  ],
  exports: [
    RouterModule,
    LibraryFilterPipe
  ],
  declarations: [
    LibraryComponent,
    LibraryFilterPipe,
    //JwPaginationComponent
    
  ]
})
export class LibraryModule { }
