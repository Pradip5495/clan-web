import {
  Component,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  AfterViewInit
} from "@angular/core";
import { Pipe, PipeTransform } from "@angular/core";
import { IdentityService } from "../../../_services/identity.service";
import { LibraryService } from "../../../_services/library.service";
import { FeeMasterService } from "../../../_services/fee-master.service";
import { ClassService } from "../../../_services/class.service";
import { ToasterService } from "../../../_services/toaster.service";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { StudentService } from "../../../_services/student.service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { TeacherService } from "../../../_services/teacher.service";
import { ScriptLoaderService } from "../../../_services/script-loader.service";
import { DomSanitizer } from "@angular/platform-browser";
import { LibraryFilterPipe } from "./filter.pipe";

declare var $: any;
declare const mApp, moment, Chartist, todayDate;
@Component({
  selector: "app-library",
  templateUrl: "./library.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./library.component.css"]
})
export class LibraryComponent implements OnInit {
  userForm: FormGroup;



  items = [];
  pageOfItems: Array<any>;

  public bookList: any = [];
  public bookDetailList: any = [];
  public borrowList: any;
  public libraryStats: any = true;
  public searchTerm: string;
  public data: any;
  public class_data: any;
  public book1: any;
  public book_Info: any = true;
  public book2: any;
  id: 0;
  startIndex = 0;
  lastIndex = 6;
  public bookDetails: any = false;
  public bookDetailsTable: any = true;
  public icons: any = false;
  public classContentView: any = false;
  public classBorrowView: any = false;
  public bookDetailsList: any = false;
  public libraryStatsData: any;
  public image_file: File = null;
  public autherList: any;
  public borrowBook: any;
  public borrowerlist: any;
  public classList: any;
  public classView: any = true;
  public currentDate: any;
  public studentClass: any;
  public bookReturnDetails: any;
  public studentView: any = false;
  public studentList: any;
  public studentBorrowData: any;
  public studentDetails: any;
  public teacherDetails: any;
  public studentName: any;
  public teacherName: any;
  public bookListArray: Array<any> = [];
  public pageNo: any;
  public profile: any;
  private role: any;
  public router;
  showmorebutton = true;
  public bookListView: any = false;
  private str;
  private uid;

  bookCount = 1;

  content:any[]=new Array();
  counter:number;

  show = 6;

  bookForm: FormGroup;
  @ViewChild("fileInput") fileInput;

  borrowForm: FormGroup;
  bookReturnForm: FormGroup;
  authorForm: FormGroup;

  constructor(
    private _library: LibraryService,
    private formBuilder: FormBuilder,
    private toasterService: ToasterService,
    private _classService: ClassService,
    private route: ActivatedRoute,
    private _identity: IdentityService,
    private _feesMaster: FeeMasterService
  ) {
    this.counter=0;
    //this.getMoreBooks();
    this.role = this._identity.role;
    const todayDate1 = moment().startOf("day");
    this.currentDate = moment(todayDate1).format("YYYY-MM-DD");
  }

  ngOnInit() {
    this.id = 0;

    this.items = Array(20)
      .fill(0)
      .map((x, i) => ({ id: i + 1, name: `Item ${i + 1}` }));

    this.bookForm = this.formBuilder.group({
      //imageFile: [null, Validators.required],
      title: ["", Validators.required],
      isbn: [
        "",
        [
          Validators.required,
          Validators.minLength(15),
          Validators.maxLength(15)
        ]
      ],
      authorCode: ["", Validators.required],
      category: [],
      pages: ["", Validators.required],
      description: [],
      noOfCopies: ["", Validators.required],
      bookCoverLocation: [null, Validators.required]
    });

    if (this.role == "student") {
      this.profile = this._identity.identity;

      this.showBorrowBookDetails(this.profile);
    }

    this.borrowForm = this.formBuilder.group({
      borrowerCode: ["", Validators.required],
      bookCode: []
    });

    this.authorForm = this.formBuilder.group({
      authorName: ["", Validators.required]
    });

    this.bookReturnForm = this.formBuilder.group({
      uidBorrow: [],
      isAvailable: []
    });

    this.getAllBooks(this.id);
    this.getClass();

    this.getLibraryStats();
    this.getAutherList(); //Fetch All Auther List
    this.getClasses();

    if (this.route.params["value"].book) {
      this.book1 = this.route.params["value"].book;
      this.bookListView = true;
    } else {
      this.bookListView = false;
    }
  }
  updateIndex(pageIndex) {
    this.startIndex = pageIndex * 3;
    this.lastIndex = this.startIndex + 3;
  }
  getArrayFromNumber(length) {
    return new Array(length);
  }

  onChangePage(bookList: Array<any>) {
    // update current page of items
    this.bookList = bookList;
  }

  getAllBooks(id) {
    this._library.getBooks(id).subscribe(bookList => {
      //this.bookList = [];
      if (bookList.status === 200) {
        this.bookList = bookList.payload;
      }
    });
  }

  getClass() {
    this.studentBorrowData = [];
    this._classService.getClass().subscribe(
      class_data => {
        console.log('singleclass',class_data.payload)
        if (class_data.status == 200) {
          this.class_data = class_data.payload;
          this.classContentView = true;
          this.classBorrowView = false;
          this.bookDetails = false;
          this.studentView = false;
        }
      },
      error => {
        console.error("Error while getting class data. stack: ", error);
      }
    );
  }

  getClasses() {
    this._classService.getClasses().subscribe(
      classList => {
        console.log('classlist',classList);
        if (classList.status == 200) {
          this.classList = classList.payload;
        }
      },
      error => {
        console.error("Error while getting class data. stack: ", error);
      }
    );
  }
  getBorrowList(std) {
    this.bookDetails = false;
    this._library.getBorrowList(std).subscribe(borrowList => {
      if (borrowList.status == 200) {
        this.borrowList = borrowList.payload;
      }
    });
  }

  onFileChange(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = () => {
        this.bookForm.get("bookCoverLocation").setValue(event.target.files[0]);
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  onSubmit() {
    const formData: FormData = new FormData();
    formData.append(
      "bookCoverLocation",
      this.bookForm.get("bookCoverLocation").value
    );
    formData.append("title", this.bookForm.get("title").value);
    formData.append("isbn", this.bookForm.get("isbn").value);
    formData.append("category", this.bookForm.get("category").value);
    formData.append("pages", this.bookForm.get("pages").value);
    formData.append("authorCode", this.bookForm.get("authorCode").value);
    formData.append("description", this.bookForm.get("description").value);
    formData.append("noOfCopies", this.bookForm.get("noOfCopies").value);
    this._library.postNewBook(formData).subscribe(newBook => {
      if (newBook.status === 201) {
        this.toasterService.Success("Successfully book added");
        $("#m_modal_8").modal("hide");
        this.getAllBooks(this.id);
      } else {
        this.toasterService.Error("book Not added!");
      }
    });
  }

  getBorrowByClass(std_div) {
    this.studentClass = std_div;
    this._library.getBorrowList(std_div).subscribe(borrowList => {
      console.log('borrowerlist',borrowList)
      if (borrowList.status == 200) {
        this.borrowList = borrowList.payload;
        this.classContentView = false;
        this.classBorrowView = true;
        this.bookDetails = false;
      }
    });
  }

  // getlist(){
  //   this.bookDetails = true;
  //   this.bookList = true;
  //   this.bookDetailsList = false;
  // }

  getBorrowBook(bookDetails) {
    console.log('clicked');
    this.borrowBook = bookDetails;
    $("#m_modal_9").modal("show");
  }

  getUserList($event) {
    if ($event.srcElement.value === "teacher") {
      this._library.getStaffList().subscribe(staffList => {
        console.log('staffData',staffList.payload);
        if (staffList.status == 200) {
          this.classView = false;
          this.borrowerlist = staffList.payload;
        }
      });
    } else if ($event.srcElement.value === "student") {
      this.classView = true;
    }
  }

  getStaffList() {
    this.studentBorrowData = [];
    this._library.getStaffList().subscribe(staffList => {
      console.log('staffdata', staffList);
      if (staffList.status === 200) {
        this.borrowerlist = staffList.payload;
        if (this.borrowerlist.length > 0) {
          this.showBorrowBookDetails(this.borrowerlist[0]);
        }
      }
    });
  }

  loading1 = false;
  

  getStudentList(studClass) {
    this.loading1 = true;
    console.log('studclass',studClass);
    this._library
      .getStudentList(studClass)
      .subscribe(studentData => {
        console.log('studdata',studentData)
        if (studentData.status == 200) {
          this.loading1 = false;
          console.log('studdata',studentData)
          this.borrowerlist = studentData.payload;
          
        }
      });
  }

  getClassStudent(studClass) {
    this.studentView = true;
    this.classContentView = false;
    this._library.getStudentList(studClass).subscribe(studentData => {
      console.log('student1', studentData.payload);
      if (studentData.status === 200) {
        this.studentList = studentData.payload;
        if (this.studentList.length > 0) {
          this.showBorrowBookDetails(this.studentList[0]);
        }
      }
    });
  }

  loading = false;
  

  onSubmitBorrow() {
    this.loading = true;
    let borrowForm = this.borrowForm.value;
    borrowForm.bookCode = this.borrowBook.uidBook;
    this._library.postBorrowBook(borrowForm).subscribe(borrowData => {
      console.log('onsubmit',borrowData)
      this.loading = false;
      if (borrowData.status === 201) {
        this.toasterService.Success("Book borrowed Successfully");
        this.getAllBooks(this.id);
        this.borrowForm.reset();
        $("#m_modal_9").modal("hide");
      } else {
        this.toasterService.Error("Book Not borrowed!");
      }
    });
  }

  onSubmitAuthor() {
    //const formData: FormData = new FormData();
    let authorForm = this.authorForm.value;
    //authorForm.bookCode = this.authorForm.uidBook;
    this._library.postAuthor(authorForm).subscribe(authorData => {
      if (authorData.status === 201) {
        this.toasterService.Success("Successfully Author added");
        //this.getAllBooks(this.id);
        this.authorForm.reset();
        $("#m_modal_15").modal("hide");
      } else {
        this.toasterService.Error("Book Not borrowed!");
      }
    });
  }

  bookReturn(bookDetails) {
    let bookReturn = this.bookReturnForm.value;
    bookReturn.uidBorrow = bookDetails.uidBorrow;
    bookReturn.isAvailable = true;
    this._library.updateReturnBook(bookReturn).subscribe(bookReturnDetails => {
      if (bookReturnDetails.status == 200) {
        this.bookReturnDetails = bookReturnDetails.payload;
        this.getBorrowByClass(this.studentClass);
      }
    });
  }

  studBookReturn(bookDetails) {
    let bookReturn = this.bookReturnForm.value;
    bookReturn.uidBorrow = bookDetails.uidBorrow;
    bookReturn.isAvailable = true;
    this._library.updateReturnBook(bookReturn).subscribe(bookReturnDetails => {
      if (bookReturnDetails.status == 200) {
        this.bookReturnDetails = bookReturnDetails.payload;
        this.showBorrowBookDetails(this.studentDetails);
      }
    });
  }
  getBookDetailsList() {
    this.bookDetailsList = true;
    //this.bookList = false;
  }

  bookGallery() {
    this.bookDetailsList = false;
    this.icons = true;
  }
  getLibraryStats() {
    this._library.getLibraryStats().subscribe(libraryStats => {
      this.libraryStats = libraryStats.payload;
      this.libraryChart();
    });
  }

  /** Get Auther List */
  getAutherList() {
    this._library.getAutherList().subscribe(autherList => {
      if (autherList.status == 200) {
        this.autherList = autherList.payload;
      }
    });
  }
  getBorrowerList(type) {
    if (type === "book") {
      this.bookDetailsTable = true;
    } else if (type === "defaulter") {
      if (this.bookList.returnDate != null) {
        this.bookDetailsTable = true;
      } else {
        this.bookDetailsTable = false;
      }
    }
  }

  showBookDetails(books_Information) {
    this.book_Info = true;
    this.book_Info = books_Information;
    $("#m_modal_14").show("modal");
  }

  showBorrowBookDetails(student) {
    console.log("borowerListStudent", student);
    this.studentDetails = student;
    this.studentName = student.name;
    this._library.getStudentBorrowBook(student.uid).subscribe(studentData => {
      if (studentData.status === 200) {
        this.studentBorrowData = studentData.payload;
      }
    });
  }
  /** Library stats Bar Chart */
  libraryChart = function() {
    if ($("#m_chart_library").length === 0) {
      return;
    }
    const series = [];
    const label = [];
    for (const e of this.libraryStats.libraryStats) {
      series.push(e.issuedBookCount);
      label.push(e.Month);
    }
    const chart_data = Chartist.Bar(
      "#m_chart_library",
      {
        labels: label,
        series: series
      },
      {
        barwidth: 100,
        distributeSeries: true,
        low: 0,
        // high: 100,
        axisX: {
          showGrid: false
        },
        axisY: {
          showGrid: false,
          showLabel: false,
          offset: 0
        }
      }
    );
    chart_data.on("draw", function(data) {
      if (data.type === "bar") {
        data.element.attr({
          style: "stroke-width: 5%; stroke: #e18215"
        });
      }
    });
  };

  
  /*getMoreBooks(){
    console.log(this.counter + 'dat size'+this.data.length)

    for(let i=this.counter+1;i<this.data.length;i++)
    {
    this.bookList.bookDetails.push(this.data[i]);
    if(i%10==0) break;
    }
    this.counter+=10;

  }*/

  getMoreBooks(){
    
    this.show = 3;
    
    this.showmorebutton = false;
    this.bookCount = 1;
       
  }

  getLessBooks(number){
    //this.getAllBooks(id + 1);
    /*this.show += 3;
    console.log("showmorebutton:",this.showmorebutton);
    this.showmorebutton = true;*/
    if(this.bookCount * 3 < number)
    { 
      this.show += 3;
      this.showmorebutton = false;
      this.bookCount += 1;
    }

    else{
      this.showmorebutton = true;
    } 
  }

  showMore(id){
    //this.show += 3;
    this.getAllBooks(id);
  }

  showLess(id){
    this.getAllBooks(id);
  }

  // studentBorrowBooklist(student) {
  //   this.studentDetails = student;
  //   this.studentName = student.name;
  //   this._library.getStudentBorrowBook(student.uid).subscribe(studentData => {
  //     if(studentData.status === 200){
  //       this.studentBorrowData = studentData.payload;
  //     }
  //   })
  // }
}
