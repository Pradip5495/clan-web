
import { AfterViewInit, Component, OnInit, ViewEncapsulation } from '@angular/core';

// Models
import { User } from '../../../_models/user';

// Services
import { IdentityService } from '../../../_services';
import { ListInboxItemsService } from '../../../_services/listInboxItems.service';
import { MessageService } from '../../../_services/message.service';
import { ConversationService } from '../../../_services/conversation.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  encapsulation: ViewEncapsulation.None,
})

export class MessagesComponent implements OnInit {

  public profile: User;
  public role: string;
  public uid: any;

  private complain_list: any;
  private suggestion_list: any;
  private praise_list: any;
  private feedback_list: any;
  private conversation: any;
  public sendmsg: any;


  public id: any;

  private fullview: String = 'col-xl-12';
  private halfview: String = 'col-xl-6 col-lg-12';
  private applyfullview: Boolean = true;
  private applyhalfview: Boolean = false;

  constructor(private identityService: IdentityService,
    private _listInboxItemsService: ListInboxItemsService,
    private _messageService: MessageService,
    private _conversionService: ConversationService) {

    this.profile = this.identityService.identity;
    this.role = this.identityService.role;
    this.uid = this.identityService.uid;
  }

  ngOnInit() {
    this.complainClick();
    this.userClick();
  }

  addCssClass(isapplyhalfview: boolean, isapplyfullview: boolean) {

    let classes = {
      'col-xl-6 col-lg-12': isapplyhalfview,
      'col-xl-12': isapplyfullview
    }
    return classes;
  }

  item_inbox_complain_click(id) {

    this.id = id;

    this.applyfullview = false;
    this.applyhalfview = true;

    console.log(id);

    this._messageService.getMessage(id).subscribe(data => {
      this.conversation = data.payload;

    },
      error => {
        console.error('Error while getting message. stack: ', error);
      });
  }
  userClick() {


  }

  complainClick() {
    this._listInboxItemsService.getListInboxItemsService('Complain').subscribe(complain_list => {
      this.complain_list = complain_list.payload;
      console.log('complain data:', this.complain_list);

      // for(let i of this.complain_list) {
      //   this.complain_message = this.getMessage(i.id);
      //   console.log('complain message: ');
      //
      //   if (this.complain_message){
      //     console.log(this.complain_message);
      //   }
      // }

    },
      error => {
        console.error('Error while getting complain data. stack: ', error);
      });


    // this.getMessage(this.complain_list.id);
  }
  getMessage(id: any): any {

    this._messageService.getMessage(id).subscribe(data => {
      this.conversation = data.payload;
      // console.log(this.message);

    },
      error => {
        console.error('Error while getting message. stack: ', error);
      });
  }

  postMessage(msg) {

    if (msg) {

      this._messageService.postMessage(this.id, msg).subscribe(data => {

        console.log('message is send');
        this.getMessage(this.id);


      },
        error => {
          console.error('Error while getting conversation list. stack: ', error);
        });

      this.sendmsg = '';
      this.getMessage(this.id);
    }
  }
}
