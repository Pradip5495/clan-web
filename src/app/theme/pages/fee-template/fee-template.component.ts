import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { ActivatedRoute, Router, ParamMap, NavigationExtras } from '@angular/router';

import { StudentService } from '../../../_services/student.service';
import { FeeTemplateService } from '../../../_services/fee-template.service';
import { FeesTypeService } from '../../../_services/fees-type.service';
import { ClassService } from '../../../_services/class.service';
import { IdentityService } from '../../../_services/identity.service';
import { isEmpty } from 'rxjs/operator/isEmpty';
import { FormGroup, FormBuilder, FormArray, FormControl, ValidatorFn, Validators } from '@angular/forms';

declare var $: any;
@Component({
  selector: 'app-fee-template',
  templateUrl: './fee-template.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./fee-template.component.css']
})
export class FeeTemplateComponent implements OnInit {

  public class_list: any;
  public colors: any;
  public contentView: any;
  public feeTemplateView: any;
  public feeTemplateData: any;
  public std: any;
  public templateList: any;
  public type_list: any;
  public totalAmount: number;
  //totalAmount: number;
  feeTemplate: FormGroup;
  subDetails: FormArray;  
  templateForm: FormGroup; 

  constructor(private _classService: ClassService,
    private _studentService: StudentService,
    private _identityService: IdentityService,
    private route: ActivatedRoute,
    private router: Router,
    private _feeTemplateService: FeeTemplateService,
    private _formBuilder: FormBuilder,
    private _feesTypeService: FeesTypeService,
  ) {
    this.totalAmount = 0;
  }

  ngOnInit() {
    if (this.route.params['value'].std) {
      this.std = this.route.params['value'].std;
      this.getFeeTemplate();
    } else {
      this.getClasses();
    }
    this.getFeesTypeList();

    this.feeTemplate = this._formBuilder.group({
      name: ['', Validators.required],
      class: [],
      dueDate: [],
      totalAmount: [],
      subDetails: this._formBuilder.array([
        this.addSubdetails()
      ])
    });


    this.templateForm = this._formBuilder.group({
      uidFeeTemplateMaster: [],
      name: [],
      class: [],      
      dueDate: [],
      totalAmount: [],      
      subDetails: this._formBuilder.array([
        this.addTemplateSub()
      ])
    });

  }

  /** Get All Class List By School*/
  getClasses() {
    const MethodName = 'getClassBySchool';
    this._classService.getClasses().subscribe(class_list => {
      this.class_list = class_list.payload;
      this.colors = ['#F68A22', '#EF5350', '#009587', '#F47F1F'];
      this.contentView = true;
    },
      error => {
        console.error(MethodName, 'Error while getting class data. stack: ', error);
      });
  }

  /** Get Fee Template By Class */

  getFeeTemplate() {
    const MethodName = 'getFeeTemplate';
    this._feeTemplateService.getFeeTemplate(this.std).subscribe(tamplateList => {
      this.feeTemplateView = true;
      this.contentView = false;
      if (tamplateList.status === 200) {
        this.templateList = tamplateList.payload;
      }
    });
  }

  /** Get Fee Template Sub Details */
  getSubDetails(data) {
    this.subDetails = data.subDetails;
    $('#subDetailModal').modal('show');
  }

  addSubdetails(): FormGroup {
    return this._formBuilder.group({
      type: '',
      uidFeeType: '',
      amount: '',
    });
  }

  addTemplateSub(): FormGroup {
    return this._formBuilder.group({
      type: '',
      uidFeeType: '',
      uidTemplateSubDetail: '',
      amount: ''
    });
  }

  addSubDetailsButtonClick(): void {
    this.subDetails = this.feeTemplate.get('subDetails') as FormArray;
    this.subDetails.push(this.addSubdetails());
  }

  /** Submit New Fee Template details */
  onSubmit() {
    let feeTemplateData = this.feeTemplate.value;
    feeTemplateData.class = this.std;
    feeTemplateData.totalAmount = this.totalAmount;
    console.info('New Fee Template Data: ', feeTemplateData);
    this._feeTemplateService.postFeeTemplate(this.feeTemplate.value).subscribe(feeTemplates => {
      if (feeTemplates.status === 201) {
        this.getFeeTemplate();
        $('#NewFeeTemplate').modal('hide');
      }
    });
  }

  /** Delete Fee Sub Details Row */
  deleteRow(index: number, subDetailsAmt) {
    this.totalAmount = +this.totalAmount - +subDetailsAmt.amount;
    this.subDetails.removeAt(index);
  }
  /** Get All Fee Type List */
  getFeesTypeList() {
    this._feesTypeService.getFeeTypes().subscribe(feestypelist => {
      this.type_list = feestypelist.payload;
    });
  }

  /** Total Amount calculate on change event */
  changeAmount($event, feeTemplate) {
    this.totalAmount = +this.totalAmount + +$event.target.value;
  }

  loadForm(templateData) {
    this.feeTemplateData = templateData;
    let amount = 0;
    this.totalAmount = 0;
    const updateformArray = this.templateForm.controls['subDetails'] as FormArray;
    // For Remove Previouse FormArray data
    while (updateformArray.length) {
      updateformArray.removeAt(updateformArray.length-1);
    }
    // For New Sub Details generate formArray 
    for (let line = 0; line < templateData.subDetails.length; line++) {
      amount = +amount + +templateData.subDetails[line].amount;
      const subdata = this._formBuilder.group({
        type: [templateData.subDetails[line].type],
        uidFeeType: [templateData.subDetails[line].uidFeeType],
        uidTemplateSubDetail: [templateData.subDetails[line].uidTemplateSubDetail],
        amount: [templateData.subDetails[line].amount]
      });
      updateformArray.push(subdata);
    }
    this.totalAmount = amount;
    this.templateForm.patchValue(templateData);
    $('#updateFeeTemplate').modal('show');
  }



  addSubDetail(): void {
    this.subDetails = this.feeTemplateData.subDetails as FormArray;
    this.subDetails.push(this.addSubdetails());
  }

  /** Delete Fee Sub Details Row */
  deleteUpdateRow(index: number, subDetailsAmt) {
    this.totalAmount = +this.totalAmount - +subDetailsAmt.amount;
    //this.feeTemplateData.subDetails.removeAt(index);
  }

  onUpdate() {
     let feeTemplateData = this.templateForm.value;
     feeTemplateData.totalAmount = this.totalAmount;
     this._feeTemplateService.putFeeTemplate(feeTemplateData).subscribe(feeTempate => {
      if (feeTempate.status === 200) {
      }
     });
  }

  selectchange($event, idx) {
    const tagId = '#type' + idx;
    const controlArray = this.templateForm.get('subDetails') as FormArray;
    controlArray.controls[idx].get('type').setValue($event.target.options[$event.target.selectedIndex].text);    
  }

  /** Delete Fee Sub Details Row */
  deleteSubDetailRow(index: number, subDetailsAmt) {
    const formArray = this.templateForm.controls['subDetails'] as FormArray;
    formArray.removeAt(index);
    const subdata = this.templateForm.value.subDetails;
    let amount = 0;
    for (let i = 0; i < subdata.length; i++) {
      amount = +amount + +subdata[i].amount;
    }
    this.totalAmount = amount;
  }

  /** Add Assign Fee Template Sub details Row  */
  addTemplateSubData(): void {
    this.subDetails = this.templateForm.get('subDetails') as FormArray;
    this.subDetails.push(this.addTemplateSub());
  }

  /** Delete Fee Template by uidFeeTemplateMaster */
  deleteFeeTemplate(uidFeeTemplateMaster) {
    this._feeTemplateService.deleteFeeTemplate(uidFeeTemplateMaster).subscribe(feeTData => {
      this.getFeeTemplate();
    });
  }

}
