

import { Component, OnInit, Input, ViewEncapsulation, AfterViewInit } from '@angular/core';

// Models
import { User } from '../../../_models/user';

// Services
import { IdentityService } from '../../../_services';
import { ListInboxItemsService } from '../../../_services/listInboxItems.service';
import { AssignService } from '../../../_services/assign.service';
import { Router } from '@angular/router';
import { PassengerService } from '../../../_services/passenger.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TransportService } from '../../../_services/transport.service';
import { ScriptLoaderService } from '../../../_services/script-loader.service';

@Component({
  selector: 'app-transport',
  templateUrl: './transport.component.html',
  encapsulation: ViewEncapsulation.None,
})

export class TransportComponent implements OnInit, AfterViewInit {

  public profile: User;
  public role: string;
  public uid: any;
  public coadmin: any;
  public number: any;

  private bus_data: any;
  private data: any;
  private driver_data: any;
  private selected_bus_info: any;
  private insuranceLastDate: any;
  public driver: any;
  public selected_route_info: any;
  public route_code: any;
  public str: any;

  busesForm: FormGroup;
  routeForm: FormGroup;

  public id: any;

  private fullview: String = 'col-xl-12';
  private halfview: String = 'col-xl-6 col-lg-12';
  private applyfullview: Boolean = true;
  private applyhalfview: Boolean = false;

  @Input()
  ruid: string = this.route_code;
  constructor(private identityService: IdentityService,
    private _assignService: AssignService,
    private _router: Router,
    private _script: ScriptLoaderService,
    private formBuilder: FormBuilder,
    private _transportService: TransportService) {

    this.profile = this.identityService.identity;
    this.role = this.identityService.role;
    this.uid = this.identityService.uid;
    this.coadmin = this.identityService.isCoadmin;

    console.log('roleee ', this.role);
    console.log(this.uid);
  }

  ngOnInit() {

    this.busClick();
    // this.driverClick();
    this.getDriverById(this.str);
    this.busesForm = this.formBuilder.group({
      number: [],
      registrationNumber: [],
      insuranceLastDate: [],
      capacity: []
    });
    this.routeForm = this.formBuilder.group({
      number: [],
      locations: []
    });
  }
  ngAfterViewInit() {
    this._script.loadScripts('app-transport', ['assets/demo/default/custom/components/calendar/basic.js']);
  }

  addCssClass(isapplyhalfview: boolean, isapplyfullview: boolean) {

    let classes = {
      'col-xl-6 col-lg-12': isapplyhalfview,
      'col-xl-12': isapplyfullview
    }
    return classes;
  }

  busClick() {
    this._assignService.getBus().subscribe(bus_data => {
      this.bus_data = bus_data.payload;
      console.log('bus data:', this.bus_data);
    },
      error => {
        console.error('Error while getting bus data. stack: ', error);
      });
  }
  addBus() {
    console.log(this.busesForm.value.number);
    console.log(this.busesForm.value.registrationNumber);
    console.log(this.busesForm.value.insuranceLastDate);
    console.log(this.busesForm.value.capacity);
    this._transportService.onSubmit(this.busesForm.value.number, this.busesForm.value.registrationNumber, this.busesForm.value.insuranceLastDate, this.busesForm.value.capacity).subscribe(data =>
      this.data = data.payload);
    this._assignService.getBus().subscribe(bus_data => {
      this.bus_data = bus_data.payload;
      console.log('bus data:', this.bus_data);
      console.log('report generate' + this.data);
      if (this.busesForm.valid) {
        alert('Report Generated Successfully!');
        this.busesForm.reset();
      }
    },
      error => {
        console.error('Error while getting bus data. stack: ', error);
      });

  }
  driverClick() {
    this._assignService.getRouteDriver().subscribe(driver_data => {
      this.driver_data = driver_data.payload;
      console.log('driver data:', this.driver_data);
    },
      error => {
        console.error('Error while getting driver data. stack: ', error);
      });
  }
  addRoute() {
    this._assignService.getRouteDriver().subscribe(driver_data => {
      this.driver_data = driver_data.payload;
      console.log('driver data:', this.driver_data);

    },
      error => {
        console.error('Error while getting driver data. stack: ', error);
      });
  }

  getDriverById(str) {
    this._assignService.getDriverById().subscribe(route_code => {
      this.route_code = route_code.payload;
      console.log('route data:', this.route_code);
      for (const route of this.route_code) {
        this.selected_route_info = route;
        if (this.selected_route_info.ruid === str) {
          this._assignService.saveData(this.selected_route_info.ruid);
          console.log('wlvdhbwj', this.selected_route_info.ruid);
          this._router.navigate(['passenger']);
          //    this._router.navigate(['passenger']);

        }
      }

    },
      error => {
        console.error('Error while getting bus data. stack: ', error);
      });
  }
  addRoutes() {
    console.log(this.routeForm.value.number);
    console.log(this.routeForm.value.locations);
    this._transportService.addRoutes(this.routeForm.value.number, this.routeForm.value.locations).subscribe(data =>
      this.data = data.payload);
    console.log('report generate' + this.data);
    /* alert('Report Generated Successfully');*/
    if (this.routeForm.valid) {
      alert('Report Generated Successfully!');
      this.routeForm.reset();

    }
  }

  onSubmit() {
    //  console.log(this.category);
    console.log(this.busesForm.value.number);
    console.log(this.busesForm.value.registrationNumber);
    console.log(this.busesForm.value.insuranceLastDate);
    console.log(this.busesForm.value.capacity);
    this._transportService.onSubmit(this.busesForm.value.number, this.busesForm.value.registrationNumber, this.busesForm.value.insuranceLastDate, this.busesForm.value.capacity).subscribe(data =>
      this.data = data.payload);
    //  console.log('report generate' + this.data);
    /* alert('Report Generated Successfully');*/
    if (this.busesForm.valid) {
      //   alert('Report Generated Successfully!');
      this.addBus();
    }
    this.busesForm.reset();

  }
}
