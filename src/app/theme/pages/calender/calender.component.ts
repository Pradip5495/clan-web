/*
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calender',
  template: `
    <p>
      calender works!
    </p>
  `,
  styles: []
})
export class CalenderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
*/
import {
  Component,
  OnInit,
  ViewEncapsulation,
  AfterViewInit
} from "@angular/core";
import { ScriptLoaderService } from "../../../_services/script-loader.service";
import { EventService } from "../../../_services/event.service";
import { IdentityService } from "../../../_services/identity.service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormArray,
  FormControl
} from "@angular/forms";
import { NoticeService } from "../../../_services/notice.service";

// import { MomentModule } from 'angular2-moment';
// import * as $ from 'jquery'

declare let $: any;
declare let mApp, moment;

@Component({
  selector: "app-calender",
  templateUrl: "./calender.component.html",
  encapsulation: ViewEncapsulation.None
})
export class CalenderComponent implements OnInit, AfterViewInit {
  private event_list: any = null;
  // public audieance_list: any = ['TR', 'PR', 'ST'];

  public role: string;
  public notice_list1: any;
  public notice_date: any;
  public event_list1: any;
  userForm: FormGroup;
  public school: any;
  myForm: FormGroup;
  audience = [
    { audiences: "Teachers" },
    { audiences: "Parents" },
    { audiences: "Students" }
  ];

  constructor(
    private _script: ScriptLoaderService,
    private _eventService: EventService,
    private identityService: IdentityService,
    private _noticeService: NoticeService,
    private formBuilder: FormBuilder
  ) {
    this.school = this.identityService.school;

    this.role = this.identityService.role;
  }

  ngOnInit() {
    this.getRecentEvents();
    this.getEvent();
    this.getRecentNotice();
    this.getNotice();
    //  this.loadNotice(notice);
  }

  ngAfterViewInit() {
    this._script.loadScripts("app-events", [
      "assets/demo/default/custom/components/calendar/basic.js"
    ]);
  }
  getEvent() {
    this._eventService.getEvent(this.school).subscribe(
      event_list1 => {
        this.event_list1 = event_list1.payload;
        console.log("notice data:", this.event_list1);
      },
      error => {
        console.error("Error while getting event data. stack: ", error);
      }
    );
  }
  getRecentEvents() {
    this._eventService.getRecentEvents().subscribe(
      event_data => {
        this.event_list = event_data.payload;
        console.log(this.event_list);
        this.loadCalender(this.event_list);
      },
      error => {
        console.error("Error while getting event data. stack: ", error);
      }
    );
  }

  loadNotice(notice) {
    this.loadCalender(event);
    let noticeArr = [];

    for (let n of notice) {
      console.log(n);
      noticeArr.push({
        title: n.type,
        start: n.createdAt,
        description: n.message,
        className: "m-fc-event--light m-fc-event--solid-warning"
      });
    }

    if ($("#m_calendar_event").length === 0) {
      return;
    }

    let todayDate = moment().startOf("day");

    $("#m_calendar_event").fullCalendar({
      header: {
        left: "prev,next today",
        center: "title",
        right: "month,agendaWeek,agendaDay,listWeek"
      },

      editable: true,
      eventLimit: true, // allow "more" link when too many events
      navLinks: true,
      defaultDate: moment(todayDate),

      events: noticeArr,

      eventRender: function(event, element) {
        if (element.hasClass("fc-day-grid-event")) {
          element.data("content", event.description);
          element.data("placement", "top");
          mApp.initPopover(element);
        } else if (element.hasClass("fc-time-grid-event")) {
          element
            .find(".fc-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        } else if (element.find(".fc-list-item-title").lenght !== 0) {
          element
            .find(".fc-list-item-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        }
      }
    });
  }

  loadCalender(event) {
    let notice;
    //  this.loadNotice(notice);

    let eventsArr = [];

    for (let e of event) {
      console.log(e);
      eventsArr.push({
        title: e.title,
        start: e.date,
        description: e.description,
        className: "m-fc-event--light m-fc-event--solid-warning"
      });
    }
    if ($("#m_calendar_event").length === 0) {
      return;
    }

    let todayDate = moment().startOf("day");
    // let YM = todayDate.format('YYYY-MM');
    // let YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
    // let TODAY = todayDate.format('YYYY-MM-DD');
    // let TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

    $("#m_calendar_event").fullCalendar({
      header: {
        left: "prev,next today",
        center: "title",
        right: "month,agendaWeek,agendaDay,listWeek"
      },

      editable: true,
      eventLimit: true, // allow "more" link when too many events
      navLinks: true,
      defaultDate: moment(todayDate),

      events: eventsArr,

      eventRender: function(event, element) {
        if (element.hasClass("fc-day-grid-event")) {
          element.data("content", event.description);
          element.data("placement", "top");
          mApp.initPopover(element);
        } else if (element.hasClass("fc-time-grid-event")) {
          element
            .find(".fc-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        } else if (element.find(".fc-list-item-title").lenght !== 0) {
          element
            .find(".fc-list-item-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        }
      }
    });
  }
  event() {
    console.log(this.userForm.value);
  }
  onSubmit() {
    console.log(this.userForm.value);

    this._eventService
      .postResentEvent(
        this.userForm.value.title,
        this.userForm.value.description,
        this.userForm.value.audience,
        this.userForm.value.date,
        this.userForm.value.time,
        "",
        "",
        ""
      )
      .subscribe(
        event_data => {
          this.event_list = event_data.payload;
          console.log(this.event_list);
          this.loadCalender(this.event_list);
        },
        error => {
          console.error("Error while getting event data. stack: ", error);
        }
      );
  }
  getRecentNotice() {
    this._noticeService.getRecentNotice().subscribe(
      notice_data => {
        this.notice_date = notice_data.payload;
        console.log("notice data:", this.notice_date);
        this.loadNotice(this.notice_date);
      },
      error => {
        console.error("Error while getting notice data. stack: ", error);
      }
    );
  }
  getNotice() {
    this._noticeService.getNotice(this.school).subscribe(
      notice_list1 => {
        this.notice_list1 = notice_list1.payload;
        console.log("event data:", this.notice_list1);
      },
      error => {
        console.error("Error while getting notice data. stack: ", error);
      }
    );
  }

  // moment(n.createdAt).format('YYYY-MM-DD')
}
