import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SecurityComponent } from './security.component';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../default/default.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LibraryService } from '../../../_services/library.service';
import { ClassService } from '../../../_services/class.service';
import { ActivatedRoute, ParamMap ,Router } from '@angular/router';
import { IdentityService } from '../../../_services/identity.service';
import { SecurityFilterPipe } from './filter.pipe';
const routes: Routes = [
  {
    'path': '',
    'component': DefaultComponent,
    'children': [
      {
        'path': '',
        'component': SecurityComponent
      },
    ]
  }
];
@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule,
    SecurityFilterPipe
  ], declarations: [
    SecurityComponent,
    SecurityFilterPipe
  ]
})
export class SecurityModule {


  
  
 }
