import { PipeTransform, Pipe } from "@angular/core";


@Pipe({
    name: 'filter'
})


export class SecurityFilterPipe implements PipeTransform{
    transform(items: any[],  searchText: string) {
        if (!items || !searchText) {
            return items;
        }
    return items.filter(item => {
        if(item.number){
            const filter = Object.keys(item);
            return filter.some(
                key => item.number.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
            )
        }
        if(item.name){
            const filter = Object.keys(item);
            return filter.some(
                key => item.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
            )
        }
        if(item.visitorName){
            const filter = Object.keys(item);
            return filter.some(
                key => item.visitorName.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
            )
        }
    });
    }
}