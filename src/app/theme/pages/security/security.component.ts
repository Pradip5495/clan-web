import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  FormArray,
  Validators,
  FormControl
} from "@angular/forms";
import { SecurityService } from "../../../_services/security.service";
import { Pipe, PipeTransform } from "@angular/core";
import { ClassService } from "../../../_services/class.service";
import { ChildService } from "../../../_services/child.service";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { ToasterService } from "../../../_services/toaster.service";
import { IdentityService } from "../../../_services/identity.service";
import { LibraryService } from "../../../_services/library.service";
import { Routes, RouterModule } from "@angular/router";
import { SecurityFilterPipe } from "./filter.pipe";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";

//import { IMyOptions } from 'ng-uikit-pro-standard';

//import {DemoMaterialModule} from './material-module';

//import {DatepickerOverviewExample} from './datepicker-overview-example';

declare var $: any;
declare const mApp, moment, Chartist, todayDate;

@Component({
  selector: "app-security",
  templateUrl: "security.component.html",
  styleUrls: ["security.component.css"]
})
export class SecurityComponent implements OnInit {
  userForm: FormGroup;
  public searchTerm: string;
  public displayTimeIn: any = false;
  public displayTimeInbtn: any = true;
  public teach: any;
  public visitorsStats: any;
  public visitors_data: any;
  public teaching: any = true;
  public nonTeaching: any = false;
  public busDetails: any = false;
  public visitorsDetails: any = false;
  public selectedDate: any;
  public teacherSecurityData: any;
  public currentEntry: any;
  public securityStats: any;
  public visitorTypes: any;
  public visitorStudent: any;
  public classList: any;
  public studentList: any;
  public studentNameList: any = [];
  public studentName: any;
  public isStudentOut: any = false;
  public role: any;
  public selectedChild: any;
  public schoolCode: any;
  public studentDetails: any;
  public _child_list: any;
  public uid: any;
  //public inTimebutton: any = false;
  //public inTimeText: any = true;
  public currentDay: any;
  public deliveryDetails: any = false;
  public externalStudentDetails: any = false;
  public id;
  public str: any;
  //public visitorPhotoLocation: File = null;
  public meetingPerson: any;
  securityForm: FormGroup;
  visitorOut: FormGroup;
  /*public myDatePickerOptions: IMyOptions = {
    // Your options
    };*/

  constructor(
    private formBuilder: FormBuilder,
    private _classService: ClassService,
    private route: ActivatedRoute,
    private _child: ChildService,
    private toasterService: ToasterService,
    private _identity: IdentityService,
    private _security: SecurityService,
    private _library: LibraryService
  ) {
    this.role = this._identity.role;
  }

  ngOnInit() {
    const todayDate1 = moment().startOf("day");
    const YM = todayDate1.format("YYYY-MM-DD");
    this.currentDay = todayDate1.format("YYYY-MM-DD");

    if (this.role == "parent") {
      if (localStorage.getItem("selectedChild")) {
        this.selectedChild = JSON.parse(localStorage.getItem("selectedChild"));
        console.log("this.selectedChild: ", this.selectedChild);
        this.uid = this.selectedChild.uid;
        this.schoolCode = this.selectedChild.schoolCode;
        this.studentDetails = this.selectedChild;
        let std = this.selectedChild.std;
        let div = this.selectedChild.div;
        let studClass = std + "" + div;
      } else {
        this.getChild();
      }
    } else {
      this.getVisitorsStats();
      this.getSecurityStats();
      this.getTeachingDetails(this.currentDay);
    }

    // New Visitor Form
    this.securityForm = this.formBuilder.group({
      inTime: ["", Validators.required],
      userCode: ["", Validators.required],
      visitorPhotoLocation: [""],
      visitorName: ["", Validators.required],
      note: [],
      address: ["", Validators.required],
      phone: [
        "",
        Validators.required,
        Validators.maxLength(10),
        Validators.minLength(10)
      ],
      email: [""],
      vehicleNo: [],
      purpose: ["", Validators.required],
      visitorType: ["", Validators.required]
    });

    this.visitorOut = this.formBuilder.group({
      uidVisitor: [""],
      userCodes: this.formBuilder.array([]),
      inTime: [],
      outTime: ["", Validators.required],
      reasonForEarlyOut: []
    });
  }

  //  Get Teacher Visitor List
  getTeachingDetails(entryDate) {
    this.visitorTypes = "teacher";
    this._security.getTeaching(entryDate).subscribe(securityData => {
      if (securityData.status == 200) {
        this.nonTeaching = false;
        this.busDetails = false;
        this.visitorsDetails = false;
        this.deliveryDetails = false;
        this.externalStudentDetails = false;
        if (securityData.status == 200) {
          this.teacherSecurityData = securityData.payload;
          console.log("teacher details:", securityData);
        }
      }
      // error => {
      //     console.error('Error while getting class data. stack: ', error);
      //   }
    });
  }

  get child_list(): any {
    return this._child_list;
  }

  set child_list(value: any) {
    this._child_list = value;
  }

  /** get Child list of Parent */
  getChild() {
    let value: any;
    this._child.getChild().subscribe(data => {
      this._child_list = data.payload;
      $("#m_modal_child_5").modal("show");
    });
  }

  /** Set Child list for View rendar */
  setChild(data) {
    console.log("set child", data);
    localStorage.setItem("selectedChild", JSON.stringify(data));
    this.uid = data.uid;
    $("#m_modal_child_5").modal("hide");
  }
  getClasses() {
    this._classService.getClasses().subscribe(
      classList => {
        if (classList.status == 200) {
          this.classList = classList.payload;
        }
      },
      error => {
        console.error("Error while getting class data. stack: ", error);
      }
    );
  }

  // get Select type of Visitor
  getSelectedOption(selectedValue) {
    console.log("selectedValue: ", selectedValue);
    if (selectedValue == "Teacher") {
      this.getTeachingDetails(this.str);
    } else if (selectedValue == "Non-Teacher") {
      this.getNonTeachingDetails(this.currentDay);
    } else if (selectedValue == "Delivery") {
      this.getDeliveryDetails(this.currentDay);
    } else if (selectedValue == "Bus") {
      this.getBusDetails(this.currentDay);
    } else if (selectedValue == "ExternalStudents") {
      this.getExternalStudents(this.currentDay);
    } else if (selectedValue == "Visitors") {
      this.getVisitorsDetails(this.currentDay);
    }
  }

  // Get Non Teaching list by Date
  getNonTeachingDetails(securityTime) {
    this.visitorTypes = "nonteacher";
    this._security.getNonTeaching(securityTime).subscribe(nonTeaching => {
      this.nonTeaching = true;
      this.teacherSecurityData = false;
      this.busDetails = false;
      this.visitorsDetails = false;
      this.deliveryDetails = false;
      this.externalStudentDetails = false;
      if (nonTeaching.status == 200) {
        this.nonTeaching = nonTeaching.payload;
        console.log("nonTeaching.payload: ", nonTeaching.payload);
      }
    });
  }

  // Get Delivery Person List by Date
  getDeliveryDetails(securityTime) {
    this.visitorTypes = "delivery";
    this._security.getDelivery(securityTime).subscribe(deliveryDetails => {
      this.nonTeaching = false;
      this.teacherSecurityData = false;
      this.busDetails = false;
      this.visitorsDetails = false;
      this.deliveryDetails = true;
      this.externalStudentDetails = false;
      if (deliveryDetails.status == 200) {
        this.deliveryDetails = deliveryDetails.payload;
      }
    });
  }

  // Get Bus list by Date
  getBusDetails(entryDate) {
    this.visitorTypes = "bus";
    this._security.getBus(entryDate).subscribe(busDetails => {
      this.nonTeaching = false;
      this.teacherSecurityData = false;
      this.busDetails = true;
      this.visitorsDetails = false;
      this.deliveryDetails = false;
      this.externalStudentDetails = false;
      if (busDetails.status == 200) {
        console.log("this.busDetails: ", busDetails.payload);
        this.busDetails = busDetails.payload;
      }
    });
  }

  // Get External student list by date
  getExternalStudents(securityEntry) {
    this.visitorTypes = "external";
    this._security
      .getExternalStudents(securityEntry)
      .subscribe(externalStudentDetails => {
        this.nonTeaching = false;
        this.teacherSecurityData = false;
        this.busDetails = false;
        this.visitorsDetails = false;
        this.deliveryDetails = false;
        this.externalStudentDetails = true;
        if (externalStudentDetails.status == 200) {
          this.externalStudentDetails = externalStudentDetails.payload;
        }
      });
  }

  // Get Visitor Entered visitor List by Date
  getVisitorsDetails(securityEntry) {
    this.visitorTypes = "visitor";
    this._security.getVisitors(securityEntry).subscribe(visitorsDetails => {
      this.nonTeaching = false;
      this.teacherSecurityData = false;
      this.busDetails = false;
      this.visitorsDetails = true;
      this.deliveryDetails = false;
      this.externalStudentDetails = false;
      if (visitorsDetails.status == 200) {
        this.visitorsDetails = visitorsDetails.payload;
      }
    });
  }

  // get Visitor List for Student Out
  getVisitorsStudentOut(securityEntry) {
    this.visitorTypes = "student";
    this._security.getVisitors(securityEntry).subscribe(visitorsDetails => {
      this.nonTeaching = false;
      this.teacherSecurityData = false;
      this.busDetails = false;
      this.visitorsDetails = false;
      this.visitorStudent = true;
      this.deliveryDetails = false;
      this.externalStudentDetails = false;
      if (visitorsDetails.status == 200) {
        this.visitorStudent = visitorsDetails.payload;
      }
    });
  }
  showTeachingDetails(books_Information) {
    //this.book_Info = true;
    this.teach = books_Information;
    $("#tab_teaching").show("div");
  }
  getTimeIn() {
    this.displayTimeIn = true;
    this.displayTimeInbtn = false;
  }

  onFileChange(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = () => {
        this.securityForm
          .get("visitorPhotoLocation")
          .setValue(event.target.files[0]);
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  onSubmit() {
    let headers = new HttpHeaders();
    console.log("file before post: ", this.securityForm.value);

    let currentT = this.securityForm.value.inTime;
    const todayDate1 = moment(currentT).startOf("day");
    let currentTime = todayDate1.format("hh:mm:ss");
    let inTime = this.currentDay + "T" + currentT + ":00";
    const formData: FormData = new FormData();
    formData.append(
      "visitorPhotoLocation",
      this.securityForm.value.visitorPhotoLocation
    );
    formData.append("visitorName", this.securityForm.value.visitorName);
    formData.append("phone", this.securityForm.value.phone);
    formData.append("userCode", this.securityForm.value.userCode);
    formData.append("email", this.securityForm.value.email);
    formData.append("address", this.securityForm.value.address);
    formData.append("vehicleNo", this.securityForm.value.vehicleNo);
    formData.append("purpose", this.securityForm.value.purpose);
    formData.append("note", this.securityForm.value.note);
    formData.append("visitorType", this.securityForm.value.visitorType);
    formData.append("inTime", inTime);
    this._security.postNewVisitorInTime(formData).subscribe(
      visitorData => {
        if (visitorData.status === 201) {
          this.visitors_data = visitorData.payload;
          $("#m_modal_10").modal("hide");
          this.toasterService.Success("Successfully Submitted!");
          this.securityForm.reset();
        } else {
          this.toasterService.Error("Not Submitted!");
        }
      },
      error => {
        this.toasterService.Error("Error while submitting data!");
        console.error("Error while submitting data!");
      }
    );
  }

  getVisitorsStats() {
    this._security.getVisitorsStats().subscribe(visitorsStats => {
      if (visitorsStats.status === 200) {
        this.visitorsStats = visitorsStats.payload;
      }
      error => {
        console.error("Error while getting class data. stack: ", error);
      };
    });
  }
  getPastEntry(event) {
    let eventEleValue = event.srcElement.value;
    this._security.getTeaching(eventEleValue).subscribe(securityData => {
      if (securityData.status === 200) {
        this.teacherSecurityData = securityData.payload;
      }
    });
  }
  /** School Staff [Teacher , Non- Teaching] In Time Entry By uid*/
  staffSchoolEntry(staffUid, staff) {
    this._security.postStaffInTime(staffUid).subscribe(
      staffData => {
        if (staffData.status === 201) {
          this.toasterService.Success("Successfully done!");
          if (staff == "nonteaching") {
            this.getNonTeachingDetails(this.currentDay);
          } else {
            this.getTeachingDetails(this.currentDay);
          }
        } else {
          this.toasterService.Error("Not done!");
        }
      },
      error => {
        this.toasterService.Error("Error while Submitting data!");
      }
    );
  }

  /** School Staff [Teacher , Non- Teaching] Out Time By uid*/
  staffSchoolEntryOut(staffOutUid, staff) {
    this._security.updateStaffOutTime(staffOutUid).subscribe(staffData => {
      if (staffData.status === 200) {
        this.getCurrentEntry();
        if (staff == "nonteaching") {
          this.getNonTeachingDetails(this.currentDay);
        } else {
          this.getTeachingDetails(this.currentDay);
        }
      }
    });
  }

  /** School Bus  In Time Entry By buid*/
  schoolBusEntry(buid) {
    this._security.postBusInTime(buid).subscribe(
      staffData => {
        if (staffData.status === 201) {
          this.getBusDetails(this.currentDay);
          this.toasterService.Success("Successfully done!");
        } else {
          this.toasterService.Error("Not Done!");
        }
      },
      error => {
        this.toasterService.Error("Error while submitting!");
      }
    );
  }

  /** School Bus  Out Time Entry By uidBusLogBook*/
  schoolBusOut(uidBusLogBook) {
    this._security.updateBusOutTime(uidBusLogBook).subscribe(staffData => {
      if (staffData.status === 200) {
        this.getBusDetails(this.currentDay);
      }
    });
  }

  /** School visitor  Out Time Entry By uidBusLogBook*/
  schoolVisitorOut(uidVisitor, visitorType) {
    this._security.updateVisitorOutTime(uidVisitor).subscribe(staffData => {
      if (staffData.status === 200) {

        if (visitorType == "Delivery") {
          this.getDeliveryDetails(this.currentDay);
        } else if (visitorType == "ExternalStudents") {
          this.getExternalStudents(this.currentDay);
        } else if (visitorType == "Visitors") {
          this.getVisitorsDetails(this.currentDay);
        }
      }
    });
  }

  /** School visitor  Out Time Entry By uidBusLogBook*/
  studentOut(Visitor, visitorType) {
    this.visitorsDetails = Visitor;
    $("#m_modal_11").modal("show");
  }

  /** Get Current Entry  */
  getCurrentEntry() {
    this._security.getCurrentEntry(this.currentDay).subscribe(currentEntry => {
      if (currentEntry.status === 200) {
        this.currentEntry = currentEntry.payload;
      }
    });
  }

  /** Get Security Stats Data */
  getSecurityStats() {
    this._security.getSecurityStats().subscribe(securityStats => {
      if (securityStats.status === 200) {
        this.securityStats = securityStats.payload;
        this.securityChart();
      }
    });
  }

  /** Security stats Bar Chart */
  securityChart = function() {
    if ($("#m_chart_security").length === 0) {
      return;
    }
    const series = [];
    const label = [];
    for (const e of this.securityStats.visitorsStats) {
      series.push(e.visitorCount);
      label.push(e.Month);
    }
    const chart_data = Chartist.Bar(
      "#m_chart_security",
      {
        labels: label,
        series: series
      },
      {
        barwidth: 100,
        distributeSeries: true,
        low: 0,
        // high: 100,
        axisX: {
          showGrid: false
        },
        axisY: {
          showGrid: false,
          showLabel: false,
          offset: 0
        }
      }
    );
    chart_data.on("draw", function(data) {
      if (data.type === "bar") {
        data.element.attr({
          style: "stroke-width: 10%; stroke: #e18215"
        });
      }
    });
  };

  getSelectedClass(event) {
    let selectedClass = event.srcElement.value;
    this._library.getStudentList(selectedClass).subscribe(studentList => {
      if (studentList.status == 200) {
        this.studentList = studentList.payload;
      }
    });
  }

  onChange(userCodes: string, studnetData: string) {
    this.studentName = studnetData;
    const studentFormArray = <FormArray>this.visitorOut.controls.userCodes;
    this.studentNameList.push(studnetData);
    studentFormArray.push(new FormControl(userCodes));
    $("#m_modal_12").modal("hide");
  }

  onSubmitVisitorOut() {
    let visitor = this.visitorOut.value;
    visitor.uidVisitor = this.visitorsDetails.uidVisitor;
    visitor.inTime = this.visitorsDetails.inTime;
    let currentT = this.visitorOut.value.outTime;
    const todayDate1 = moment(currentT).startOf("day");
    let inTime = this.currentDay + "T" + currentT + ":00";
    visitor.outTime = inTime;
    this._security.postStudentOut(visitor).subscribe(
      visitorData => {
        if (visitorData.status === 201) {
          this.visitors_data = visitorData.payload;
          this.toasterService.Success("Successfully Submitted!");
          this.getVisitorsStudentOut(this.currentDay);
          $("#m_modal_11").modal("hide");
        } else {
          this.toasterService.Error("Not Submitted!");
        }
      },
      error => {
        this.toasterService.Error("Error while submitting data");
        console.error("Error while submitting data");
      }
    );
  }

  studentOutheck(isChecked: boolean) {
    if (isChecked) {
      this.isStudentOut = true;
    } else {
      this.isStudentOut = false;
    }
  }
}
