import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { DailyThoughtsService } from '../../../_services/daily-thoughts.service';
import { HttpClient } from '@angular/common/http';
import { AddSuperUserService } from "../../../_services/add-super-user.service";


@Component({
  selector: 'app-add-super-user',
  templateUrl: './add-super-user.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class AddSuperUserComponent implements OnInit {
  userForm: FormGroup;
  public dailythoughts: any;

  constructor(public formBuilder: FormBuilder,
    public dailyThoughtsService: DailyThoughtsService,
    public addSuperUserService: AddSuperUserService,
    public http: HttpClient) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      name: [],
      email: [],
      type: ['SU'],
      phone: [],
      username: [],
      password: []
    });
    this.onSubmit();
  }
  onSubmit() {
    console.log(this.userForm.value);
    this.addSuperUserService.addSuperUser(this.userForm.value.name, this.userForm.value.email, this.userForm.value.type, this.userForm.value.phone, this.userForm.value.username, this.userForm.value.password).subscribe(dailythoughts =>
      this.dailythoughts = dailythoughts.payload);
    console.log('daily thoughts list', this.dailythoughts);
  }
}

