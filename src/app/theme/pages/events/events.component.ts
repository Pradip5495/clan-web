import {
  Component,
  OnInit,
  ViewEncapsulation,
  AfterViewInit
} from "@angular/core";
import { ScriptLoaderService } from "../../../_services/script-loader.service";
import { EventService } from "../../../_services/event.service";
import { ToasterService } from "../../../_services/toaster.service";
import { IdentityService } from "../../../_services/identity.service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormArray,
  FormControl
} from "@angular/forms";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";

// import { MomentModule } from 'angular2-moment';
// import * as $ from 'jquery'

declare let $: any;
declare let mApp, moment;

@Component({
  selector: "app-events",
  templateUrl: "./events.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./events.component.css"]
})
export class EventsComponent implements OnInit, AfterViewInit {
  private event_list: any = null;
  // public audieance_list: any = ['TR', 'PR', 'ST'];

  public role: string;
  public event_list1: any;

  userForm: FormGroup;
  public eventDetails: any;
  public school: any;
  public selectedChild: any;
  public uid: any;
  public event_list2:any;
  myForm: FormGroup;
  audience = [
    { audiences: "Teachers" },
    { audiences: "Parents" },
    { audiences: "Students" }
  ];

  constructor(
    private _script: ScriptLoaderService,
    private _eventService: EventService,
    private toasterService: ToasterService,
    private identityService: IdentityService,
    private formBuilder: FormBuilder
  ) {
    this.school = this.identityService.school;

    this.role = this.identityService.role;
  }

  ngOnInit() {
    /* let today = new Date().toISOString().split('T')[0];
     document.getElementsByName('setTodaysDate')[0].setAttribute('min', today);*/

     if (this.role === "parent") {
      if (localStorage.getItem("selectedChild")) {
        this.selectedChild = JSON.parse(localStorage.getItem("selectedChild"));
        this.uid = this.selectedChild.uid;
        console.log('id for student',this.uid);
        this.getEventForParent();
      } 
     
    }

    this.userForm = this.formBuilder.group({
      title: [],
      audience: this.formBuilder.array([]),
      description: [],
      date: [],
      time: [],
      startDate: [],
      endDate: [],
      startTime: [],
      endTime: [],
      location: []
    });

    if (this.role === 'school' || this.role === 'teacher' || this.role === 'coadmin' || this.role === 'librarian' || this.role == 'clerk' || this.role === 'director') {
      this.getEvent();
      this.getRecentEvents();
      
    }

    
    
    
  }

  ngAfterViewInit() {
    this._script.loadScripts("app-events", [
      "assets/demo/default/custom/components/calendar/basic.js"
    ]);
  }

  onChange(audiences: string, isChecked: boolean) {
    const emailFormArray = <FormArray>this.userForm.controls.audience;

    if (isChecked) {
      emailFormArray.push(new FormControl(audiences));
    } else {
      let index = emailFormArray.controls.findIndex(x => x.value === audiences);
      emailFormArray.removeAt(index);
    }
  }

  getEvent() {
    this._eventService.getEvent(this.school).subscribe(
      event_list1 => {
        this.event_list1 = event_list1.payload;
        console.log('getevent',this.event_list1)
      },
      error => {
        console.error("Error while getting event data on. stack: ", error);
      }
    );
  }

  getEventForParent() {
    this._eventService.getEventForParent(this.uid).subscribe(
      event_list2 => {
        this.event_list2 = event_list2.payload;
        console.log('event for parent',this.event_list2);
      },
      error => {
        console.error("Error while getting event data. stack: ", error);
      }
    );
  }

  getRecentEvents() {
    this._eventService.getRecentEvents().subscribe(
      event_data => {
        this.event_list = event_data.payload;
        this.loadCalender(this.event_list);
      },
      error => {
        console.error("Error while getting event data recent. stack: ", error);
      }
    );
  }

  loadCalender(event) {
    let eventsArr = [];

    for (let e of event) {
      eventsArr.push({
        title: e.title,
        start: e.date,
        description: e.description,
        className: "m-fc-event--light m-fc-event--solid-warning"
      });
    }
    if ($("#m_calendar_event").length === 0) {
      return;
    }

    let todayDate = moment().startOf("day");
    // let YM = todayDate.format('YYYY-MM');
    // let YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
    // let TODAY = todayDate.format('YYYY-MM-DD');
    // let TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

    $("#m_calendar_event").fullCalendar({
      header: {
        left: "prev,next today",
        center: "title",
        right: "month,agendaWeek,agendaDay,listWeek"
      },

      editable: true,
      eventLimit: true, // allow "more" link when too many events
      navLinks: true,
      defaultDate: moment(todayDate),

      events: eventsArr,

      eventRender: function(event, element) {
        if (element.hasClass("fc-day-grid-event")) {
          element.data("content", event.description);
          element.data("placement", "top");
          mApp.initPopover(element);
        } else if (element.hasClass("fc-time-grid-event")) {
          element
            .find(".fc-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        } else if (element.find(".fc-list-item-title").lenght !== 0) {
          element
            .find(".fc-list-item-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        }
      }
    });
  }

  event() {
    $("#m_modal_4").modal("hide");
  }

  onSubmit() {

    this._eventService
      .postResentEvent(
        this.userForm.value.title,
        this.userForm.value.description,
        this.userForm.value.audience,
        this.userForm.value.startDate,
        this.userForm.value.endDate,
        this.userForm.value.startTime,
        this.userForm.value.endDate,
        this.userForm.value.location
      )
      .subscribe(
        event_data => {
          if (event_data.status == 201) {
            this.event_list = event_data.payload;
            this.toasterService.Error("Successfully Added Event!");
            this.loadCalender(this.event_list);
            $("#m_modal_5").modal("hide");
          } else {
            this.toasterService.Error("Event Not Added!");
          }
        },
        error => {
          console.error("Error while getting event data. stack: ", error);
          this.toasterService.Error("Error while getting event data");
        }
      );
  }

  resetAll(audiences: string, isChecked: boolean) {
    this.onChange(audiences, isChecked);
    /* this.audience.forEach((data) => {
       data.checked = false;
     });*/
  }

  cancel(userForm) {
    userForm.reset();
    $("input[type=checkbox]").removeAttr("checked");

    // userForm.value.audience.reset();
    $("#m_modal_4").modal("hide");
  }
  getEventDetails(event) {
    this.eventDetails = event;
    console.log("length:",event.audience.length);

    for(let i=0;i<=event.audience.length;i++){
      if(event.audience[i] == ("LB")){
        this.eventDetails.audience[i] = "Librarian";
      }
      else if(event.audience[i] == ("ST")){
        this.eventDetails.audience[i] = "Student";
      }
      else if(event.audience[i] == ("TE")){
        this.eventDetails.audience[i] = "Teacher";
      }
      else if(event.audience[i] == ("PR")){
        this.eventDetails.audience[i] = "Parent";
      }
      else if(event.audience[i] == ("CL")){
        this.eventDetails.audience[i] = "Clerk";
      }
      else if(event.audience[i] == ("SQ")){
        this.eventDetails.audience[i] = "Security";
      }


    }
    $("#m_modal_5").modal("show");

   
    //this.onChange(event.audiences, event.target.checked);
  }

  // calendarInit = function() {
  //
  //   if ($('#m_calendar_event').length === 0) {
  //     return;
  //   }
  //
  //   let todayDate = moment().startOf('day');
  //   let YM = todayDate.format('YYYY-MM');
  //   let YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
  //   let TODAY = todayDate.format('YYYY-MM-DD');
  //   let TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');
  //
  //
  //
  //   ($('#m_calendar_event') as any).fullCalendar({
  //     header: {
  //       left: 'prev,next today',
  //       center: 'title',
  //       right: 'month,agendaWeek,agendaDay,listWeek'
  //     },
  //
  //     editable: true,
  //     eventLimit: true, // allow "more" link when too many events
  //     navLinks: true,
  //     defaultDate: moment('2017-09-15'),
  //     events: [
  //       {
  //         title: 'Test Event',
  //         start: moment('2017-08-28'),
  //         description: 'This is a test event',
  //         className: "m-fc-event--light m-fc-event--solid-warning"
  //       },
  //       {
  //         title: 'Conference',
  //         description: 'Lorem ipsum dolor incid idunt ut labore',
  //         start: moment('2017-08-29T13:30:00'),
  //         end: moment('2017-08-29T17:30:00'),
  //         className: "m-fc-event--accent"
  //       },
  //       {
  //         title: 'Dinner',
  //         start: moment('2017-08-30'),
  //         description: 'Lorem ipsum dolor sit tempor incid',
  //         className: "m-fc-event--light  m-fc-event--solid-danger"
  //       }
  //     ],
  //
  //     eventRender: function(event, element) {
  //       if (element.hasClass('fc-day-grid-event')) {
  //         element.data('content', event.description);
  //         element.data('placement', 'top');
  //         mApp.initPopover(element);
  //       } else if (element.hasClass('fc-time-grid-event')) {
  //         element.find('.fc-title').append('<div class="fc-description">' + event.description + '</div>');
  //       } else if (element.find('.fc-list-item-title').lenght !== 0) {
  //         element.find('.fc-list-item-title').append('<div class="fc-description">' + event.description + '</div>');
  //       }
  //     }
  //
  //   });
  // }
}
