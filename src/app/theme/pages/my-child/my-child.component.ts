import {
  Component,
  OnInit,
  ViewEncapsulation,
  AfterViewInit
} from "@angular/core";
import { ScriptLoaderService } from "../../../_services/script-loader.service";
import { StudentService } from "../../../_services/student.service";
import { IdentityService } from "../../../_services/identity.service";
import { ClassService } from "../../../_services/class.service";
import { ChildService } from "../../../_services/child.service";
import { AttendanceService } from "../../../_services/attendance.service";
import { ToasterService } from "../../../_services/toaster.service";
import { User } from "../../../_models/user";
import { HomeworkService } from "../../../_services/homework.service";
import { ExaminationService } from "../../../_services/examination.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TransportService } from "../../../_services/transport.service";
import { PerformanceByClassService } from "../../../_services/performanceByClass.service";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { ListInboxItemsService } from "../../../_services/listInboxItems.service";

//declare let $: JQueryStatic, selectpicker, any;
declare let Morris;

declare const $: any;
declare const mApp, moment, Chartist, todayDate;

@Component({
  selector: "app-my-child",
  templateUrl: "./my-child.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./my-child.component.css"]
})
export class MyChildComponent implements OnInit, AfterViewInit {
  get class_data(): any {
    return this._class_data;
  }

  set class_data(value: any) {
    this._class_data = value;
  }
  get class_value(): any {
    return this._class_value;
  }

  set class_value(value: any) {
    this._class_value = value;
  }
  get students_list(): any {
    return this._students_list;
  }

  set students_list(value: any) {
    this._students_list = value;
  }

  public child_data: any;
  public selected_child_info: any;
  private id: any;
  public selected_data: any;
  public stats: any;
  public attendance_data: any;
  public present_count: any;
  public absent_count: any;
  public attendance1: any;
  public present_percent: any;
  public absent_percent: any;
  public day_count: any = 0;
  public present: any;
  public absent: any;
  public report: any;
  public category_list: any = ["complain", "Suggestion", "Praise", "Feedback"];
  public studentAttendance: any;
  public _class_data: any;
  public _students_list: any;
  public _class_value: any;
  public homework_data: any;
  public myChild1: any;
  public child: any;
  public myChild: any;
  public class1: any;
  public role: any;
  public profile: User;
  public std: any;
  public div: any;
  public childDetailsView: any = false;
  public data: any;
  public exam_data: any;
  public router;
  public calendar_date: any;
  public calendar_btn_status = false; //Calendar Next & Previous button status
  public uid: any;
  public YM: any;
  public leave: any = 0;
  public attendance2: any;
  public studentWeekAttendance: any;
  public attendanceFlag: any;
  public leave_percent: any;
  public leaveStudent: any;
  public homeworkDetails: any;
  public student_performance: any;
  public subjectList: any;
  private schoolCode: any;

  childForm: FormGroup;
  messageForm: FormGroup;

  constructor(
    private _script: ScriptLoaderService,
    private _router: Router,
    private _studentService: StudentService,
    private toasterService: ToasterService,
    private _identityService: IdentityService,
    private _classService: ClassService,
    private _childService: ChildService,
    private _listInboxItemsService: ListInboxItemsService,
    private _attendanceService: AttendanceService,
    private _homeworkService: HomeworkService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _examinationService: ExaminationService,
    private _performance: PerformanceByClassService,
    private _transportService: TransportService
  ) {
    this.role = this._identityService.role;
    // if (this.role === this._identityService.role) {
    //   this.getAttendanceBySelectedChildId(this.id);
    // }
    this.stats = {};
  }
  ngOnInit() {
    const todayDate1 = moment().startOf("day");
    const YM = todayDate1.format("YYYY-MM");
    this.getChild();
    //  this.childCode();
    //  this.getProfileChild(this.id);
    // this.getAttendanceForStudentByMonth(YM);
    // this.attendanceChart();

    this.childForm = this.formBuilder.group({
      child: [],
      confirmcode: []
    });

    this.messageForm = this.formBuilder.group({
      category: ["", Validators.required],
      subject: ["", Validators.required],
      message: ["", Validators.required]
    });

    if (this.route.params["value"].myChild) {
      this.myChild1 = this.route.params["value"].myChild;
      this.getProfileChild(this.id);
      this.childDetailsView = true;
    } else {
      this.childDetailsView = false;
    }
  }

  ngAfterViewInit() {
    this._script.loadScripts("app-child", [
      "assets/demo/default/custom/components/calendar/basic.js"
    ]);
  }
  getSelectedChildInfo(id) {
    for (const child of this.child_data) {
      if (child.uid === id) {
        this.selected_child_info = child;
        this.getAttendanceBySelectedChildId(child.uid);
        this._examinationService.getExaminationResult(child.uid).subscribe(
          exam_data => {
            this.exam_data = exam_data.payload;
            
          },
          error => {
            console.error("Error while getting exam data. stack: ", error);
          }
        );
      }
    }
  }
  getProfileChild(uid) {
    this._childService.getChildById(uid).subscribe(
      data => {
        this.selected_data = data.payload;
        this.getSelectedChildInfo(uid);
      },
      error => {
        console.error("Error while getting leture data. stack: ", error);
      }
    );
  }

  getChild() {
    this._childService.getChild().subscribe(
      child_data => {
        this.child_data = child_data.payload;
      },
      error => {
        console.error("Error while getting student list. stack: ", error);
      }
    );
  }

  onMessageSubmit() {
    // this.category, this.messageForm.value.subject, this.to, this.messageForm.value.message
    this._listInboxItemsService
      .postMessage(this.messageForm.value)
      .subscribe(data => {
        if (data.status === 201) {
          this.report = data.payload;
          this.toasterService.Success("Report Generated Successfully");
          $("#m_modal_8").modal("hide");
          this.messageForm.reset();
        } else {
          this.toasterService.Error("Report Not Generated!");
        }
      });

    /*this.report = data.payload);
    console.log('report generate' + this.report);
    if (this.userForm.value.subject === null && this.userForm.value.message === null) {
      alert('Please Enter Subject & message');
    } else {
      // alert('Report Generated Successfully');
      //$('#m_modal_5').modal('hide');
    }
    this.userForm.value.reset();*/
  }

  getAttendanceBySelectedChildId(id) {
    const todayDate = moment().startOf("day");
    const YM = todayDate.format("YYYY-MM-DD");

    this._attendanceService
      .getAttendanceForStudent(this.myChild.uid, YM)
      .subscribe(attendance => {
        if (attendance !== null) {
          this.attendance1 = attendance.payload;
          
          this.present = 0;
          this.absent = 0;
          this.leave = 0;
          this.present_percent = 0;
          this.absent_percent = 0;
          this.leave_percent = 0;
          for (const a of this.attendance1) {
            this.day_count++;
            if (a.isPresent) {
              this.present++;
            } else if (!a.isPresent && !a.onLeave) {
              this.absent++;
            } else if (!a.isPresent && a.onLeave) {
              this.leave++;
            }
          }
         
          this.present_percent = (
            (this.present / this.day_count) *
            100
          ).toFixed(2);
          this.absent_percent = ((this.absent / this.day_count) * 100).toFixed(
            2
          );
          this.leave_percent = ((this.leave / this.day_count) * 100).toFixed(2);
          
          $("#m_calendar_event").fullCalendar("destroy");
          $("#m_calendar_event").fullCalendar("render");
          this.attendanceChart();
          this.getStudentPerformanceBySubject();
          this.loadCalender(this.attendance1);
        } else {
          this.attendance1 = [];
          this.present = 0;
          this.absent = 0;
          this.leave = 0;
          this.present_percent = 50;
          this.absent_percent = 25;
          this.leave_percent = 25;
          this.attendanceChart();
          this.getStudentPerformanceBySubject();
          this.loadCalender(this.attendance1);
        }
      });
  }

  getAttendanceForStudentByMonth(currentMonth) {
 
    this._attendanceService
      .getAttendanceForStudentByMonth(currentMonth)
      .subscribe(
        attendance => {
          this.attendance1 = attendance.payload;
          
          this.loadAttendence(this.attendance1);

          for (const a of this.attendance1) {
            this.day_count++;

            if (a.isPresent) {
              this.present++;
            } else {
              this.absent++;
            }
          }

          this.present_percent = (this.present / this.day_count) * 100;
          this.absent_percent = (this.absent / this.day_count) * 100;

          this.loadAttendence(this.studentAttendance);
          this.attendanceChart();
        },
        error => {
          console.error("Error while getting attendance. stack: ", error);
        }
      );
  }
  childCode() {
    
    this._childService.childCode(this.childForm.value.child).subscribe(
      data => {
        if (data.status == 201) {
          this.data = data.payload;
          $("#m_modal_5").modal("hide");
          this.toasterService.Success("Successfully Added!");
          
          this.childForm.reset();
        } else {
          this.toasterService.Error("Not Submitted");
        }

        /*this.data = data.payload;
      console.log("child data:", this.data);*/
      },
      error => {
        this.toasterService.Error("Error while submitting data.");
      }
    ); /*
    if (this.childForm.valid) {
      //alert("Report Generated Successfully!");
      //this.getChild();
    }
    this.childForm.reset();*/
  }

  loadAttendence(stu_attendance) {
    const SA = [];
    
    this.getAttendanceBySelectedChildId(this.selected_child_info.uid);
    
    const todayDate1 = moment().startOf("day");
    const YM = todayDate1.format("YYYY-MM");
   

    for (const s of stu_attendance) {

      if (s.isPresent) {
        SA.push({
          title: "present",
          start: s.date,
          description: "present",
          className: "m-fc-event--danger m-fc-event--solid-warning"
        });
      } else {
        SA.push({
          title: "absent",
          start: s.absent,
          description: "absent",
          className: "m-fc-event--danger m-fc-event--solid-warning"
        });
      }
    }

    /*   if ($('#m_calendar_attendance').length === 0) {
         return;
       }*/

    /*  const todayDate = moment().startOf('day');
      console.log('hii')

      const month = todayDate.format('YYYY-MM');
      console.log('current month: ' + month);


      $('#m_calendar_attendance').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay,listWeek'
        },

        editable: true,
        eventLimit: true, // allow 'more' link when too many events
        navLinks: true,
        defaultDate: moment(todayDate),

        events: SA,

        eventRender: function(event, element) {
          if (element.hasClass('fc-day-grid-event')) {
            element.data('content', event.description);
            element.data('placement', 'top');
            mApp.initPopover(element);
          } else if (element.hasClass('fc-time-grid-event')) {
            element.find('.fc-title').append('<div class= "fc-description">' + event.description + '</div>');
          } else if (element.find('.fc-list-item-title').lenght !== 0) {
            element.find('.fc-list-item-title').append('<div class= "fc-description">' + event.description + '</div>');
          }
        }

      }); */
  }

  /** Student Subject Attendance Bar Chart */
  attendanceChart = function() {

    if ($("#m_chart_attendance").length === 0) {
      return;
    }

    const chart = Chartist.Pie(
      "#m_chart_attendance",
      {
        series: [
          {
            value: this.present_percent,
            className: "present",
            meta: {
              color: "#28a745"
            }
          },
          {
            value: this.absent_percent,
            className: "absent",
            meta: {
              color: "#dc3545"
            }
          },
          {
            value: this.leave_percent,
            className: "leave",
            meta: {
              color: "#ffb822"
            }
          }
        ]
      },
      {
        donut: true,
        donutWidth: 30,
        showLabel: true
      }
    );

    chart.on("draw", function(data) {
      if (data.type === "slice") {
        // Get the total path length in order to use for dash array animation
        const pathLength = data.element._node.getTotalLength();

        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
          "stroke-dasharray": pathLength + "px " + pathLength + "px"
        });

        // Create animation definition while also assigning an ID to the animation for later sync usage
        const animationDefinition = {
          "stroke-dashoffset": {
            id: "anim" + data.index,
            dur: 1000,
            from: -pathLength + "px",
            to: "0px",
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: "freeze",
            stroke: data.meta.color
          }
        };

        // // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if (data.index !== 0) {
          animationDefinition["stroke-dashoffset"]["begin"] =
            "anim" + (data.index - 1) + ".end";
        }

        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

        data.element.attr({
          "stroke-dashoffset": -pathLength + "px",
          stroke: data.meta.color
        });

        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
      }
    });
  };
  onSubmit() {
    // console.log(this.childForm.value.capacity);
    this._transportService
      .childCode(this.childForm.value.childCode)
      .subscribe(data => (this.data = data.payload));
    //  console.log('report generate' + this.data);
    /* alert('Report Generated Successfully');*/
    if (this.childForm.valid) {
      //   alert('Report Generated Successfully!');
      this.getChild();
    }
    this.childForm.reset();
  }

  getExamination() {
    let result = true;
    this._examinationService.getStudentExam(this.myChild.uid).subscribe(
      exam_data => {
        this.exam_data = exam_data.payload;
      },
      error => {
        console.error("Error while getting exam data. stack: ", error);
      }
    );
  }

  /** Get Homework data for student */
  getHomework() {
    this._homeworkService.getHomeworkById(this.myChild.uid).subscribe(
      homework_data => {
        if (homework_data !== null) {
          this.homework_data = homework_data.payload;
        }
      },
      error => {
        console.error("Error while getting homework data. stack: ", error);
      }
    );
  }

  getHomeworkDetails(homework) {
    this.homeworkDetails = homework;
    $("#m_modal_homework").modal("show");
  }

  DatatableDataLocalDemo(exam) {
    // == Private functions

    // demo initializer
    /*
        var demo = function() {

          // console.log('exam data:', exam);


          var datatable = $('.m_datatable').mDatatable({

            data: {
              type: 'local',
              source: exam,
              pageSize: 10
            },

            // layout definition
            layout: {
              theme: 'default', // datatable theme
              class: '', // custom wrapper class
              scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
              // height: 450, // datatable's body's fixed height
              footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
              input: $('#generalSearch')
            },

            columns: [{
              field: 'subject',
              title: 'subject',
              width: 60,
              textAlign: 'center',
            }, {
              field: 'title',
              title: 'title',
              textAlign: 'center',
            }, {
              field: 'description',
              title: 'description',
              width: 180,
              textAlign: 'center',
            }, {
              field: 'totalMarks',
              title: 'total Marks',
              width: 50,
              textAlign: 'center',
            },

              // TODO: add class = std+div
              {
                field: 'std',
                //   field: "div",
                title: 'class',
                width: 80,
                textAlign: 'center',
              }, /!*{
              field: "",
              title: "division",
              width: 70,
              textAlign: 'center',
            },*!/
              {
                field: 'date',
                title: 'date',
                type: 'date',
                format: 'MM/DD/YYYY',
                width: 70,
                textAlign: 'center',
              }, {
                field: 'time',
                title: 'time',
                width: 50,
                textAlign: 'center',
              }, {
                field: 'duration',
                title: 'duration',
                width: 70,
                textAlign: 'center',
              },


              {
                field: 'state',
                title: 'state',
                width: 80,
                textAlign: 'center',

                // TODO: css is not working(can't read property 'class' of undefined)
                // template: function (row) {
                //   var status = {
                //     1: {'title': 'In-Progress', 'class': 'm-badge--warning'},
                //     2: {'title': 'Completed', 'class': 'm-badge--success'},
                //     3: {'title': 'Scheduled', 'class': 'm-badge--danger'},
                //   };
                //   return '<span class="m-badge ' + status[row.state].class + ' m-badge--wide">' + status[row.state].title + '</span>';
                // }

              },
              {
                field: this.result_data,
                title: 'result',
                width: 70,
                textAlign: 'center',
              },

            ]
          });

          var query = datatable.getDataSourceQuery();

          $('#m_form_status').on('change', function() {
            datatable.search($(this).val(), 'Status');
          }).val(typeof query.Status !== 'undefined' ? query.Status : '');

          $('#m_form_type').on('change', function() {
            datatable.search($(this).val(), 'Type');
          }).val(typeof query.Type !== 'undefined' ? query.Type : '');

          $('#m_form_status, #m_form_type').selectpicker();

        };
    */

    return {
      // == Public functions
      init: function() {
        // init dmeo
        // demo();
      }
    };
  }
  getChildDetails(myChild) {
    this.childDetailsView = true;
    this.myChild = myChild;
    this.schoolCode = myChild.schoolCode;
    this.selected_child_info = myChild;
    //this.getProfileChild(myChild);
  }
  getchildlist() {
    this.childDetailsView = false;
  }

  /** Student Attendace show by Calendar */
  loadCalender(event) {
    const eventsArr = [];
    const recall_value = 1;
    let recall_status = false;
    for (const e of event) {
      /*console.log('student attendance', e);*/
      eventsArr.push({
        title: e.title,
        start: e.date,
        description: e.description,
        className: "m-fc-event--light m-fc-event--solid-warning"
      });
    }
    if ($("#m_calendar_event").length === 0) {
      return;
    }
    const YMcan = this.calendar_date;
    /*const todayDate = moment(YMcan).format('YYYY-MM-DD');*/
    const todayDate = moment(YMcan).startOf("day");
    const YESTERDAY = todayDate
      .clone()
      .subtract(1, "month")
      .format("YYYY-MM-DD");
    $("#m_calendar_event").fullCalendar({
      header: {
        left: "prev",
        center: "title",
        right: "next" // agendaWeek,agendaDay,listWeek
      },
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      navLinks: false,
      defaultDate: moment(todayDate),
      showNonCurrentDates: false,
      fixedWeekCount: false,
      dayRender: function(date, cell) {
        const today = $.fullCalendar.moment();
        for (const eventdata of event) {
          const eventdate = eventdata.date;
          recall_status = true;
          const calendarDates = date._d;
          const calendarDate = moment(calendarDates).format("YYYY-MM-DD");
          const event_date = moment(eventdate).format("MM");
          const days = moment(calendarDates).format("DD");
          const calendar_date = moment(calendarDate).format("MM");

          if (calendarDate === eventdate) {
            $("td")
              .find("[data-date='" + calendarDate + "']")
              .css("color", "white");
            if (eventdata.isPresent) {
              cell.css("background", "green");
              cell.css("color", "#fff");
              cell.css("border-radius", "40px");
              cell.css("text-align", "center");
              cell.addClass("class", "set-text-color");
            } else if (!eventdata.isPresent && !eventdata.onLeave) {
              cell.css("background", "red");
              cell.css("textColor", "white");
              cell.css("border-radius", "40px");
              cell.addClass("class", "set-text-color");
            } else if (!eventdata.isPresent && eventdata.onLeave) {
              cell.css("background", "#ffb822");
              cell.css("textColor", "white");
              cell.css("border-radius", "40px");
              cell.addClass("class", "set-text-color");
            }
          } else {
          }
        }
      },
      //events: eventsArr,
      eventRender: function(event, element) {
        if (element.hasClass("fc-day-grid-event")) {
          element.data("content", event.description);
          element.data("placement", "top");
          mApp.initPopover(element);
        } else if (element.hasClass("fc-time-grid-event")) {
          element
            .find(".fc-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        } else if (element.find(".fc-list-item-title").lenght !== 0) {
          element
            .find(".fc-list-item-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        }
      }
    });

    if (this.calendar_btn_status == true) {
      this.calendar_btn_status = false;
      $(".fc-prev-button").on("click", e => {
        this.getAttendanceforstudentByChange(null, true);
      });
      $(".fc-next-button").on("click", e => {
        this.getAttendanceforstudentByChange(null, false);
      });
    }

    if (recall_status) {
      $(".fc-prev-button").on("click", e => {
        this.getAttendanceforstudentByChange(null, true);
      });
      $(".fc-next-button").on("click", e => {
        this.getAttendanceforstudentByChange(null, false);
      });
    }
  }

  getAttendanceforstudentByChange(
    recall_value: any,
    isPreviousMonthAttandance: boolean
  ) {
    /*  const calendar_current_month = $('#m_calendar_event').fullCalendar('getDate');
      const YMcan = calendar_current_month;
      console.log('YMcan', YMcan);*/
    const todayDate1 = moment().startOf("day");
    const YM = isPreviousMonthAttandance
      ? moment(this.calendar_date)
          .subtract(1, "month")
          .format("YYYY-MM-DD")
      : moment(this.calendar_date)
          .add(1, "month")
          .format("YYYY-MM-DD");
    // const YM = todayDate1.format('YYYY-MM-DD');
    this.attendance1 = [];
    this.calendar_date = YM;
    this._attendanceService
      .getAttendanceForStudent(this.myChild.uid, YM)
      .subscribe(attendance => {
        
        if (attendance !== null) {
          this.attendance1 = attendance.payload;
          this.present = 0;
          this.absent = 0;
          this.leave = 0;
          this.present_percent = 0;
          this.absent_percent = 0;
          this.leave_percent = 0;
          this.day_count = 0;
          for (const a of this.attendance1) {
            this.day_count++;
            if (a.isPresent) {
              this.present++;
            } else if (!a.isPresent && !a.onLeave) {
              this.absent++;
            } else if (!a.isPresent && a.onLeave) {
              this.leave++;
            }
          }
          this.present_percent = (
            (this.present / this.day_count) *
            100
          ).toFixed(2);
          this.absent_percent = ((this.absent / this.day_count) * 100).toFixed(
            2
          );
          this.leave_percent = ((this.leave / this.day_count) * 100).toFixed(2);
          $("#m_calendar_event").fullCalendar("destroy");
          $("#m_calendar_event").fullCalendar("render");
          this.attendanceChart();
          this.loadCalender(this.attendance1);
        } else {
          this.attendance2 = {};
          this.present = 0;
          this.absent = 0;
          this.leave = 0;
          this.day_count = 0;
          this.present_percent = 50;
          this.absent_percent = 25;
          this.leave_percent = 25;
          this.attendanceChart();
          this.loadCalender(this.attendance1);
        }
      });
  }

  /** Get Student Examination Data */
  getStudentPerformanceBySubject() {
    this._performance
      .getStudentPerfomanceBySubject(this.myChild.uid, this.schoolCode)
      .subscribe(
        subjectList => {
          if (subjectList !== null) {
            this.subjectList = subjectList.payload;
            this.performaceChart(this.subjectList);
          }
        },
        error => {
          console.error("Error while getting exam list. stack: ", error);
        }
      );
  }

  /** Student Subject perfomance Bar Chart */
  performaceChart = function(subjectList) {
    if ($("#m_chart_performance").length === 0) {
      return;
    }
    const series = [];
    const label = [];
    for (const e of subjectList) {
      series.push(e.performance);
      label.push(e.subject + "\n" + e.performance + "%");
    }
    const chart_data = Chartist.Bar(
      "#m_chart_performance",
      {
        labels: label,
        series: series
      },
      {
        barwidth: 100,
        distributeSeries: true,
        // low: 0,
        // high: 100,
        axisX: {
          showGrid: false
        },
        axisY: {
          showGrid: false,
          showLabel: false,
          offset: 0
        }
      }
    );
    chart_data.on("draw", function(data) {
      if (data.type === "bar") {
        data.element.attr({
          style: "stroke-width: 20px; "
        });
      }
    });
  };
}
