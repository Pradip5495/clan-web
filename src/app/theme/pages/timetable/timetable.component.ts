import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from "@angular/core";
import { IdentityService } from "../../../_services/identity.service";
import { TimetableService } from "../../../_services/timetable.service";
import { LectureService } from "../../../_services/lecture.service";
import { ChildService } from "../../../_services/child.service";
import { ToasterService } from "../../../_services/toaster.service";
import { ClassService } from "../../../_services/class.service";
import { environment } from "../../../../environments/environment";
import { Observable } from "rxjs/Observable";
import { Response } from "../../../_models";
import { FormBuilder, FormGroup, FormArray, Validators } from "@angular/forms";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Pipe, PipeTransform } from "@angular/core";
import { TimeFormat } from "./timeformat.pipe";
import { ArrayValidators } from "./array.validator";
import { Data } from "../../../_provider/data/data";
import { DatePipe } from "@angular/common";
import "rxjs/add/operator/map";

declare var $: any;
declare const mApp, moment, Chartist, todayDate;
@Component({
  selector: "app-timetable",
  templateUrl: "./timetable.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./timetable.component.css"],
  providers: [TimeFormat]
})
export class TimetableComponent implements OnInit, AfterViewInit {
  public timetables: any;
  public class: any;
  public role: any;
  public schoolCode: any;
  public leacture_data: any;
  public static_lecture_data: any = [];
  public std_data: any = [];
  public div_data: any = [];
  public static_std_data: any = [];
  public static_div_data: any = [];
  public selected_std_value: any = null;
  public selected_div_value: any = null;
  public image_file: File = null;
  //public child_list: any;
  public currentDay: any;
  public sessionList: any;
  public sessionTimeSlot: any;
  public timetableStart: any;
  public class_data: any;
  public classData: any;
  public classContentView: any = false;
  public timetableview: any = false;
  public templateData: any;
  public selectClass: any;
  public teacherList: any;
  public std: any;
  public div: any;
  public isClassActive: any = "disabled";
  public sessionData: any;
  public timetableData: any;
  public templateTab: any = "mon";
  public isHoliday: any = [];
  public isHolidayUpdate: any = [];
  public tabView: any = [];
  public tempData: any;
  public isHolidayFlag: any;
  public currentSession: any;
  public sessionClassList: any;
  public istimetableCreated: any = false;
  public selectedChild: any;
  public profile: any;
  public _child_list: any;
  public _school_name: any;
  public uid: any;
  public studentDetails: any;
  public _school_code: any;
  public colors;

  /** Multiselect Dropdown Var */
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  userForm: FormGroup;
  sessionForm: FormGroup;
  timetableSlotsForm: FormGroup; // New Slots Template Generate
  updateTimetableSlotsForm: FormGroup; // Update Slots Template
  timeTableData: FormArray;
  slots: FormArray;

  timetable: FormGroup;
  timetableWeekData: FormArray;
  timetableSlots: FormArray;

  updateTimetable: FormGroup;

  slot: FormGroup;
  @ViewChild("fileInput") fileInput;

  constructor(
    private _identityService: IdentityService,
    private timetableService: TimetableService,
    private lectureService: LectureService,
    private formBuilder: FormBuilder,
    private toasterService: ToasterService,
    private http: HttpClient,
    private _child: ChildService,
    private timeFormat: TimeFormat,
    private _classService: ClassService
  ) {
    this.timetables = null;
    this.colors = ['#53D476', '#078EFC', '#31A3AB', '#FF6565', '#FFB300', '#6B5AED', '#DCA758', '#F82ABA', '#BABF24', '#A067D1'];
    this.class = _identityService.class;
    this.profile = _identityService.identity;
    this.role = _identityService.role;
    this.schoolCode = _identityService.school;
  }

  ngOnInit() {
    const todayDate1 = moment().startOf("day");
    this.currentDay = todayDate1.format("YYYY-MM-DD");

    this.userForm = this.formBuilder.group({
      imageFile: [null, Validators.required]
      // std: std [],
      //  div: div []
    });

    if (this.role == "parent") {
      if (localStorage.getItem("selectedChild")) {
        this.selectedChild = JSON.parse(localStorage.getItem("selectedChild"));
        
        this.uid = this.selectedChild.uid;
        this.schoolCode = this.selectedChild.schoolCode;
        this.studentDetails = this.selectedChild;
        this.profile = this.selectedChild;
        let std = this.selectedChild.std;
        let div = this.selectedChild.div;
        let studClass = std + "" + div;
       
        this.getChildClassTimetable(studClass, this.schoolCode);
      } else {
        this.getChild();
      }
    } else {
      this.getAllTimeTable();
    //  this.getLecture();
      this.getStaticLecture();
      this.getClass();
    }

    this.sessionForm = this.formBuilder.group({
      name: ["", Validators.required],
      startTime: [],
      endTime: [],
      class: [this.selectedItems]
    });

    this.timetableSlotsForm = this.formBuilder.group({
      sessionUid: [],
      timeTableData: this.formBuilder.array([this.addtimeTableData()])
    });

    this.updateTimetableSlotsForm = this.formBuilder.group({
      timeTableData: this.formBuilder.array([this.addtimeTableData()])
    });

    this.slot = this.formBuilder.group({
      dayOfWeek: [],
      isWeekOff: [],
      noOfLecture: [],
      periodTime: [],
      recessTime: []
    });

    this.timetable = this.formBuilder.group({
      timeTableData: this.formBuilder.array([this.addtimeTableWeekData()])
    });

    this.updateTimetable = this.formBuilder.group({
      timeTableCode: [],
      std: [],
      div: [],
      dayOfWeek: [],
      isWeekOff: [],
      slots: this.formBuilder.array([this.timetableUpateSlots()])
    });

    if (this.role == "student") {
      let std = this.profile.std;
      let div = this.profile.div;
      let studClass = std + "" + div;
      this.getClassTimetable(studClass);
    }

    this.selectedItems = [];
    this.dropdownSettings = {
      singleSelection: false,
      text: "Choose Class",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
  }

  

  ngAfterViewInit() {}

  get school_name(): any {
    return this._school_name;
  }

  set school_name(value: any) {
    this._school_name = value;
  }

  get school_code(): any {
    return this._school_code;
  }

  set school_code(value: any) {
    this._school_code = value;
  }

  get child_list(): any {
    return this._child_list;
  }

  set child_list(value: any) {
    this._child_list = value;
  }

  getParamsUid() {
  
  }

  onItemSelect(item: any) {
    
  }
  OnItemDeSelect(item: any) {
  
  }
  onSelectAll(items: any) {
   
  }
  onDeSelectAll(items: any) {
    
  }

  getColor(colorId, id, idx) {
    if (colorId > 6) {
      if (id > idx) {
        return id - idx + 1;
      } else {
        return idx - id + 1;
      }
    } else {
      return colorId;
    }
  }
  /** Get School Class List */
  getClass() {
    this.timetableService.getAvailableTimetableClass(this.schoolCode).subscribe(
      class_data => {
        if (class_data.status == 200) {
          this.class_data = class_data.payload;
          for (let i = 0; i < this.class_data.length; i++) {
            let options = {
              id: i,
              std: this.class_data[i].std,
              div: this.class_data[i].div,
              itemName: this.class_data[i].class
            };
            this.dropdownList.push(options);
          }
          this.classContentView = true;
          this.timetableview = false;
        }
      },
      error => {
        console.error("Error while getting class data. stack: ", error);
      }
    );
  }

  /** get Child list of Parent */
  getChild() {
    let value: any;
    this._child.getChild().subscribe(data => {
      this._child_list = data.payload;
      $("#m_modal_5").modal("show");
    });
  }

  /** Set Child list for View rendar */
  setChild(data) {
   
    localStorage.setItem("selectedChild", JSON.stringify(data));
    this.uid = data.uid;
    $("#m_modal_5").modal("hide");
  }

  /** AddtimeTableData Dynamically */
  addtimeTableData() {
    return this.formBuilder.group({
      dayOfWeek: "",
      isWeekOff: "",
      slots: this.formBuilder.array([])
    });
  }

  addslots() {
    return this.formBuilder.group({
      startTime: "",
      endTime: "",
      isRecess: ""
    });
  }

  timetableUpateSlots() {
    return this.formBuilder.group({
      teachingCode: "",
      startTime: "",
      endTime: "",
      isRecess: ""
    });
  }

  /** new Time table week data FormArray */
  addtimeTableWeekData() {
    return this.formBuilder.group({
      std: "",
      div: "",
      dayOfWeek: "",
      isWeekOff: "",
      slots: this.formBuilder.array([this.addDaySlotsData()])
    });
  }

  /** new Time table Day Slots data FormArray */
  addDaySlotsData() {
    return this.formBuilder.group({
      startTime: "",
      endTime: "",
      isRecess: "",
      teachingCode: ["", Validators.required]
    });
  }

  getTimedata(): FormArray {
    return this.timetableSlotsForm.get("timeTableData") as FormArray;
  }

  setSlots(Index: number): FormArray {
    return this.getTimedata()
      .at(Index)
      .get("slots") as FormArray;
  }
  /** Set Assign Master Form Data */
  setTimetableSlots(index) {
    let amount = 0;
    let formArray = this.timetableSlotsForm.controls[
      "timeTableData"
    ] as FormArray;
    let timeTableSlotgenerate = this.slot.value;
    this.tabView.push(index);
    
    // For New Sub Details generate formArray
    const slotsformArray = this.addtimeTableData().controls[
      "slots"
    ] as FormArray;

    while (slotsformArray.length) {
      slotsformArray.removeAt(slotsformArray.length - 1);
    }

    let startTime = this.sessionTimeSlot.startTime;
    let endTime = "";

    /** Convert string time to 24 hour format */
    // let startTime24 = this.timeFormat.transform(startTime).split(":");
    // let endTime24 = this.timeFormat.transform(endTime).split(":");
    // console.log('endTime24: ', endTime24);

    // let startTimeconvert = new Date().setHours(
    //   Number(startTime24[0]),
    //   Number(startTime24[1])
    // );
    // let endTimeconvert = new Date().setHours(
    //   Number(endTime24[0]),
    //   Number(endTime24[1])
    // );

    // console.log('endTimeconvert: ', endTimeconvert);
    for (let line = 0; line < timeTableSlotgenerate.noOfLecture; line++) {
      if (line === 0) {
        // let newtime = new Date(
        //   startTimeconvert + timeTableSlotgenerate.periodTime * 60000
        // );
        // let getStartTime = moment(newtime).format('h:mm A');
        // console.log('getStartTime: ', getStartTime);
        // this.timetableStart =
        //   startTimeconvert + timeTableSlotgenerate.periodTime * 60000;
        // console.log('timetablStartTime-1: ', this.timetableStart);
        endTime = moment(startTime, ["h:mm A"])
          .add(timeTableSlotgenerate.periodTime, "minutes")
          .format("hh:mm A");
        const subdata = this.formBuilder.group({
          startTime: startTime,
          endTime: endTime,
          isRecess: false
        });
       
        slotsformArray.push(subdata);
       
      } else {
        startTime = endTime;
        endTime = moment(startTime, ["h:mm A"])
          .add(timeTableSlotgenerate.periodTime, "minutes")
          .format("hh:mm A");
        

        // console.log('timetablStartTime: ', this.timetableStart);
        // let newtime = new Date(
        //   this.timetableStart + timeTableSlotgenerate.periodTime * 60000
        // );

        // console.log('newtime eld: ', newtime);
        // let getStartTime = moment(this.timetableStart).format('h:mm A');
        // let getEndtTime = moment(newtime).format('h:mm A');
        // console.log('getStartTime: ', getStartTime);
        // this.timetableStart =
        //   this.timetableStart + timeTableSlotgenerate.periodTime * 60000;
        const subdata = this.formBuilder.group({
          startTime: startTime,
          endTime: endTime,
          isRecess: false
        });
        slotsformArray.push(subdata);
      }
    }
    const timetableData = this.formBuilder.group({
      dayOfWeek: index + 1,
      isWeekOff: false,
      recessTime: timeTableSlotgenerate.recessTime,
      lectureTime: timeTableSlotgenerate.periodTime,
      noOfLecture: timeTableSlotgenerate.noOfLecture,
      slots: slotsformArray
    });
    let removeIndex = <FormArray>this.timetableSlotsForm.controls.timeTableData;
    removeIndex.removeAt(index);
    formArray.insert(index, timetableData);
    this.timetableSlotsForm.patchValue(formArray);
    
    // $('#NewFeeMaster').modal('show');
  }

  /** Add  Recess in template form by index at position of id */
  addRecessTemplate(index, id) {
    let amount = 0;
    let formArray = this.timetableSlotsForm.controls[
      "timeTableData"
    ] as FormArray;
    let timeTableSlotgenerate = this.slot.value;

    // For New Sub Details generate formArray
    const slotsformArray = this.addtimeTableData().controls[
      "slots"
    ] as FormArray;

    while (slotsformArray.length) {
      slotsformArray.removeAt(slotsformArray.length - 1);
    }

    const slotformArray = this.timetableSlotsForm.controls["timeTableData"]
      .value[index];
    const slotArray = slotformArray.slots;

    
    let startTime = slotformArray.slots[0].startTime;
    let endTime = slotformArray.slots[0].endTime;

    /** Convert string time to 24 hour format */
    let startTime24 = this.timeFormat.transform(startTime).split(":");
    let endTime24 = this.timeFormat.transform(endTime).split(":");

    let startTimeconvert = new Date().setHours(
      Number(startTime24[0]),
      Number(startTime24[1])
    );
    let endTimeconvert = new Date().setHours(
      Number(endTime24[0]),
      Number(endTime24[1])
    );

    
    let noOfLecture = Number(slotformArray.noOfLecture) + 1;
    for (let line = 0; line < noOfLecture; line++) {
      let timetablStartTime = startTime;
      let timetablEndTime = endTime;
      let isrecess = false;
      if (line < slotformArray.noOfLecture) {
        isrecess = slotArray[line].isRecess;
      }

      if (isrecess) {
        if (line === 0) {
          let newtime = new Date(
            startTimeconvert + Number(slotformArray.recessTime) * 60000
          );
          let getStartTime = moment(newtime).format("h:mm A");
         
          this.timetableStart =
            startTimeconvert + Number(slotformArray.recessTime) * 60000;
          
          const subdata = this.formBuilder.group({
            startTime: startTime,
            endTime: getStartTime,
            isRecess: true
          });
         
          slotsformArray.push(subdata);
          
        } else {
         
          let newtime = new Date(
            this.timetableStart + Number(slotformArray.recessTime) * 60000
          );
         
          let getStartTime = moment(this.timetableStart).format("h:mm A");
          let getEndtTime = moment(newtime).format("h:mm A");
         
          this.timetableStart =
            this.timetableStart + Number(slotformArray.recessTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: getStartTime,
            endTime: getEndtTime,
            isRecess: true
          });
          slotsformArray.push(subdata);
        }
      } else if (line === id) {
        if (line === 0) {
          let newtime = new Date(
            startTimeconvert + Number(slotformArray.recessTime) * 60000
          );
          let getStartTime = moment(newtime).format("h:mm A");
          
          this.timetableStart =
            startTimeconvert + Number(slotformArray.recessTime) * 60000;
          
          const subdata = this.formBuilder.group({
            startTime: startTime,
            endTime: getStartTime,
            isRecess: false
          });
         
          slotsformArray.push(subdata);
         
        } else {
         
          let newtime = new Date(
            this.timetableStart + Number(slotformArray.recessTime) * 60000
          );
          
          let getStartTime = moment(this.timetableStart).format("h:mm A");
          let getEndtTime = moment(newtime).format("h:mm A");
          
          this.timetableStart =
            this.timetableStart + Number(slotformArray.recessTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: getStartTime,
            endTime: getEndtTime,
            isRecess: true
          });
          slotsformArray.push(subdata);
        }
      } else {
        if (line === 0) {
          let newtime = new Date(
            startTimeconvert + Number(slotformArray.lectureTime) * 60000
          );
         
          let getStartTime = moment(newtime).format("h:mm A");
          
          this.timetableStart =
            startTimeconvert + Number(slotformArray.lectureTime) * 60000;
         
          const subdata = this.formBuilder.group({
            startTime: startTime,
            endTime: getStartTime,
            isRecess: false
          });
         
          slotsformArray.push(subdata);
         
        } else {
          
          let newtime = new Date(
            this.timetableStart + Number(slotformArray.lectureTime) * 60000
          );
         
          let getStartTime = moment(this.timetableStart).format("h:mm A");
          let getEndtTime = moment(newtime).format("h:mm A");
          
          this.timetableStart =
            this.timetableStart + Number(slotformArray.lectureTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: getStartTime,
            endTime: getEndtTime,
            isRecess: false
          });
          slotsformArray.push(subdata);
        }
      }
    }
    const timetableData = this.formBuilder.group({
      dayOfWeek: index + 1,
      isWeekOff: false,
      recessTime: slotformArray.recessTime,
      lectureTime: slotformArray.lectureTime,
      noOfLecture: noOfLecture,
      slots: slotsformArray
    });
    const removeIndex = <FormArray>(
      this.timetableSlotsForm.controls.timeTableData
    );
    removeIndex.removeAt(index);
    formArray.insert(index, timetableData);
    this.timetableSlotsForm.patchValue(formArray);
    
  }

  /** Remove Field from template form by index and formarray index as of id */
  removeFieldTemplate(index, id) {
    let amount = 0;
    let formArray = this.timetableSlotsForm.controls[
      "timeTableData"
    ] as FormArray;
    let timeTableSlotgenerate = this.slot.value;

    // For New Sub Details generate formArray
    const slotsformArray = this.addtimeTableData().controls[
      "slots"
    ] as FormArray;

    while (slotsformArray.length) {
      slotsformArray.removeAt(slotsformArray.length - 1);
    }

    const slotformArray = this.timetableSlotsForm.controls["timeTableData"]
      .value[index];
    const slotArray = slotformArray.slots;

    
    let startTime = slotformArray.slots[0].startTime;
    let endTime = slotformArray.slots[0].endTime;

    /** Convert string time to 24 hour format */
    let startTime24 = this.timeFormat.transform(startTime).split(":");
    let endTime24 = this.timeFormat.transform(endTime).split(":");
    

    let startTimeconvert = new Date().setHours(
      Number(startTime24[0]),
      Number(startTime24[1])
    );
    let endTimeconvert = new Date().setHours(
      Number(endTime24[0]),
      Number(endTime24[1])
    );

    let noOfLecture = Number(slotformArray.noOfLecture);
    for (let line = 0; line < noOfLecture; line++) {
      let timetablStartTime = startTime;
      let timetablEndTime = endTime;
      let isrecess = false;
      if (line < slotformArray.noOfLecture) {
        isrecess = slotArray[line].isRecess;
      }

      if (line === id) {
        // const removeIndex = <FormArray>this.timetableSlotsForm.controls.timeTableData[index].constrols.slots;
        //       removeIndex.removeAt(index);
      } else if (isrecess) {
        if (line === 0) {
          let newtime = new Date(
            startTimeconvert + Number(slotformArray.recessTime) * 60000
          );
          let getStartTime = moment(newtime).format("h:mm A");
          
          this.timetableStart =
            startTimeconvert + Number(slotformArray.recessTime) * 60000;
          
          const subdata = this.formBuilder.group({
            startTime: startTime,
            endTime: getStartTime,
            isRecess: true
          });
         
          slotsformArray.push(subdata);
          
        } else {
         
          let newtime = new Date(
            this.timetableStart + Number(slotformArray.recessTime) * 60000
          );
          
          let getStartTime = moment(this.timetableStart).format("h:mm A");
          let getEndtTime = moment(newtime).format("h:mm A");
          
          this.timetableStart =
            this.timetableStart + Number(slotformArray.recessTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: getStartTime,
            endTime: getEndtTime,
            isRecess: true
          });
          slotsformArray.push(subdata);
        }
      } else {
        if (line === 0) {
          let newtime = new Date(
            startTimeconvert + Number(slotformArray.lectureTime) * 60000
          );
          
          
          
          let getStartTime = moment(newtime).format("h:mm A");
          
          this.timetableStart =
            startTimeconvert + Number(slotformArray.lectureTime) * 60000;
          
          const subdata = this.formBuilder.group({
            startTime: startTime,
            endTime: getStartTime,
            isRecess: false
          });
          
          slotsformArray.push(subdata);
          
        } else {
         
          let newtime = new Date(
            this.timetableStart + Number(slotformArray.lectureTime) * 60000
          );
          
          let getStartTime = moment(this.timetableStart).format("h:mm A");
          let getEndtTime = moment(newtime).format("h:mm A");
         
          this.timetableStart =
            this.timetableStart + Number(slotformArray.lectureTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: getStartTime,
            endTime: getEndtTime,
            isRecess: false
          });
          slotsformArray.push(subdata);
        }
      }
    }
    const timetableData = this.formBuilder.group({
      dayOfWeek: index + 1,
      isWeekOff: false,
      recessTime: slotformArray.recessTime,
      lectureTime: slotformArray.lectureTime,
      noOfLecture: noOfLecture - 1,
      slots: slotsformArray
    });
    const removeIndex = <FormArray>(
      this.timetableSlotsForm.controls.timeTableData
    );
    removeIndex.removeAt(index);
    formArray.insert(index, timetableData);
    this.timetableSlotsForm.patchValue(formArray);
    
  }

  /** Add Field in template form by index at id position */
  addFieldTemplate(index, idx, isrecessFlag) {
    const id = idx + 1;
    let formArray = this.timetableSlotsForm.controls[
      "timeTableData"
    ] as FormArray;
    let timeTableSlotgenerate = this.slot.value;

    // For New Sub Details generate formArray
    const slotsformArray = this.addtimeTableData().controls[
      "slots"
    ] as FormArray;

    while (slotsformArray.length) {
      slotsformArray.removeAt(slotsformArray.length - 1);
    }

    const slotformArray = this.timetableSlotsForm.controls["timeTableData"]
      .value[index];
    const slotArray = slotformArray.slots;

    
    let startTime = slotformArray.slots[0].startTime;
    let endTime = slotformArray.slots[0].endTime;

    /** Convert string time to 24 hour format */
    let startTime24 = this.timeFormat.transform(startTime).split(":");
    let endTime24 = this.timeFormat.transform(endTime).split(":");
    

    let startTimeconvert = new Date().setHours(
      Number(startTime24[0]),
      Number(startTime24[1])
    );
    let endTimeconvert = new Date().setHours(
      Number(endTime24[0]),
      Number(endTime24[1])
    );

    console.log("endTimeconvert: ", endTimeconvert);
    let noOfLecture = Number(slotformArray.noOfLecture);
    for (let line = 0; line < noOfLecture; line++) {
      let timetablStartTime = startTime;
      let timetablEndTime = endTime;
      let isrecess = false;
      if (line < slotformArray.noOfLecture) {
        isrecess = slotArray[line].isRecess;
      }

      if (line === id) {
        if (!isrecessFlag) {
          if (line === 0) {
            let newtime = new Date(
              startTimeconvert + Number(slotformArray.lectureTime) * 60000
            );
            let getStartTime = moment(newtime).format("h:mm A");
            
            this.timetableStart =
              startTimeconvert + Number(slotformArray.lectureTime) * 60000;
           
            const subdata = this.formBuilder.group({
              startTime: startTime,
              endTime: getStartTime,
              isRecess: false
            });
           
            slotsformArray.push(subdata);
            
          } else {
            
            let newtime = new Date(
              this.timetableStart + Number(slotformArray.lectureTime) * 60000
            );
            
            let getStartTime = moment(this.timetableStart).format("h:mm A");
            let getEndtTime = moment(newtime).format("h:mm A");
           
            this.timetableStart =
              this.timetableStart + Number(slotformArray.lectureTime) * 60000;
            const subdata = this.formBuilder.group({
              startTime: getStartTime,
              endTime: getEndtTime,
              isRecess: false
            });
            slotsformArray.push(subdata);
          }
        } else if (isrecessFlag) {
          if (line === 0) {
            let newtime = new Date(
              startTimeconvert + Number(slotformArray.recessTime) * 60000
            );
            let getStartTime = moment(newtime).format("h:mm A");
            
            this.timetableStart =
              startTimeconvert + Number(slotformArray.recessTime) * 60000;
            
            const subdata = this.formBuilder.group({
              startTime: startTime,
              endTime: getStartTime,
              isRecess: true
            });
           
            slotsformArray.push(subdata);
            
          } else {
           
            let newtime = new Date(
              this.timetableStart + Number(slotformArray.recessTime) * 60000
            );
            
            let getStartTime = moment(this.timetableStart).format("h:mm A");
            let getEndtTime = moment(newtime).format("h:mm A");
            
            this.timetableStart =
              this.timetableStart + Number(slotformArray.recessTime) * 60000;
            const subdata = this.formBuilder.group({
              startTime: getStartTime,
              endTime: getEndtTime,
              isRecess: true
            });
            slotsformArray.push(subdata);
          }
        }

        if (!isrecess) {
          if (line === 0) {
            let newtime = new Date(
              startTimeconvert + Number(slotformArray.lectureTime) * 60000
            );
            let getStartTime = moment(newtime).format("h:mm A");
            this.timetableStart =
              startTimeconvert + Number(slotformArray.lectureTime) * 60000;
            const subdata = this.formBuilder.group({
              startTime: startTime,
              endTime: getStartTime,
              isRecess: false
            });
            slotsformArray.push(subdata);
          } else {
            let newtime = new Date(
              this.timetableStart + Number(slotformArray.lectureTime) * 60000
            );
            let getStartTime = moment(this.timetableStart).format("h:mm A");
            let getEndtTime = moment(newtime).format("h:mm A");
            this.timetableStart =
              this.timetableStart + Number(slotformArray.lectureTime) * 60000;
            const subdata = this.formBuilder.group({
              startTime: getStartTime,
              endTime: getEndtTime,
              isRecess: false
            });
            slotsformArray.push(subdata);
          }
        } else if (isrecess) {
          if (line === 0) {
            let newtime = new Date(
              startTimeconvert + Number(slotformArray.recessTime) * 60000
            );
            let getStartTime = moment(newtime).format("h:mm A");
            this.timetableStart =
              startTimeconvert + Number(slotformArray.recessTime) * 60000;
            const subdata = this.formBuilder.group({
              startTime: startTime,
              endTime: getStartTime,
              isRecess: true
            });
            slotsformArray.push(subdata);
          } else {
            let newtime = new Date(
              this.timetableStart + Number(slotformArray.recessTime) * 60000
            );
            let getStartTime = moment(this.timetableStart).format("h:mm A");
            let getEndtTime = moment(newtime).format("h:mm A");
            this.timetableStart =
              this.timetableStart + Number(slotformArray.recessTime) * 60000;
            const subdata = this.formBuilder.group({
              startTime: getStartTime,
              endTime: getEndtTime,
              isRecess: true
            });
            slotsformArray.push(subdata);
          }
        }
      } else if (!isrecess) {
        if (line === 0) {
          let newtime = new Date(
            startTimeconvert + Number(slotformArray.lectureTime) * 60000
          );
          let getStartTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            startTimeconvert + Number(slotformArray.lectureTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: startTime,
            endTime: getStartTime,
            isRecess: false
          });
          slotsformArray.push(subdata);
        } else {
          let newtime = new Date(
            this.timetableStart + Number(slotformArray.lectureTime) * 60000
          );
          let getStartTime = moment(this.timetableStart).format("h:mm A");
          let getEndtTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            this.timetableStart + Number(slotformArray.lectureTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: getStartTime,
            endTime: getEndtTime,
            isRecess: false
          });
          slotsformArray.push(subdata);
        }
      } else if (isrecess) {
        if (line === 0) {
          let newtime = new Date(
            startTimeconvert + Number(slotformArray.recessTime) * 60000
          );
          let getStartTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            startTimeconvert + Number(slotformArray.recessTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: startTime,
            endTime: getStartTime,
            isRecess: true
          });
          slotsformArray.push(subdata);
        } else {
          let newtime = new Date(
            this.timetableStart + Number(slotformArray.recessTime) * 60000
          );
          let getStartTime = moment(this.timetableStart).format("h:mm A");
          let getEndtTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            this.timetableStart + Number(slotformArray.recessTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: getStartTime,
            endTime: getEndtTime,
            isRecess: true
          });
          slotsformArray.push(subdata);
        }
      }
    }
    const timetableData = this.formBuilder.group({
      dayOfWeek: index + 1,
      isWeekOff: false,
      recessTime: slotformArray.recessTime,
      lectureTime: slotformArray.lectureTime,
      noOfLecture: noOfLecture + 1,
      slots: slotsformArray
    });
    const removeIndex = <FormArray>(
      this.timetableSlotsForm.controls.timeTableData
    );
    removeIndex.removeAt(index);
    formArray.insert(index, timetableData);
    this.timetableSlotsForm.patchValue(formArray);
  }

  /** Add Holiday in template form by index */
  setIsHoliday(index, isChecked: boolean) {
    let amount = 0;
    let formArray = this.timetableSlotsForm.controls[
      "timeTableData"
    ] as FormArray;
    let timeTableSlotgenerate = this.slot.value;
    if (isChecked) {
      this.tabView.push(index);
      this.isHoliday.push(index);
      const timetableData = this.formBuilder.group({
        dayOfWeek: index + 1,
        isWeekOff: true,
        slots: [""]
      });
      let removeIndex = <FormArray>(
        this.timetableSlotsForm.controls.timeTableData
      );
      removeIndex.removeAt(index);
      formArray.insert(index, timetableData);
      this.timetableSlotsForm.patchValue(formArray);
    } else {
      const tabViewIndex: number = this.tabView.indexOf(index);
      const isHolidayIndex: number = this.isHoliday.indexOf(index);
      this.tabView.splice(index, tabViewIndex);
      this.isHoliday.splice(index, isHolidayIndex);
      const timetableData = this.formBuilder.group({
        dayOfWeek: index + 1,
        isWeekOff: false,
        slots: [""]
      });

      let removeIndex = <FormArray>(
        this.timetableSlotsForm.controls.timeTableData
      );
      removeIndex.removeAt(index);
      formArray.insert(index, timetableData);
      this.timetableSlotsForm.patchValue(formArray);
    }
    // $('#NewFeeMaster').modal('show');
  }

  findIsHoliday(id) {
    if (this.isHoliday.length) {
      if (this.isHoliday.some(x => x === id)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  findTabActive(id) {
    if (this.tabView.length) {
      if (this.tabView.some(x => x === id)) {
        return "";
      } else {
        return "disabled";
      }
    } else {
      return "disabled";
    }
  }

  tabActivete(day, id) {
    if (this.tabView.length) {
      if (this.tabView.some(x => x === id)) {
        this.templateTab = day;
        return "";
      } else {
        return "disabled";
      }
    } else {
      return "disabled";
    }
  }

  submitTimetableSlots() {
    let timetableTemplate = this.timetableSlotsForm.value;
    timetableTemplate.sessionUid = this.sessionTimeSlot.ssuid;
    this.timetableService.createTimetableTemplate(timetableTemplate).subscribe(
      templateData => {
        if (templateData.status == 201) {
          this.timetableSlotsForm.reset();
          $("#m_modal_timetable_slots").modal("hide");
        }
      },
      error => {}
    );
  }

  settimetableSlots(session) {
    this.sessionTimeSlot = session;
    this.timetableService.getTemplateDataByUid(session.ssuid).subscribe(
      templateData => {
        if (templateData.status === 200) {
          this.tempData = templateData.payload;
          if (this.tempData.timeSlotsDetails.length > 0) {
            this.setUpdateSlots(0);
            $("#m_modal_timetable_slots_update").modal("show");
          } else {
            $("#m_modal_timetable_slots").modal("show");
          }
        }
      },
      error => {}
    );
  }

  setUpdateTemplateValue(ssuid, index) {
    this.timetableService.getTemplateDataByUid(ssuid).subscribe(
      templateData => {
        if (templateData.status === 200) {
          this.tempData = templateData.payload;
          this.setUpdateSlots(index);
        }
      },
      error => {}
    );
  }

  /** Set Assign Master Form Data */
  setUpdateSlots(index) {
    let amount = 0;
    let formArray = this.updateTimetableSlotsForm.controls[
      "timeTableData"
    ] as FormArray;
    let timeTableSlotgenerate = this.slot.value;
    let updateTempData = this.tempData.timeSlotsDetails[index];
    this.tabView.push(index);
    // For New Sub Details generate formArray
    const slotsformArray = this.addtimeTableData().controls[
      "slots"
    ] as FormArray;

    while (slotsformArray.length) {
      slotsformArray.removeAt(slotsformArray.length - 1);
    }

    if (updateTempData.isWeekOff) {
      this.isHolidayFlag = true;
      const isHolidayIndex: number = this.isHoliday.findIndex(x => x == index);
      this.isHoliday.splice(isHolidayIndex, 1);
      this.isHoliday.push(index);
      const timetableData = this.formBuilder.group({
        wtuid: updateTempData.wtuid,
        dayOfWeek: Number(updateTempData.dayOfWeek),
        isWeekOff: Boolean(updateTempData.isWeekOff),
        slots: [""]
      });
      let removeIndex = <FormArray>(
        this.updateTimetableSlotsForm.controls.timeTableData
      );
      removeIndex.reset();
      while (removeIndex.length) {
        removeIndex.removeAt(removeIndex.length - 1);
      }
      formArray.insert(index, timetableData);
      this.updateTimetableSlotsForm.patchValue(formArray);
    } else {
      this.isHolidayFlag = false;
      const isHolidayIndex: number = this.isHoliday.indexOf(index);
      this.isHoliday.splice(isHolidayIndex, 1);

      let recessTime = 0;
      let periodTime = 0;
      /** Convert string time to 24 hour format */

      // console.log('endTimeconvert: ', endTimeconvert);
      let noOfLecture = 0;
      for (let line = 0; line < updateTempData.slots.length; line++) {
        let startTime = updateTempData.slots[line].startTime;
        let endTime = updateTempData.slots[line].endTime;

        var startTimeDate = moment(startTime, ["h:mm A"]);
        var endTimeDate = moment(endTime, ["h:mm A"]);

        if (updateTempData.slots[line].isRecess) {
          recessTime = moment
            .duration(endTimeDate.diff(startTimeDate))
            .asMinutes();
        } else {
          noOfLecture = noOfLecture + 1;
          periodTime = moment
            .duration(endTimeDate.diff(startTimeDate))
            .asMinutes();
        }

        // console.log('timetablStartTime: ', this.timetableStart);
        // let newtime = new Date(
        //   this.timetableStart + timeTableSlotgenerate.periodTime * 60000
        // );
        // console.log('newtime eld: ', newtime);
        // let getStartTime = moment(this.timetableStart).format('h:mm A');
        // let getEndtTime = moment(newtime).format('h:mm A');
        // console.log('getStartTime: ', getStartTime);

        // this.timetableStart =
        //   this.timetableStart + timeTableSlotgenerate.periodTime * 60000;

        //let recessTime =  moment("12:00 AM", ["h:mm A"]).add(30, 'minutes').format("hh:mm A" )

        //  startTime = endTime
        //  endTime = moment(startTime, ["h:mm A"]).add(timeTableSlotgenerate.periodTime, 'minutes').format("hh:mm A"
        const subdata = this.formBuilder.group({
          ttsuid: updateTempData.slots[line].ttsuid,
          startTime: updateTempData.slots[line].startTime,
          endTime: updateTempData.slots[line].endTime,
          isRecess: updateTempData.slots[line].isRecess
        });
        slotsformArray.push(subdata);
      }
      const timetableData = this.formBuilder.group({
        wtuid: updateTempData.wtuid,
        dayOfWeek: Number(updateTempData.dayOfWeek),
        isWeekOff: Boolean(updateTempData.isWeekOff),
        recessTime: recessTime,
        lectureTime: periodTime,
        noOfLecture: noOfLecture,
        slots: slotsformArray
      });

      let slots = this.slot.value;
      slots.noOfLecture = noOfLecture;
      slots.periodTime = periodTime;
      slots.recessTime = recessTime;

      let removeIndex = <FormArray>(
        this.updateTimetableSlotsForm.controls.timeTableData
      );
      removeIndex.reset();
      while (removeIndex.length) {
        removeIndex.removeAt(removeIndex.length - 1);
      }
      formArray.insert(index, timetableData);
      this.updateTimetableSlotsForm.patchValue(formArray);

      // $('#NewFeeMaster').modal('show');
    }
  }

  /** Add Holiday in template form by index */
  setUpdateIsHoliday(index, isChecked: boolean) {
    let amount = 0;
    let formArray = this.updateTimetableSlotsForm.controls[
      "timeTableData"
    ] as FormArray;
    let timeTableSlotgenerate = this.slot.value;
    let updateTempData = this.tempData.timeSlotsDetails[index];
    if (isChecked) {
      this.tabView.push(index);
      const isHolidayIndex: number = this.isHoliday.findIndex(x => x == index);
      //this.isHoliday.splice(isHolidayIndex, 1);
      this.findandDelete(index);
      this.isHoliday.push(index);
      const timetableData = this.formBuilder.group({
        wtuid: updateTempData.wtuid,
        dayOfWeek: Number(updateTempData.dayOfWeek),
        isWeekOff: true,
        slots: [""]
      });
      let removeIndex = <FormArray>(
        this.updateTimetableSlotsForm.controls.timeTableData
      );
      removeIndex.removeAt(index);
      while (removeIndex.length) {
        removeIndex.removeAt(removeIndex.length - 1);
      }
      formArray.insert(index, timetableData);
      this.updateTimetableSlotsForm.patchValue(formArray);
    } else {
      const tabViewIndex: number = this.tabView.indexOf(index);
      // const isHolidayIndex: number = this.isHoliday.indexOf(index);
      const isHolidayIndex: number = this.isHoliday.findIndex(x => x == index);
      this.tabView.splice(tabViewIndex, 1);
      // this.isHoliday.splice(isHolidayIndex, 1);
      this.findandDelete(index);
      const timetableData = this.formBuilder.group({
        wtuid: updateTempData.wtuid,
        dayOfWeek: Number(updateTempData.dayOfWeek),
        isWeekOff: false,
        slots: [""]
      });

      let removeIndex = <FormArray>(
        this.updateTimetableSlotsForm.controls.timeTableData
      );
      removeIndex.removeAt(index);
      while (removeIndex.length) {
        removeIndex.removeAt(removeIndex.length - 1);
      }
      formArray.insert(index, timetableData);
      this.updateTimetableSlotsForm.patchValue(formArray);
    }

    // $('#NewFeeMaster').modal('show');
  }

  findandDelete(id) {
    for (let i = 0; i < this.isHoliday.length; i++) {
      if (this.isHoliday[i] === id) {
        this.isHoliday.splice(i, 1);
      }
    }
  }

  /** Set Assign Master Form Data */
  setUpdateTimetableSlots(index) {
    let amount = 0;
    let formArray = this.updateTimetableSlotsForm.controls[
      "timeTableData"
    ] as FormArray;
    let timeTableSlotgenerate = this.slot.value;
    let updateTempData = this.tempData.timeSlotsDetails[index];
    this.tabView.push(index);
    // For New Sub Details generate formArray
    const slotsformArray = this.addtimeTableData().controls[
      "slots"
    ] as FormArray;

    while (slotsformArray.length) {
      slotsformArray.removeAt(slotsformArray.length - 1);
    }

    const slotformArray = this.updateTimetableSlotsForm.controls[
      "timeTableData"
    ].value[index];

    let wtuid = updateTempData.wtuid;
    let dayOfWeek = updateTempData.dayOfWeek;

    let startTime = this.sessionTimeSlot.startTime;
    let endTime = this.sessionTimeSlot.endTime;

    /** Convert string time to 24 hour format */
    let startTime24 = this.timeFormat.transform(startTime).split(":");
    let endTime24 = this.timeFormat.transform(endTime).split(":");

    let startTimeconvert = new Date().setHours(
      Number(startTime24[0]),
      Number(startTime24[1])
    );
    let endTimeconvert = new Date().setHours(
      Number(endTime24[0]),
      Number(endTime24[1])
    );

    for (let line = 0; line < timeTableSlotgenerate.noOfLecture; line++) {
      let timetablStartTime = startTime;
      let timetablEndTime = endTime;
      if (line === 0) {
        let newtime = new Date(
          startTimeconvert + timeTableSlotgenerate.periodTime * 60000
        );
        let getStartTime = moment(newtime).format("h:mm A");
        
        this.timetableStart =
          startTimeconvert + timeTableSlotgenerate.periodTime * 60000;
        const subdata = this.formBuilder.group({
          startTime: startTime,
          endTime: getStartTime,
          isRecess: false
        });
        slotsformArray.push(subdata);
      } else {
        let newtime = new Date(
          this.timetableStart + timeTableSlotgenerate.periodTime * 60000
        );
        let getStartTime = moment(this.timetableStart).format("h:mm A");
        let getEndtTime = moment(newtime).format("h:mm A");
        this.timetableStart =
          this.timetableStart + timeTableSlotgenerate.periodTime * 60000;
        const subdata = this.formBuilder.group({
          startTime: getStartTime,
          endTime: getEndtTime,
          isRecess: false
        });
        slotsformArray.push(subdata);
      }
    }
    const timetableData = this.formBuilder.group({
      wtuid: wtuid,
      dayOfWeek: dayOfWeek,
      isWeekOff: false,
      recessTime: timeTableSlotgenerate.recessTime,
      lectureTime: timeTableSlotgenerate.periodTime,
      noOfLecture: timeTableSlotgenerate.noOfLecture,
      slots: slotsformArray
    });
    let removeIndex = <FormArray>(
      this.updateTimetableSlotsForm.controls.timeTableData
    );
    removeIndex.removeAt(index);
    removeIndex.removeAt(0);
    formArray.insert(index, timetableData);
    this.updateTimetableSlotsForm.patchValue(formArray);
    // $('#NewFeeMaster').modal('show');
  }

  /** Add  Recess in template form by index at position of id */
  updateRecessTemplate(index, id) {
    let amount = 0;
    let formArray = this.updateTimetableSlotsForm.controls[
      "timeTableData"
    ] as FormArray;
    let timeTableSlotgenerate = this.slot.value;
    let updateTempData = this.tempData.timeSlotsDetails[index];
    // For New Sub Details generate formArray
    const slotsformArray = this.addtimeTableData().controls[
      "slots"
    ] as FormArray;

    while (slotsformArray.length) {
      slotsformArray.removeAt(slotsformArray.length - 1);
    }

    const slotformArray = this.updateTimetableSlotsForm.controls[
      "timeTableData"
    ].value[index];
    const slotArray = slotformArray.slots;
    let startTime = updateTempData.slots[0].startTime;
    let endTime = updateTempData.slots[0].endTime;

    /** Convert string time to 24 hour format */
    let startTime24 = this.timeFormat.transform(startTime).split(":");
    let endTime24 = this.timeFormat.transform(endTime).split(":");

    let startTimeconvert = new Date().setHours(
      Number(startTime24[0]),
      Number(startTime24[1])
    );
    let endTimeconvert = new Date().setHours(
      Number(endTime24[0]),
      Number(endTime24[1])
    );
    let noOfLecture = Number(slotformArray.slots.length) + 1;
    for (let line = 0; line < noOfLecture; line++) {
      let timetablStartTime = startTime;
      let timetablEndTime = endTime;
      let isrecess = false;
      if (line < slotformArray.slots.length) {
        isrecess = slotArray[line].isRecess;
      }
      if (isrecess) {
        if (line === 0) {
          let newtime = new Date(
            startTimeconvert + Number(slotformArray.recessTime) * 60000
          );
          let getStartTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            startTimeconvert + Number(slotformArray.recessTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: startTime,
            endTime: getStartTime,
            isRecess: true
          });
          slotsformArray.push(subdata);
        } else {
          let newtime = new Date(
            this.timetableStart + Number(slotformArray.recessTime) * 60000
          );
          let getStartTime = moment(this.timetableStart).format("h:mm A");
          let getEndtTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            this.timetableStart + Number(slotformArray.recessTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: getStartTime,
            endTime: getEndtTime,
            isRecess: true
          });
          slotsformArray.push(subdata);
        }
      } else if (line === id) {
        if (line === 0) {
          let newtime = new Date(
            startTimeconvert + Number(slotformArray.recessTime) * 60000
          );
          let getStartTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            startTimeconvert + Number(slotformArray.recessTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: startTime,
            endTime: getStartTime,
            isRecess: false
          });
          slotsformArray.push(subdata);
        } else {
          let newtime = new Date(
            this.timetableStart + Number(slotformArray.recessTime) * 60000
          );
          let getStartTime = moment(this.timetableStart).format("h:mm A");
          let getEndtTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            this.timetableStart + Number(slotformArray.recessTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: getStartTime,
            endTime: getEndtTime,
            isRecess: true
          });
          slotsformArray.push(subdata);
        }
      } else {
        if (line === 0) {
          let newtime = new Date(
            startTimeconvert + Number(slotformArray.lectureTime) * 60000
          );
          let getStartTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            startTimeconvert + Number(slotformArray.lectureTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: startTime,
            endTime: getStartTime,
            isRecess: false
          });
          slotsformArray.push(subdata);
        } else {
          let newtime = new Date(
            this.timetableStart + Number(slotformArray.lectureTime) * 60000
          );
          let getStartTime = moment(this.timetableStart).format("h:mm A");
          let getEndtTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            this.timetableStart + Number(slotformArray.lectureTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: getStartTime,
            endTime: getEndtTime,
            isRecess: false
          });
          slotsformArray.push(subdata);
        }
      }
    }
    const timetableData = this.formBuilder.group({
      wtuid: slotformArray.wtuid,
      dayOfWeek: Number(slotformArray.dayOfWeek),
      isWeekOff: Boolean(slotformArray.isWeekOff),
      recessTime: slotformArray.recessTime,
      lectureTime: slotformArray.lectureTime,
      noOfLecture: noOfLecture,
      slots: slotsformArray
    });
    const removeIndex = <FormArray>(
      this.updateTimetableSlotsForm.controls.timeTableData
    );
    removeIndex.removeAt(index);
    formArray.insert(index, timetableData);
    this.updateTimetableSlotsForm.patchValue(formArray);
  }

  /** Remove Field from template form by index and formarray index as of id */
  removeUpdateFieldTemplate(index, id) {
    let amount = 0;
    let formArray = this.updateTimetableSlotsForm.controls[
      "timeTableData"
    ] as FormArray;
    let timeTableSlotgenerate = this.slot.value;
    let updateTempData = this.tempData.timeSlotsDetails[index];
    // For New Sub Details generate formArray
    const slotsformArray = this.addtimeTableData().controls[
      "slots"
    ] as FormArray;

    while (slotsformArray.length) {
      slotsformArray.removeAt(slotsformArray.length - 1);
    }

    const slotformArray = this.updateTimetableSlotsForm.controls[
      "timeTableData"
    ].value[index];
    const slotArray = slotformArray.slots;

    let startTime = slotformArray.slots[0].startTime;
    let endTime = slotformArray.slots[0].endTime;

    /** Convert string time to 24 hour format */
    let startTime24 = this.timeFormat.transform(startTime).split(":");
    let endTime24 = this.timeFormat.transform(endTime).split(":");

    let startTimeconvert = new Date().setHours(
      Number(startTime24[0]),
      Number(startTime24[1])
    );
    let endTimeconvert = new Date().setHours(
      Number(endTime24[0]),
      Number(endTime24[1])
    );

    let noOfLecture = Number(slotformArray.slots.length);
    for (let line = 0; line < noOfLecture; line++) {
      let timetablStartTime = startTime;
      let timetablEndTime = endTime;
      let isrecess = false;
      if (line < slotformArray.noOfLecture) {
        isrecess = slotArray[line].isRecess;
      }

      if (line === id) {
        // const removeIndex = <FormArray>this.updateTimetableSlotsForm.controls.timeTableData[index].constrols.slots;
        //       removeIndex.removeAt(index);
      } else if (isrecess) {
        if (line === 0) {
          let newtime = new Date(
            startTimeconvert + Number(slotformArray.recessTime) * 60000
          );
          let getStartTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            startTimeconvert + Number(slotformArray.recessTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: startTime,
            endTime: getStartTime,
            isRecess: true
          });
          slotsformArray.push(subdata);
        } else {
          let newtime = new Date(
            this.timetableStart + Number(slotformArray.recessTime) * 60000
          );
          let getStartTime = moment(this.timetableStart).format("h:mm A");
          let getEndtTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            this.timetableStart + Number(slotformArray.recessTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: getStartTime,
            endTime: getEndtTime,
            isRecess: true
          });
          slotsformArray.push(subdata);
        }
      } else {
        if (line === 0) {
          let newtime = new Date(
            startTimeconvert + Number(slotformArray.lectureTime) * 60000
          );
          let getStartTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            startTimeconvert + Number(slotformArray.lectureTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: startTime,
            endTime: getStartTime,
            isRecess: false
          });
          slotsformArray.push(subdata);
        } else {
          let newtime = new Date(
            this.timetableStart + Number(slotformArray.lectureTime) * 60000
          );
          let getStartTime = moment(this.timetableStart).format("h:mm A");
          let getEndtTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            this.timetableStart + Number(slotformArray.lectureTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: getStartTime,
            endTime: getEndtTime,
            isRecess: false
          });
          slotsformArray.push(subdata);
        }
      }
    }
    const timetableData = this.formBuilder.group({
      wtuid: slotformArray.wtuid,
      dayOfWeek: Number(slotformArray.dayOfWeek),
      isWeekOff: Boolean(slotformArray.isWeekOff),
      recessTime: slotformArray.recessTime,
      lectureTime: slotformArray.lectureTime,
      noOfLecture: noOfLecture - 1,
      slots: slotsformArray
    });
    const removeIndex = <FormArray>(
      this.updateTimetableSlotsForm.controls.timeTableData
    );
    removeIndex.removeAt(index);
    formArray.insert(index, timetableData);
    this.updateTimetableSlotsForm.patchValue(formArray);
  }

  /** Add Field in template form by index at id position */
  updateFieldTemplate(index, idx, isrecessFlag) {
    const id = idx;
    let formArray = this.updateTimetableSlotsForm.controls[
      "timeTableData"
    ] as FormArray;
    let timeTableSlotgenerate = this.slot.value;
    let updateTempData = this.tempData.timeSlotsDetails[index];
    // For New Sub Details generate formArray
    const slotsformArray = this.addtimeTableData().controls[
      "slots"
    ] as FormArray;

    while (slotsformArray.length) {
      slotsformArray.removeAt(slotsformArray.length - 1);
    }

    const slotformArray = this.updateTimetableSlotsForm.controls[
      "timeTableData"
    ].value[index];
    const slotArray = slotformArray.slots;

    let startTime = slotformArray.slots[0].startTime;
    let endTime = slotformArray.slots[0].endTime;

    /** Convert string time to 24 hour format */
    let startTime24 = this.timeFormat.transform(startTime).split(":");
    let endTime24 = this.timeFormat.transform(endTime).split(":");

    let startTimeconvert = new Date().setHours(
      Number(startTime24[0]),
      Number(startTime24[1])
    );
    let endTimeconvert = new Date().setHours(
      Number(endTime24[0]),
      Number(endTime24[1])
    );

    let noOfLecture = Number(slotformArray.slots.length);
    for (let line = 0; line < noOfLecture; line++) {
      let timetablStartTime = startTime;
      let timetablEndTime = endTime;
      let isrecess = false;
     
      if (line < slotformArray.noOfLecture) {
        console.log("line: ", line);
        isrecess = slotformArray.slots[line].isRecess;
      }

      if (line === id) {
        if (!isrecessFlag) {
          if (line === 0) {
            let newtime = new Date(
              startTimeconvert + Number(slotformArray.lectureTime) * 60000
            );
            let getStartTime = moment(newtime).format("h:mm A");
            this.timetableStart =
              startTimeconvert + Number(slotformArray.lectureTime) * 60000;
            const subdata = this.formBuilder.group({
              startTime: startTime,
              endTime: getStartTime,
              isRecess: false
            });
            slotsformArray.push(subdata);
          } else {
            let newtime = new Date(
              this.timetableStart + Number(slotformArray.lectureTime) * 60000
            );
            let getStartTime = moment(this.timetableStart).format("h:mm A");
            let getEndtTime = moment(newtime).format("h:mm A");
            this.timetableStart =
              this.timetableStart + Number(slotformArray.lectureTime) * 60000;
            const subdata = this.formBuilder.group({
              startTime: getStartTime,
              endTime: getEndtTime,
              isRecess: false
            });
            slotsformArray.push(subdata);
          }
        } else if (isrecessFlag) {
          if (line === 0) {
            let newtime = new Date(
              startTimeconvert + Number(slotformArray.recessTime) * 60000
            );
            let getStartTime = moment(newtime).format("h:mm A");
            this.timetableStart =
              startTimeconvert + Number(slotformArray.recessTime) * 60000;
            const subdata = this.formBuilder.group({
              startTime: startTime,
              endTime: getStartTime,
              isRecess: true
            });
            slotsformArray.push(subdata);
          } else {
            let newtime = new Date(
              this.timetableStart + Number(slotformArray.recessTime) * 60000
            );
            let getStartTime = moment(this.timetableStart).format("h:mm A");
            let getEndtTime = moment(newtime).format("h:mm A");
            this.timetableStart =
              this.timetableStart + Number(slotformArray.recessTime) * 60000;
            const subdata = this.formBuilder.group({
              startTime: getStartTime,
              endTime: getEndtTime,
              isRecess: true
            });
            slotsformArray.push(subdata);
          }
        }

        if (!isrecess) {
          if (line === 0) {
            let newtime = new Date(
              startTimeconvert + Number(slotformArray.lectureTime) * 60000
            );
            let getStartTime = moment(newtime).format("h:mm A");
            this.timetableStart =
              startTimeconvert + Number(slotformArray.lectureTime) * 60000;
            const subdata = this.formBuilder.group({
              startTime: startTime,
              endTime: getStartTime,
              isRecess: false
            });
            slotsformArray.push(subdata);
          } else {
            let newtime = new Date(
              this.timetableStart + Number(slotformArray.lectureTime) * 60000
            );
            let getStartTime = moment(this.timetableStart).format("h:mm A");
            let getEndtTime = moment(newtime).format("h:mm A");
            this.timetableStart =
              this.timetableStart + Number(slotformArray.lectureTime) * 60000;
            const subdata = this.formBuilder.group({
              startTime: getStartTime,
              endTime: getEndtTime,
              isRecess: false
            });
            slotsformArray.push(subdata);
          }
        } else if (isrecess) {
          if (line === 0) {
            let newtime = new Date(
              startTimeconvert + Number(slotformArray.recessTime) * 60000
            );
            let getStartTime = moment(newtime).format("h:mm A");
            this.timetableStart =
              startTimeconvert + Number(slotformArray.recessTime) * 60000;
            const subdata = this.formBuilder.group({
              startTime: startTime,
              endTime: getStartTime,
              isRecess: true
            });
            slotsformArray.push(subdata);
          } else {
            let newtime = new Date(
              this.timetableStart + Number(slotformArray.recessTime) * 60000
            );
            let getStartTime = moment(this.timetableStart).format("h:mm A");
            let getEndtTime = moment(newtime).format("h:mm A");
            this.timetableStart =
              this.timetableStart + Number(slotformArray.recessTime) * 60000;
            const subdata = this.formBuilder.group({
              startTime: getStartTime,
              endTime: getEndtTime,
              isRecess: true
            });
            slotsformArray.push(subdata);
          }
        }
      } else if (!isrecess) {
        if (line === 0) {
          let newtime = new Date(
            startTimeconvert + Number(slotformArray.lectureTime) * 60000
          );
          let getStartTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            startTimeconvert + Number(slotformArray.lectureTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: startTime,
            endTime: getStartTime,
            isRecess: false
          });
          slotsformArray.push(subdata);
        } else {
          let newtime = new Date(
            this.timetableStart + Number(slotformArray.lectureTime) * 60000
          );
          let getStartTime = moment(this.timetableStart).format("h:mm A");
          let getEndtTime = moment(newtime).format("h:mm A");
          this.timetableStart =
            this.timetableStart + Number(slotformArray.lectureTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: getStartTime,
            endTime: getEndtTime,
            isRecess: false
          });
          slotsformArray.push(subdata);
        }
      } else if (isrecess) {
        if (line === 0) {
          let newtime = new Date(
            startTimeconvert + Number(slotformArray.recessTime) * 60000
          );
          let getStartTime = moment(newtime).format("h:mm A");
          
          this.timetableStart =
            startTimeconvert + Number(slotformArray.recessTime) * 60000;
        
          const subdata = this.formBuilder.group({
            startTime: startTime,
            endTime: getStartTime,
            isRecess: true
          });
         
          slotsformArray.push(subdata);
         
        } else {
         
          let newtime = new Date(
            this.timetableStart + Number(slotformArray.recessTime) * 60000
          );
      
          let getEndtTime = moment(newtime).format("h:mm A");
          //console.log('getStartTime: ', getStartTime);
          let getStartTime = moment(this.timetableStart).format("h:mm A");
          this.timetableStart =
            this.timetableStart + Number(slotformArray.recessTime) * 60000;
          const subdata = this.formBuilder.group({
            startTime: getStartTime,
            endTime: getEndtTime,
            isRecess: true
          });
          slotsformArray.push(subdata);
        }
      }
    }
    const timetableData = this.formBuilder.group({
      wtuid: slotformArray.wtuid,
      dayOfWeek: Number(slotformArray.dayOfWeek),
      isWeekOff: Boolean(slotformArray.isWeekOff),
      recessTime: slotformArray.recessTime,
      lectureTime: slotformArray.lectureTime,
      noOfLecture: noOfLecture + 1,
      slots: slotsformArray
    });
    const removeIndex = <FormArray>(
      this.updateTimetableSlotsForm.controls.timeTableData
    );
    removeIndex.removeAt(index);
    formArray.insert(index, timetableData);
    this.updateTimetableSlotsForm.patchValue(formArray);
    
  }

  updateTimetableTemplate(index) {
   
    this.timetableService
      .updateTimetableTemplate(
        this.updateTimetableSlotsForm.value.timeTableData[0]
      )
      .subscribe(templateDate => {
        if (templateDate.status == 201) {
       
          this.setUpdateTemplateValue(this.sessionTimeSlot.ssuid, index);
          this.toasterService.Success("Successfully Updated!");
        } else {
          this.toasterService.Error("Not Updated!");

        }
      });
  }

  getLecture() {
    this.lectureService.getLecture().subscribe(
      response => {
        this.leacture_data = response.payload;
      
        for (const data of this.leacture_data) {
          this.std_data.push(data.std);
          this.div_data.push(data.div);
        }
        // eleminating duplicate values
        this.std_data = Array.from(new Set(this.std_data));
        this.div_data = Array.from(new Set(this.div_data));
      },
      error => {
        console.error("Error while getting lecture data. Stack: ", error);
        this.toasterService.Error("Error while getting lecture data!");
      }
    );
  }

  getStaticLecture() {
    this.static_lecture_data = [
      { std: "I", div: "A" },
      { std: "I", div: "B" },
      { std: "I", div: "C" },
      { std: "II", div: "A" },
      { std: "II", div: "B" },
      { std: "II", div: "C" },
      { std: "III", div: "A" },
      { std: "III", div: "B" },
      { std: "III", div: "C" },
      { std: "IV", div: "A" },
      { std: "IV", div: "B" },
      { std: "IV", div: "C" }
    ];

    //  console.log('static lecture for teacher: ', this.static_lecture_data);

    for (const data of this.static_lecture_data) {
      this.static_std_data.push(data.std);
      this.static_div_data.push(data.div);
    }
    // eleminating duplicate values
    this.static_std_data = Array.from(new Set(this.static_std_data));
    this.static_div_data = Array.from(new Set(this.static_div_data));
  }

  setStd(data) {
    this.selected_std_value = data;
    
  }

  setDiv(data) {
    this.selected_div_value = data;

  }

  getAllTimeTable() {
    if (this.role === "student") {
      this.timetableService.getTimetableByClass().subscribe(
        response => {
          // console.log('TimeTable by class: ', response);
          this.timetables = response.payload;
        },
        error => {
          console.error(error);
          console.error("Error while getting timetable data. Stack: ", error);
        }
      );
    } else {
      this.timetableService.getAllTimeTableForSchool().subscribe(
        response => {
          this.timetables = response.payload;
          // console.log('TimeTable by school: ', this.timetables);
        },
        error => {
          console.error(error);
          console.error("Error while getting timetable data. Stack: ", error);
        }
      );
    }
  }

  onFileChange(event) {
    this.image_file = <File>event.target.files[0];
  }
  onUpload() {
    let headers = new HttpHeaders();
    // headers.set('Content-Type', null);
    headers.set("Accept", "multipart/form-data");
    let params = new HttpParams();
    const formData: FormData = new FormData();
    for (let i = 0; i < 1; i++) {
      formData.append("myfile", this.image_file, this.image_file.name);
    }
    formData.append("std", this.selected_std_value);
    formData.append("div", this.selected_div_value);
    this.http
      .post(`${environment.API_ENDPOINT}/timetable`, formData, {
        params,
        headers
      })
      .subscribe(res => {
       
      });
  }

  getSessionList() {
    this.timetableService.getTimeSession().subscribe(
      sessionList => {
        if (sessionList.status === 200) {
          this.sessionList = sessionList.payload;
         
        }
      },
      error => {
        this.toasterService.Error("List Not Found!");
        console.error("List Not Found!");
      }
    );
  }

  onSubmitSession() {
    let sessionForm = this.sessionForm.value;
   
    const todayDate1 = moment().startOf("day");
    if (this.sessionForm.value.startTime == null) {
      let day = new Date();
      let dayWrapper = moment(day);
      let currentTime = dayWrapper.format("hh:mm a");
      sessionForm.startTime = currentTime;
    }

    if (this.sessionForm.value.endTime == null) {
      let day = new Date();
      var dayWrapper = moment(day);
      let currentTime = dayWrapper.format("hh:mm a");
      sessionForm.endTime = currentTime;
    }

    this.timetableService.postSchoolSession(sessionForm).subscribe(
      sessionData => {
        if (sessionData.status === 201) {
       
          $("#new_session").modal("hide");
        } else {
          this.toasterService.Error("Session Not Submitted!");
        }
      },
      error => {
        this.toasterService.Error("Session Not Submitted!");
        console.error("Session Not Submitted!");
      }
    );
  }

  getTemplateData($event) {
    this.selectClass = $event.target.value;
    this.std = this.selectClass.substr(0, this.selectClass.length - 1); // get the last character
    this.div = this.selectClass.substr(this.selectClass.length - 1);
    for (let line = 0; line < this.classData.length; line++) {
      if (this.classData[line].class === this.selectClass) {
   
        if (this.classData[line].isTimeTableCreated) {
      
          this.istimetableCreated = true;
          this.setTimetableForUpdate(this.std, this.div, 1);
        } else {
      
          this.istimetableCreated = false;
          this.setTimetableTemplate(1);
        }
      }
    }
    //this.isClassActive = "";
  }

  getTemplateOnDayData(day) {
    for (let line = 0; line < this.classData.length; line++) {
      if (this.classData[line].class === this.selectClass) {
        
        if (this.classData[line].isTimeTableCreated) {
         
          this.istimetableCreated = true;
          this.setTimetableForUpdate(this.std, this.div, day);
        } else {
     
          this.istimetableCreated = false;
          this.setTimetableTemplate(day);
        }
      }
    }
    //this.isClassActive = "";
  }

  setTimetableTemplate(index) {
    this.timetableService
      .getTemplateDataByUid(this.currentSession.ssuid)
      .subscribe(
        templateData => {
          if (templateData.status === 200) {
            this.templateData = templateData.payload;
            const formArray = this.timetable.controls[
              "timeTableData"
            ] as FormArray;
            // For Remove Previouse FormArray data
            while (formArray.length) {
              formArray.removeAt(formArray.length - 1);
            }
            console.log("formArray: ", formArray);
            let line = index - 1;
            const slotsformArray = this.addtimeTableWeekData().get(
              "slots"
            ) as FormArray;
            while (slotsformArray.length) {
              slotsformArray.removeAt(slotsformArray.length - 1);
            }

            for (
              let x = 0;
              x < this.templateData.timeSlotsDetails[line].slots.length;
              x++
            ) {
              let slot = this.templateData.timeSlotsDetails[line].slots[x];
              const subdata = this.formBuilder.group({
                startTime: slot.startTime,
                endTime: slot.endTime,
                isRecess: slot.isRecess
              });
            
              slotsformArray.push(subdata);
            }

            const timetableData = this.formBuilder.group({
              std: this.std,
              div: this.div,
              dayOfWeek: this.templateData.timeSlotsDetails[line].dayOfWeek,
              isWeekOff: this.templateData.timeSlotsDetails[line].isWeekOff,
              slots: slotsformArray
            });
            formArray.push(timetableData);
            this.timetable.patchValue(formArray);
            
          }
        },
        error => {
          this.toasterService.Error("Timetable Not Set!");
          console.error("Timetable Not Set!");
        }
      );
  }

  setTeacherForSlot(idx, key, teachingCode) {
 
    const slotsformArray = this.timetable.controls["timeTableData"].value[idx]
      .slots[key];
    //console.log('slotsformArray: ', slotsformArray);
    let teacherstr = teachingCode.target.value.split(":");
    if (teacherstr[1]) {
      slotsformArray.teachingCode = teacherstr[0];
      slotsformArray.teacherName = teacherstr[1];
    }
  
  }

  /** set time table for update form */
  setTimetableForUpdate(std, div, id) {
    this.timetableService
      .getClassTimetable(std, div)
      .subscribe(timetablebyclass => {
        if (timetablebyclass.status == 200) {
          //this.timetableData = timetablebyclass.payload;
          this.templateData = timetablebyclass.payload;
          let indexMatch = false;
          
          let index = 0;
          for (let line = 0; line < this.templateData.length; line++) {
            let dayofWeek = Number(this.templateData[line].dayOfWeek);
           
            if (dayofWeek == id) {
              index = line;
              indexMatch = true;
            }
          }

        

          if (indexMatch) {
            this.istimetableCreated = true;
            const slotsformArray = this.updateTimetable.controls[
              "slots"
            ] as FormArray;

            while (slotsformArray.length) {
              slotsformArray.removeAt(slotsformArray.length - 1);
            }
            
            for (
              let x = 0;
              x < this.templateData[index].timeSlotsDetails.length;
              x++
            ) {
              let slot = this.templateData[index].timeSlotsDetails[x];
              const subdata = this.formBuilder.group({
                teachingCode: slot.ttuid,
                teacher: slot.teacher,
                tchuid: slot.tchuid,
                subject: slot.subject,
                startTime: slot.startTime,
                endTime: slot.endTime,
                isRecess: slot.isRecess
              });
           
              slotsformArray.push(subdata);
            }

            const timetableData = this.formBuilder.group({
              timetableCode: this.templateData[index].ttmuid,
              std: this.templateData[index].std,
              div: this.templateData[index].div,
              class: this.templateData[index].class,
              dayOfWeek: this.templateData[index].dayOfWeek,
              isWeekOff: this.templateData[index].isWeekOff,
              slots: slotsformArray
            });
            
            this.updateTimetable.patchValue(timetableData);
          } else {
            this.istimetableCreated = false;
            this.setTimetableTemplate(id);
          }
        } else {
        }
      });
  }

  selectedClass(session) {
    this.currentSession = session;
    this.timetableService.getClassSession(this.currentSession.ssuid).subscribe(
      classSession => {
        if (classSession.status == 200) {
          this.sessionClassList = classSession.payload;
          this.classData = this.sessionClassList.class;
         
          let isTimeTableCreated = false;
        }
      },
      error => {
        this.toasterService.Error("Not Found!");
        console.error("Not Found");
      }
    );
   
  }

  getTecharList(dayOfWeek, startTime, endTime) {
    const div = this.selectClass.substr(this.selectClass.length - 1); // get the last character
    const std = this.selectClass.substr(0, this.selectClass.length - 1); // "12345.0"
    
    this.timetableService
      .getTeacherForTimetable(std, div, dayOfWeek, startTime, endTime)
      .subscribe(
        teacherList => {
          if (teacherList.status === 200) {
            this.teacherList = teacherList.payload;
      
          }
        },
        error => {
          this.toasterService.Error("List Not Found!");
          console.error("List Not Found");
        }
      );
  }

  onSubmitTimetable(index) {
  
    this.timetableService.createTimetable(this.timetable.value).subscribe(
      timetableData => {
        if (timetableData.status === 201) {
          this.timetableData = timetableData.payload;
          this.toasterService.Success("Successfully Submitted!");
          
          this.istimetableCreated = false;
          this.setTimetableTemplate(index);
          this.timetable.reset();
        } else {
          this.toasterService.Error("Not Submitted!");
         
        }
      },
      error => {
        this.toasterService.Error("Not Submitted!");
        console.error("Not Submitted");
      }
    );
  }

  getClassTimetable(className) {
    const div = className.substr(className.length - 1); // get the last character
    const std = className.substr(0, className.length - 1); // "12345.0"
    this.timetableService
      .getClassTimetable(std, div)
      .subscribe(timetablebyclass => {
        if (timetablebyclass.status == 200) {
          this.timetableData = timetablebyclass.payload;
          this.classContentView = false;
          this.timetableview = true;
        }
      });
  }

  getChildClassTimetable(className, schoolClass) {
    const div = className.substr(className.length - 1); // get the last character
    const std = className.substr(0, className.length - 1); // "12345.0"
    this.timetableService
      .getChildClassTimetable(std, div, schoolClass)
      .subscribe(timetablebyclass => {
        if (timetablebyclass.status == 200) {
          this.timetableData = timetablebyclass.payload;
          this.classContentView = false;
          this.timetableview = true;
        }
      });
  }

  setTimetableTeacherForSlot(idx, key, teachingCode) {
    const slotsformArray = this.updateTimetable.value.slots[key];
    let teacherstr = teachingCode.target.value.split(":");
    if (teacherstr[1]) {
      slotsformArray.teachingCode = teacherstr[0];
      slotsformArray.teacher = teacherstr[1];
    }
    
  }

  onSubmitTimetableUpdate(index) {
    let updateForm = this.updateTimetable.value;
    updateForm.timeTableCode = this.templateData[index].ttmuid;
    updateForm.std = this.templateData[index].std;
    updateForm.div = this.templateData[index].div;
    updateForm.class = this.templateData[index].class;
    updateForm.dayOfWeek = this.templateData[index].dayOfWeek;
    updateForm.isWeekOff = this.templateData[index].isWeekOff;
    this.timetableService.updateTimetable(updateForm).subscribe(
      timetableData => {
        if (timetableData.status === 201) {
         
          this.toasterService.Success("Successfully Updated!");
        } else {
          this.toasterService.Error("Not Updated!");
        }
      },
      error => {
        this.toasterService.Error("Not Updated!");
    
      }
    );
  
  }
}
