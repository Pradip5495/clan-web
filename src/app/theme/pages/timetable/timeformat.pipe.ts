import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'my',
  })

@Pipe({name: 'convertFrom24To12Format'})
export class TimeFormat implements PipeTransform {
     transform(time: any): any {
      var colon = time.indexOf(':');
      var hours = time.substr(0, colon),
          minutes = time.substr(colon+1, 2),
          meridian = time.substr(colon+4, 2).toUpperCase();     
      
      var hoursInt = parseInt(hours, 10),
          offset = meridian == 'PM' ? 12 : 0;
      
      if (hoursInt === 12) {
        hoursInt = offset;
      } else {
        hoursInt += offset;
      }
      return hoursInt + ":" + minutes;
       }
   }