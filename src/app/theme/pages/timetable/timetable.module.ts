import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { TimetableComponent } from "./timetable.component";
import { DefaultComponent } from "../default/default.component";
import { LayoutModule } from "../../layouts/layout.module";
import { ReactiveFormsModule } from "@angular/forms";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
// Multiselect dropdown
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";

const routes: Routes = [
  {
    path: "",
    component: DefaultComponent,
    children: [
      {
        path: "",
        component: TimetableComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LayoutModule,
    ReactiveFormsModule,
    FormsModule,
    AngularMultiSelectModule //Multiselect dropdown
  ],
  exports: [RouterModule],
  declarations: [TimetableComponent]
})
export class TimetableModule {}
