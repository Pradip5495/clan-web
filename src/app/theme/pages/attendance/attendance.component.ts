import {
  Component,
  OnInit,
  ViewEncapsulation,
  AfterViewInit,
  Pipe,
  PipeTransform,
  OnChanges,
  ElementRef,
  Renderer2,
  ViewChild
} from "@angular/core";
import { ScriptLoaderService } from "../../../_services/script-loader.service";
import { AttendanceService } from "../../../_services/attendance.service";
import { IdentityService } from "../../../_services/identity.service";
import { ChildService } from "../../../_services/child.service";
import { StudentService } from "../../../_services/student.service";
import { ToasterService } from "../../../_services/toaster.service";
import { ApprovalService } from "../../../_services/approval.service";
import { ExaminationService } from "../../../_services/examination.service";
import { ClassService } from "../../../_services/class.service";
import { PerformanceByClassService } from "../../../_services/performanceByClass.service";
import { DashboardService } from "../../../_services/dashboard.service";
import { TeacherService } from "../../../_services/teacher.service";
import {
  ActivatedRoute,
  Router,
  ParamMap,
  NavigationExtras
} from "@angular/router";
import { Data } from "../../../_provider/data/data";
import { DatePipe } from "@angular/common";
import "rxjs/add/operator/map";
import { FormGroup, FormBuilder, FormArray, FormControl } from "@angular/forms";
// Models
import { User } from "../../../_models/user";
import { isEmpty } from "rxjs/operator/isEmpty";
import { count } from "rxjs/operator/count";

declare const $: any;
declare const mApp, moment, Chartist, todayDate;

@Component({
  selector: "app-attendance",
  templateUrl: "./attendance.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./attendance.component.css"]
})
export class AttendanceComponent implements OnInit, AfterViewInit {
  emailFormArray: Array<any> = [];
  myForm: FormGroup;

  get class_data(): any {
    return this._class_data;
  }

  set class_data(value: any) {
    this._class_data = value;
  }

  get class_value(): any {
    return this._class_value;
  }

  set class_value(value: any) {
    this._class_value = value;
  }

  get students_list(): any {
    return this._students_list;
  }

  set students_list(value: any) {
    this._students_list = value;
  }

  get isClassTeacher(): boolean {
    return this._isClassTeacher;
  }

  set isClassTeacher(value: boolean) {
    this._isClassTeacher = value;
  }

  public user: User;
  public attendance1: any;
  public _child_list: any;
  public attendance2: any;
  public studentAttendance: any;
  public classAttendanceView: any = false;
  public role: any;
  public present: any = 0;
  public absent: any = 0;
  public day_count: any = 0;
  public present_percent: any = 0;
  public absent_percent: any = 0;
  private _class_value: any = null;
  private _students_list: any;
  private _class_data: any;
  private _isClassTeacher = true;
  private uid: any;
  public profile: any;
  public studentDetails: any;
  public storage: any;
  public schoolCode: any;
  public subjectList: any;
  public schoolPerformanaceStatus: any = false;
  public exam_data;
  public student_attendance_state = false;
  public attendance_status = false;
  public class_list: any;
  public teacher_data: any;
  public calendar_date: any;
  public calendar_btn_status = false; //Calendar Next & Previous button status
  private element: any;
  public colors;
  public className: any;
  public currentDay: any;
  public totalStudent: any;
  public presentStudent: any;
  public absentStudent: any;
  public allStudentAttendanceView: any = false;
  public allClassView: any = false;
  public studentWeekAttendance: any;
  public attendanceFlag: any;
  public leave_percent: any;
  public leaveStudent: any;
  public leave: any;
  public selectedChild: any;
  public _school_name: any;
  public _school_code: any;

  constructor(
    private _script: ScriptLoaderService,
    private _attendanceService: AttendanceService,
    private _identityService: IdentityService,
    private _studentService: StudentService,
    private _approvalService: ApprovalService,
    private fb: FormBuilder,
    private _child: ChildService,
    private _classService: ClassService,
    private toasterService: ToasterService,
    private route: ActivatedRoute,
    private _examinationService: ExaminationService,
    private data: Data,
    private _performance: PerformanceByClassService,
    private _teacherService: TeacherService,
    private router: Router,
    private el: ElementRef,
    private renderer: Renderer2
  ) {
    this.element = el.nativeElement;
    this.role = _identityService.role;
    this.schoolCode = this._identityService.identity.schoolCode;
    this.route.queryParams.subscribe(params => {
      this.profile = params;
      this.uid = this.profile.uid;
      //this.profile = this.storage;
    });

    /* if (this.role === _identityService.role) {
      //  this.getAttendanceForStudentByMonth(YM);
      // this.getAttendance();

     }*/
  }

  ngOnInit() {
    this.myForm = this.fb.group({
      attendance: this.fb.array([]),
      leave: this.fb.array([])
    });
    const todayDate1 = moment().startOf("day");
    const YM = todayDate1.format("YYYY-MM-DD");
    this.currentDay = todayDate1.format("YYYY-MM-DD");
    this.uid = this._identityService.uid;

    if (this.role == "student") {
      this.studentDetails = this._identityService.identity;
    }

    if (this.role == "parent") {
      if (localStorage.getItem("selectedChild")) {
        this.selectedChild = JSON.parse(localStorage.getItem("selectedChild"));
        this.uid = this.selectedChild.uid;
        this.schoolCode = this.selectedChild.schoolCode;
        this.studentDetails = this.selectedChild;
      } else {
        this.getChild();
      }
    }

    this.getClass();
    this.getStudent(event);
    this.getAttendanceforstudent();
    this._isClassTeacher = this._identityService.isClassTeacher;
    this.class_value = this._identityService.class;

    /* this.route.queryParams.subscribe(params => {
       this.profile = params;
       if ( this.profile.uid !== undefined ) {
         console.log('Profile Details:', params);
         this.profile = params;
         this.uid = this.profile.uid;
         console.log('UID set Here:', params);
         this.getStudentPerformane();
         this.schoolPerformanaceStatus = true;
         this.calendar_date = todayDate1.format('YYYY-MM-DD');
         console.log("status: ", this.schoolPerformanaceStatus);
       } else {
         this.attendance_status = true;
         console.log("status: ", this.attendance_status);
       }
     });*/

    if (this.route.params["value"].uid) {
      this.uid = this.route.params["value"].uid;
      if (this.uid !== undefined && this.uid != null) {
        this.profile = JSON.parse(localStorage.getItem("StudentProfile"));
        this.uid = this.profile.uid;
        this.getStudentPerformane();
        this.schoolPerformanaceStatus = true;
        this.calendar_date = todayDate1.format("YYYY-MM-DD");
      } else {
        this.attendance_status = true;
      }
    } else if (this.route.params["value"].class) {
      this.className = this.route.params["value"].class;
      this.getAttendance(this.className);
      this.classAttendanceView = true;
      this.attendance_status = false;
    } else if (this.route.params["value"].std) {
      this.className = this.route.params["value"].std;
      this.getAllClassAttendance(this.className);
      // this.getAttendance(this.className);
      this.allStudentAttendanceView = true;
      this.classAttendanceView = false;
      this.attendance_status = false;
    } else {
      this.attendance_status = true;
    }

    if (this.router.url === "/attendance/classes") {
      this.getClassAttendance(this.className);
      this.allClassView = true;
      this.classAttendanceView = false;
      this.attendance_status = false;
    }

    /*if (this.route.params['value'].uid) {
      alert('hello' + this.route.params['value'].uid);

      this.uid = this.route.params['value'].uid;
      this.getStudentPerformane();
      this.schoolPerformanaceStatus = true;
      this.calendar_date = todayDate1.format('YYYY-MM-DD');
      console.log("status: ", this.schoolPerformanaceStatus);
      /!*this.schoolPerformanaceStatus = false;
      this.attendance_status = false;
      this.student_attendance_state = true;
      this.getClassBySchool();*!/
    }*/

    if (this.route.params["value"].student === "student") {
      if (this.role == "student") {
        this.calendar_date = todayDate1.format("YYYY-MM-DD");
        this.profile = this._identityService.identity;
        this.getStudentPerformane();
      } else {
        this.schoolPerformanaceStatus = false;
        this.attendance_status = false;
        this.student_attendance_state = true;
        this.getClassBySchool();
      }
    }
  }

  get school_name(): any {
    return this._school_name;
  }

  set school_name(value: any) {
    this._school_name = value;
  }

  get school_code(): any {
    return this._school_code;
  }

  set school_code(value: any) {
    this._school_code = value;
  }

  get child_list(): any {
    return this._child_list;
  }

  set child_list(value: any) {
    this._child_list = value;
  }

  getParamsUid() {
  }

  ngAfterViewInit() {
    this._script.loadScripts("app-attendance", [
      "assets/demo/default/custom/components/calendar/basic.js"
    ]);
  }

  getChild() {
    let value: any;
    this._child.getChild().subscribe(data => {
      this._child_list = data.payload;
      $("#m_modal_5").modal("show");
    });
  }

  setChild(data) {
    localStorage.setItem("selectedChild", JSON.stringify(data));
    this.uid = data.uid;
    $("#m_modal_5").modal("hide");
  }

  getInitialStudent(event) {
    if (this.class_value) {
      this._studentService.getStudentByClass(event).subscribe(
        students_list => {
          if (students_list) {
            this.students_list = students_list.payload;
          } else {
          }
        },
        error => {
          console.error("Error while getting student list. stack: ", error);
          this.toasterService.Error("Error while getting student list");
        }
      );
    }
  }

  /** attendanceFlag set */
  attendancePerson(flag) {
    this.attendanceFlag = flag;
  }
  /** Get All Class List*/
  getClass() {
    this._classService.getClass().subscribe(
      class_data => {
        this.class_data = class_data.payload;
      },
      error => {
        console.error("Error while getting class data. stack: ", error);
        this.toasterService.Error("Error while getting class data");
      }
    );
  }

  getAttendance(event: any) {
    this.class_value = event;
    const todayDate1 = moment().startOf("day");
    const YM = todayDate1.format("YYYY-MM-DD");
    const class_val = this.class_value;
    this.totalStudent = 0;
    const div = class_val.substr(class_val.length - 1); // get the last character
    const std = class_val.substr(0, class_val.length - 1); // "12345.0"
    this._attendanceService.getAttendance1(std, div, YM).subscribe(
      attendance1 => {
        this.attendance1 = attendance1.payload;
        this.totalStudent = this.attendance1.length;
        this.present_percent = 0;
        this.absent_percent = 0;
        this.absentStudent = 0;
        this.presentStudent = 0;

        this.leave_percent = 0;
        this.leaveStudent = 0;

        for (const data of this.attendance1) {
          // if (data.isPresent === true) {
          //   this.present_percent++;
          //   this.presentStudent++;
          // }
          if (data.isPresent === true  && !data.onLeave) {
            this.absent_percent++;
            this.absentStudent++;
          }

          // if (data.isPresent === false && !data.onLeave) {
          //   this.absent_percent++;
          //   this.absentStudent++;
          // }
          if (data.isPresent === false) {
            this.present_percent++;
            this.presentStudent++;
          }

          if (data.isPresent === false && data.onLeave) {
            this.leave_percent++;
            this.leaveStudent++;
          }
        }
        // this.attendanceChart();
        // this.loadCalender(event);
      },
      error => {
        console.error("Error while getting attendance data. stack: ", error);
        this.toasterService.Error("Error while getting attendance data");
      }
    );
  }

  getAttendanceforstudent() {
    const YMcan = this.calendar_date;
    const todayDate = moment().startOf("day");
    this.calendar_btn_status = true;
    const YESTERDAY = todayDate
      .clone()
      .subtract(1, "month")
      .format("YYYY-MM-DD");
    /*const YMcan = moment(YMcan).format('YYYY-MM-DD');
    console.log('YMcan', YMcan);
    const todayDate1 = moment(YMcan).startOf('day');*/
    const YM = todayDate.format("YYYY-MM-DD");;
    this.calendar_date = YM;
    this.day_count = 0;
    this._attendanceService
      .getAttendanceForStudent(this.uid, YM)
      .subscribe(attendance => {
        if (attendance !== null) {
          this.attendance1 = attendance.payload;
          this.present = 0;
          this.absent = 0;
          this.leave = 0;
          this.present_percent = 0;
          this.absent_percent = 0;
          this.leave_percent = 0;
          for (const a of this.attendance1) {
            this.day_count++;
            if (a.isPresent) {
              this.present++;
            } else if (!a.isPresent && !a.onLeave) {
              this.absent++;
            } else if (!a.isPresent && a.onLeave) {
              this.leave++;
            }
          }
          this.present_percent = (
            (this.present / this.day_count) *
            100
          ).toFixed(2);
          this.absent_percent = ((this.absent / this.day_count) * 100).toFixed(
            2
          );
          this.leave_percent = ((this.leave / this.day_count) * 100).toFixed(2);
          this.attendanceChart();
          this.loadCalender(this.attendance1);
        } else {
          this.attendance1 = [];
          this.present = 0;
          this.absent = 0;
          this.leave = 0;
          this.present_percent = 50;
          this.absent_percent = 25;
          this.leave_percent = 25;
          this.attendanceChart();
          this.loadCalender(this.attendance1);
        }
      });
  }

  takeAttendance() {
    const attendanceFormArray = <FormArray>this.myForm.controls.attendance;
    const leaveFormArray = <FormArray>this.myForm.controls.leave;
    const class_val = this.class_value;
    const div = class_val.substr(class_val.length - 1); // get the last character
    const std = class_val.substr(0, class_val.length - 1); // "12345.0"
    this._attendanceService
      .takeAttendance(std, div, attendanceFormArray.value, leaveFormArray.value)
      .subscribe(
        attendance => {
          this.studentAttendance = attendance.payload;
          //this.getAllClassAttendance(class_val);
          this.router.navigate(["attendance/students/" + class_val]);
          for (const a of this.attendance1) {
            this.day_count++;
            if (a.isPresent) {
              this.present++;
            } else if (!a.isPresent && !a.onLeave) {
              this.absent++;
            } else if (!a.isPresent && a.onLeave) {
              this.leave++;
            }
          }

          this.present_percent = this.present;
          this.absent_percent = this.absent;
          this.leave_percent = this.leave;
          // this.present_percent = (this.present / this.day_count) * 100;
          // this.absent_percent = (this.absent / this.day_count) * 100;

          // this.loadAttendence();
          // this.attendanceChart();
        },
        error => {
          console.error("Error while getting attendance. stack: ", error);
          this.toasterService.Error("Error while getting attendance");
        }
      );
  }

  getAttendanceForStudentByMonth() {
    const calendar_current_month = $("#m_calendar_event").fullCalendar(
      "getDate"
    );
    this.calendar_date = calendar_current_month;
    const YMcan = this.calendar_date;
    const YM = moment(YMcan).format("YYYY-MM-DD");
    // const todayDate1 = moment().startOf('day');
    // const YM = todayDate1.format('YYYY-MM-DD');
    this._attendanceService.getAttendanceForStudent(this.uid, YM).subscribe(
      attendance => {
        if (attendance) {
          this.attendance1 = attendance.payload;
        }
        // this.loadAttendence(this.attendance1);
        this.present = 0;
        this.absent = 0;
        this.day_count = 0;
        this.present_percent = 0;
        this.absent_percent = 0;
        for (const a of this.attendance1) {
          this.day_count++;
          if (a.isPresent) {
            this.present++;
          } else if (!a.isPresent && !a.onLeave) {
            this.absent++;
          } else if (!a.isPresent && a.onLeave) {
            this.leave++;
          }
        }

        this.present_percent = (this.present / this.day_count) * 100;
        this.absent_percent = (this.absent / this.day_count) * 100;
        console.log("Attendance Present day:", this.present_percent);
        this.loadAttendence();
        this.attendanceChart();
      },
      error => {
        console.error("Error while getting attendance. stack: ", error);
        this.toasterService.Error("Error while getting attendance");
      }
    );
  }

  getStudent(event: any) {
    this.class_value = event;
    this._studentService.getStudentByClass(event).subscribe(
      students_list => {
        if (students_list !== null) {
          this.students_list = students_list.payload;
          console.log("student-list123:", this.students_list);

          console.log(students_list.payload);
          if (students_list.payload === null) {
            console.log("attendance data not found");
          }
          console.log("attendance data:", this.students_list);

          for (const data of this.students_list) {
            if (data.isPresent === true) {
              this.present_percent++;
              console.log("present" + this.present_percent);
            }

            if (data.isPresent === false) {
              this.absent_percent++;
              console.log("absent" + this.absent_percent);
            }
          }

          this.attendanceChart();
          this.getAttendance(event);
        }
      },
      error => {
        console.error("Error while getting attendance data. stack: ", error);
        this.toasterService.Error("Error while getting attendance data");
      }
    );
  }

  loadAttendence() {
    const SA = [];
    // this.getAttendance();
    const todayDate1 = moment().startOf("day");
    const YM = todayDate1.format("YYYY-MM-DD");
    console.log("YM: " + YM);
    for (const s of this.attendance1) {
      console.log("bhhc" + s);
      console.log(s.isPresent === true);

      if (s.isPresent) {
        SA.push({
          title: "present",
          start: s.date,
          description: "present",
          className: "m-fc-event--danger m-fc-event--solid-warning"
        });
      } else {
        SA.push({
          title: "absent",
          start: s.absent,
          description: "absent",
          className: "m-fc-event--danger m-fc-event--solid-warning"
        });
      }
    }

    if ($("#m_calendar_attendance").length === 0) {
      return;
    }

    const todayDate = moment().startOf("day");

    const month = todayDate.format("YYYY-MM");
    console.log("current month: " + month);

    $("#m_calendar_attendance").fullCalendar({
      header: {
        left: "prev",
        center: "title",
        right: "next"
      },

      editable: true,
      eventLimit: true, // allow 'more' link when too many events
      navLinks: true,
      defaultDate: moment(todayDate),

      events: SA,

      eventRender: function(event, element) {
        if (element.hasClass("fc-day-grid-event")) {
          element.data("content", event.description);
          element.data("placement", "top");
          mApp.initPopover(element);
        } else if (element.hasClass("fc-time-grid-event")) {
          element
            .find(".fc-title")
            .append(
              '<div class= "fc-description">' + event.description + "</div>"
            );
        } else if (element.find(".fc-list-item-title").lenght !== 0) {
          element
            .find(".fc-list-item-title")
            .append(
              '<div class= "fc-description">' + event.description + "</div>"
            );
        }
      }
    });
  }

  onChange(uid: string, isChecked: boolean, flag: string) {
    if (this.attendance1) {
      const selectedChild = this.attendance1.find((student: any) => {
        return student.uid === uid;
      });
      const selectedChildIndex = this.attendance1.findIndex((student: any) => {
        return student.uid === uid;
      });
      console.log(selectedChild, selectedChildIndex)
      if (selectedChild && selectedChildIndex !== -1) {
        switch (flag) {
          case 'present':
            selectedChild.isPresent = true;
            selectedChild.onLeave = 0;
            break;
          case 'absent':
            selectedChild.isPresent = false;
            selectedChild.onLeave = 0;
            break;
          case 'leave':
            selectedChild.isPresent = false;
            selectedChild.onLeave = 1;
            break;
        
          default:
            break;
        }
      }
      this.attendance1[selectedChildIndex] = selectedChild;
    }
    const attendanceFormArray = <FormArray>this.myForm.controls.attendance;
    const leaveFormArray = <FormArray>this.myForm.controls.leave;
    if (flag == "present") {
      const index = attendanceFormArray.controls.findIndex(
        x => x.value === uid
      );
      if (index >= 0) {
        attendanceFormArray.removeAt(index);
      }
      const leaveIndex = leaveFormArray.controls.findIndex(
        x => x.value === uid
      );
      if (leaveIndex >= 0) {
        leaveFormArray.removeAt(leaveIndex);
      }
      attendanceFormArray.push(new FormControl(uid));
    } else if (flag == "absent") {
      const leaveIndex = leaveFormArray.controls.findIndex(
        x => x.value === uid
      );
      if (leaveIndex >= 0) {
        leaveFormArray.removeAt(leaveIndex);
      }

      const index = attendanceFormArray.controls.findIndex(
        x => x.value === uid
      );
      if (index >= 0) {
        attendanceFormArray.removeAt(index);
      }
    } else if (flag == "leave") {
      const leaveIndex = leaveFormArray.controls.findIndex(
        x => x.value === uid
      );
      if (leaveIndex >= 0) {
        leaveFormArray.removeAt(leaveIndex);
      }

      leaveFormArray.push(new FormControl(uid));

      const index = attendanceFormArray.controls.findIndex(
        x => x.value === uid
      );
      if (index >= 0) {
        attendanceFormArray.removeAt(index);
      }
    }
    console.log("attendanceFormArray: ", this.myForm);
  }

  duplicate() {
    console.log(this.emailFormArray);
  }
  /** Get Student Performance Details */
  getStudentPerformane() {
    //  const uid;
    const todayDate1 = moment().startOf("day");
    const YMcan = this.calendar_date;
    const YM = moment(YMcan).format("YYYY-MM-DD");
    this._attendanceService
      .getAttendanceForStudent(this.uid, YM)
      .subscribe(attendance => {
        if (attendance !== null) {
          this.attendance1 = attendance.payload;
          console.log("this.attendance1", this.attendance1);
          for (const a of this.attendance1) {
            this.day_count++;
            if (a.isPresent) {
              this.present++;
            } else {
              this.absent++;
            }
          }
          this.present_percent = (
            (this.present / this.day_count) *
            100
          ).toFixed(2);
          this.absent_percent = ((this.absent / this.day_count) * 100).toFixed(
            2
          );
          this.attendanceChart();
          this.getStudentPerformanceBySubject();
          this.getExamination();
          this.loadCalender(this.attendance1);
        } else {
          this.attendance1 = [];
          this.present = 0;
          this.absent = 0;
          this.present_percent = 50;
          this.absent_percent = 50;
          this.absent_percent = 50;
          this.attendanceChart();
          this.getStudentPerformanceBySubject();
          this.getExamination();
          this.loadCalender(this.attendance1);
        }
      });
  }
  /** Get Student Examination Data */
  getStudentPerformanceBySubject() {
    this._performance
      .getStudentPerfomanceBySubject(this.uid, this.schoolCode)
      .subscribe(
        subjectList => {
          this.subjectList = subjectList.payload;
          console.log("Subject-list:", this.subjectList);
          this.performaceChart(this.subjectList);
        },
        error => {
          console.error("Error while getting exam list. stack: ", error);
          this.toasterService.Error("Error while getting exam list");
        }
      );
  }

  /* loadCalender(event) {

     const eventsArr = [];

     for (const e of event) {
       console.log(e);
       eventsArr.push({
         title: e.title,
         start: e.date,
         description: e.description,
         className: 'm-fc-event--light m-fc-event--solid-warning'
       });
     }



     if ($('#m_calendar_attendance').length === 0) {
       return;
     }

     const todayDate = moment().startOf('day');
     // const YM = todayDate.format('YYYY-MM');
     // const YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
     // const TODAY = todayDate.format('YYYY-MM-DD');
     // const TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');




     $('#m_calendar_attendance').fullCalendar({
       header: {
         left: 'prev,next today',
         center: 'title',
         right: 'month,agendaWeek,agendaDay,listWeek'
       },

       editable: true,
       eventLimit: true, // allow "more" link when too many events
       navLinks: true,
       defaultDate: moment(todayDate),

       events: eventsArr,

       eventRender: function(event, element) {
         if (element.hasClass('fc-day-grid-event')) {
           element.data('content', event.description);
           element.data('placement', 'top');
           mApp.initPopover(element);
         } else if (element.hasClass('fc-time-grid-event')) {
           element.find('.fc-title').append('<div class="fc-description">' + event.description + '</div>');
         } else if (element.find('.fc-list-item-title').lenght !== 0) {
           element.find('.fc-list-item-title').append('<div class="fc-description">' + event.description + '</div>');
         }
       }

     });
   }*/
  /** Student Attendace show by Calendar */
  loadCalender(event) {
    const eventsArr = [];
    const recall_value = 1;
    let recall_status = false;
    console.log("Event Calendar Load", event);
    for (const e of event) {
      /*console.log('student attendance', e);*/
      eventsArr.push({
        title: e.title,
        start: e.date,
        description: e.description,
        className: "m-fc-event--light m-fc-event--solid-warning"
      });
    }
    if ($("#m_calendar_event").length === 0) {
      return;
    }
    const YMcan = this.calendar_date;
    /*const todayDate = moment(YMcan).format('YYYY-MM-DD');*/
    const todayDate = moment(YMcan).startOf("day");
    console.log("date today", todayDate);
    const YESTERDAY = todayDate
      .clone()
      .subtract(1, "month")
      .format("YYYY-MM-DD");
    $("#m_calendar_event").fullCalendar({
      header: {
        left: "prev",
        center: "title",
        right: "next" // agendaWeek,agendaDay,listWeek
      },
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      navLinks: false,
      defaultDate: moment(todayDate),
      dayRender: function(date, cell) {
        const today = $.fullCalendar.moment();
        for (const eventdata of event) {
          const eventdate = eventdata.date;
          recall_status = true;
          const calendarDates = date._d;
          //const calendarDate = moment(calendarDates).format('YYYY-MM');
          //this.calendar_date = date._d;
          const calendarDate = moment(calendarDates).format("YYYY-MM-DD");
          const event_date = moment(eventdate).format("MM");
          const days = moment(calendarDates).format("DD");
          const calendar_date = moment(calendarDate).format("MM");
          if (calendarDate === eventdate) {
            $("td")
              .find("[data-date='" + calendarDate + "']")
              .css("color", "white");
            if (eventdata.isPresent) {
              cell.css("background", "green");
              cell.css("color", "#fff");
              cell.css("width", "15px");
              cell.css("height", "20px");
              cell.css("border-radius", "40px");
              cell.css("text-align", "center");
              cell.addClass("class", "set-text-color");
            } else if (!eventdata.isPresent && !eventdata.onLeave) {
              console.log("isPresent: ", eventdata.isPresent);
              cell.css("background", "red");
              cell.css("color", "#fff");
              cell.css("width", "15px");
              cell.css("height", "20px");
              cell.css("border-radius", "40px");
              cell.css("text-align", "center");
              cell.addClass("class", "set-text-color");
            } else if (!eventdata.isPresent && eventdata.onLeave) {
              console.log("onLeave: ", eventdata.onLeave);
              cell.css("background", "#ffb822");
              cell.css("color", "#fff");
              cell.css("width", "15px");
              cell.css("height", "20px");
              cell.css("border-radius", "40px");
              cell.css("text-align", "center");
              cell.addClass("class", "set-text-color");
            }
          } else {
            /*  console.log('Else: ',calendarDate);
              console.log('calendarDates: ',calendarDate);
              console.log('calendarDates title: ',eventdata);*/
          }
        }
      },
      events: eventsArr,
      eventRender: function(event, element) {
        if (element.hasClass("fc-day-grid-event")) {
          element.data("content", event.description);
          element.data("placement", "top");
          mApp.initPopover(element);
        } else if (element.hasClass("fc-time-grid-event")) {
          element
            .find(".fc-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        } else if (element.find(".fc-list-item-title").lenght !== 0) {
          element
            .find(".fc-list-item-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        }
      }
    });

    if (this.calendar_btn_status == true) {
      console.log("calendar_btn_status", this.calendar_btn_status);
      this.calendar_btn_status = false;
      $(".fc-prev-button").on("click", e => {
        this.getAttendanceforstudentByChange(null, true);
      });
      $(".fc-next-button").on("click", e => {
        this.getAttendanceforstudentByChange(null, false);
      });
    }

    if (recall_status) {
      $(".fc-prev-button").on("click", e => {
        this.getAttendanceforstudentByChange(null, true);
      });
      $(".fc-next-button").on("click", e => {
        this.getAttendanceforstudentByChange(null, false);
      });
    }
  }

  /** Student Subject Attendance Bar Chart */
  attendanceChart = function() {
    console.log("attendanceChart attendance pie: " + this.attendance1);
    if ($("#m_chart_attendance").length === 0) {
      return;
    }

    const chart = Chartist.Pie(
      "#m_chart_attendance",
      {
        series: [
          {
            value: this.present_percent,
            className: "present",
            meta: {
              color: "#28a745"
            }
          },
          {
            value: this.absent_percent,
            className: "absent",
            meta: {
              color: "#dc3545"
            }
          },
          {
            value: this.leave_percent,
            className: "leave",
            meta: {
              color: "#ffb822"
            }
          }
        ]
      },
      {
        donut: true,
        donutWidth: 10,
        showLabel: false
      }
    );

    chart.on("draw", function(data) {
      if (data.type === "slice") {
        // Get the total path length in order to use for dash array animation
        const pathLength = data.element._node.getTotalLength();

        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
          "stroke-dasharray": pathLength + "px " + pathLength + "px"
        });

        // Create animation definition while also assigning an ID to the animation for later sync usage
        const animationDefinition = {
          "stroke-dashoffset": {
            id: "anim" + data.index,
            dur: 1000,
            from: -pathLength + "px",
            to: "0px",
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: "freeze",
            stroke: data.meta.color
          }
        };

        // // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if (data.index !== 0) {
          animationDefinition["stroke-dashoffset"]["begin"] =
            "anim" + (data.index - 1) + ".end";
        }

        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

        data.element.attr({
          "stroke-dashoffset": -pathLength + "px",
          stroke: data.meta.color
        });

        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
      }
    });
  };

  /** Student Subject perfomance Bar Chart */
  performaceChart = function(subjectList) {
    console.log("performance Bar: " + subjectList);
    if ($("#m_chart_performance").length === 0) {
      return;
    }
    const series = [];
    const label = [];
    console.log("Subjectlist:", subjectList);
    for (const e of subjectList) {
      console.log(e);
      series.push(e.performance);
      label.push(e.subject + "\n" + e.performance + "%");
    }
    console.log(series);
    const chart_data = Chartist.Bar(
      "#m_chart_performance",
      {
        labels: label,
        series: series
      },
      {
        barwidth: 100,
        distributeSeries: true,
        // low: 0,
        // high: 100,
        axisX: {
          showGrid: false
        },
        axisY: {
          showGrid: false,
          showLabel: false,
          offset: 0
        }
      }
    );
    chart_data.on("draw", function(data) {
      if (data.type === "bar") {
        data.element.attr({
          style: "stroke-width: 20px; "
        });
      }
    });
  };

  /** Get Examination Details */
  getExamination() {
    this._examinationService.getExamResultByStudent(this.uid).subscribe(
      exam_data => {
        this.exam_data = exam_data.payload;
        console.log("exam data:", this.exam_data);
      },
      error => {
        console.error("Error while getting exam data. stack: ", error);
      }
    );
  }

  /** Get All Class List By School*/
  getClassBySchool() {
    this._classService.getClassBySchool().subscribe(
      class_list => {
        this.class_list = class_list.payload;
        this.colors = ["#F68A22", "#EF5350", "#009587", "#F47F1F"];
        console.log("class data54321:", this.class_list); // student count
      },
      error => {
        console.error("Error while getting class data. stack: ", error);
        this.toasterService.Error("Error while getting class data");
      }
    );
  }

  /** Get Teacher List */
  getTeacher(class1: any) {
    console.log("class value in component for teacher: " + event);
    this._teacherService.getTeacherByClass(class1).subscribe(
      teacher_data => {
        this.teacher_data = teacher_data.payload;
        console.log("teacher data:", this.teacher_data);
      },
      error => {
        console.error("Error while getting teacher data. stack: ", error);
        this.toasterService.Error("Error while getting teacher data");
      }
    );
  }
  gotoAttendace(student: any, student_uid: any) {
    localStorage.setItem("StudentProfile", JSON.stringify(student));
    const navigationExtras: NavigationExtras = {
      queryParams: student
    };
    this.router.navigate(["attendance/student/" + student_uid]);
  }
  getStudentPerformanceRedirect(student: any, student_uid: any) {
    localStorage.setItem("StudentProfile", JSON.stringify(student));
    const navigationExtras: NavigationExtras = {
      queryParams: student
    };
    this.router.navigate(["performance/student/" + student_uid]);
  }

  getAttendanceforstudentByChange(
    recall_value: any,
    isPreviousMonthAttandance: boolean
  ) {
    /*  const calendar_current_month = $('#m_calendar_event').fullCalendar('getDate');
      const YMcan = calendar_current_month;
      console.log('YMcan', YMcan);*/
    const todayDate1 = moment().startOf("day");
    const YM = isPreviousMonthAttandance
      ? moment(this.calendar_date)
          .subtract(1, "month")
          .format("YYYY-MM-DD")
      : moment(this.calendar_date)
          .add(1, "month")
          .format("YYYY-MM-DD");

    // const YM = todayDate1.format('YYYY-MM-DD');
    this.attendance2 = [];
    this.calendar_date = YM;
    this._attendanceService
      .getAttendanceForStudent(this.uid, YM)
      .subscribe(attendance => {
        if (attendance !== null) {
          this.attendance2 = attendance.payload;
          this.present = 0;
          this.absent = 0;
          this.leave = 0;
          this.present_percent = 0;
          this.absent_percent = 0;
          this.leave_percent = 0;
          this.day_count = 0;
          for (const a of this.attendance2) {
            this.day_count++;
            if (a.isPresent) {
              this.present++;
            } else if (!a.isPresent && !a.onLeave) {
              this.absent++;
            } else if (!a.isPresent && a.onLeave) {
              this.leave++;
            }
          }
          this.present_percent = (
            (this.present / this.day_count) *
            100
          ).toFixed(2);
          this.absent_percent = ((this.absent / this.day_count) * 100).toFixed(
            2
          );
          this.leave_percent = ((this.leave / this.day_count) * 100).toFixed(2);
          console.log(
            "Attendance Absent percentage Days:",
            this.absent_percent
          );
          $("#m_calendar_event").fullCalendar("destroy");
          $("#m_calendar_event").fullCalendar("render");
          this.attendanceChart();
          this.loadCalender(this.attendance2);
        } else {
          this.attendance2 = {};
          this.present = 0;
          this.absent = 0;
          this.leave = 0;
          this.day_count = 0;
          this.present_percent = 50;
          this.absent_percent = 25;
          this.leave_percent = 25;
          this.attendanceChart();
          this.loadCalender(this.attendance2);
        }
      });
  }

  /** Student Attendace show by Calendar */
  loadAttendanceCalender(event) {
    const eventsArr = [];
    const recall_status = false;
    const recall_value = 0;
    const recall_val = recall_value;
    console.log("Event Calendar Load loadAttendanceCalender", event);
    for (const e of event) {
      console.log("student attendance", e);
      eventsArr.push({
        title: e.title,
        start: e.date,
        description: e.description,
        className: "m-fc-event--light m-fc-event--solid-warning"
      });
    }
    if ($("#m_calendar_event").length === 0) {
      return;
    }
    // const YMcan = this.calendar_date;
    //const todayDate = moment().format('YYYY-MM-DD');
    const todayDate = moment().startOf("day");
    console.log("date today loadAttendanceCalender", todayDate);
    const YESTERDAY = todayDate
      .clone()
      .subtract(1, "month")
      .format("YYYY-MM-DD");
    $("#m_calendar_event").fullCalendar({
      header: {
        left: "prev,next today",
        center: "title",
        right: "month" // agendaWeek,agendaDay,listWeek
      },
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      navLinks: false,
      defaultDate: moment(YESTERDAY),
      dayRender: function(date, cell) {
        console.log("loadAttendanceCalender dayRender", date);
        const today = $.fullCalendar.moment();
        this.recall_status = true;
        /*recall_val = recall_val + 1;
        recall_value = recall_value + 1;*/
        for (const eventdata of event) {
          this.recall_status = true;
          const eventdate = eventdata.date;
          const calendarDates = date._d;
          //const calendarDate = moment(calendarDates).format('YYYY-MM');
          //this.calendar_date = date._d;
          const calendarDate = moment(calendarDates).format("YYYY-MM-DD");
          const event_date = moment(eventdate).format("MM");
          const days = moment(calendarDates).format("DD");
          const calendar_date = moment(calendarDate).format("MM");
          if (calendarDate === eventdate) {
            if (eventdata.isPresent) {
              cell.css("background", "green");
              cell.css("color", "white");
              cell.addClass("class", "set-text-color");
            } else {
              cell.css("background", "red");
              cell.css("color", "white");
              cell.addClass("class", "set-text-color");
            }
          } else {
            /*  console.log('Else: ',calendarDate);
              console.log('calendarDates: ',calendarDate);
              console.log('calendarDates title: ',eventdata);*/
          }
        }
      },
      events: eventsArr,
      eventRender: function(event, element) {
        if (element.hasClass("fc-day-grid-event")) {
          element.data("content", event.description);
          element.data("placement", "top");
          mApp.initPopover(element);
        } else if (element.hasClass("fc-time-grid-event")) {
          element
            .find(".fc-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        } else if (element.find(".fc-list-item-title").lenght !== 0) {
          element
            .find(".fc-list-item-title")
            .append(
              '<div class="fc-description">' + event.description + "</div>"
            );
        }
      }
    });
  }

  getAllClassAttendance(event: any) {
    this.class_value = event;
    const todayDate1 = moment().startOf("day");
    const YM = todayDate1.format("YYYY-MM-DD");
    const class_val = this.class_value;
    this.totalStudent = 0;
    const div = class_val.substr(class_val.length - 1); // get the last character
    const std = class_val.substr(0, class_val.length - 1); // "12345.0"
    this._attendanceService.getAttendance1(std, div, YM).subscribe(
      attendance1 => {
        this.studentWeekAttendance = attendance1.payload;
        console.log(
          "attendance data from get getAllClassAttendance:",
          this.studentWeekAttendance
        );
        this.totalStudent = this.studentWeekAttendance.length;
        this.present_percent = 0;
        this.absent_percent = 0;
        this.absentStudent = 0;
        this.presentStudent = 0;
        this.leave_percent = 0;
        this.leaveStudent = 0;
        for (const data of this.studentWeekAttendance) {
          // if (data.isPresent === true) {
          //   this.present_percent++;
          //   this.presentStudent++;
          //   console.log("present" + this.present_percent);
          // }

          // if (data.isPresent === false && !data.onLeave) {
          //   this.absent_percent++;
          //   this.absentStudent++;
          //   console.log("absent" + this.absent_percent);
          // }
            if (data.isPresent === false  && !data.onLeave) {
              this.present_percent++;
              this.presentStudent++;
              console.log("present" + this.present_percent);
            }
  
            if (data.isPresent === true) {
              this.absent_percent++;
              this.absentStudent++;
              console.log("absent" + this.absent_percent);
            }

          if (data.isPresent === false && data.onLeave) {
            this.leave_percent++;
            this.leaveStudent++;
            console.log("absent" + this.leave_percent);
          }
        }
        // this.attendanceChart();
        // this.loadCalender(event);
      },
      error => {
        console.error("Error while getting attendance data. stack: ", error);
        this.toasterService.Error("Error while getting attendance data");
      }
    );
  }

  getClassAttendance(event: any) {
    this.class_value = event;
    const todayDate1 = moment().startOf("day");
    const YM = todayDate1.format("YYYY-MM-DD");
    console.log("current class value", this.class_value);
    console.log("current date", this.class_value);
    // this.class_value
    const class_val = "IA";
    this.totalStudent = 0;
    const div = class_val.substr(class_val.length - 1); // get the last character
    const std = class_val.substr(0, class_val.length - 1); // "12345.0"
    this._attendanceService.getAttendance1(std, div, YM).subscribe(
      attendance1 => {
        this.attendance1 = attendance1.payload;
        console.log("attendance data from get Attendance:", this.attendance1);
        this.totalStudent = this.attendance1.length;
        this.present_percent = 0;
        this.absent_percent = 0;
        this.absentStudent = 0;
        this.presentStudent = 0;
        this.leave_percent = 0;
        this.leaveStudent = 0;
        for (const data of this.attendance1) {
          if (data.isPresent === false  && !data.onLeave) {
            this.present_percent++;
            this.presentStudent++;
          }

          if (data.isPresent === true) {
            this.absent_percent++;
            this.absentStudent++;
          }

          if (data.isPresent === false && data.onLeave) {
            this.leave_percent++;
            this.leaveStudent++;
          }
        }
        // this.attendanceChart();
        // this.loadCalender(event);
      },
      error => {
        console.error("Error while getting attendance data. stack: ", error);
        this.toasterService.Error("Error while getting attendance data");
      }
    );
  }
}
