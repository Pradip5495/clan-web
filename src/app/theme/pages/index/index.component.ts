import {
  Component,
  OnInit,
  ViewEncapsulation,
  AfterViewInit,
  AfterContentInit,
  OnChanges,
  AfterContentChecked,
  AfterViewChecked
} from "@angular/core";
import {
  ScriptLoaderService,
  DashboardService,
  IdentityService,
  NoticeService,
  EventService
} from "../../../_services";
import { User } from "../../../_models";

import { ChildService } from "../../../_services/child.service";
import { HomeworkService } from "../../../_services/homework.service";
import { ExaminationService } from "../../../_services/examination.service";
import { LibraryService } from "../../../_services/library.service";
import { FetchschoolService } from "../../../_services/fetchschool.service";
import { ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { PerformanceByClassService } from "../../../_services/performanceByClass.service";
import { AttendanceService } from "../../../_services/attendance.service";
import { FormArray, FormGroup, FormControl } from "@angular/forms";
import { SecurityService } from "../../../_services/security.service";
import { FeeMasterService } from "../../../_services/fee-master.service";
import { ToasterService } from "../../../_services/toaster.service";

declare let $: any;
declare let Morris, Chartist;

@Component({
  selector: "app-index",
  templateUrl: "./index.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./visibility.css", "./index.component.css"]
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndexComponent implements OnInit, AfterViewInit {
  title = "CLAN | Dashboard";

  public stats: any;
  public profile: User;
  public notices: any;
  public _child_list: any;
  public events: any;
  public role: any;
  public homework_data: any;
  public performance_data: any;
  public exam_data: any;
  public school_data: any;
  public attendance_data: any;
  public showData: Boolean;
  public _school_code: any = null;
  public HW_complete_count: any = 0;
  public HW_inprogress_count: any = 0;
  public completed_exam_count: any = 0;
  public inprogress_exam_count: any = 0;
  public present_count: any = 0;
  public absent_count: any = 0;
  public _school_name: any = null;
  public flag: Number = 0;
  public uid: any;
  public present: any;
  public absent: any;
  public avaraveAttendance: any;
  public attendance: any;
  public selectedChild: any;
  public myDate: any;
  public staff: any;
  public studentChart: any;
  public libraryStats: any;
  public visitorsStats: any;
  public academic: any;
  public paymentSchoolData: any;
  public dueAmount: any;
  public totalCollection: any;

  constructor(
    private _script: ScriptLoaderService,
    private identityService: IdentityService,
    private _child: ChildService,
    private dashboardService: DashboardService,
    private noticeService: NoticeService,
    private eventService: EventService,
    private _attendanceService: AttendanceService,
    private _homeworkService: HomeworkService,
    private _examinationService: ExaminationService,
    private __performanceService: PerformanceByClassService,
    private fetchschoolService: FetchschoolService,
    private _library: LibraryService,
    private _security: SecurityService,
    private cd: ChangeDetectorRef,
    private toasterService: ToasterService,
    private _feeMasterService: FeeMasterService
  ) {
    this.profile = this.identityService.identity;
    this.role = this.identityService.role;
    this.uid = this.identityService.uid;
    this.myDate = new Date();
    if (this.role === "director") {
      this.getSchool();
    }

    // if (this.role === this.identityService.role) {
    //   this.getRecentEvents();
    //   this.getRecentNotice();
    // }
  }

  Success() {
    this.toasterService.Error("Success button click");
  }

  getSchool() {
    this.fetchschoolService.getSchool().subscribe(
      data => {
        //  this.school_data = data['list'] as string [];
        // console.log('hiii' + this.school_data);
        this.school_data = data.payload;
      },
      error => {
        console.error("Error while getting school data. stack: ", error);
      }
    );
  }

  get school_name(): any {
    return this._school_name;
  }

  set school_name(value: any) {
    this._school_name = value;
  }

  get school_code(): any {
    return this._school_code;
  }

  set school_code(value: any) {
    this._school_code = value;
  }

  get child_list(): any {
    return this._child_list;
  }

  set child_list(value: any) {
    this._child_list = value;
  }

  ngOnInit() {
    if (this.role === "director") {
      this.school_code = localStorage.getItem("schoolCode");
      this.school_name = localStorage.getItem("schoolName");
    }

    if (this.role === "director" && !this.school_code) {
      setTimeout(function() {
        alert("Your school is not selected, please select a school");
      }, 1000);
    }

    if (this.role === "director" && this.school_code) {
      this.getDashboardStatsData();
      this.getDataForDirector();
    }

    if (this.role === "parent") {
      if (localStorage.getItem("selectedChild")) {
        this.selectedChild = JSON.parse(localStorage.getItem("selectedChild"));
        this.uid = this.selectedChild.uid;
        this.getDashboardDataForStudent();
      } else {
        this.getChild();
      }
    }

    if (
      this.role === "school" ||
      this.role === "teacher" ||
      this.role === "coadmin"
    ) {
      this.getDashboardStatsData();
      this.getLibraryStats();
      this.getSecurityStats();
      this.getRecentEvents();
      this.getRecentNotice();

      this.showData = true;
      this.cd.detectChanges();
      this.cd.markForCheck();
    }

    if (this.role === "student") {
      this.getDashboardDataForStudent();
    }

    if (this.role === "clerk") {
      this.getDashboardClerk();
      this.schoolPayment();
    }

    if (this.role === "librarian") {
      this.getLibraryStats();
      this.schoolPayment();
    }

    if (this.role === "security") {
      this.getSecurityStats();
    }
  }

  ngAfterViewInit() {
    this._script.loadScripts("app-index", ["assets/app/js/dashboard.js"]);
  }

  getChild() {
    let value: any;
    this._child.getChild().subscribe(data => {
      this._child_list = data.payload;
      $("#m_modal_5").modal("show");
      this.setChild(this._child_list[0]);
    });
  }

  setChild(data) {
    localStorage.setItem("selectedChild", JSON.stringify(data));
    this.uid = data.uid;
    this.getDashboardDataForStudent();
    $("#m_modal_5").modal("hide");
  }

  setChildChart() {
    this.getDashboardDataForStudent();
  }

  setSchoolCode(code: any, name: any) {
    localStorage.setItem("schoolCode", code);
    localStorage.setItem("schoolName", name);

    this.school_code = localStorage.getItem("schoolCode");
    this.school_name = localStorage.getItem("schoolName");

    this.getDataForDirector();
  }

  getDataForDirector() {
    this.getRecentEvents();
    this.getRecentNotice();
    this.getDashboardStatsData();

    this.showData = true;
    this.cd.detectChanges();
    this.cd.markForCheck();
  }

  getAttendance() {
    this._attendanceService.getAttendance().subscribe(
      attendance_data => {
        this.attendance_data = attendance_data.payload;

        for (const data of this.attendance_data) {
          if (data.state === "Present") {
            this.present_count++;
          }

          if (data.state === "Absent") {
            this.absent_count++;
          }
        }

        this.attendanceChart();
      },
      error => {
        console.error("Error while getting attendance data. stack: ", error);
      }
    );
  }

  getDashboardDataForStudent() {
    this.dashboardService
      .getDashboardDataForStudentAndParent(this.uid)
      .subscribe(
        stat_data => {
          if (stat_data.status == 200) {
            this.stats = stat_data.payload;
            this.avaraveAttendance = this.stats.attendance;
            this.academic = this.stats.homework + this.stats.upcomingExam;
            if (this.stats) {
              this.academicChart();
              this.attendanceBarChart();
              //this.upcoming_ExamChart();
              this.attendanceChart();
              this.performanceChart();
              //this.homeworkChart();
            }
          } else {
            this.avaraveAttendance = [];
          }
        },
        error => {
          console.error("Error while getting exam data. stack: ", error);
        }
      );
  }

  getDashboardDataForParentChild() {
    this.dashboardService
      .getDashboardDataForStudentAndParent(this.uid)
      .subscribe(
        stat_data => {
          if (stat_data !== null) {
            this.stats = stat_data.payload;
            this.avaraveAttendance = this.stats.attendance;
            this.academic = this.stats.homework + this.stats.upcomingExam;
            this.upcoming_ExamChart();
            this.attendanceChart();
            this.performanceChart();
            this.academicChart();
            this.homeworkChart();
          } else {
            this.avaraveAttendance = [];
          }
        },
        error => {
          console.error("Error while getting exam data. stack: ", error);
        }
      );
  }

  getUpcomingExam() {
    this._examinationService.getExamination().subscribe(
      exam_data => {
        this.exam_data = exam_data.payload;

        for (const data of this.exam_data) {
          if (data.state === "In-Progress") {
            this.inprogress_exam_count++;
          }

          if (data.state === "Completed") {
            this.completed_exam_count++;
          }
        }

        this.upcoming_ExamChart();
      },
      error => {
        console.error("Error while getting exam data. stack: ", error);
      }
    );
  }

  getExamination() {
    this._examinationService.getExamination().subscribe(
      exam_data => {
        this.exam_data = exam_data.payload;

        for (const data of this.exam_data) {
          if (data.state === "In-Progress") {
            this.inprogress_exam_count++;
          }

          if (data.state === "Completed") {
            this.completed_exam_count++;
          }
        }

        this.upcoming_ExamChart();
      },
      error => {
        console.error("Error while getting exam data. stack: ", error);
      }
    );
  }

  getDashboardStatsData() {
    this.dashboardService.getDashboardStats(this.uid).subscribe(
      stat_data => {
        if(stat_data.status === 200 ) {
          this.stats = stat_data.payload;
          this.cd.detectChanges();
          this.cd.markForCheck();
          this.staff =
            this.stats.activeClerk +
            this.stats.activeLibrarian +
            this.stats.activeSubAdmin +
            this.stats.activeTeacher;
          this.studentChart = this.stats.activeStudent + this.stats.activeParent;
          this.attendanceChart();
          this.performanceChart();
          this.performChart();
          this.activeStaffChart();
          this.activeStudentChart();
          this.activeUsers();
        } else {
          this.stats = {};
        }
        
      },
      error => {
        console.error("Error while getting exam data. stack: ", error);
      }
    );
  }

  // Get Clerk Dashboard Data
  getDashboardClerk() {
    this.dashboardService.getDashboardDataClerk().subscribe(
      stat_data => {
        this.stats = stat_data.payload;
        this.cd.detectChanges();
        this.cd.markForCheck();
      },
      error => {
        console.error("Error while getting exam data. stack: ", error);
      }
    );
  }

  getHomework() {
    this._homeworkService.getHomework().subscribe(
      homework_data => {
        this.homework_data = homework_data.payload;

        for (const h of this.homework_data) {
          if (h.completed) {
            this.HW_complete_count++;
          }

          if (!h.completed) {
            this.HW_inprogress_count++;
          }
        }
        this.homeworkChart();
      },
      error => {
        console.error("Error while getting homework data. stack: ", error);
      }
    );
  }
  getPerformance() {
    this.__performanceService.getPerformanceByClassService().subscribe(
      performance_data => {
        this.performance_data = performance_data.payload;

        for (const h of this.performance_data) {
          if (h.completed) {
            this.HW_complete_count++;
          }

          if (!h.completed) {
            this.HW_inprogress_count++;
          }
        }
        this.performanceChart();
      },
      error => {
        console.error("Error while getting homework data. stack: ", error);
      }
    );
  }

  getRecentNotice() {
    this.noticeService.getRecentNotice().subscribe(
      noticeData => {
        this.notices = noticeData.payload;
        // console.log("notice: ", this.notices);

        this.cd.detectChanges();
        this.cd.markForCheck();
      },
      error => {
        console.error("Error while getting Recent Notice data. Stack: ", error);
      }
    );
  }

  getRecentEvents() {
    this.eventService.getRecentEvents().subscribe(
      eventData => {
        this.events = eventData.payload;
        // console.log("events: ", this.events);

        this.cd.detectChanges();
        this.cd.markForCheck();
      },
      error => {
        console.error("Error while getting Recent Notice data. Stack: ", error);
      }
    );
  }

  activeUsers = function() {
    // console.log("activeUser is called..............");
    if ($("#m_chart_active_user").length === 0) {
      return;
    }

    Morris.Donut({
      element: "m_chart_active_user",
      data: [
        {
          label: "Teacher",
          value: this.stats.activeTeacher ? this.stats.activeTeacher : 0
        },
        {
          label: "Student",
          value: this.stats.activeStudent ? this.stats.activeStudent : 0
        },
        {
          label: "Parent",
          value: this.stats.activeParent ? this.stats.activeParent : 0
        }
      ],
      colors: ["#716aca", "#00c5dc", "#f4516c"]
    });
  };

  attendanceChart = function() {
    // console.log(
    //   "attendanceChart is called.............." + this.stats.attendance
    // );
    // console.log("#m_chart_attendance length", $("#m_chart_attendance").length);
    if ($("#m_chart_attendance").length === 0) {
      return;
    }

    const chart = Chartist.Pie(
      "#m_chart_attendance",
      {
        series: [
          {
            value: this.stats.attendance ? this.stats.attendance : 0,
            className: "Present",
            meta: {
              color: "#ed8a19"
            }
          },
          {
            value: this.stats.attendance ? 100 - this.stats.attendance : 100,
            className: "Absent",
            meta: {
              color: "#f1f2f7"
            }
          }
        ]
      },
      {
        donut: true,
        donutWidth: 17,
        showLabel: false
      }
    );

    chart.on("draw", function(data) {
      if (data.type === "slice") {
        // Get the total path length in order to use for dash array animation
        const pathLength = data.element._node.getTotalLength();

        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
          "stroke-dasharray": pathLength + "px " + pathLength + "px"
        });

        // Create animation definition while also assigning an ID to the animation for later sync usage
        const animationDefinition = {
          "stroke-dashoffset": {
            id: "anim" + data.index,
            dur: 1000,
            from: -pathLength + "px",
            to: "0px",
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: "freeze",
            stroke: data.meta.color
          }
        };

        // // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if (data.index !== 0) {
          animationDefinition["stroke-dashoffset"]["begin"] =
            "anim" + (data.index - 1) + ".end";
        }

        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

        data.element.attr({
          "stroke-dashoffset": -pathLength + "px",
          stroke: data.meta.color
        });

        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
      }
    });
  };

  performChart = function() {
    // if ($("#m_chart_perform").length === 0) {
    //   return;
    // }

    if (this.role == "parent") {
    } else if ($("#m_chart_perform").length === 0) {
      return;
    }

    const chart = Chartist.Pie(
      "#m_chart_perform",
      {
        series: [
          {
            value: this.stats.performance ? this.stats.performance : 0,
            className: "scored",
            meta: {
              color: "#ed8a19"
            }
          },
          {
            value: this.stats.performance ? 100 - this.stats.performance : 100,
            className: "Others",
            meta: {
              color: "#f1f2f7"
            }
          }
        ]
      },
      {
        donut: true,
        donutWidth: 17,
        showLabel: false
      }
    );

    chart.on("draw", function(data) {
      if (data.type === "slice") {
        // Get the total path length in order to use for dash array animation
        const pathLength = data.element._node.getTotalLength();

        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
          "stroke-dasharray": pathLength + "px " + pathLength + "px"
        });

        // Create animation definition while also assigning an ID to the animation for later sync usage
        const animationDefinition = {
          "stroke-dashoffset": {
            id: "anim" + data.index,
            dur: 1000,
            from: -pathLength + "px",
            to: "0px",
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: "freeze",
            stroke: data.meta.color
          }
        };

        // // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if (data.index !== 0) {
          animationDefinition["stroke-dashoffset"]["begin"] =
            "anim" + (data.index - 1) + ".end";
        }

        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

        data.element.attr({
          "stroke-dashoffset": -pathLength + "px",
          stroke: data.meta.color
        });

        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
      }
    });
  };

  activeStaffChart = function() {
    // console.log(
    //   "attendanceChart is called.............." + this.stats.attendance
    // );
    // console.log("#m_chart_staff length", $("#m_chart_staff").length);
    if ($("#m_chart_staff").length === 0) {
      return;
    }

    const chart = Chartist.Pie(
      "#m_chart_staff",
      {
        series: [
          {
            value: this.stats.activeTeacher
              ? (this.stats.activeTeacher / this.staff) * 100
              : 0,
            className: "teacher",
            meta: {
              color: "#ed8a19"
            }
          },
          {
            value: this.staff
              ? ((this.staff - this.stats.activeTeacher) / this.staff) * 100
              : 100,
            className: "Others",
            meta: {
              color: "#f1f2f7"
            }
          }
        ]
      },
      {
        donut: true,
        donutWidth: 17,
        showLabel: false
      }
    );

    chart.on("draw", function(data) {
      if (data.type === "slice") {
        // Get the total path length in order to use for dash array animation
        const pathLength = data.element._node.getTotalLength();

        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
          "stroke-dasharray": pathLength + "px " + pathLength + "px"
        });

        // Create animation definition while also assigning an ID to the animation for later sync usage
        const animationDefinition = {
          "stroke-dashoffset": {
            id: "anim" + data.index,
            dur: 1000,
            from: -pathLength + "px",
            to: "0px",
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: "freeze",
            stroke: data.meta.color
          }
        };

        // // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if (data.index !== 0) {
          animationDefinition["stroke-dashoffset"]["begin"] =
            "anim" + (data.index - 1) + ".end";
        }

        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

        data.element.attr({
          "stroke-dashoffset": -pathLength + "px",
          stroke: data.meta.color
        });

        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
      }
    });
  };

  activeStudentChart = function() {
    // console.log(
    //   "attendanceChart is called.............." + this.stats.attendance
    // );
    // console.log("#m_chart_student length", $("#m_chart_student").length);
    if ($("#m_chart_student").length === 0) {
      return;
    }

    const chart = Chartist.Pie(
      "#m_chart_student",
      {
        series: [
          {
            value: this.studentChart
              ? ((this.studentChart - this.stats.activeStudent) /
                  this.studentChart) *
                100
              : 0,
            className: "student",
            meta: {
              color: "#ed8a19"
            }
          },
          {
            value: this.studentChart
              ? ((this.studentChart - this.stats.activeParent) /
                  this.studentChart) *
                100
              : 100,
            className: "parent",
            meta: {
              color: "#f1f2f7"
            }
          }
        ]
      },
      {
        donut: true,
        donutWidth: 17,
        showLabel: false
      }
    );

    chart.on("draw", function(data) {
      if (data.type === "slice") {
        // Get the total path length in order to use for dash array animation
        const pathLength = data.element._node.getTotalLength();

        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
          "stroke-dasharray": pathLength + "px " + pathLength + "px"
        });

        // Create animation definition while also assigning an ID to the animation for later sync usage
        const animationDefinition = {
          "stroke-dashoffset": {
            id: "anim" + data.index,
            dur: 1000,
            from: -pathLength + "px",
            to: "0px",
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: "freeze",
            stroke: data.meta.color
          }
        };

        // // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if (data.index !== 0) {
          animationDefinition["stroke-dashoffset"]["begin"] =
            "anim" + (data.index - 1) + ".end";
        }

        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

        data.element.attr({
          "stroke-dashoffset": -pathLength + "px",
          stroke: data.meta.color
        });

        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
      }
    });
  };

  academicChart = function() {
    if (this.role == "parent") {
    } else if ($("#m_chart_academic").length === 0) {
      return;
    }

    let homeowrk =
      ((this.academic - this.stats.homework) / this.academic) * 100;
    let upcomingExam =
      ((this.academic - this.stats.upcomingExam) / this.academic) * 100;
    const chart = Chartist.Pie(
      "#m_chart_academic",
      {
        series: [
          {
            value: this.academic ? homeowrk : 0,
            className: "homework",
            meta: {
              color: "#f1f2f7"
            }
          },
          {
            value: this.academic ? upcomingExam : 100,
            className: "upcomingExam",
            meta: {
              color: "#ed8a19"
            }
          }
        ]
      },
      {
        donut: true,
        donutWidth: 17,
        showLabel: false
      }
    );

    // console.log("chart: ", chart);
    chart.on("draw", function(data) {
      if (data.type === "slice") {
        // Get the total path length in order to use for dash array animation
        const pathLength = data.element._node.getTotalLength();

        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
          "stroke-dasharray": pathLength + "px " + pathLength + "px"
        });

        // Create animation definition while also assigning an ID to the animation for later sync usage
        const animationDefinition = {
          "stroke-dashoffset": {
            id: "anim" + data.index,
            dur: 1000,
            from: -pathLength + "px",
            to: "0px",
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: "freeze",
            stroke: data.meta.color
          }
        };

        // // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if (data.index !== 0) {
          animationDefinition["stroke-dashoffset"]["begin"] =
            "anim" + (data.index - 1) + ".end";
        }

        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

        data.element.attr({
          "stroke-dashoffset": -pathLength + "px",
          stroke: data.meta.color
        });

        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
      }
    });
  };

  performanceChart = function() {
    // console.log("PerformanceChart............." + this.stats.performance);
    // if ($("#m_chart_performance").length === 0) {
    //   return;
    // }

    if (this.role == "parent") {
    } else if ($("#m_chart_performance").length === 0) {
      return;
    }

    const performanceChart = Chartist.Pie(
      "#m_chart_performance",
      {
        series: [
          {
            value: this.stats.performance ? this.stats.performance : 0,
            className: "scored",
            meta: {
              color: "#ed8a19"
            }
          },
          {
            value: this.stats.performance ? 100 - this.stats.performance : 100,
            className: "Others",
            meta: {
              color: "#f1f2f7"
            }
          }
        ]
      },
      {
        donut: true,
        donutWidth: 17,
        showLabel: false
      }
    );

    performanceChart.on("draw", function(data) {
      if (data.type === "slice") {
        // Get the total path length in order to use for dash array animation
        const pathLength = data.element._node.getTotalLength();

        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
          "stroke-dasharray": pathLength + "px " + pathLength + "px"
        });

        // Create animation definition while also assigning an ID to the animation for later sync usage
        const animationDefinition = {
          "stroke-dashoffset": {
            id: "anim" + data.index,
            dur: 1000,
            from: -pathLength + "px",
            to: "0px",
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: "freeze",
            stroke: data.meta.color
          }
        };

        // // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if (data.index !== 0) {
          animationDefinition["stroke-dashoffset"]["begin"] =
            "anim" + (data.index - 1) + ".end";
        }

        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
        data.element.attr({
          "stroke-dashoffset": -pathLength + "px",
          stroke: data.meta.color
        });

        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
      }
    });
  };

  homeworkChart = function() {
    // if ($("#m_chart_homework").length === 0) {
    //   return;
    // }
    if (this.role == "parent") {
    } else if ($("#m_chart_homework").length === 0) {
      return;
    }

    Morris.Donut({
      element: "m_chart_homework",
      data: [
        {
          label: "In-progress",
          value: this.HW_inprogress_count ? this.HW_inprogress_count : 0
        },
        {
          label: "Complete",
          value: this.HW_complete_count ? this.HW_complete_count : 100
        }
      ],
      colors: ["#716aca", "#00c5dc", "#f4516c"],
      donutWidth: 10
    });
  };

  upcoming_ExamChart = function() {
    // if ($("#m_chart_upcomingexam").length === 0) {
    //   return;
    // }

    if (this.role == "parent") {
    } else if ($("#m_chart_upcomingexam").length === 0) {
      return;
    }

    Morris.Donut({
      element: "m_chart_upcomingexam",
      data: [
        {
          label: "In-progress",
          value: this.inprogress_exam_count ? this.inprogress_exam_count : 0
        },
        {
          label: "Complete",
          value: this.completed_exam_count ? this.inprogress_exam_count : 100
        }
      ],
      colors: ["#716aca", "#00c5dc", "#f4516c"]
    });
  };

  getLibraryStats() {
    this._library.getLibraryStats().subscribe(libraryStats => {
      this.libraryStats = libraryStats.payload;
    //  console.log("Library borrow List", this.libraryStats);
      this.libraryChart();
    });
  }

  /** Library stats Bar Chart */
  libraryChart = function() {
   // console.log("library Bar: " + this.libraryStats);
    if ($("#m_chart_library").length === 0) {
      return;
    }
    const series = [];
    const label = [];
   // console.log("Subjectlist:", this.libraryStats);
    for (const e of this.libraryStats.libraryStats) {
      series.push(e.issuedBookCount);
      label.push(e.Month.substring(0,3));
    }
    //console.log(series);
    const chart_data = Chartist.Bar(
      "#m_chart_library",
      {
        labels: label,
        series: series
      },
      {
        barwidth: 100,
        distributeSeries: true,
        low: 0,
        // high: 100,
        axisX: {
          showGrid: false
        },
        axisY: {
          showGrid: false,
          showLabel: false,
          offset: 0
        }
      }
    );
    chart_data.on("draw", function(data) {
      if (data.type === "bar") {
        data.element.attr({
          style: "stroke-width: 10%; stroke: #e18215"
        });
      }
    });
  };

  /** Attendance stats Bar Chart */
  attendanceBarChart = function() {
   // console.log("library Bar: ", this.stats);
    if (this.role == "parent") {
    } else if ($("#m_chart_attendance_bar").length === 0) {
      return;
    }
    const series = [];
    const label = [];
   // console.log("Subjectlist:", this.stats.attendanceStats);
    for (const e of this.stats.attendanceStats) {
    //  console.log(e);
      series.push(e.presentDays);
      let labels = e.presentDays + " Days \n" + e.Month.substring(0,3);
      label.push(labels);
    }
   // console.log(series);
    const chart_data = Chartist.Bar(
      "#m_chart_attendance_bar",
      {
        labels: label,
        series: series
      },
      {
        barwidth: 100,
        distributeSeries: true,
        low: 0,
        // high: 100,
        axisX: {
          showGrid: false
        },
        axisY: {
          showGrid: false,
          showLabel: false,
          offset: 0
        }
      }
    );
    chart_data.on("draw", function(data) {
      if (data.type === "bar") {
        data.element.attr({
          style: "stroke-width: 10%; stroke: #e18215"
        });
      }
    });
  };

  /** payment stats Bar Chart */
  schoolPayment() {
    this._feeMasterService
      .getschoolPayment(this.school_code)
      .subscribe(schoolpayment => {
        if (schoolpayment.status == 200) {
          this.paymentSchoolData = schoolpayment.payload;
       //   console.log("this.paymentSchoolData: ", this.paymentSchoolData);
          this.dueAmount = (
            (100 * this.paymentSchoolData.amountDetails.duePayment) /
            this.paymentSchoolData.amountDetails.totalAmount
          ).toFixed();
          this.totalCollection = (
            (100 * this.paymentSchoolData.amountDetails.totalCollection) /
            this.paymentSchoolData.amountDetails.totalAmount
          ).toFixed();
          this.duePaymentChart(this.dueAmount);
          this.totalCollectionChart(this.totalCollection);
          this.paymentChart();
        }
      });
  }

  /** Student Subject perfomance Bar Chart */
  paymentChart = function() {
    //console.log("payment Bar: " + this.paymentSchoolData);
    if ($("#m_chart_payment").length === 0) {
      return;
    }
    const series = [];
    const label = [];
    for (const e of this.paymentSchoolData.feesAmountStats) {
      const perc = (e.paidAmount / e.totalDue) * 100;
     // console.log(e);
      series.push(perc);
      label.push(e.Month.substring(0,3));
    }
   // console.log(series);
    const chart_data = Chartist.Bar(
      "#m_chart_payment",
      {
        labels: label,
        series: series
      },
      {
        barwidth: 100,
        distributeSeries: true,
        // low: 0,
        // high: 100,
        axisX: {
          showGrid: false
        },
        axisY: {
          showGrid: false,
          showLabel: false,
          offset: 0
        }
      }
    );
    chart_data.on("draw", function(data) {
      if (data.type === "bar") {
        data.element.attr({
          style: "stroke-width: 10%; stroke: #e18215"
        });
      }
    });
  };

  /** Due Payment Bar Chart */
  duePaymentChart = function(duePayment) {
    if ($("#m_chart_duepayment").length === 0) {
   //   console.log("duePaymentChart pie length: 0");
      return;
    }

    let dueAmount = 100 - duePayment;

    const duebarchart = Chartist.Pie(
      "#m_chart_duepayment",
      {
        series: [
          {
            value: dueAmount,
            className: "Paid",
            meta: {
              color: "#f2f3f8"
            }
          },
          {
            value: duePayment,
            className: "due",
            meta: {
              color: "#ff6565"
            }
          }
        ]
      },
      {
        donut: true,
        donutWidth: 10,
        showLabel: false
      }
    );
   // console.log("barchart: ", duebarchart);
    duebarchart.on("draw", function(data) {
     
      if (data.type === "slice") {
        // Get the total path length in order to use for dash array animation
        const pathLength = data.element._node.getTotalLength();

      //  console.log("pathLength: ", pathLength);
        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
          "stroke-dasharray": pathLength + "px " + pathLength + "px"
        });

        // Create animation definition while also assigning an ID to the animation for later sync usage
        const animationDefinition = {
          "stroke-dashoffset": {
            id: "anim" + data.index,
            dur: 1000,
            from: -pathLength + "px",
            to: "0px",
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: "freeze",
            stroke: data.meta.color
          }
        };

        // // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if (data.index !== 0) {
          animationDefinition["stroke-dashoffset"]["begin"] =
            "anim" + (data.index - 1) + ".end";
        }

        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

        data.element.attr({
          "stroke-dashoffset": -pathLength + "px",
          stroke: data.meta.color
        });

        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
      }
    });
  };

  /** Due Payment Bar Chart */
  totalCollectionChart = function(totalCollection) {
    if ($("#m_chart_totalCollection").length === 0) {
     // console.log("totalCollectionChart pie length: 0");
      return;
    }

    let totalAmount = 100 - totalCollection;
    const collectionbarchart = Chartist.Pie(
      "#m_chart_totalCollection",
      {
        series: [
          {
            value: totalCollection,
            className: "Paid",
            meta: {
              color: "#53d476"
            }
          },
          {
            value: totalAmount,
            className: "due",
            meta: {
              color: "#f2f3f8"
            }
          }
        ]
      },
      {
        donut: true,
        donutWidth: 10,
        showLabel: false
      }
    );
    //console.log("chart: ", collectionbarchart);
    collectionbarchart.on("draw", function(data) {
      // console.log("attendance pie:");
      // console.log("data draw-: ", data);
      if (data.type === "slice") {
        // Get the total path length in order to use for dash array animation
        const pathLength = data.element._node.getTotalLength();

      //  console.log("pathLength: ", pathLength);
        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
          "stroke-dasharray": pathLength + "px " + pathLength + "px"
        });

        // Create animation definition while also assigning an ID to the animation for later sync usage
        const animationDefinition = {
          "stroke-dashoffset": {
            id: "anim" + data.index,
            dur: 1000,
            from: -pathLength + "px",
            to: "0px",
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: "freeze",
            stroke: data.meta.color
          }
        };

        // // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if (data.index !== 0) {
          animationDefinition["stroke-dashoffset"]["begin"] =
            "anim" + (data.index - 1) + ".end";
        }

        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

        data.element.attr({
          "stroke-dashoffset": -pathLength + "px",
          stroke: data.meta.color
        });

        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
      }
    });
  };

  /** Get Security Stats Data */
  getSecurityStats() {
    this._security.getSecurityStats().subscribe(securityStats => {
      if (securityStats.status === 200) {
        this.visitorsStats = securityStats.payload;
       // console.log("Library borrow List", this.visitorsStats);
        this.securityChart();
      }
    });
  }

  /** Security stats Bar Chart */
  securityChart = function() {
    if ($("#m_chart_security").length === 0) {
      return;
    }
    const series = [];
    const label = [];
    for (const e of this.visitorsStats.visitorsStats) {
      series.push(e.visitorCount);
      label.push(e.Month.substring(0,3));
    }
    const chart_data = Chartist.Bar(
      "#m_chart_security",
      {
        labels: label,
        series: series
      },
      {
        barwidth: 100,
        distributeSeries: true,
        low: 0,
        // high: 100,
        axisX: {
          showGrid: false
        },
        axisY: {
          showGrid: false,
          showLabel: false,
          offset: 0
        }
      }
    );
    chart_data.on("draw", function(data) {
      if (data.type === "bar") {
        data.element.attr({
          style: "stroke-width: 10%; stroke: #e18215"
        });
      }
    });
  };
}
