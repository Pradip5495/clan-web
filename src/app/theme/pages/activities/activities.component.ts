import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { ScriptLoaderService, DashboardService, IdentityService, NoticeService, EventService } from '../../../_services';
import { User } from '../../../_models';
import { HomeworkService } from '../../../_services/homework.service';
import { ExaminationService } from '../../../_services/examination.service';
import { FetchschoolService } from '../../../_services/fetchschool.service';
import { ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { PerformanceByClassService } from '../../../_services/performanceByClass.service';
import { AttendanceService } from '../../../_services/attendance.service';

declare let $: JQueryStatic;
declare let Morris, Chartist;
@Component({
  selector: 'app-activities',
  templateUrl: './activies.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['activities.component.css']
})
export class ActivitiesComponent implements OnInit {

  public myDate: any;
  public stats: any;
  public profile: User;
  public notices: any;
  public events: any;
  public role: any;
  public uid: any;
  public _child_list: any;
  public selectedChild: any;

  constructor(private _script: ScriptLoaderService,
    private identityService: IdentityService,
    private dashboardService: DashboardService,
    private noticeService: NoticeService,
    private eventService: EventService,
    private cd: ChangeDetectorRef) {

    this.profile = this.identityService.identity;
    this.role = this.identityService.role;
    this.uid = this.identityService.uid;
    this.myDate = new Date();
  }

  get child_list(): any {
    return this._child_list;
  }

  set child_list(value: any) {
    this._child_list = value;
  }

  ngOnInit() {

    if (this.role === 'school' || this.role === 'teacher' || this.role === 'coadmin') {
      this.getDashboardStatsData();
      // this.getRecentEvents();
      // this.getRecentNotice();
    }

    if (this.role === 'parent') {
      if (localStorage.getItem("selectedChild")) {
        this.selectedChild = JSON.parse(localStorage.getItem("selectedChild"));
        this.uid = this.selectedChild.uid;
        this.getDashboardDataForStudent();
      } else {
        this.child_list = localStorage.getItem('selectedChild');
      }
    }

    if(this.role === 'student') {
      this.getDashboardDataForStudent();
    }

    if (this.role === 'clerk') {
      this.getDashboardClerk();
    }

  }

  getDashboardStatsData() {    
    this.dashboardService.getDashboardStats(this.uid).subscribe(
      stat_data => {
        this.stats = stat_data.payload;
      },
      error => {
        console.error("Error while getting exam data. stack: ", error);
      }
    );
  }

  // Get Clerk Dashboard Data
  getDashboardClerk() {
    this.dashboardService.getDashboardDataClerk().subscribe(
      stat_data => {
        this.stats = stat_data.payload;
      },
      error => {
        console.error("Error while getting exam data. stack: ", error);
      }
    );
  }

  getDashboardDataForStudent() {
    //console.log("student:", this.uid);
    this.dashboardService
      .getDashboardDataForStudentAndParent(this.uid)
      .subscribe(
        stat_data => {
          if (stat_data.status == 200) {
            this.stats = stat_data.payload;
          } 
        },
        error => {
          console.error("Error while getting exam data. stack: ", error);
        }
      );
  }

  getRecentNotice() {
    this.noticeService.getRecentNotice().subscribe(noticeData => {
      this.notices = noticeData.payload;
    },
      error => {
        console.error('Error while getting Recent Notice data. Stack: ', error);
      });
  }

  getRecentEvents() {
    this.eventService.getRecentEvents().subscribe(eventData => {
      this.events = eventData.payload;
    },
      error => {
        console.error('Error while getting Recent Notice data. Stack: ', error);
      });
  }

}
