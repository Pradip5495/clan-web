import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/_guards/auth.guard';
// import {PassengerComponent} from "./pages/passenger/passenger.component";

const routes: Routes = [
  {
    'path': '',
    'component': ThemeComponent,
    'canActivate': [AuthGuard],
    'children': [
      {
        'path': 'index',
        'loadChildren': '.\/pages\/index\/index.module#IndexModule'
      },
      {
        'path': 'schools',
        'loadChildren': '.\/pages\/schools\/schools.module#SchoolsModule'
      },
      {
        'path': 'profile',
        'loadChildren': '.\/pages\/profile\/profile.module#ProfileModule'
      },
      {
        'path': 'timetable',
        'loadChildren': '.\/pages\/timetable\/timetable.module#TimetableModule'
      },
      {
        'path': 'attendance',
        'loadChildren': '.\/pages\/attendance\/attendance.module#AttendanceModule'
      },
      {
        'path': 'attendance/:student',
        'loadChildren': '.\/pages\/attendance\/attendance.module#AttendanceModule'
      },
      {
        'path': 'attendance/class/:class',
        'loadChildren': '.\/pages\/attendance\/attendance.module#AttendanceModule'
      },
      {
        'path': 'attendance/classes',
        'loadChildren': '.\/pages\/attendance\/attendance.module#AttendanceModule'
      },
      {
        'path': 'attendance/student/:uid',
        'loadChildren': '.\/pages\/attendance\/attendance.module#AttendanceModule'
      },
      {
        'path': 'attendance/students/:std',
        'loadChildren': '.\/pages\/attendance\/attendance.module#AttendanceModule'
      },
      {
        'path': 'students',
        'loadChildren': '.\/pages\/students\/students.module#StudentsModule'
      },
      {
        'path': 'student-attendance/:uid/:div/',
        'loadChildren': '.\/pages\/attendance\/attendance.module#AttendanceModule'
      },
      {
        'path': 'notification',
        'loadChildren': '.\/pages\/notification\/notification.module#NotificationModule'
      },
      {
        'path': 'teachers',
        'loadChildren': './pages/teachers/teachers.module#TeachersModule'
      },
      {
        'path': 'classes',
        'loadChildren': '.\/pages\/classes\/classes.module#ClassesModule'
      },
      {
        'path': 'classes/:class',
        'loadChildren': '.\/pages\/classes\/classes.module#ClassesModule'
      },
      {
        'path': 'map',
        'loadChildren': '.\/pages\/map\/map.module#MapModule'
      },
      {
        'path': 'settings',
        'loadChildren': '.\/pages\/settings\/settings.module#SettingsModule'
      },
      {
        'path': 'class',
        'loadChildren': '.\/pages\/class\/class.module#ClassModule'
      },
      {
        'path': 'events',
        'loadChildren': '.\/pages\/events\/events.module#EventsModule'
      },
      {
        'path': 'notice',
        'loadChildren': '.\/pages\/notice\/notice.module#NoticeModule'
      },
      {
        'path': 'assign',
        'loadChildren': '.\/pages\/assign\/assign.module#AssignModule'
      },
      {
        'path': 'driver',
        'loadChildren': '.\/pages\/driver\/driver.module#DriverModule'
      },
      {
        'path': 'transport',
        'loadChildren': '.\/pages\/transport\/transport.module#TransportModule'
      },
      {
        'path': 'result',
        'loadChildren': '.\/pages\/result\/result.module#ResultModule'
      },
      {
        'path': 'passenger',
        'loadChildren': '.\/pages\/passenger\/passenger.module#PassengerModule'
      },
      {
        'path': 'non-teaching-staff',
        'loadChildren': '.\/pages\/non-teaching-staff\/non-teaching-staff.module#NonTeachingStaffModule'
      },
      {
        'path': 'track-buses',
        'loadChildren': '.\/pages\/track-buses\/track-buses.module#TrackBusesModule'
      },
      {
        'path': 'add-super-user',
        'loadChildren': '.\/pages\/add-super-user\/add-super-user.module#AddSuperUserModule'
      },
      {
        'path': 'my-child',
        'loadChildren': '.\/pages\/my-child\/my-child.module#MyChildModule'
      },
      {
        'path': 'daily-thoughts',
        'loadChildren': '.\/pages\/daily-thoughts\/daily-thoughts.module#DailyThoughtsModule'
      },
      {
        'path': 'calender',
        'loadChildren': '.\/pages\/calender\/calender.module#CalenderModule'
      },
      {
        'path': 'bus-passengers',
        'loadChildren': '.\/pages\/bus-passengers\/bus-passengers.module#BusPassengersModule'
      },
      {
        'path': 'inbox',
        'loadChildren': '.\/pages\/inbox\/inbox.module#InboxModule'
      },
      {
        'path': '404',
        'loadChildren': '.\/pages\/not-found\/not-found.module#NotFoundModule'
      },
      {
        'path': 'homework',
        'loadChildren': '.\/pages\/homework\/homework.module#HomeworkModule'
      },
      {
        'path': 'homework/:home',
        'loadChildren': '.\/pages\/homework\/homework.module#HomeworkModule'
      },
      {
        'path': 'examination',
        'loadChildren': '.\/pages\/examination\/examination.module#ExaminationModule'
      },
      {
        'path': 'examination/:exam',
        'loadChildren': '.\/pages\/examination\/examination.module#ExaminationModule'
      },
      {
        'path': 'performance',
        'loadChildren': '.\/pages\/performance\/performance.module#PerformanceModule'
      },
      {
        'path': 'performance/student/:uid',
        'loadChildren': '.\/pages\/performance\/performance.module#PerformanceModule'
      },
      {
        'path': 'Discuss',
        'loadChildren': '.\/pages\/Discuss\/Discuss.module#DiscussModule'
      },
      {
        'path': 'chatbox',
        'loadChildren': '.\/pages\/chatbox\/chatbox.module#ChatboxModule'
      },
      {
        'path': 'membersinfo',
        'loadChildren': '.\/pages\/members-info\/members-info.module#MembersInfoModule'
      },
      {
        'path': 'messages',
        'loadChildren': '.\/pages\/messages\/messages.module#MessagesModule'
      },
      {
        'path': 'feetypes',
        'loadChildren': '.\/pages\/fees-type\/fees-type.module#FeesTypeModule'
      },
      {
        'path': 'feetemplate',
        'loadChildren': '.\/pages\/fee-template\/fee-template.module#FeeTemplateModule'
      },
      {
        'path': 'feetemplate/:std',
        'loadChildren': '.\/pages\/fee-template\/fee-template.module#FeeTemplateModule'
      },
      {
        'path': 'assign-fees',
        'loadChildren': '.\/pages\/fee-master\/fee-master.module#FeeMasterModule'
      },
      {
        'path': 'assign-fees/:std',
        'loadChildren': '.\/pages\/fee-master\/fee-master.module#FeeMasterModule'
      },
      {
        'path': 'assign-fees/student-fee/:std/:uid',
        'loadChildren': '.\/pages\/fee-master\/fee-master.module#FeeMasterModule'
      },
      {
        'path': 'assign-fees/assign-studentfee/:uid',
        'loadChildren': '.\/pages\/fee-master\/fee-master.module#FeeMasterModule'
      },
      {
        'path': 'assign-fees/assign/:student/:class',
        'loadChildren': '.\/pages\/fee-master\/fee-master.module#FeeMasterModule'
      },
      {
        'path': 'activities',
        'loadChildren': '.\/pages\/activities\/activities.module#ActivitiesModule'
      },
      {
        'path': 'library',
        'loadChildren': '.\/pages\/library\/library.module#LibraryModule'
      },
      {
        'path': 'security',
        'loadChildren': '.\/pages\/security\/security.module#SecurityModule'
      },
      {
        'path': 'members',
        'loadChildren': '.\/pages\/members\/members.module#MembersModule'
      },
      {
        'path': 'members/:type',
        'loadChildren': '.\/pages\/members\/members.module#MembersModule'
      },
      {
        'path': 'clerk',
        'loadChildren': '.\/pages\/clerk\/clerk.module#ClerkModule'
      },
      {
        'path': 'privacy',
        'loadChildren': '.\/pages\/privacy\/privacy.module#PrivacyModule'
      },
      {
        'path': 'about-us',
        'loadChildren': '.\/pages\/about-us\/about-us.module#AboutUsModule'
      },
      {
        'path': '',
        'redirectTo': 'index',
        'pathMatch': 'full'
      }
    ]
  },
  {
    'path': '**',
    'redirectTo': '404',
    'pathMatch': 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThemeRoutingModule { }
