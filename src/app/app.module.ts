import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { ThemeComponent } from "./theme/theme.component";

import { AppRoutingModule } from "./app-routing.module";
import { ThemeRoutingModule } from "./theme/theme-routing.module";

import { LayoutModule } from "./theme/layouts/layout.module";
import { AuthModule } from "./auth/auth.module";

//import {MatSidenavModule} from '@angular/material/sidenav';

// Multiselect dropdown
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";

// Interceptors
import { TokenInterceptor } from "./auth/_interceptor/token.interceptor";

// Models
import { User } from "./_models";

// Google Map Implement module
import { AgmCoreModule, GoogleMapsAPIWrapper } from "@agm/core";
import { AgmDirectionModule } from "agm-direction"; // agm-direction

// Services
import { IdentityService } from "./_services/identity.service";
import { DashboardService } from "./_services/dashboard.service";
import { NoticeService } from "./_services/notice.service";
import { LectureService } from "./_services/lecture.service";
import { ScriptLoaderService } from "./_services/script-loader.service";
import { TimetableService } from "./_services/timetable.service";
import { TeacherService } from "./_services/teacher.service";
import { StudentService } from "./_services/student.service";
import { AttendanceService } from "./_services/attendance.service";
import { ApprovalService } from "./_services/approval.service";
import { EventService } from "./_services/event.service";
import { ClassService } from "./_services/class.service";
import { HomeworkService } from "./_services/homework.service";
import { ExaminationService } from "./_services/examination.service";
import { ProfileService } from "./_services/profile.service";
import { ParentService } from "./_services/parent.service";
import { ResultService } from "./_services/result.service";
import { ConversationService } from "./_services/conversation.service";
import { ChildService } from "./_services/child.service";
import { PerformanceByClassService } from "./_services/performanceByClass.service";
import { PerformanceBreakdownBySubjectService } from "./_services/performanceBreakdownBySubject.service";
import { ListInboxItemsService } from "./_services/listInboxItems.service";
import { NotificationListService } from "./_services/notificationList.service";
import { AvailabilityCheckService } from "./_services/availabilityCheck.service";
import { CheckSchoolService } from "./_services/checkSchool.service";
import { OtpService } from "./_services/otp.service";
import { MessageService } from "./_services/message.service";
import { FetchschoolService } from "./_services/fetchschool.service";
import { ReportService } from "./_services/report.service";
// comunication services
import { ConversationIdService } from "./_services/conversationId.service";
import { MemberinfoService } from "./_services/memberinfo.service";
import { AssignService } from "./_services/assign.service";
import { DriverService } from "./_services/driver.service";
import { TransportService } from "./_services/transport.service";
import { PassengerService } from "./_services/passenger.service";
import { TrackBusesService } from "./_services/track-buses.service";
import { TripService } from "./_services/trip.service";
import { NonTeachingStaffService } from "./_services/non-teaching-staff.service";
import { DailyThoughtsService } from "./_services/daily-thoughts.service";
import { AddSuperUserService } from "./_services/add-super-user.service";
import { FeesTypeService } from "./_services/fees-type.service";
import { Data } from "./_provider/data/data";
import { FeeTemplateService } from "./_services/fee-template.service";
import { FeeMasterService } from "./_services/fee-master.service";
import { LibraryService } from "./_services/library.service";
import { SecurityService } from "./_services/security.service";
import { ClerkService } from "./_services/clerk.service";
import { MembersService } from "./_services/members.service";
import { PaymentService } from "./_services/payment.service";
import { ToasterService } from "./_services/toaster.service";
//import { LibraryFilterPipe } from './theme/pages/library/filter.pipe';

@NgModule({
  declarations: [
    ThemeComponent,
    AppComponent,
    //MatSidenavModule
    //LibraryFilterPipe
  ],
  imports: [
    LayoutModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ThemeRoutingModule,
    AuthModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    AgmDirectionModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyBFYhE4lgP8bEq6-xuPmH9bWE_MPct1ROo" //AIzaSyBbUr9k3JchtdRRTnyrl2opuLJkBd7XT_w
    }),
    AngularMultiSelectModule //Multiselect dropdown
  ],
  providers: [
    ScriptLoaderService,
    IdentityService,
    DashboardService,
    NoticeService,
    LectureService,
    TimetableService,
    TeacherService,
    StudentService,
    AttendanceService,
    ApprovalService,
    EventService,
    ClassService,
    HomeworkService,
    LibraryService,
    ExaminationService,
    ProfileService,
    ParentService,
    ResultService,
    ConversationService,
    ChildService,
    PerformanceByClassService,
    PerformanceBreakdownBySubjectService,
    ListInboxItemsService,
    NotificationListService,
    AvailabilityCheckService,
    CheckSchoolService,
    OtpService,
    MessageService,
    ConversationIdService,
    FetchschoolService,
    MemberinfoService,
    ReportService,
    AssignService,
    TransportService,
    DriverService,
    PassengerService,
    TrackBusesService,
    TripService,
    NonTeachingStaffService,
    DailyThoughtsService,
    AddSuperUserService,
    FeesTypeService,
    FeeTemplateService,
    FeeMasterService,
    SecurityService,
    ClerkService,
    MembersService,
    PaymentService,
    ToasterService,
    //GoogleMapsAPIWrapper,
    User,
    Data,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],

  //exports: [FormsModule],
  bootstrap: [AppComponent]
})
export class AppModule {}
