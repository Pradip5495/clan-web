import { Injectable } from '@angular/core';

@Injectable()
export class User {
  uid: string;
  name: string;
  type: string;
  phone: string;
  email: string;
  avtarURL: string;
  father: string;
  school: string;
  std: string;
  div: string;
  schoolCode: string;
  isActive: boolean;
  approvalRequested: boolean;
  isClassTeacher: boolean;
}
