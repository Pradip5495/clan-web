import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { AuthenticationService } from '../_services';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private auth: AuthenticationService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // If it is a CLAN API request
    if (request.url.includes(environment.API_ENDPOINT)) {
      console.log('Request Intercepted.');
      // We need to add an auth token as a header to access the CLAN API
      request = request.clone({
        setHeaders: {
          'Authorization': `Bearer ${this.auth.getToken()}`,
          'Access-Control-Allow-Origin': '*',
          // 'Content-Type': 'multipart/form-data' // application/json
        }
      });
    }
    return next.handle(request);
  }
}
