import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { User } from '../_models';
import { Response } from '../../_models';

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {
  }

  verify(): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/profile`);
  }

  forgotPassword(email: string): Observable<Response> {
    return this.http.post<Response>(`${environment.API_ENDPOINT}/forgotpassword`, { email: email });
  }

  create(user: User): Observable<Response> {
    return this.http.post<Response>(`${environment.API_ENDPOINT}/register`, user);
  }

  update(user: User): Observable<Response> {
    return this.http.put<Response>(`${environment.API_ENDPOINT}/users/${user.id}`, user);
  }

  delete(id: number): Observable<Response> {
    return this.http.delete<Response>(`${environment.API_ENDPOINT}/users/${id}`);
  }

  postLogin(): Observable<Response> {
    console.log('Executing post login');
    return this.http.get<Response>(`${environment.API_ENDPOINT}/profile`);
  }
}
