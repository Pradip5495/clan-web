import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment';

const httpHeaders = new Headers({
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'Application/json'
});

const httpOptions = new RequestOptions({ headers: httpHeaders });

@Injectable()
export class AuthenticationService {

  constructor(private http: Http) {
  }

  public getToken(): string {
    return JSON.parse(localStorage.getItem('token'));
  }

  login(username: string, password: string, fcmTokn: string) {
    return this.http.post(`${environment.API_ENDPOINT}/login`, {
      username: username,
      password: password,
      fcmToken: fcmTokn
    }, httpOptions)
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        const user = response.json();
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('token', JSON.stringify(user.token));
        }
      });
  }

  logout() {
    // remove user's data from local storage to log user out
    localStorage.clear();
  }
}
