import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IdentityService } from './identity.service';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';
@Injectable()
export class DriverService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getDriver(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        /// school: this.identityService.school,
        uid: this.identityService.uid

      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/driver`, { params: params });
  }

}
