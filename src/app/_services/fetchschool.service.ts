import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class FetchschoolService {

  constructor(private http: HttpClient) {
  }

  getSchool(): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/school`);
  }
}
