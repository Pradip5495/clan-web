import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class ListInboxItemsService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getListInboxItemsService(category): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        teacher: '',
        category: category
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/inbox`, { params: params });
  }
  onSubmit(reportFormData, category): Observable<Response> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    const options = {
      headers: headers,
    }
    const params = new HttpParams({
      fromObject: {
        category: category,
        title: reportFormData.subject,
        to: this.identityService.uid,
        description: reportFormData.message,
      }
    });

    return this.http.post<Response>(`${environment.API_ENDPOINT}/inbox`,  params );
  }

  postMessage(reportFormData): Observable<Response> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    const options = {
      headers: headers,
    }
    const params = new HttpParams({
      fromObject: {
        category: reportFormData.category,
        title: reportFormData.subject,
        to: this.identityService.uid,
        description: reportFormData.message,
      }
    });
    return this.http.post<Response>(`${environment.API_ENDPOINT}/inbox`,  params );
  }

  postMemberMessage(reportFormData, uid): Observable<Response> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    const options = {
      headers: headers,
    }
    const params = new HttpParams({
      fromObject: {
        category: reportFormData.category,
        title: reportFormData.subject,
        to: uid,
        description: reportFormData.message,
      }
    });
    return this.http.post<Response>(`${environment.API_ENDPOINT}/inbox`,  params );
  }
}
