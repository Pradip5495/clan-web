import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from '../_services/identity.service';

@Injectable()
export class FeeTemplateService {

  constructor(private http: HttpClient, private _identityService: IdentityService) { }

  /** Get Fee Template By STD (class) */
  getFeeTemplate(std): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/fees/template/` + std);
  }

  /** New Fee Template */
  postFeeTemplate(data): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this._identityService.uid
      }
    });
    console.log('Services type: ', data);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/fees/template`, data);
  }
  
  /** New Fee Template */
  putFeeTemplate(data): Observable <Response> {
    const params = new HttpParams({
      fromObject: {
        user: this._identityService.uid
      }
    });
    console.log('Services type: ', data);
    return this.http.put<Response>(`${environment.API_ENDPOINT}/fees/template`, data);
  }
 
  /** Delete Fee Template */
  deleteFeeTemplate(uidFeeTemplateMaster): Observable<Response> {
    console.log('Services type: ', uidFeeTemplateMaster);
    return this.http.delete<Response>(`${environment.API_ENDPOINT}/fees/template/` + uidFeeTemplateMaster);
  }

  /** Delete Fee Subdetails */
  deleteTempSubDetails(uidTemplateSubDetail): Observable <Response> {
    console.log('Services : ', uidTemplateSubDetail);
    return this.http.delete<Response>(`${environment.API_ENDPOINT}/fees/template/subdetails/` + uidTemplateSubDetail);
  }

}
