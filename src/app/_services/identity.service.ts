import { Injectable } from '@angular/core';

import { User } from '../_models';

const roles = {
  PR: 'parent',
  ST: 'student',
  TE: 'teacher',
  SA: 'school',
  DR: 'director',
  DU: 'driver',
  SU: 'admin',
  CA: 'coadmin',
  CL: 'clerk',
  LB: 'librarian',
  SQ: 'security'
};

@Injectable()
export class IdentityService {

  get identity(): User {
    this.constructor();
    return this._identity;
  }

  get school(): string {
    return this._identity.schoolCode;
  }

  get type(): string {
    return this._identity.type;
  }

  get uid(): string {
    return this._identity.uid;
  }

  get role(): string {
    return this._role;
  }
  get class(): string {
    return this._identity.std + this._identity.div;
  }

  get isAdmin(): boolean {
    return this._role === roles.SA;
  }
  get isCoadmin(): boolean {
    return this._role === roles.CA;
  }

  get isClassTeacher(): boolean {
    return this._identity.isClassTeacher;
  }
  get isDirector(): boolean {
    return this._role === roles.DR;
  }

  get isLocalStore(): boolean {
    return this._role === roles.PR;
  }

  private readonly _role: string;
  private readonly _identity: User;

  constructor() {
    this._identity = JSON.parse(localStorage.getItem('currentUser'));
    if (this._identity) {
      this._role = roles[this._identity.type];
    }
  }

}
