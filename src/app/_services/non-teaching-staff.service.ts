import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';
@Injectable()
export class NonTeachingStaffService {

  constructor(private identityService: IdentityService,
    private http: HttpClient) { }
  getAssistant(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school,
        //  class: 'class1'
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/assistant`, { params: params });
  }
  getDriver(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school,
        //  class: 'class1'
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/driver`, { params: params });
  }
}
