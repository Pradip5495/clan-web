import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class PerformanceBreakdownBySubjectService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getPerformanceBreakdownBySubjectService(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid,
        result: 'true'
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/performance`, { params: params });
  }
}
