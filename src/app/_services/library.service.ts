import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';
import { IdentityService } from '../_services/identity.service';

@Injectable()
export class LibraryService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getBooks(id): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/library/books/available/` + id);
  }

  getBorrowList(std_div): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/library/borrower/` + std_div);
  }

  getLibraryStats(): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/library/stats`);
  }
  
  getAutherList(): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/library/author`);
  }
 
  getLibrarian(): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/librarian`);
  }


  postAuthor(formData): Observable<Response> {
    let headers = new HttpHeaders();
    let params = new HttpParams();
    //headers.set('Accept', 'multipart/form-data');
    console.log('Services type: ', formData);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/library/author`, formData, { params, headers });
  }

  

  postNewBook(formData): Observable<Response> {
    let headers = new HttpHeaders();
    let params = new HttpParams();
    headers.set('Accept', 'multipart/form-data');
    console.log('Services type: ', formData);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/library/book`, formData, { params, headers });
  }

  getStaffList(): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/library/staff`);
  }
  
  getStudentList(std): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/library/student/` + std);
  }

  /** Post Borrower data */
  postBorrowBook(borrowForm): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid
      }
    });
    console.log('Services type: ', borrowForm);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/library/borrow`, borrowForm);
  }

  updateReturnBook(bookReturnDetails): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid
      }
    });
    return this.http.put<Response>(`${environment.API_ENDPOINT}/library/borrow/`, bookReturnDetails);
  }

  /** Get Borrow Book Details By uid */
  getStudentBorrowBook(uid): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/library/borrow/student/` + uid);
  }
}
