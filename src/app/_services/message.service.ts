


import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class MessageService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getMessage(id): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/conversation/` + id + `/message`);
  }

  postMessage(id, msg): Observable<Response> {
    /* const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
     const options =  {
       headers: headers
     };*/
    const params = new HttpParams({
      fromObject: {
        student: '',
        message: msg,
      }
    });


    return this.http.post<Response>(`${environment.API_ENDPOINT}/conversation/` + id + `/message`, { message: msg }, { params: params });
  }
}
