import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class ClassService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getClass(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school
      }
    });

    console.log(this.identityService.school);
    return this.http.get<Response>(`${environment.API_ENDPOINT}/class/` + this.identityService.school);
  }

  getClassBySchool(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school
      }
    });

    console.log(this.identityService.school);
    return this.http.get<Response>(`${environment.API_ENDPOINT}/class/` + this.identityService.school + `/?attendance=true`);
  }

  getClasses(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school
      }
    });

    console.log(this.identityService.school);
    return this.http.get<Response>(`${environment.API_ENDPOINT}/class/` + this.identityService.school + `?class=true`);
  }
}
