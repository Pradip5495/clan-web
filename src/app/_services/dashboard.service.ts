import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { environment } from "../../environments/environment";
import { Response } from "../_models";
// import 'rxjs/add/operator/toPromise';

import { IdentityService } from "./identity.service";

@Injectable()
export class DashboardService {
  private role: string;

  constructor(
    private http: HttpClient,
    private identityService: IdentityService
  ) {
    this.role = this.identityService.role;
  }

  getDashboardStats(uid): Observable<Response> {
    let params = null;
    let schoolCode = null;

    if (this.identityService.role === "director") {
      schoolCode = localStorage.getItem("schoolCode");
     // console.log("role in service : " + this.identityService.role);
      params = new HttpParams({
        fromObject: {
          school: schoolCode
        }
      });
    } else {
      //console.log("role in service: " + this.identityService.role);
      params = new HttpParams({
        fromObject: {
          studentId: this.identityService.uid
        }
      });
    }

    return this.http.get<Response>(
      `${environment.API_ENDPOINT}/dashboard/stats/school?school=` +
        uid +
        `&userType= + ${this.role}`,
      { params: params }
    );
  }
  getDashboardData(uid): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school,
        uid: this.identityService.uid
      }
    });
    return this.http.get<Response>(
      `${environment.API_ENDPOINT}/dashboard/stats/school?school=` +
        uid +
        `&userType= +  ${this.role} `,
      { params: params }
    );
  }
  getDashboardDataForStudentAndParent(uid): Observable<Response> {
    return this.http.get<Response>(
      `${environment.API_ENDPOINT}/dashboard/stats/student?studentId=` + uid
    );
  }
  getDashboardDataCoadmin(schoolCode) {
    return this.http.get<Response>(
      `${environment.API_ENDPOINT}/dashboard/stats/school?school=` + schoolCode
    );
  }

  // Get Clerk Dashboard Data
  getDashboardDataClerk() {
    const schoolCode = this.identityService.school;
    return this.http.get<Response>(
      `${environment.API_ENDPOINT}/dashboard/stats/clerk?school=` + schoolCode
    );
  }
}
