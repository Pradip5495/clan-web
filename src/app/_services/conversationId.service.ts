import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ConversationIdService {

  private id = new BehaviorSubject<any>('');

  currentId = this.id.asObservable();


  constructor() {
    // console.log('hii' + this.id);

  }

  changeId(id: any) {
    this.id.next(id);
  }

  
}
