import { Injectable, Input, Output, EventEmitter } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { environment } from "../../environments/environment";
import { Response } from "../_models";
import { Subject } from "rxjs/Subject";

import { IdentityService } from "./identity.service";
import { isNumber } from "@ng-bootstrap/ng-bootstrap/util/util";
import * as moment from "moment";
import _date = moment.unitOfTime._date;
export interface Mydata {
  // name: string
  exam_code: string;
  totalMarks: number;
  date;
}
@Injectable()
export class ExaminationService {
  name: string;
  sharingData: Mydata = { exam_code: "", totalMarks: 0, date: Date };

  private sub = new Subject();
  subj$ = this.sub.asObservable();

  constructor(
    private http: HttpClient,
    private identityService: IdentityService
  ) {}

  send(value: string) {
    this.sub.next(value);
  }
  getExamination(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid,
      }
    });

    console.log("uid in  service: " + this.identityService.uid);

    return this.http.get<Response>(`${environment.API_ENDPOINT}/examination`, {
      params: params
    });
  }

  getStudentExam(uid): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: uid,
        result: "true"
      }
    });

    console.log("uid in  service: " + this.identityService.uid);

    return this.http.get<Response>(`${environment.API_ENDPOINT}/examination`, {
      params: params
    });
  }

  getExaminationOfChild(uid): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid
      }
    });

    console.log("uid in  service: " + this.identityService.uid);

    return this.http.get<Response>(
      `${environment.API_ENDPOINT}/examination?user=` + uid + "&result=true"
    );
  }

  getExaminationResult(uid): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid,
        result: "true"
      }
    });

    console.log("uid in  service: " + this.identityService.uid);

    return this.http.get<Response>(
      `${environment.API_ENDPOINT}/examination?user=` + uid
    );
  }
  getExaminationResultOfChild(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid,
        result: "true"
        // colors: ['#F68A22', '#F47F1F', '#009587', '#EF5350']
      }
    });

    console.log("uid in  service: " + this.identityService.uid);

    return this.http.get<Response>(`${environment.API_ENDPOINT}/examination`);
  }
  getUpcomingExam(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid,
        result: "true"
      }
    });

    console.log("uid in  service: " + this.identityService.uid);

    return this.http.get<Response>(`${environment.API_ENDPOINT}/examination`, {
      params: params
    });
  }
  getCompletedExam(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid,
        result: "true"
      }
    });

    console.log("uid in  service: " + this.identityService.uid);

    return this.http.put<Response>(
      `${environment.API_ENDPOINT}/examination/VyckO893`,
      { params: params }
    );
  }

  saveData(str, totalMarks, date) {
    console.log("save data function called" + str, totalMarks);
    this.sharingData.exam_code = str;
    this.sharingData.totalMarks = totalMarks;
    this.sharingData.date = date;
  }
  getData(str) {
    console.log("get data function called");
    return this.sharingData.exam_code;
  }
  getMarks(totalMarks) {
    console.log("get marks function called");
    return this.sharingData.totalMarks;
  }
  getDate(date) {
    console.log("get date function called");
    return this.sharingData.date;
  }
  getExamResultByStudent(uid): Observable<Response> {
    return this.http.get<Response>(
      `${environment.API_ENDPOINT}/examination?user=` + uid + "&result=true"
    );
  }
}
