import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';
import { RequestOptions } from '@angular/http';
@Injectable()
export class MembersService {

  constructor(private http: HttpClient, private identityService: IdentityService) { }

  getAssistant(): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/assistant`);
  }

  getSecurity(): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/security`);
  }

}
