import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class FeesTypeService {
  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getFeeTypes(): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/fees/type`);
  }

  postFeeType(type): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid
      }
    });
    console.log('Services type: ', type);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/fees/type`, { type: type });
  }

  updateFeeType(uidFeeType, type): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid
      }
    });
    console.log('Services type: ', type);
    return this.http.put<Response>(`${environment.API_ENDPOINT}/fees/type/` + uidFeeType, { type: type });
  }

  deleteFeeType(uidFeeType): Observable<Response> {
    console.log('Services type: ', uidFeeType);
    return this.http.delete<Response>(`${environment.API_ENDPOINT}/fees/type/` + uidFeeType);
  }

}
