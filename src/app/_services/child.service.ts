import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class ChildService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getChild(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school,
        class: 'class1'
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/profile/child`);
  }
  childCode(child): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        child: child
      }
    });
    return this.http.post<Response>(`${environment.API_ENDPOINT}/profile/child`, { child: child });
  }
  getChildById(id: any): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school,
        id: id
      }
    });

    console.log('id name ' + id);

    return this.http.get<Response>(`${environment.API_ENDPOINT}/profile/child`, { params: params });
  }
}
