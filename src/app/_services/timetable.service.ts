import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';
import { RequestOptions } from '@angular/http';

@Injectable()
export class TimetableService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getAllTimeTableForSchool(): Observable<Response> {

    let params = null;
    let schoolCode = null;

    if (this.identityService.role === 'director') {
      schoolCode = localStorage.getItem('schoolCode');
      console.log('school code in service: ' + this.identityService.school);
      params = new HttpParams({
        fromObject: {
          school: schoolCode
        }
      });
    }
    //return params;
    return this.http.get<Response>(`${environment.API_ENDPOINT}/timetable/ZIQFF7QO`, { params: params });
  }

  getTimetableByClass(): Observable<Response> {

    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school,
        std: this.identityService.identity.std,
        div: this.identityService.identity.div
      }
    });

    return this.http.get<Response>(`${environment.API_ENDPOINT}/timetable`, { params: params });
  }

  postTimetable(std, div, timetable, school) {
    console.log('all data: ' + std + ' ' + div + ' ' + timetable.name);
    const httpOptions = {
      headers: new HttpHeaders({
        //    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9eyJ1aWQiOjQ4MCwiaWF0IjoxNTQ5NjkzODg0LCJleHAiOjE1NTIyODU4ODR9UvRSVTtPFMDZUUsaFQbV3gUn70OPbyMFMEjUlVKmf1g',
        'Content-Type': 'multipart/form-data'
      })
    };

    const input = new FormData();
    input.append('std', std);
    input.append('div', div);
    input.append('image', timetable, timetable.name);
    input.append('school', school);


    console.log('all data is service: ' + std + ' ' + div + ' ' + timetable.name + ' ' + school);

    return this.http.post<Response>(`${environment.API_ENDPOINT}/timetable`, { std: std, div: div, timetable: timetable.name, school: school }, httpOptions);

  }

  postTimeSlots(timeSlots): Observable<Response> {
    let params = new HttpParams();
    console.log('Services type: ', timeSlots);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/timetable/session`, timeSlots);
  }

  postSchoolSession(sessionForm): Observable<Response> {
    let params = new HttpParams();
    console.log('Services type: ', sessionForm);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/timetable/session`, sessionForm);
  }

  getTimeSlots(): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/timetable/session`);
  }

  getTimeSession(): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/timetable/session`);
  }

  getTemplateDataByUid(sessionUid) {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/timetable/week/slots/` + sessionUid);
  }

  createTimetableTemplate(timetableTemplateForm): Observable<Response> {
    let params = new HttpParams();
    console.log('Services type: ', timetableTemplateForm);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/timetable/week/slots`, timetableTemplateForm);
  }

  updateTimetableTemplate(timetableTemplateForm): Observable<Response> {
    let params = new HttpParams();
    console.log('Services type: ', timetableTemplateForm);
    return this.http.put<Response>(`${environment.API_ENDPOINT}/timetable/week/slots`, timetableTemplateForm);
  }

  getAvailableTimetableClass(schoolCode) {
    // const params = new HttpParams({
    //   fromObject: {
    //     school: schoolCode,
    //     timetable: 
    //   }
    // });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/class/` + schoolCode + '?school=' + schoolCode + '&timetable=true');
  }

  getClassSession(ssuid) {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/timetable/session/class/` + ssuid);
  }

  getTeacherForTimetable(std, div, dayOfWeek, startTime, endTime) {
    const params = new HttpParams({
      fromObject: {
        std: std,
        div: div,
        dayOfWeek: dayOfWeek,
        startTime: startTime,
        endTime: endTime
      }
    });

    return this.http.get<Response>(`${environment.API_ENDPOINT}/timetable/teacher/available`, { params: params });
  }

  createTimetable(timetableForm): Observable<Response> {
    let params = new HttpParams();
    console.log('Services type: ', timetableForm);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/timetable`, timetableForm);
  }

  updateTimetable(timetableForm): Observable<Response> {
    let params = new HttpParams();
    console.log('Services type: ', timetableForm);
    return this.http.put<Response>(`${environment.API_ENDPOINT}/timetable`, timetableForm);
  }

  getClassTimetable(std, div) {
    const params = new HttpParams({
      fromObject: {
        std: std,
        div: div
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/timetable/school/class`, { params: params });
  }
  
  getChildClassTimetable(std, div, schoolCode) {
    const params = new HttpParams({
      fromObject: {
        std: std,
        div: div,
        schoolCode: schoolCode
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/timetable/school/class`, { params: params });
  }
  
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    throw new Error(errorMessage);
  }

  
}
