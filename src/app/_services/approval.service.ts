import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class ApprovalService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  isApproved: Boolean = null;

  getTeacherAproval(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        type: 'TE'
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/approval`, { params: params });
  }

  getClerkAproval(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        type: 'CL'
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/approval`, { params: params });
  }

  getLibrarianAproval(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        type: 'LB'
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/approval`, { params: params });
  }

  getAssistantAproval(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        type: 'CA'
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/approval`, { params: params });
  }

  getAssistantSecurity(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        type: 'SQ'
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/approval`, { params: params });
  }

  getStudentAproval(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        type: 'ST'
      }
    });

    return this.http.get<Response>(`${environment.API_ENDPOINT}/approval`, { params: params });
  }

  getStudentAprovalByClass(class1: any): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        type: 'ST',
        class: class1
      }
    });

    console.log('class name in approval service: ' + class1);

    return this.http.get<Response>(`${environment.API_ENDPOINT}/approval`, { params: params });
  }

  approveTeacher(id): Observable<Response> {
    const isApproved = true;
    return this.http.post<Response>(`${environment.API_ENDPOINT}/approval/` + id, { isApproved: isApproved });
  }

  rejectTeacher(id): Observable<Response> {
    const isApproved = false;
    return this.http.post<Response>(`${environment.API_ENDPOINT}/approval/` + id, { isApproved: isApproved });
  }
}
