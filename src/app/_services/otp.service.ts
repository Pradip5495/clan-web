import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class OtpService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getOtpService(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        phone: '9865412356'
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/otp`, { params: params });
  }
}
