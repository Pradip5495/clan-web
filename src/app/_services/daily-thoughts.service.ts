import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';
// import 'rxjs/add/operator/toPromise';

import { IdentityService } from './identity.service';


@Injectable()
export class DailyThoughtsService {

  constructor(private identityService: IdentityService,
    private http: HttpClient) { }
  dailyThoughts(title, message, attachment): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        //   school: schoolCode
      }
    });
    console.log('uid in  service: ' + this.identityService.uid);

    return this.http.post<Response>(`${environment.API_ENDPOINT}/dailythoughts`, { title: title, message: message, attachment: attachment }, { params: params });
  }

}
