
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';
import { RequestOptions } from '@angular/http';
import { Subject } from 'rxjs/Subject';

export interface Mydata {
  trip_id: string;
}
@Injectable()
export class TripService {
  name: string;
  sharingData: Mydata = { trip_id: '' };

  private sub = new Subject();
  subj$ = this.sub.asObservable();

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }


  getTrip(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
      }

    });


    return this.http.get<Response>(`${environment.API_ENDPOINT}/transport/trip`, { params: params });
  }
  getTripId(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
      }

    });


    return this.http.get<Response>(`${environment.API_ENDPOINT}/transport/trip`, { params: params });


  }
  getTripPassenger(tuid) {
    const params = new HttpParams({
      fromObject: {
        // tuid: this.identityService.uid,
      }

    });


    return this.http.get<Response>(`${environment.API_ENDPOINT}/transport/trip/` + tuid + '/passenger', { params: params });
  }
  saveData(str) {
    console.log('save data function called' + str);
    this.sharingData.trip_id = str;
  }
  getData(str) {
    console.log('get data function called');
    return this.sharingData.trip_id;
  }

}
