import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class TeacherService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getTeacher(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school

      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/teacher`, { params: params });
  }

  getTeacherByClass(class1: any): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school,
        class: class1
      }
    });
    console.log('class name for teachers ' + class1);

    return this.http.get<Response>(`${environment.API_ENDPOINT}/teacher`, { params: params });
  }

  getTeacherByClassForParent(class1: any, schoolCode: any ): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: schoolCode,
        class: class1
      }
    });
    console.log('class name for teachers ' + class1);

    return this.http.get<Response>(`${environment.API_ENDPOINT}/teacher`, { params: params });
  }

}
