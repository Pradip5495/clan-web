import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';
import { RequestOptions } from '@angular/http';

@Injectable()
export class TransportService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }


  onSubmit(number, registrationNumber, insuranceLastDate, capacity): Observable<Response> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    const options = {
      headers: headers,
    }
    const params = new HttpParams({
      fromObject: {
        number: number,
        registrationNumber: registrationNumber,
        insuranceLastDate: insuranceLastDate,
        capacity: capacity,
      }
    });
    return this.http.post<Response>(`${environment.API_ENDPOINT}/transport/bus`, { number, insuranceLastDate, registrationNumber, capacity }, { params: params });
  }
  childCode(childCode): Observable<Response> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    const options = {
      headers: headers,
    }
    const params = new HttpParams({
      fromObject: {
        childCode: childCode,
      }
    });
    return this.http.post<Response>(`${environment.API_ENDPOINT}/my-child`, { childCode }, { params: params });
  }

  examForm(title, std, div, duration, subject, description, totalMarks, date, time): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        title: title,
        std: std,
        div: div,
        duration: duration,
        subject: subject,
        description: description,
        totalMarks: totalMarks,
        date: date,
        time: time
      }
    });
    return this.http.post<Response>(`${environment.API_ENDPOINT}/examination`, { title, std, div, duration, subject, description, totalMarks, date, time }, { params: params });
  }

  addRoutes(number, locations): Observable<Response> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    const options = {
      headers: headers,
    }
    const params = new HttpParams({
      fromObject: {
        number: number,
        locations: locations,
      }
    });
    return this.http.post<Response>(`${environment.API_ENDPOINT}/transport/route`, { number, locations }, { params: params });
  }
}
