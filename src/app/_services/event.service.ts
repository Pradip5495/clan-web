import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { environment } from "../../environments/environment";

import { Response } from "../_models";

import { IdentityService } from "./identity.service";

@Injectable()
export class EventService {
  constructor(
    private http: HttpClient,
    private identityService: IdentityService
  ) {}

  getRecentEvents(): Observable<Response> {
    let schoolCode = null;
    let params;
    if (this.identityService.role === "director") {
      schoolCode = localStorage.getItem("schoolCode");
      console.log("school code in service: " + schoolCode);
      params = new HttpParams({
        fromObject: {
          school: schoolCode
        }
      });
    } else {
      params = new HttpParams({
        fromObject: {
          school: this.identityService.school
        }
      });
    }
    return this.http.get<Response>(`${environment.API_ENDPOINT}/event`, {
      params: params
    });
  }
  postResentEvent(
    title,
    description,
    audience,
    startDate,
    endDate,
    startTime,
    endTime,
    location
  ): Observable<Response> {
    return this.http.post<Response>(`${environment.API_ENDPOINT}/event`, {
      title: title,
      description: description,
      audience: audience,
      startDate: startDate,
      endDate: endDate,
      startTime: startTime,
      endTime: endTime,
      location: location
    });
  }
  getEvent(school): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school
        //   role: this.identityService.role
      }
    });
    return this.http.get<Response>(
      `${environment.API_ENDPOINT}/event?school?school=` + school,
      { params: params }
    );
  }
  getEventForParent(uid): Observable<Response> {
  let selectedChild;
    let params;
    if (this.identityService.role === "parent") {
      // schoolCode = localStorage.getItem("schoolCode");
      selectedChild = JSON.parse(localStorage.getItem("selectedChild"))
      console.log("school code in service: " + selectedChild.schoolCode);
      params = new HttpParams({
        fromObject: {
          school: selectedChild.schoolCode
        }
      });
    } 
    // else {
    //   params = new HttpParams({
    //     fromObject: {
    //       school: this.identityService.school
    //     }
    //   });
    // }
    return this.http.get<Response>(`${environment.API_ENDPOINT}/event?studentId=` + uid, {
      params: params
    });
  }
}
