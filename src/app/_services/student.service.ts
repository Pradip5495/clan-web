import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class StudentService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getStudent(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school,
        class: ''
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/student`, { params: params });
  }

  getStudentByClass(class1: any): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school,
        class: class1
      }
    });

    console.log('class name ' + class1);

    return this.http.get<Response>(`${environment.API_ENDPOINT}/student`, { params: params });
  }
}
