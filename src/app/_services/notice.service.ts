import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { environment } from "../../environments/environment";
import { Response } from "../_models";

import { IdentityService } from "./identity.service";

@Injectable()
export class NoticeService {
  private role: string;

  constructor(
    private http: HttpClient,
    private identityService: IdentityService
  ) {
    this.role = this.identityService.role;
  }

  getRecentNotice(): Observable<Response> {
    let schoolCode = null;
    let params;
    if (this.identityService.role === "director") {
      schoolCode = localStorage.getItem("schoolCode");
      console.log("school code in service: " + schoolCode);
      params = new HttpParams({
        fromObject: {
          school: schoolCode.schoolCode
        }
      });
    } else {
      params = new HttpParams({
        fromObject: {
          school: this.identityService.school
        }
      });
    }
    return this.http.get<Response>(`${environment.API_ENDPOINT}/notice`, {
      params: params
    });
  }
  postResentNotice(type, message, audience, dateTime): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school
      }
    });
    //return this.http.post<Response>(`${environment.API_ENDPOINT}/notice`, { type: type, message: message, audience: audience, school: school });
    return this.http.post<Response>(`${environment.API_ENDPOINT}/notice`, {
      type: type,
      message: message,
      audience: audience,
      dateTime: dateTime
    });
  } // {{url}}/notice?school=QHSY2DVZ&userType=ST

  getNotice(school): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school,
        role: this.identityService.role
      }
    });
    return this.http.get<Response>(
      `${environment.API_ENDPOINT}/notice?school?school=` +
        school +
        `&userType= +  ${this.role}`,
      { params: params }
    );
  }

  getNoticeForParent(uid): Observable<Response> {
    let selectedChild;
      let params;
      if (this.identityService.role === "parent") {
        // schoolCode = localStorage.getItem("schoolCode");
        selectedChild = JSON.parse(localStorage.getItem("selectedChild"))
        console.log("school code in service: " + selectedChild.schoolCode);
        params = new HttpParams({
          fromObject: {
            school: selectedChild.schoolCode
          }
        });
      }
      //  else {
      //   params = new HttpParams({
      //     fromObject: {
      //       school: this.identityService.school
      //     }
      //   });
      // }
      return this.http.get<Response>(`${environment.API_ENDPOINT}/notice?studentId=` + uid, {
        params: params
      });
    }
}
