import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ExaminationService } from './examination.service';

import { IdentityService } from './identity.service';

export interface Mydata {
  // name: string;
  ruid: string;
}

@Injectable()
export class PassengerService {
  name: string;
  sharingData: Mydata = { ruid: '' };


  constructor(private http: HttpClient, private _examinationService: ExaminationService,
    private identityService: IdentityService) {

  }

  saveData(str) {
    console.log('save data function called' + str + this.sharingData.ruid);
    this.sharingData.ruid = str;
    console.log('k.vdwbkd', this.sharingData.ruid);
  }

  getPassenger(ruid): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        ruid: this.identityService.uid,
      }
    });
    console.log('ruid in service from classes', ruid);
    return this.http.get<Response>(`${environment.API_ENDPOINT}/transport/route/` + ruid + '/passenger');
  }

  getPassengerList(ruid, passengers): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        //   uid: this.identityService.uid,
        passengers: passengers,
      }
    });
    console.log('uid in service from classes', ruid);
    console.log('uid in service from passenger', passengers);

    return this.http.post<Response>(`${environment.API_ENDPOINT}/transport/route/` + ruid + '/passenger', { passengers: passengers });
  }

  getBus(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        // ruid: this.identityService.uid,
      }
    });

    return this.http.get<Response>(`${environment.API_ENDPOINT}/transport/bus`, { params: params });
  }

  onDelete(ruid, uid): Observable<Response> {

    const params = new HttpParams({
      fromObject: {
        uid: this.identityService.uid,
      }
    });
    console.log('service', ruid, uid);
    return this.http.delete<Response>(`${environment.API_ENDPOINT}/transport/route/` + ruid + '/passenger/' + uid); //  {params: params}
  }
}
