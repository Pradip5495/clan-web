import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class NotificationListService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getNotificationListService(): Observable<Response> {
    //const params = new HttpParams({
    // fromObject: {
    //  student: 'null',
    //  category: 'Complain'
    // }
    //});
    return this.http.get<Response>(`${environment.API_ENDPOINT}/notification`);
  }
}
