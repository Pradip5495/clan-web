
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';
import { Subject } from 'rxjs/Subject';

import { IdentityService } from './identity.service';

export interface Mydata {
  // name: string;
  route_code: string;

}
@Injectable()
export class AssignService {
  name: string;
  sharingData: Mydata = { route_code: '' };


  private sub = new Subject();
  subj$ = this.sub.asObservable();

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }
  send(value: string) {
    this.sub.next(value);
  }

  getBusRoute(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        uid: this.identityService.uid,
      }
    });

    return this.http.get<Response>(`${environment.API_ENDPOINT}/transport/route`, { params: params });
  }
  getRouteDriver(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        uid: this.identityService.uid,
      }

    });


    return this.http.get<Response>(`${environment.API_ENDPOINT}/transport/route`, { params: params });
  }
  getBus(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        ruid: this.identityService.uid,
      }
    });

    return this.http.get<Response>(`${environment.API_ENDPOINT}/transport/bus`, { params: params });
  }
  getDriverById(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        ruid: this.identityService.uid,
      }
    });

    return this.http.get<Response>(`${environment.API_ENDPOINT}/transport/route`, { params: params });
  }

  getRouteById(ruid): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/transport/route`);
  }
  getRoutPassenger(ruid): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/transport/route/` + ruid + '/passenger');
  }
  saveData(str) {
    console.log('save data function called' + str);
    this.sharingData.route_code = str;
  }
  getData(str) {
    console.log('get data function called');
    return this.sharingData.route_code;
  }
  addDriverForRoute(): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/driver`);
  }
  deleteDriver(ruid, uid) {
    return this.http.delete<Response>(`${environment.API_ENDPOINT}/transport/route/` + ruid + '/driver/' + uid);
  }

  deleteRoute(ruid, buid) {
    return this.http.delete<Response>(`${environment.API_ENDPOINT}/transport/route/` + ruid + '/bus/' + buid);
  }

  assignDriver(ruid, uid) {
    return this.http.put<Response>(`${environment.API_ENDPOINT}/transport/route/` + ruid, { driver: uid });
  }
  assignRoute(ruid, buid) {
    return this.http.put<Response>(`${environment.API_ENDPOINT}/transport/route/` + ruid, { bus: buid });
  }
  busList() {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/transport/bus`);
  }
}
