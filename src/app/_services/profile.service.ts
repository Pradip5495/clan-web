import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class ProfileService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getProfile(): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/profile/parent`);
  }

  putProfile(): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/profile`);
  }
  changePassword(currentPassword, newPassword): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        currentPassword: currentPassword,
        newPassword: newPassword
      }
    });
    return this.http.put<Response>(`${environment.API_ENDPOINT}/profile/password`, { currentPassword: currentPassword, newPassword: newPassword }, { params: params });
  }
  addParents(parent): Observable<Response> {
    return this.http.post<Response>(`${environment.API_ENDPOINT}/profile/parent`, { parent: parent });
  }
}
