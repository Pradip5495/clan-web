import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from '../_services/identity.service';

@Injectable()
export class FeeMasterService {

  constructor(private http: HttpClient, private _identityService: IdentityService) { }

  /** Get Fee Template By STD (class) */
  getFeeMaster(std): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/fees/class/` + std);
  }

  /** Get Fee Template By STD (class) */
  getFeeAssignedDetails(uid): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/fees/student/` + uid);
  }

  /** Get Student Fee Details by UID */
  getStudentFeeDetails(uid): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/fees/student/` + uid);
  }
  
  /** Get school payment Details (class) */
  getschoolPayment(schoolCode): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/fees/school/` + schoolCode);
  }

  /** New Fee Master */
  postFeeMaster(data): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this._identityService.uid
      }
    });
    console.log('Services type: ', data);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/fees/assign`, data);
  }
  
  /** New Payment */
  postCashPayment(data): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this._identityService.uid
      }
    });
    console.log('Services type: ', data);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/payment/fees/cash`, data);
  }

  /** Update Fee Master */
  putFeeMaster(data): Observable <Response> {
    const params = new HttpParams({
      fromObject: {
        user: this._identityService.uid
      }
    });
    console.log('Services type: ', data);
    return this.http.put<Response>(`${environment.API_ENDPOINT}/fees/assign/`, data);
  }
 
  /** Delete Fee Template */
  deleteFeeTemplate(uidFeeTemplateMaster): Observable<Response> {
    console.log('Services type: ', uidFeeTemplateMaster);
    return this.http.delete<Response>(`${environment.API_ENDPOINT}/fees/template/` + uidFeeTemplateMaster);
  }

  /** Delete Fee Template */
  deleteFeeMaster(uidFeeMaster): Observable <Response> {
    console.log('Services type: ', uidFeeMaster);
    return this.http.delete<Response>(`${environment.API_ENDPOINT}/fees/` + uidFeeMaster);
  }

  /** Delete Fee Subdetails */
  deleteFeeSubDetails(uidFeesSubDetail): Observable <Response> {
    console.log('Services : ', uidFeesSubDetail);
    return this.http.delete<Response>(`${environment.API_ENDPOINT}/fees/subdetails/` + uidFeesSubDetail);
  }

  /** Get school payment Details (class) */
  generateOrderIdforPayment(data): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this._identityService.uid
      }
    });
    console.log('Services type: ', data);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/payment/fees/students`, data);
  }

  /** Update Fee Master */
  updatePaymentTransaction(data): Observable <Response> {
    const params = new HttpParams({
      fromObject: {
        user: this._identityService.uid
      }
    });
    console.log('Services type: ', data);
    return this.http.put<Response>(`${environment.API_ENDPOINT}/payment/fees/students`, data);
  }

  /** Get Student payment log Details (uid) */
  getPaymentLog(uid): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/payment/transaction/student/` + uid);
  }

}
