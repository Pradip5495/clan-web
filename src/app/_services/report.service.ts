import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';
import { IdentityService } from './identity.service';


@Injectable()
export class ReportService {

  constructor(private http: HttpClient, private identityService: IdentityService) {

  }
  setCategory(category): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        child: this.identityService.uid,
        category: category,
        title: '',
        to: '',
        description: '',
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/inbox`, { params: params });
  }
}



