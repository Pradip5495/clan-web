import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class PerformanceByClassService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getPerformanceByClassService(): Observable<Response> {


    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/performance/class`, { params: params });
  }

  getStudentPerfomanceBySubject(uid, schoolcode): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid
      }
    });

    console.log('uid in  service: ' + this.identityService.uid);

    return this.http.get<Response>(`${environment.API_ENDPOINT}/performance?user=` + uid + '&result=true' + '&school=' + schoolcode);
  }

}
