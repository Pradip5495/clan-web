import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';
import { IdentityService } from '../_services/identity.service';

@Injectable()
export class SecurityService {

  constructor(private http: HttpClient, private identityService: IdentityService) { 
   
  }
  getTeaching(entryDate): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/security/teachers/` + entryDate);
  }
  getNonTeaching(currentDay): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/security/nonteachers/` + currentDay);
  }
  getDelivery(entryDate): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/security/visitor/` + entryDate + '/D');
  }
  getBus(entryDate): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/security/bus/` + entryDate);
  }
  getExternalStudents(entryDate): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/security/visitor/` + entryDate + '/E');
  }
  getVisitors(entryDate): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/security/visitor/` + entryDate + '/V');
  }

  postNewVisitorInTime(formData): Observable<Response> {
    let headers = new HttpHeaders();
    let params = new HttpParams();
    headers.set('Accept', 'multipart/form-data');
    console.log('Services type: ', formData);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/security/visitor`, formData, { params, headers });
  }

  getVisitorsStats(): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/security/daily/stats`);
  }

  postStaffInTime(staffUid): Observable<Response> {
    let params = new HttpParams();
    console.log('Services type: ', staffUid);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/security/school/staff/` + staffUid, params);
  }
  
  postStudentOut(visitorForm): Observable<Response> {
    let params = new HttpParams();
    console.log('Services type: ', visitorForm);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/security/student/out`, visitorForm);
  }
 
  updateStaffOutTime(staffUid): Observable<Response> {
    let params = new HttpParams();
    console.log('Services type: ', staffUid);
    return this.http.put<Response>(`${environment.API_ENDPOINT}/security/school/staff/` + staffUid, params);
  }

  postBusInTime(buid): Observable<Response> {
    let params = new HttpParams();
    console.log('Services type: ', buid);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/security/bus/` + buid, params);
  }
 
  updateBusOutTime(buid): Observable<Response> {
    let params = new HttpParams();
    console.log('Services type: ', buid);
    return this.http.put<Response>(`${environment.API_ENDPOINT}/security/bus/` + buid, params);
  }

  updateVisitorOutTime(uidVisitor): Observable<Response> {
    let params = new HttpParams();
    console.log('Services type: ', uidVisitor);
    return this.http.put<Response>(`${environment.API_ENDPOINT}/security/visitor/` + uidVisitor, params);
  }

  getCurrentEntry(currentDay): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/security/all/visitor/` + currentDay );
  }

  getSecurityStats(): Observable<Response>{
    return this.http.get<Response>(`${environment.API_ENDPOINT}/security/daily/stats`);
  }

}
