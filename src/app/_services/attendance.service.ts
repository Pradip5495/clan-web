import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class AttendanceService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }
  getAttendance(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        //  std: std,
        //  div: div,
        //  date: date,
        //  school: school
      }
    });
    //  return this.http.get<Response>(`${environment.API_ENDPOINT}/attendance?std=` + std + `&div= ` + div + `&date= ` + date, { params: params });


    return this.http.get<Response>(`${environment.API_ENDPOINT}/attendance?std=V&div=B&date=2019-02-25`, { params: params });
  }

  getAttendance1(std, div, date): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        //  std: std,
        //  div: div,
        //  date: date,
        //  school: school
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/attendance?std=` + std + `&div=` + div + `&date=` + date, { params: params });


    //  return this.http.get<Response>(`${environment.API_ENDPOINT}/attendance?std=II&div=A&date=2019-02-03`, { params: params });
  }
  takeAttendance(std, div, attendance, leave): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        attendance: attendance,
        leave: leave
      }
    });
    return this.http.post<Response>(`${environment.API_ENDPOINT}/attendance?std=` + std + `&div=` + div, { attendance: attendance, leave: leave });


    //  return this.http.get<Response>(`${environment.API_ENDPOINT}/attendance?std=II&div=A&date=2019-02-03`, { params: params });
  }
  getAttendanceForStudentByMonth(month): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        date: month
      }

    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/attendance/` + this.identityService.uid, { params: params });
  }
  getAttendanceForStudent(uid, date): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/attendance/` + uid + `?date=` + date);
  }
  getAttendanceBySelectedChildId(uid): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        std: 'II',
        div: 'A',
        date: '2017-11-05',
        school: 'VPS'
      }
    });
    // console.log('attendamnce id', +uid);
    return this.http.get<Response>(`${environment.API_ENDPOINT}/attendance`, { params: params });
  }
}
 /*  return this.http.get<Response>(`${environment.API_ENDPOINT}/attendance?std=` + std + `&div= ` + div + `&date= ` + date, { params: params });*/
