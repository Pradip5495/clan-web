
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class AddSuperUserService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  addSuperUser(name, email, type, phone, username, password): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        //   phone: '9865412356'
      }
    });
    return this.http.post<Response>(`${environment.API_ENDPOINT}/admin`, { name: name, email: email, type: type, phone: phone, username: username, password: password }, { params: params });
  }
}
