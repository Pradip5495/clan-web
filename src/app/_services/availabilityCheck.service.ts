import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class AvailabilityCheckService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getAvailabilityCheckService(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        phone: '9096068619'// this.identityService.identity.phone
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/availability`, { params: params });
    // not working for existing number
  }
}
