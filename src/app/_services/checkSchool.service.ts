import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class CheckSchoolService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getCheckSchoolService(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/school/check`, { params: params });
  }
  getSchools(): Observable<Response> {
    return this.http.get<Response>(`${environment.API_ENDPOINT}/school`);
  }
  addSchool(name, type, board, medium, phoneNumber, email, state, district, city, area, zip, directorPhoneNumber, adminsPhoneNumber, addressLn1, startTime, endTime, country): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school
      }
    });
    return this.http.post<Response>(`${environment.API_ENDPOINT}/school`, {
      name: name, type: type, board: board, medium: medium, phoneNumber: phoneNumber, email: email, state: state, district: district, city: city, area: area, zip: zip,
      directorPhoneNumber: directorPhoneNumber, adminsPhoneNumber: adminsPhoneNumber, addressLn1: addressLn1, startTime: startTime, endTime: endTime, country: country
    });

  }
}
