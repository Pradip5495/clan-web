import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';

import { IdentityService } from './identity.service';

@Injectable()
export class HomeworkService {

  constructor(private http: HttpClient, private identityService: IdentityService) {
  }

  getHomework(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid
      }
    });

    console.log('igfef', this.identityService.uid);

    return this.http.get<Response>(`${environment.API_ENDPOINT}/homework`);
  }
  
  getHomeworkById(uid): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid
      }
    });

    console.log('igfef', this.identityService.uid);

    return this.http.get<Response>(`${environment.API_ENDPOINT}/homework?user=` + uid);
  }
  getHomeworkByClass(class1: any): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid
      }
    });

    console.log(this.identityService.class);

    return this.http.get<Response>(`${environment.API_ENDPOINT}/homework`, { params: params });
  }
  postHomework(title, std, div, subject, description, submission, attachments) {

    console.log('all data: ' + std + ' ' + div + ' ' + attachments.selectedFile);


    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'multipart/form-data'
      })
    };

    const input = new FormData();
    input.append('title', title);
    input.append('std', std);
    input.append('div', div);
    input.append('subject', subject);
    input.append('description', description);
    input.append('submission', submission);
    input.append('attachments', attachments);

    console.log('all data is service: ' + title + ' ' + std + ' ' + div + ' ' + subject + '  ' + description + '  ' + submission + ' ' + attachments.selectedFile);

    return this.http.post<Response>(`${environment.API_ENDPOINT}/homework`, { title: title, std: std, div: div, subject: subject, description: description, submission: submission, attachments: attachments }, httpOptions);

  }

}

