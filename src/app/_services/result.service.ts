import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Response } from '../_models';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ExaminationService } from './examination.service';

import { IdentityService } from './identity.service';
export interface Mydata {
  // name: string;
  exam_code: string;
}
@Injectable()
export class ResultService {
  name: string;
  sharingData: Mydata = { exam_code: '' };



  constructor(private http: HttpClient, private _examinationService: ExaminationService,
    private identityService: IdentityService) {

  }
  saveData(str) {
    console.log('save data function called' + str + this.sharingData.exam_code);
    this.sharingData.exam_code = str;
    console.log('k.vdwbkd', this.sharingData.exam_code);
  }
  getResult(id): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school,
        class: 'IA',
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/result/` + id);  // what is b4f0RBRm????? //VJSJe74Q
  }
  getResultByCode(): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        school: this.identityService.school,
        class: 'IA',
      }
    });
    return this.http.get<Response>(`${environment.API_ENDPOINT}/result `);  // what is b4f0RBRm????? //VJSJe74Q
  }
  getExamination(code): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        user: this.identityService.uid
      }
    });

    console.log('uid in  service: ' + this.identityService.uid);
    console.log(code)

    return this.http.get<Response>(`${environment.API_ENDPOINT}/examination`, { params: params });
  }
  getMarks(student, exam_code, score): Observable<Response> {
    const params = new HttpParams({
      fromObject: {
        // student: this.identityService.uid,
        examination: exam_code,
        // score: score
      }
    });
    console.log('dode', exam_code);
    return this.http.post<Response>(`${environment.API_ENDPOINT}/result `, { student: student, examination: exam_code, score: score });  // what is b4f0RBRm????? //VJSJe74Q
  }
}
