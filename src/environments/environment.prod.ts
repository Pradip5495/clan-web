export const environment = {
  production: true,
  API_ENDPOINT: 'https://api.clan.school',
  APP_NAME: 'CLAN.School'
};
