export const environment = {
  production: false,
  API_ENDPOINT: 'https://dapi.clan.school',
  APP_NAME: 'CLAN.School'
};
//staging   http API_ENDPOINT: 'http://clan.initbitlabs.com'
//prod   http://api.clan.school
// Dev http://dapi.clan.school
